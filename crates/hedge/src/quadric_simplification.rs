use std::{fmt::Display, ops::Neg};

use algebra::na::{Matrix4, Vector4};
use chicken_wire::VectorSpace;
use containers::PQueue;
use linear_isomorphic::RealField;
use mesh::{verify_topology, GeomId, InternalMeshAccess};
use mesh_deleter::HalfMeshDeleter;
use num::traits::float::FloatCore;

use crate::*;

pub fn quadric_simplify<S, MeshType, T>(mesh: &mut MeshType, mut simplify_count: usize)
where
    T: linear_isomorphic::InnerSpace<S> + Debug + Clone + Display + Neg<Output = T>,
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    MeshType: HalfMeshLike
        + InternalMeshAccess<
            <MeshType as HalfMeshLike>::VertData,
            <MeshType as HalfMeshLike>::EdgeData,
            <MeshType as HalfMeshLike>::FaceData,
        >,
    S: RealField
        + algebra::na::ComplexField
        + FloatCore
        + Mul<Vector4<S>, Output = Vector4<S>>
        + Mul<T, Output = T>,
{
    let (mut quadrics, mut queue) = initialize_vertex_quadrics(mesh);
    let deleter = HalfMeshDeleter::new(mesh);

    // println!("simplifying");
    while !queue.is_empty() && simplify_count > 0
    {
        let (eid, _) = queue.pop().unwrap();

        let hedge_handle = deleter.hedge_handle(GeomId::<1>::from(eid * 2));
        // TODO(high): handle boundary edges as well.
        if hedge_handle.is_disabled()
            || hedge_handle.is_in_boundary_edge()
            || hedge_handle
                .source()
                .iter_hedges()
                .any(|e| e.is_in_boundary_edge())
            || hedge_handle
                .dest()
                .iter_hedges()
                .any(|e| e.is_in_boundary_edge())
            || hedge_handle
                .iterate_butterfly()
                .any(|e| e.is_in_boundary_edge())
            || !hedge_handle.can_collapse()
        {
            continue;
        }

        simplify_count -= 1;
        // if simplify_count % 100 == 0
        // {
        //     println!("{}", simplify_count);
        // }

        // Remove all edges that touch the current edge from the queue.
        for e in hedge_handle.source().iter_hedges()
        {
            let id = e.id().0 as usize / 2;
            queue.remove(id);
        }

        for e in hedge_handle.dest().iter_hedges()
        {
            let id = e.id().0 as usize / 2;
            queue.remove(id);
        }

        let (_cost, point) = compute_edge_weight(hedge_handle.clone(), &quadrics);
        let new_quadric = quadrics[hedge_handle.source().id().0 as usize]
            + quadrics[hedge_handle.dest().id().0 as usize];

        let v = hedge_handle.collapse();
        let mesh = unsafe { &mut *deleter.mesh.inner_mesh() };
        debug_assert!(verify_topology(mesh).is_none());

        let v_handle = deleter.vert_handle(v);

        quadrics[v_handle.id().0 as usize] = new_quadric;

        // Update the position of the collapsed vertex to that wich minimizes the
        // quadric error.
        let mut pos = T::default();
        pos[0] = point.x;
        pos[1] = point.y;
        pos[2] = point.z;
        v_handle.mutate_data(&pos);

        // Add all edges touching the collapsed vertex with update costs.
        for e in v_handle.iter_hedges()
        {
            debug_assert!(!e.is_disabled());
            let (cost, _) = compute_edge_weight(e.clone(), &quadrics);
            queue.push(e.id().0 as usize / 2, cost);
        }
    }

    deleter.finish();
}

fn initialize_vertex_quadrics<S, MeshType, T>(
    mesh: &MeshType,
) -> (Vec<Matrix4<S>>, PQueue<usize, S>)
where
    T: linear_isomorphic::InnerSpace<S> + Debug + Clone + Display + Neg<Output = T>,
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    MeshType: HalfMeshLike,
    S: RealField
        + algebra::na::ComplexField
        + FloatCore
        + Mul<algebra::na::Vector4<S>, Output = algebra::na::Vector4<S>>
        + Mul<T, Output = T>,
{
    let mut vertex_quadrics = vec![Matrix4::<S>::zeros(); mesh.vert_count()];

    for face in mesh.iter_faces()
    {
        let normal = face.naive_normal().normalized();

        let position = face.vertex_handles().next().unwrap().data();
        let d = position.dot(&normal);

        let q = Vector4::<S>::new(normal[0], normal[1], normal[2], -d);
        let quadric = q * q.transpose();

        for v in face.vertex_handles()
        {
            vertex_quadrics[v.id().0 as usize] += quadric;
        }
    }

    let mut queue = PQueue::new();
    for (eid, edge) in mesh.iter_edges().enumerate()
    {
        let (cost, _) = compute_edge_weight(edge, &vertex_quadrics);
        queue.push(eid, cost);
    }

    (vertex_quadrics, queue)
}

fn compute_edge_weight<S, V, E, F>(
    edge: HEdgeHandle<V, E, F>,
    quadrics: &Vec<Matrix4<S>>,
) -> (S, Vector4<S>)
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
    S: RealField + algebra::na::ComplexField,
    V::PrimitiveData: VectorSpace<Scalar = S>,
{
    debug_assert!(!edge.is_disabled());
    let cost_matrix =
        quadrics[edge.source().id().0 as usize] + quadrics[edge.dest().id().0 as usize];

    let intermediate = match cost_matrix.try_inverse()
    {
        None =>
        {
            let half = S::from(0.5).unwrap();
            (edge.dest().data() + edge.source().data()) * half
        }
        Some(inverse) =>
        {
            let b = Vector4::<S>::new(
                S::from(0.).unwrap(),
                S::from(0.).unwrap(),
                S::from(0.).unwrap(),
                S::from(1.).unwrap(),
            );
            // Find the point that minimizes the quadric error.
            let sol = inverse * b;

            // Homogenize the coordinates.
            let mut res = V::PrimitiveData::default();
            res[0] = sol.x / sol.w;
            res[1] = sol.y / sol.w;
            res[2] = sol.z / sol.w;

            res
        }
    };

    let new_point = Vector4::<S>::new(
        intermediate[0],
        intermediate[1],
        intermediate[2],
        S::from(1.0).unwrap(),
    );

    (
        -(new_point.transpose() * cost_matrix * new_point)[(0, 0)],
        new_point,
    )
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests
{
    use std::collections::BTreeSet;

    use chicken_wire::wavefront_loader::ObjData;
    use itertools::Itertools;

    use super::*;
    use crate::mesh::HalfMesh;

    #[test]
    fn test_simplify_one_mesh() {
        let ObjData {
            vertices: vs,
            vertex_face_indices: vids,
            objects,
            ..
        } = ObjData::from_disk_file(
            "/home/makogan/Foresight/virtual_meshes/assets/stanford_dragon.obj",
        );
        let mut mesh = HalfMesh::new(vs.to_vec(), (), (), vids.to_vec());

        let count = mesh.vert_count() as f64 * (1.0 - 0.01);
        quadric_simplify(&mut mesh, count as usize);

        ObjData::export(&mesh, "tmp/simple_dragon.obj");
    }

    #[test]
    fn test_simplify() {
        let ObjData {
            vertices: vs,
            vertex_face_indices: vids,
            objects,
            ..
        } = ObjData::from_disk_file("../../../large_bunny_clusters.obj"); //ObjData::from_disk_file("../../Assets/airplane_small.obj");

        use std::time::Instant;

        use rayon::iter::IntoParallelRefIterator;
        use rayon::iter::ParallelIterator;
        let start = Instant::now();
        objects.par_iter().for_each(|cluster| {
            let vs = &vs[cluster.positions.0 as usize..cluster.positions.1 as usize];
            let vids =
                &vids[cluster.polygons[0].0 as usize..cluster.polygons[0].1 as usize];

            let mut set = BTreeSet::from_iter(vids.iter().flatten());

            let vids = vids
                .iter()
                .map(|poly| {
                    poly.iter()
                        .map(|i| {
                            debug_assert!(
                                *i >= cluster.positions.0,
                                "{} {}",
                                i,
                                cluster.positions.0
                            );
                            *i - cluster.positions.0
                        })
                        .collect_vec()
                })
                .collect_vec();
            // println!("{}", cluster.name);
            // ObjData::export(&(&vs.to_vec(), &vids), "broken_cluster.obj");
            let mut mesh = HalfMesh::new(vs.to_vec(), (), (), vids.to_vec());
            // let start = Instant::now();
            if verify_topology(&mesh).is_some()
            {
                return;
            }
            // let duration = start.elapsed();
            // println!("Time elapsed in simplify() is: {:?}", duration);

            let count = mesh.vert_count() as f64 * (1.0 - 0.01);
            quadric_simplify(&mut mesh, count as usize);
        });

        let duration = start.elapsed();
        println!("Time elapsed in simplify() is: {:?}", duration);

        // ObjData::export(&mesh, "tmp/massive_bunny.obj");
    }
}
