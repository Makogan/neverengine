#![deny(rust_2018_idioms)]

use std::cell::RefCell;
use std::f32::consts::PI;
use std::rc::Rc;

use algebra::*;
use dem_core::rendering::*;
use dem_gui::DebugGui;
use dem_peripherals::*;
use numerical_analysis::{
    differential_operators::gradient, multilinear::sample_multilinear,
};
use rand::distributions::Uniform;
use rand::prelude::Distribution;
use rand::SeedableRng;
use rand_pcg::Pcg32;
use vulkan_bindings::RenderContext;

use crate::fluid::*;

mod fluid;

const PARTICLE_MAX: usize = 50_000;

#[repr(C)]
struct Vertex {
    position: Vec2,
}

impl From<(f32, f32)> for Vertex {
    fn from(tuple: (f32, f32)) -> Self {
        Self {
            position: Vec2::new(tuple.0, tuple.1),
        }
    }
}

#[repr(C)]
#[derive(Default, Copy, Clone, Debug)]
struct SimulationParameters {
    delta_time: f32,
    pad1: u32,
    pad2: u32,
    pad3: u32,
}

fn create_terrain(corner: &Vec4, extent: &Vec4, step: f32) -> (Vec<f32>, IVec4) {
    let resolution = IVec4::new(
        (extent.x / step) as i32,
        (extent.y / step) as i32,
        (extent.z / step) as i32,
        (extent.w / step) as i32,
    );
    use iterators::IterDimensions;
    use noise::perlin;
    use numerical_analysis::fast_sweeping;

    let function_sampling = |x: f32| {
        let mut value = 0.0;

        let persistence = 0.5;
        let lacunarity = 2.0;
        let mut amplitude = 1.0;
        let mut frequency = 1.0;
        for _ in 0..4 {
            value += amplitude * perlin(&[x * frequency], 1283791379);
            amplitude *= persistence;
            frequency *= lacunarity;
        }

        value
    };

    let mut grid_data = vec![f32::MAX; (resolution[0] * resolution[1]) as usize];
    let mut sign_data = vec![0_i8; (resolution[0] * resolution[1]) as usize];

    for [i, j] in IterDimensions::new([resolution[0] as usize, resolution[1] as usize]) {
        let x = (i as f32) / resolution[0] as f32;
        let y = (resolution[1] - j as i32) as f32 / resolution[1] as f32;
        #[rustfmt::skip]
            let value = (function_sampling(x * 2.0 + 100.0) + 1.0) * 0.5 - 0.3;

        let delta = 0.01 * step;
        if (y - value).abs() <= delta {
            grid_data[i + j * resolution[0] as usize] = 0.0;
        };

        sign_data[i + j * resolution[0] as usize] = if y > value { 1 } else { -1 };
    }

    fast_sweeping(
        &Vec2::new(corner[0], corner[1]),
        &Vec2::new(extent[0], extent[1]),
        step,
        &|_| 1.0,
        &mut grid_data,
    );

    for [i, j] in IterDimensions::new([resolution[0] as usize, resolution[1] as usize]) {
        let resolution = resolution[1] as usize;
        grid_data[i + j * resolution] *= sign_data[i + j * resolution] as f32;
    }

    (grid_data, resolution)
}

fn get_terrain_collission_force(
    coords: &[f32; 2],
    terrain_grid_data: &GridSampler,
    terrain_data: &Vec<f32>,
) -> Vec2 {
    assert!(!coords[0].is_nan() && !coords[1].is_nan());
    let mut signed_distance = sample_multilinear(
        &[coords[0], coords[1]],
        &[terrain_grid_data.corner.x, terrain_grid_data.corner.y],
        &[terrain_grid_data.extents.x, terrain_grid_data.extents.y],
        terrain_grid_data.cell_size,
        terrain_data,
    );

    let eval_grad = |coords: &[f32]| {
        gradient(
            &Vec2::new(coords[0], coords[1]),
            &|pos| terrain_grid_data.linear_sample(pos, terrain_data),
            terrain_grid_data.cell_size * 1.0,
        )
    };

    let grad = eval_grad(coords);

    signed_distance = signed_distance.min(0.0) * -1.0;

    grad * signed_distance * 10.0
}

fn build_terrain_data(fluid_grid: &mut FluidGrid) -> (GridSampler, Vec<f32>, Vec<f32>) {
    let (terrain_data, terrain_resolution) = create_terrain(
        &fluid_grid.grid_data.corner,
        &fluid_grid.grid_data.extents,
        fluid_grid.grid_data.cell_size / 8.0,
    );

    let terrain_grid_data = GridSampler {
        corner: fluid_grid.grid_data.corner,
        extents: fluid_grid.grid_data.extents,
        cell_size: fluid_grid.grid_data.cell_size / 8.0,
        resolution: terrain_resolution,
    };

    let fluid_res = fluid_grid.grid_data.resolution();
    let mut constant_fluid_data = vec![0.0; (fluid_res[0] * fluid_res[1]) as usize];
    for [i, j] in
        iterators::IterDimensions::new([fluid_res[0] as usize, fluid_res[1] as usize])
    {
        let pos = fluid_grid
            .grid_data
            .coord_to_pos(&IVec2::new(i as i32, j as i32));

        let sdf_val = sample_multilinear(
            &[pos[0], pos[1]],
            &[terrain_grid_data.corner.x, terrain_grid_data.corner.y],
            &[terrain_grid_data.extents.x, terrain_grid_data.extents.y],
            terrain_grid_data.cell_size,
            &terrain_data,
        );

        if sdf_val < 0.0 {
            constant_fluid_data[j + i * fluid_res[1] as usize] = 8.0;
        }
    }
    let terrain_density_grid = constant_fluid_data.clone();
    for [i, j] in
        iterators::IterDimensions::new([fluid_res[0] as usize, fluid_res[1] as usize])
    {
        let pos = fluid_grid
            .grid_data
            .coord_to_pos(&IVec2::new(i as i32, j as i32));

        let sdf_val = sample_multilinear(
            &[pos[0], pos[1]],
            &[terrain_grid_data.corner.x, terrain_grid_data.corner.y],
            &[terrain_grid_data.extents.x, terrain_grid_data.extents.y],
            terrain_grid_data.cell_size,
            &terrain_data,
        );

        if sdf_val > 0.0 {
            let weights = fluid_grid.eval_smooth_kernel(&pos);

            for y in -1..=1 {
                if j == 0 || j == (fluid_res[1] - 1) as usize {
                    continue;
                }
                for x in -1..=1 {
                    if i == 0 || i == (fluid_res[0] - 1) as usize {
                        continue;
                    }

                    let w = weights[(x + 1 + (y + 1) * 3) as usize];
                    let index = fluid_grid
                        .grid_data
                        .coord_to_index(&IVec2::new(i as i32 + x, j as i32 + y));

                    constant_fluid_data[index] += w * terrain_density_grid[index];
                }
            }
        }
    }

    (terrain_grid_data, terrain_data, constant_fluid_data)
}

fn main() {
    let mut io_context = IoContext::new("Fluid", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let cursor_pos = Rc::new(RefCell::new(Vec2::zeros()));
    {
        let cursor_pos = cursor_pos.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, _ox, _oy| {
                *cursor_pos.borrow_mut() = Vec2::new(x as f32, y as f32);
            }),
        );
    }

    let particle_shader =
        render_context.add_shader(vec!["examples/06_fluid/particles_shader/"]);

    let terrain_shader =
        render_context.add_shader(vec!["examples/06_fluid/terrain_shader/"]);

    let mut fluid_grid = fluid::FluidGrid::new();

    let (terrain_grid_data, terrain_data, terrain_density_data) =
        build_terrain_data(&mut fluid_grid);

    let get_terrain_collission_force = |coords: &[f32; 2]| {
        get_terrain_collission_force(coords, &terrain_grid_data, &terrain_data)
    };

    #[rustfmt::skip]
        let terrain_image_data: Vec<u8> = terrain_data.iter().map(|&x| {
        -(x * 255.0) as u8
    }).collect();

    let terrain_resolution = terrain_grid_data.resolution();
    let terrain_image = render_context.allocate_image(&RawImageData {
        width: terrain_resolution[0] as u32,
        height: terrain_resolution[1] as u32,
        depth: 0,
        format: ImageFormat::R8,
        data: Some(terrain_image_data.as_ptr() as *const u8),
    });

    let terrain_render_request = RenderRequest::new(terrain_shader)
        .vertex_modifiers(GraphicsInputModifiers {
            element_count: 6,
            ..Default::default()
        })
        .add_image(1, 0, terrain_image)
        .should_clear(true)
        .add_ubo(0, terrain_grid_data)
        .build();

    let particles = vec![Particle::default(); PARTICLE_MAX];
    let particle_ssbo = render_context.allocate_buffer(&particles);

    let gpu_mesh = GraphicsInput {
        buffers: vec![particle_ssbo.to_buffer_handle()],
        index_count: fluid_grid.particles.len(),
        ..Default::default()
    };

    let mut particle_render_request = RenderRequest::new(particle_shader)
        .vertex_input(gpu_mesh.clone())
        .should_clear(false)
        .add_ubo(0, Mat2::identity())
        .build();

    let mut gui = Some(DebugGui::init(
        &mut render_context,
        io_context.window().get_window_size(),
    ));

    let between = Uniform::from(0..10_000);
    let mut rng = Pcg32::seed_from_u64(42);
    let mut sample_uniform = || between.sample(&mut rng) as f32 / 10_000.0;

    use std::time::Instant;
    let mut start = Instant::now();
    let mut spawn_speed = 10.0;
    while io_context.poll_io(&mut gui) {
        render_context.start_frame(&io_context);

        let elapsed_time = start.elapsed().as_micros() as f32 / 1000.0;
        start = Instant::now();

        render_context.draw(&terrain_render_request);

        if fluid_grid.particles.len() < PARTICLE_MAX {
            let samples: Vec<Vec2> = (0..5)
                .map(|_| {
                    let r = (sample_uniform() * 3.0).sqrt();
                    let theta = sample_uniform() * 2.0 * PI;
                    Vec2::new(r * theta.cos() + 5.0, r * theta.sin() + 5.0)
                })
                .collect();

            fluid_grid.add_particles(
                &samples,
                vec![Vec2::new(spawn_speed, 0.0); samples.len()].as_slice(),
            );
        }

        fluid_grid.sim(&get_terrain_collission_force, &terrain_density_data);

        render_context.update_buffer_from_slice(&particle_ssbo, &fluid_grid.particles);

        particle_render_request.vertex_modifiers.element_count =
            fluid_grid.particles.len();
        render_context.draw(&particle_render_request);

        gui.as_mut().unwrap().draw(&mut render_context, &mut |ctx| {
            dem_gui::Window::new("Program info:").show(ctx, |ui| {
                ui.label(format!(
                    "{:.0}ms ({:.0} FPS)",
                    elapsed_time,
                    1000.0 / elapsed_time
                ));

                ui.label(format!("{} Particles", fluid_grid.particles.len()));

                ui.add(
                    dem_gui::Slider::new(&mut fluid_grid.simulation_speed, 0.0..=0.05)
                        .text("simulation speed"),
                );

                ui.add(
                    dem_gui::Slider::new(&mut fluid_grid.eos_stiffness, 0.0..=10.0)
                        .text("eos stifness"),
                );

                ui.add(
                    dem_gui::Slider::new(&mut fluid_grid.eos_power, 0.0..=10.0)
                        .text("eos power"),
                );

                ui.add(
                    dem_gui::Slider::new(&mut spawn_speed, 0.0..=100.0)
                        .text("spawn speed"),
                );

                if ui.button("Restart").clicked() {
                    let mut new_fluid_grid = fluid::FluidGrid::new();
                    new_fluid_grid.simulation_speed = fluid_grid.simulation_speed;
                    new_fluid_grid.eos_stiffness = fluid_grid.eos_stiffness;
                    new_fluid_grid.eos_power = fluid_grid.eos_power;
                    new_fluid_grid.dynamic_viscosity = fluid_grid.dynamic_viscosity;
                    new_fluid_grid.rest_density = fluid_grid.rest_density;
                    fluid_grid = new_fluid_grid;
                }
            });
        });
    }

    render_context.free_input(gpu_mesh);
}
