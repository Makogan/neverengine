mod gltf;
use std::{
    collections::{HashMap, HashSet},
    fmt::Debug,
    mem::size_of,
};

pub use base64;
use nalgebra as na;

pub use crate::gltf::gltf_types::*;

pub type Vec2 = na::Vector2<f32>;
pub type Vec3 = na::Vector3<f32>;
pub type Vec4 = na::Vector4<f32>;

pub type BVec4 = na::Vector4<u8>;
pub type USVec4 = na::Vector4<u16>;
pub type IVec4 = na::Vector4<i32>;

pub type Quatf = na::UnitQuaternion<f32>;

pub type Mat2 = na::Matrix2<f32>;
pub type Mat3 = na::Matrix3<f32>;
pub type Mat4 = na::Matrix4<f32>;


macro_rules! VecAccessorUpdate {
    (
        $gltf : expr,
        $vec_type : tt,
        $scalar_type : tt,
        $accessor_id : expr) => {
        let accessor = &mut $gltf.accessors[$accessor_id];
        let view: &[$vec_type] =
            access_view(accessor, &$gltf.buffer_views, &$gltf.buffers);

        let mut max_bounds = $vec_type::default();
        let mut min_bounds = $vec_type::default();

        for entry in &mut max_bounds
        {
            *entry = $scalar_type::MIN;
        }
        for entry in &mut min_bounds
        {
            *entry = $scalar_type::MAX;
        }

        for datum in view
        {
            max_bounds = $vec_type::sup(&max_bounds, &datum);
            min_bounds = $vec_type::inf(&min_bounds, &datum);
        }
        for val in &max_bounds
        {
            accessor.max.push((*val).into());
        }
        for val in &min_bounds
        {
            accessor.min.push((*val).into());
        }
    };
}

#[derive(Debug)]
pub struct AABB
{
    pub min_x: f32,
    pub min_y: f32,
    pub min_z: f32,

    pub max_x: f32,
    pub max_y: f32,
    pub max_z: f32,
}

#[derive(serde::Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Gltf
{
    #[serde(with = "gltf_header")]
    asset: (),
    pub scene: usize,
    pub scenes: Vec<Scene>,
    pub accessors: Vec<Accessor>,
    pub skins: Vec<Skin>,
    pub nodes: Vec<Node>,
    pub animations: Vec<Animation>,
    pub meshes: Vec<Mesh>,
    pub materials: Vec<Material>,
    pub textures: Vec<Texture>,
    pub samplers: Vec<TextureSampler>,
    pub images: Vec<Image>,

    pub buffer_views: Vec<BufferView>,

    #[serde(with = "gltf_buffers")]
    pub buffers: Vec<Vec<u8>>,

    pub data_path: String,
}


mod gltf_buffers
{
    use serde::ser::SerializeSeq;

    #[derive(serde::Serialize)]
    #[serde(rename_all = "camelCase")]
    struct BufferData
    {
        byte_length: usize,
        uri: String,
    }

    pub fn serialize<S>(val: &Vec<Vec<u8>>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut strings = Vec::new();
        for buffer in val
        {
            let encoded = format!(
                "data:application/octet-stream;base64,{}",
                base64::encode(&buffer)
            );

            strings.push(encoded);
        }

        let mut seq = serializer.serialize_seq(Some(strings.len()))?;

        for (i, buffer) in strings.iter().enumerate()
        {
            seq.serialize_element(&BufferData {
                uri: (*buffer).clone(),
                byte_length: val[i].len(),
            })
            .unwrap();
        }
        seq.end()
    }
}

mod gltf_header
{
    use serde::ser::SerializeStruct;

    pub fn serialize<S>(_val: &(), serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("asset", 2)?;
        state
            .serialize_field(
                "generator",
                &format!(
                    "{} v{}.{}.{}",
                    env!("CARGO_PKG_NAME"),
                    env!("CARGO_PKG_VERSION_MAJOR"),
                    env!("CARGO_PKG_VERSION_MINOR"),
                    env!("CARGO_PKG_VERSION_PATCH")
                ),
            )
            .unwrap();
        state.serialize_field("version", "2.0").unwrap();
        state.end()
    }
}

impl Gltf
{
    pub fn remove_animation(&mut self, animation_id: usize)
    {
        self.animations.remove(animation_id);
    }

    pub fn rename_animation(&mut self, animation_id: usize, name: &String)
    {
        self.animations[animation_id].name = name.clone();
    }

    pub fn aabb_dims(&self) -> AABB
    {
        let mut max_x = f32::MIN;
        let mut min_x = f32::MAX;
        let mut max_y = f32::MIN;
        let mut min_y = f32::MAX;
        let mut max_z = f32::MIN;
        let mut min_z = f32::MAX;

        for mesh in &self.meshes
        {
            for primitive in &mesh.primitives
            {
                let vert_data = self
                    .data_from_accessor::<Vec3>(primitive.attributes.position)
                    .unwrap();

                for position in vert_data
                {
                    max_x = f32::max(max_x, position.x);
                    max_y = f32::max(max_y, position.y);
                    max_z = f32::max(max_z, position.z);

                    min_x = f32::min(min_x, position.x);
                    min_y = f32::min(min_y, position.y);
                    min_z = f32::min(min_z, position.z);
                }
            }
        }

        return AABB {
            min_x,
            min_y,
            min_z,
            max_x,
            max_y,
            max_z,
        };
    }

    pub fn data_from_accessor<T>(&self, accessor_id: usize) -> Option<&mut [T]>
    where
        T: DataIntrospection,
        T: Default,
        T: Debug,
    {
        if accessor_id == !0
        {
            return None;
        }

        return Some(access_view::<T>(
            &self.accessors[accessor_id],
            &self.buffer_views,
            &self.buffers,
        ));
    }

    pub fn update_accessors(&mut self)
    {
        for i in 0..self.accessors.len()
        {
            let data_type = (self.accessors[i].component_type, self.accessors[i].kind);
            match data_type
            {
                (ComponentType::SBYTE, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<i8>(self, i);
                }
                (ComponentType::UBYTE, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<u8>(self, i);
                }
                (ComponentType::SSHORT, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<i16>(self, i);
                }
                (ComponentType::USHORT, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<u16>(self, i);
                }
                (ComponentType::UINT, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<u32>(self, i);
                }
                (ComponentType::F32, DataType::SCALAR) =>
                {
                    scalar_accessor_update::<f32>(self, i);
                }
                (ComponentType::F32, DataType::VEC2) =>
                {
                    VecAccessorUpdate!(self, Vec2, f32, i);
                }
                (ComponentType::F32, DataType::VEC3) =>
                {
                    VecAccessorUpdate!(self, Vec3, f32, i);
                }
                (ComponentType::F32, DataType::VEC4) =>
                {
                    VecAccessorUpdate!(self, Vec4, f32, i);
                }
                (ComponentType::UBYTE, DataType::VEC4) =>
                {
                    VecAccessorUpdate!(self, BVec4, u8, i);
                }
                (ComponentType::USHORT, DataType::VEC4) =>
                {
                    VecAccessorUpdate!(self, USVec4, u16, i);
                }
                (ComponentType::F32, DataType::MAT4) =>
                {
                    VecAccessorUpdate!(self, Mat4, f32, i);
                }

                _ =>
                {
                    todo!("{:?}", data_type)
                }
            };
        }
    }

    pub fn from_string(data: &str, path_to_data: &str) -> Gltf
    {
        let json: serde_json::Value =
            serde_json::from_str(&data).expect("JSON was not well-formatted");

        let buffers = parse_buffers(&json, &path_to_data);
        let buffer_views = BufferView::parse_buffer_views(&json);
        let accessors = Accessor::parse_accessors(&json);
        let scenes = Scene::parse_scenes(&json);

        let meshes = if !json["meshes"].is_null()
        {
            Mesh::parse_meshes(&json)
        }
        else
        {
            Vec::new()
        };
        let skins = if !json["skins"].is_null()
        {
            Skin::parse_skins(&json)
        }
        else
        {
            Vec::new()
        };
        let nodes = if !json["nodes"].is_null()
        {
            Node::parse_nodes(&json)
        }
        else
        {
            Vec::new()
        };
        let animations = if !json["animations"].is_null()
        {
            Animation::parse_animations(&json)
        }
        else
        {
            Vec::new()
        };
        let materials = if !json["materials"].is_null()
        {
            Material::parse_materials(&json)
        }
        else
        {
            Vec::new()
        };
        let textures = if !json["textures"].is_null()
        {
            Texture::parse_textures(&json)
        }
        else
        {
            Vec::new()
        };
        let images = if !json["textures"].is_null()
        {
            Image::parse_images(&json)
        }
        else
        {
            Vec::new()
        };
        let samplers = if !json["samplers"].is_null()
        {
            TextureSampler::parse_texture_samplers(&json)
        }
        else
        {
            Vec::new()
        };

        let mut ret = Gltf {
            asset: (),
            scene: json["scene"].as_u64().unwrap() as usize,
            scenes,
            buffers,
            buffer_views,
            accessors,
            skins,
            nodes,
            animations,
            meshes,
            materials,
            textures,
            samplers,
            images,
            data_path: path_to_data.to_string(),
        };

        ret.update_accessors();

        return ret;
    }

    pub fn collect_data_from_views(
        buffers: &Vec<Vec<u8>>,
        views: &Vec<BufferView>,
    ) -> Vec<u8>
    {
        let mut new_buffer = Vec::<u8>::new();
        for view in views
        {
            let slice = &buffers[view.buffer]
                [view.byte_offset..(view.byte_offset + view.byte_length)];
            new_buffer.extend_from_slice(&slice);
        }
        return new_buffer;
    }

    pub fn append_buffer_views(
        src_buffer_view_ids: &Vec<usize>,
        src_buffer_views: &Vec<BufferView>,
        dest_buffer_views: &mut Vec<BufferView>,
        dest_buffer_index: usize,
        dest_buffer_size: usize,
    ) -> HashMap<usize, usize>
    {
        assert!(src_buffer_view_ids.len() == src_buffer_views.len());
        let mut buffer_view_map = HashMap::<usize, usize>::new();
        let mut inner_offset = 0;
        for (i, view) in src_buffer_views.iter().enumerate()
        {
            buffer_view_map.insert(src_buffer_view_ids[i], dest_buffer_views.len());

            dest_buffer_views.push(BufferView {
                buffer: dest_buffer_index,
                byte_offset: dest_buffer_size + inner_offset,
                byte_length: view.byte_length,
            });

            inner_offset += view.byte_length;
        }

        return buffer_view_map;
    }

    pub fn append_accessors(
        src_accessor_ids: &Vec<usize>,
        src_accessors: &Vec<Accessor>,
        dest_accessors: &mut Vec<Accessor>,
        buffer_view_map: &HashMap<usize, usize>,
    ) -> HashMap<usize, usize>
    {
        assert!(src_accessor_ids.len() == src_accessors.len());
        let mut accessor_map = HashMap::<usize, usize>::new();
        for (i, accessor) in src_accessors.iter().enumerate()
        {
            accessor_map.insert(src_accessor_ids[i], dest_accessors.len());
            dest_accessors.push(Accessor {
                buffer_view: buffer_view_map[&accessor.buffer_view],
                component_type: accessor.component_type,
                count: accessor.count,
                max: accessor.max.clone(),
                min: accessor.min.clone(),
                kind: accessor.kind,
                byte_offset: accessor.byte_offset,
            });
        }

        return accessor_map;
    }

    pub fn append_animations(dest_gltf: &mut Gltf, src_gltf: &Gltf)
    {
        assert!(dest_gltf.buffers.len() == 1);
        assert!(src_gltf.buffers.len() == 1);

        let mut dest_names = HashMap::<String, usize>::new();
        for (i, node) in dest_gltf.nodes.iter().enumerate()
        {
            dest_names.insert(node.name.clone(), i);
        }

        let mut node_map = HashMap::<usize, usize>::with_capacity(src_gltf.nodes.len());
        for (i, node) in src_gltf.nodes.iter().enumerate()
        {
            let same_name_node = dest_names.get(&node.name);
            match same_name_node
            {
                None => continue,
                Some(dest_index) => node_map.insert(i, *dest_index),
            };
        }

        let mut accessor_set = HashSet::<usize>::new();
        for animation in &src_gltf.animations
        {
            for sampler in &animation.samplers
            {
                accessor_set.insert(sampler.input);
                accessor_set.insert(sampler.output);
            }
        }

        let mut buffer_view_set = HashSet::<usize>::new();
        let mut accessors = Vec::<Accessor>::new();
        let mut accessor_ids = Vec::<usize>::new();
        for accessor_id in &accessor_set
        {
            accessor_ids.push(*accessor_id);
            accessors.push(src_gltf.accessors[*accessor_id].clone());
            buffer_view_set.insert(src_gltf.accessors[*accessor_id].buffer_view);
        }

        let mut buffer_views = Vec::<BufferView>::new();
        let mut buffer_view_ids = Vec::<usize>::new();
        for view_index in buffer_view_set
        {
            buffer_view_ids.push(view_index);
            buffer_views.push(src_gltf.buffer_views[view_index].clone());
        }

        let mut src_data =
            Gltf::collect_data_from_views(&src_gltf.buffers, &buffer_views);

        let buffer_len = dest_gltf.buffers[0].len();
        let buffer_view_map = Gltf::append_buffer_views(
            &buffer_view_ids,
            &buffer_views,
            &mut dest_gltf.buffer_views,
            0,
            buffer_len,
        );

        let accessor_map = Gltf::append_accessors(
            &accessor_ids,
            &accessors,
            &mut dest_gltf.accessors,
            &buffer_view_map,
        );
        dest_gltf.buffers[0].append(&mut src_data);

        for animation in &src_gltf.animations
        {
            let mut new_animation = Animation {
                name: animation.name.clone(),
                channels: Vec::new(),
                samplers: Vec::new(),
            };

            debug_assert!(animation.channels.len() == animation.samplers.len());

            // Note this will add samplers that may have no channel targetting them.
            for src_sampler in &animation.samplers
            {
                let new_sampler = Sampler {
                    input: accessor_map[&src_sampler.input],
                    output: accessor_map[&src_sampler.output],
                    interpolation: src_sampler.interpolation.clone(),
                };

                new_animation.samplers.push(new_sampler);
            }

            for src_channel in &animation.channels
            {
                if !node_map.contains_key(&src_channel.target.node)
                {
                    continue;
                }

                let new_channel = Channel {
                    target: Target {
                        node: node_map[&src_channel.target.node],
                        path: src_channel.target.path,
                    },
                    sampler: src_channel.sampler,
                };

                new_animation.channels.push(new_channel);
            }

            dest_gltf.animations.push(new_animation);
        }
    }
}

fn access_view<'a, T>(
    accessor: &'a Accessor,
    buffer_views: &'a Vec<BufferView>,
    buffers: &'a Vec<Vec<u8>>,
) -> &'a mut [T]
where
    T: DataIntrospection,
{
    let buff_view_id = accessor.buffer_view;
    let expected_kind = &accessor.kind;
    let expected_component_type = &accessor.component_type;

    let actual_kind = T::get_data_type();
    let actual_componet_type = T::get_component_type();

    assert!(
        *expected_kind == actual_kind,
        "Expected {:?}, found {:?}.",
        expected_kind,
        actual_kind
    );
    assert!(
        *expected_component_type == actual_componet_type,
        "Expected {:?}, found {:?}.",
        expected_component_type,
        actual_componet_type
    );

    let count = accessor.count;

    let buff_view = &buffer_views[buff_view_id];

    let buff_id = buff_view.buffer;
    let offset = buff_view.byte_offset + accessor.byte_offset;

    let bytes = &buffers[buff_id][offset..(offset + count * size_of::<T>())];
    let ptr = bytes.as_ptr() as *mut T;

    return unsafe { std::slice::from_raw_parts_mut(ptr, count) };
}

fn scalar_accessor_update<T>(gltf: &mut Gltf, accessor_id: usize)
where
    T: num::NumCast + DataIntrospection + Copy,
{
    let accessor = &mut gltf.accessors[accessor_id];
    let view: &[T] = access_view(accessor, &gltf.buffer_views, &gltf.buffers);

    let mut max = f32::MIN;
    let mut min = f32::MAX;

    for datum in view
    {
        let val = <f32 as num::NumCast>::from(*datum).unwrap();
        max = f32::max(max, val);
        min = f32::min(min, val);
    }

    accessor.max = vec![<f32 as num::NumCast>::from(max).unwrap()];
    accessor.min = vec![<f32 as num::NumCast>::from(min).unwrap()];
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use std::fs;

    use crate::Gltf;

    #[test]
    fn test_gltf_loader()
    {
        let path = "../../../Assets/werewolf_animated.gltf".to_string();
        let data = fs::read_to_string(&path)
            .expect(format!("Unable to read file {}.", &path).as_str());

        let gltf = Gltf::from_string(&data, &path);

        assert_eq!(gltf.materials.len(), 4);
        assert_eq!(gltf.materials[0].name, "Material #25");
        assert_eq!(gltf.materials[0].double_sided, true);
        assert_eq!(
            gltf.materials[0]
                .pbr_metallic_roughness
                .base_color_texture
                .as_ref()
                .unwrap()
                .index,
            0
        );
        assert_eq!(
            gltf.materials[0].pbr_metallic_roughness.roughness_factor,
            0.8585786
        );
    }

    #[test]
    fn test_adding_animation()
    {
        let path1 = "../../../Assets/mixamo.gltf".to_string();
        let path2 = "../../../Assets/extras/falling_idle/falling_idle.gltf".to_string();
        let data = fs::read_to_string(&path1)
            .expect(format!("Unable to read file {}.", &path1).as_str());
        let mut gltf1 = Gltf::from_string(&data, &path1);

        let data = fs::read_to_string(&path2)
            .expect(format!("Unable to read file {}.", &path2).as_str());
        let gltf2 = Gltf::from_string(&data, &path2);

        Gltf::append_animations(&mut gltf1, &gltf2);
        let mut json_string = serde_json::to_string_pretty(&gltf1).unwrap();
        json_string = json_string.replace("kind", "type");

        fs::write("/tmp/out.gltf", json_string).unwrap();
    }
}
