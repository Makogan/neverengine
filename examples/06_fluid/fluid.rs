// https://matthias-research.github.io/pages/tenMinutePhysics/index.html
// https://nialltl.neocities.org/articles/mpm_guide
// https://github.com/robkau/mlsmpm-particles-rs
// This one is for larger spheres.
// https://github.com/kaegi/adaptive-sph

use algebra::*;
pub use numerical_analysis::sampler::GridSampler;

const MARGIN_PROPORTION: f32 = 1.01;

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub(crate) enum ParticleModel {
    #[default]
    Fluid,
    NeoHookean,
}

#[repr(C)]
#[derive(Default, Copy, Clone, Debug)]
pub struct Particle {
    pub pos: Vec2,
    pub vel: Vec2,
    pub mass: f32,
}
#[derive(Clone, Debug, Default)]
pub struct ParticleProperties {
    // Fluid
    pub(crate) momentum_matrix: Mat2,

    // Solid
    pub(super) deformation_gradient: Mat2,

    // Both
    pub(crate) particle_model: ParticleModel,
}

#[repr(C, align(16))]
#[derive(Default, Copy, Clone, Debug)]
pub struct MpmCell {
    pub velocity: Vec2,
    pub mass: f32,
}

fn component_exp(v: Vec2, exponent: f32) -> Vec2 {
    Vec2::new(v.x.powf(exponent), v.y.powf(exponent))
}

pub(super) fn quadratic_interpolation_weights(cell_diff: Vec2) -> [Vec2; 3] {
    [
        0.5 * component_exp(Vec2::new(0.5, 0.5) - cell_diff, 2.0),
        Vec2::new(0.75, 0.75) - component_exp(cell_diff, 2.0),
        0.5 * component_exp(Vec2::new(0.5, 0.5) + cell_diff, 2.0),
    ]
}

pub struct FluidGrid {
    pub particles: Vec<Particle>,
    pub particle_properties: Vec<ParticleProperties>,
    pub grid: Vec<MpmCell>,
    pub grid_data: GridSampler,
    pub simulation_speed: f32,

    // Fluid constants.
    pub eos_stiffness: f32,
    pub rest_density: f32,
    pub eos_power: f32,
    pub dynamic_viscosity: f32,

    // Solid constants.
    // youngs modulus
    pub elastic_lambda: f32,
    // shear modulus
    pub elastic_mu: f32,
}

pub fn clear_grid_cell(id: usize, cell: &mut MpmCell, grid_clear_data: &Vec<f32>) {
    // Reset grid scratchpad.
    cell.mass = grid_clear_data[id];
    cell.velocity = Vec2::zeros();
}

pub fn update_surrounding_cells(
    particle: &mut Particle,
    particle_properties: &ParticleProperties,
    grid: &mut [MpmCell],
    grid_data: &GridSampler,
) {
    let cell_size = grid_data.cell_size;
    let position = &particle.pos;
    let velocity = &particle.vel;

    let affine_momentum = particle_properties.momentum_matrix;

    let cell_pos = grid_data.quantize_position(position);
    let cell_coord = grid_data.position_to_grid_coord(position);
    let cell_diff = (position - cell_pos) / cell_size - Vec2::new(0.5, 0.5);
    let weights = quadratic_interpolation_weights(cell_diff);

    // Collect momentum changes for surrounding 9 cells.
    for gx in 0..3 {
        for gy in 0..3 {
            let weight = weights[gx as usize].x * weights[gy as usize].y;
            assert!((0.0..=1.0).contains(&weight));
            let cell_neigh = cell_coord + IVec2::new(gx - 1, gy - 1);
            let cell_neigh_pos = grid_data.coord_to_pos(&cell_neigh);

            let cell_dist = cell_neigh_pos + Vec2::new(0.5, 0.5) * cell_size - position;

            let cell_at_index = grid_data.coord_to_index(&cell_neigh);

            let q = affine_momentum * cell_dist / cell_size;
            let mass_contrib = weight * particle.mass;

            // mass and momentum update
            grid[cell_at_index].mass += mass_contrib;
            grid[cell_at_index].velocity += (velocity + q) * mass_contrib;
        }
    }
}

pub fn particle_to_grid(
    particle: &mut Particle,
    particle_properties: &ParticleProperties,
    read_grid: &[MpmCell],
    write_grid: &mut [MpmCell],
    grid_data: &GridSampler,
    eos_stiffness: f32,
    eos_power: f32,
    rest_density: f32,
    dynamic_viscosity: f32,
    elastic_mu: f32,
    elastic_lambda: f32,
    simulation_speed: f32,
) {
    let cell_size = grid_data.cell_size;
    let position = &particle.pos;

    let cell_pos = grid_data.quantize_position(position);
    let cell_coord = grid_data.position_to_grid_coord(position);
    let cell_diff = (position - cell_pos) / cell_size - Vec2::new(0.5, 0.5);
    let weights = quadratic_interpolation_weights(cell_diff);

    // Check surrounding 9 cells to get density of the material at this point.
    let mut density: f32 = 0.0;
    for gx in 0..3 {
        for gy in 0..3 {
            let weight = weights[gx as usize].x * weights[gy as usize].y;
            let cell_neigh = cell_coord + IVec2::new(gx - 1, gy - 1);
            let cell_at_index = grid_data.coord_to_index(&cell_neigh);

            density += read_grid[cell_at_index].mass * weight * cell_size.powf(2.0);
        }
    }

    let constitutive_model = particle_properties.particle_model;

    // Modifications of equation 16 from MLS-MPM
    let eq_16_term_0 = match constitutive_model {
        ParticleModel::Fluid => fluid_constitutive_model(
            particle,
            density,
            &particle_properties.momentum_matrix,
            eos_stiffness,
            eos_power,
            rest_density,
            dynamic_viscosity,
            cell_size,
        ),
        ParticleModel::NeoHookean => solid_constitutive_model(
            particle,
            density,
            &particle_properties.deformation_gradient,
            elastic_mu,
            elastic_lambda,
        ),
    } * simulation_speed;

    // For all surrounding 9 cells record momentum updates.
    for gx in 0..3 {
        for gy in 0..3 {
            let weight = weights[gx as usize].x * weights[gy as usize].y;
            let cell_neigh = cell_coord + IVec2::new(gx - 1, gy - 1);
            let cell_neigh_pos = grid_data.coord_to_pos(&cell_neigh);
            let cell_dist = cell_neigh_pos + Vec2::new(0.5, 0.5) * cell_size - position;

            let cell_at_index = grid_data.coord_to_index(&cell_neigh);
            let cell_force = eq_16_term_0 * weight * cell_dist;

            write_grid[cell_at_index].velocity += cell_force;
        }
    }
}

pub fn update_grid_cell(
    cell: &mut MpmCell,
    grid_data: &GridSampler,
    simulation_speed: f32,
    cell_id: usize,
) {
    let resolution = grid_data.resolution();
    if cell.mass > 0.0 {
        // Convert momentum to velocity, apply gravity.
        cell.velocity /= cell.mass;
        cell.velocity.y += simulation_speed * 9.8;

        let coords = grid_data.index_to_coord(cell_id);
        for dim in 0..2 {
            if coords[dim] < 2 && cell.velocity[dim] < 0.0 {
                cell.velocity[dim] = 0.0;
            }
            if coords[dim] > resolution[dim] - 3 && cell.velocity[dim] > 0.0 {
                cell.velocity[dim] = 0.0;
            }
        }
    }
}

pub(super) fn grid_to_particles(
    particle: &mut Particle,
    particle_properties: &mut ParticleProperties,
    grid: &[MpmCell],
    grid_data: &GridSampler,
    collission: &dyn Fn(&[f32; 2]) -> Vec2,
    simulation_speed: f32,
) {
    let cell_size = grid_data.cell_size;
    let bounds = grid_data.bounds();

    let position = &mut particle.pos;
    let velocity = &mut particle.vel;
    let affine_momentum = &mut particle_properties.momentum_matrix;

    // reset particle velocity. we calculate it from scratch each step using the
    // grid
    *velocity = Vec2::zeros();

    let cell_pos = grid_data.quantize_position(position);
    let cell_coord = grid_data.position_to_grid_coord(position);
    let cell_diff = (*position - cell_pos) / cell_size - Vec2::new(0.5, 0.5);
    let weights = quadratic_interpolation_weights(cell_diff);

    // affine per-particle momentum matrix from APIC / MLS-MPM.
    // see APIC paper (https://web.archive.org/web/20190427165435/https://www.math.ucla.edu/~jteran/papers/JSSTS15.pdf), page 6
    // below equation 11 for clarification. this is calculating C = B * (D^-1) for
    // APIC equation 8, where B is calculated in the inner loop at (D^-1) = 4 is
    // a constant when using quadratic interpolation functions
    let mut b = Mat2::zeros();
    // for all surrounding 9 cells
    for gx in 0..3 {
        for gy in 0..3 {
            let weight = weights[gx as usize].x * weights[gy as usize].y;
            let cell_neigh = cell_coord + IVec2::new(gx - 1, gy - 1);
            let cell_neigh_pos = grid_data.coord_to_pos(&cell_neigh);
            let cell_dist = cell_neigh_pos + Vec2::new(0.5, 0.5) * cell_size - *position;

            let cell_at_index = grid_data.coord_to_index(&cell_neigh);

            let weighted_velocity = grid[cell_at_index].velocity * weight;
            b += Mat2::from_columns(&[
                weighted_velocity * cell_dist[0] / cell_size,
                weighted_velocity * cell_dist[1] / cell_size,
            ]);
            *velocity += weighted_velocity;
        }
    }

    *affine_momentum = b * 4.0;

    let collission_force = collission(&[position.x, position.y]);
    // Add forces from collissions.
    *velocity += collission_force * 0.25;
    // let damping_force = -*velocity * 0.01;
    // *velocity += damping_force;

    // Advect particles.
    *position += *velocity * simulation_speed;

    // Never move a particle out of grid bounds.
    for (i, coord) in position.iter_mut().enumerate() {
        *coord = coord.clamp(
            bounds.0[i] + cell_size * MARGIN_PROPORTION,
            bounds.1[i] - cell_size * MARGIN_PROPORTION,
        );
    }

    // Safety clamp to ensure particles don't exit simulation domain.
    let (min, max) = grid_data.bounds();
    for dim in 0..2 {
        position[dim] = position[dim].clamp(min[dim] + cell_size, max[dim] - cell_size);
    }
}

// Only for solids.
// Deformation gradient theory.
// https://osupdocs.forestry.oregonstate.edu/index.php/MPM_Methods_and_Simulation_Timing
// More in depth resource:
// https://www.cs.jhu.edu/~misha/ReadingSeminar/Papers/Moler03.pdf
fn update_deformation_gradients(
    particle_properties: &mut ParticleProperties,
    simulation_speed: f32,
) {
    let affine_momentum = particle_properties.momentum_matrix * simulation_speed;
    // Taylor approximation of the expression:
    // dF \cdot F = exp(\delta t \nabla v) F(t)
    // Which is a numerical update of the deformation gradient.
    // Potential alternative is the Crank Nicholson Method.
    let deformation_new: Mat2 =
        (Mat2::identity() + affine_momentum + affine_momentum * affine_momentum / 2.0)
            * particle_properties.deformation_gradient;

    particle_properties.deformation_gradient = deformation_new;
}

impl FluidGrid {
    pub fn eval_smooth_kernel(&self, position: &Vec2) -> Vec<f32> {
        let cell_size = self.grid_data.cell_size;
        let cell_pos = self.grid_data.quantize_position(position);
        let cell_diff = (*position - cell_pos) / cell_size - Vec2::new(0.5, 0.5);
        let weights = quadratic_interpolation_weights(cell_diff);

        let mut results = Vec::with_capacity(9);
        for gx in 0..3 {
            for gy in 0..3 {
                let weight = weights[gx as usize].x * weights[gy as usize].y;

                results.push(weight);
            }
        }

        results
    }

    pub fn sim(
        &mut self,
        collission: &dyn Fn(&[f32; 2]) -> Vec2,
        grid_clear_data: &Vec<f32>,
    ) {
        for _ in 0..1 {
            for i in 0..self.grid.len() {
                clear_grid_cell(i, &mut self.grid[i], grid_clear_data);
            }
            for i in 0..self.particles.len() {
                update_surrounding_cells(
                    &mut self.particles[i],
                    &self.particle_properties[i],
                    &mut self.grid,
                    &self.grid_data,
                );
            }
            let grid = self.grid.clone();
            for i in 0..self.particles.len() {
                particle_to_grid(
                    &mut self.particles[i],
                    &self.particle_properties[i],
                    &grid,
                    &mut self.grid,
                    &self.grid_data,
                    self.eos_stiffness,
                    self.eos_power,
                    self.rest_density,
                    self.dynamic_viscosity,
                    self.elastic_mu,
                    self.elastic_lambda,
                    self.simulation_speed,
                );
            }
            for i in 0..self.grid.len() {
                update_grid_cell(
                    &mut self.grid[i],
                    &self.grid_data,
                    self.simulation_speed,
                    i,
                );
            }
            for i in 0..self.particles.len() {
                grid_to_particles(
                    &mut self.particles[i],
                    &mut self.particle_properties[i],
                    &self.grid,
                    &self.grid_data,
                    collission,
                    self.simulation_speed,
                );
            }

            for i in 0..self.particles.len() {
                // TODO: skip to indices where solid particles are defined.
                let properties = &mut self.particle_properties[i];
                if properties.particle_model != ParticleModel::NeoHookean {
                    continue;
                }
                update_deformation_gradients(
                    &mut self.particle_properties[i],
                    self.simulation_speed,
                );
            }
        }
    }

    pub fn add_particles(&mut self, positions: &[Vec2], velocities: &[Vec2]) {
        assert!(positions.len() == velocities.len());
        let bounds = self.grid_data.bounds();
        let safety_margin = self.grid_data.cell_size * MARGIN_PROPORTION;
        for (id, position) in positions.iter().enumerate() {
            let mut position = *position;
            for (i, coord) in position.iter_mut().enumerate() {
                *coord = coord.clamp(
                    bounds.0[i] + safety_margin,
                    bounds.1[i] - safety_margin - 1.0,
                );
            }

            self.particles.push(Particle {
                pos: Vec2::new(position.x, position.y),
                vel: velocities[id],
                mass: 1.0,
                ..Default::default()
            });

            self.particle_properties.push(ParticleProperties {
                momentum_matrix: Mat2::zeros(),
                particle_model: ParticleModel::Fluid,
                ..Default::default()
            });
        }
    }

    pub fn new() -> FluidGrid {
        let fluid_particle_resolution = 00;
        let mut particles =
            Vec::with_capacity(fluid_particle_resolution * fluid_particle_resolution);
        // for i in 0..fluid_particle_resolution
        // {
        //     let x = i as f32 / (fluid_particle_resolution - 1) as f32;
        //     let x = (1.0 - x) * 25.0 + x * 75.0;
        //     for j in 0..fluid_particle_resolution
        //     {
        //         let y = j as f32 / (fluid_particle_resolution - 1) as f32;
        //         let y = (1.0 - y) * 25.0 + y * 75.0;

        //         particles.push(Particle {
        //             pos: Vec2::new(x, y),
        //             vel: Vec2::zeros(),
        //             mass: 1.0,
        //             ..Default::default()
        //         });
        //     }
        // }

        let mut particle_properties = vec![
            ParticleProperties {
                momentum_matrix: Mat2::zeros(),
                particle_model: ParticleModel::Fluid,
                ..Default::default()
            };
            particles.len()
        ];

        let cutoff = particles.len();

        let solid_particle_resolution = 00;
        for i in 0..solid_particle_resolution {
            let x = i as f32 / (solid_particle_resolution - 1) as f32;
            let x = (1.0 - x) * 30.0 + x * 50.0;
            for j in 0..solid_particle_resolution {
                let y = j as f32 / (solid_particle_resolution - 1) as f32;
                let y = (1.0 - y) * 10.0 + y * 30.0;

                particles.push(Particle {
                    pos: Vec2::new(x, y),
                    vel: Vec2::zeros(),
                    mass: 1.0,
                    ..Default::default()
                });
            }
        }

        let mut solid_particle_properties = vec![
            ParticleProperties {
                deformation_gradient: Mat2::identity(),
                particle_model: ParticleModel::NeoHookean,
                ..Default::default()
            };
            particles.len() - cutoff
        ];

        particle_properties.append(&mut solid_particle_properties);

        let extent = 100.0;
        let resolution = 100;
        let dx = extent / resolution as f32;
        let grid_resolution = (extent / dx).ceil() as usize;
        assert!(grid_resolution == resolution);

        let grid_cells = vec![
            MpmCell {
                mass: 0.0,
                ..Default::default()
            };
            grid_resolution * grid_resolution
        ];

        let grid_data = GridSampler::new(
            &Vec3::new(-0.0, -0.0, 0.0),
            &Vec3::new(extent, extent, 1.0 / grid_resolution as f32),
            dx,
        );

        assert!(resolution * resolution == grid_cells.len(),);
        assert!(resolution as i32 == grid_data.resolution().x);
        assert!(resolution as i32 == grid_data.resolution().y);

        FluidGrid {
            particles,
            particle_properties,
            grid: grid_cells,
            grid_data,
            simulation_speed: 0.045,
            eos_stiffness: 10.0,
            rest_density: 4.,
            eos_power: 4.,
            dynamic_viscosity: 0.1,
            elastic_lambda: 18.0 * 1000.0,
            elastic_mu: 6. * 1000.0,
        }
    }
}

// +| Internal |+ ==============================================================
fn solid_constitutive_model(
    particle: &Particle,
    density: f32,
    deformation_gradient: &Mat2,
    elastic_mu: f32,
    elastic_lambda: f32,
) -> Mat2 {
    let volume = particle.mass / density;

    let j: f32 = deformation_gradient.determinant();

    assert!(j > 0.0, "{} {}", j, deformation_gradient);
    let volume_scaled = volume * j;

    // useful matrices for Neo-Hookean model
    let f_t = deformation_gradient.transpose();
    let f_inv_t = f_t.try_inverse().unwrap_or(Mat2::identity());
    let f_minus_f_inv_t = deformation_gradient - f_inv_t;

    // MPM course equation 48
    let p_term_0 = elastic_mu * f_minus_f_inv_t;
    let p_term_1 = elastic_lambda * j.ln() * f_inv_t;
    let p_combined = p_term_0 + p_term_1;

    // cauchy_stress = (1 / det(F)) * P * F_T
    // equation 38, MPM course
    let stress = (1.0 / j) * p_combined * f_t;

    // (M_p)^-1 = 4, see APIC paper and MPM course page 42. Would be 3 in 3D.
    // this term is used in MLS-MPM paper eq. 16. with quadratic weights, Mp = (1/4)
    // * (delta_x)^2. in this simulation, delta_x = 1, because i scale the
    // rendering of the domain rather than the domain itself. we multiply by dt
    // as part of the process of fusing the momentum and force update for MLS-MPM

    -volume_scaled * 4.0 * stress
}

fn fluid_constitutive_model(
    particle: &Particle,
    density: f32,
    affine_momentum: &Mat2,
    eos_stiffness: f32,
    eos_power: f32,
    rest_density: f32,
    dynamic_viscosity: f32,
    cell_size: f32,
) -> Mat2 {
    let volume = particle.mass / density;

    // Tait equation of state.
    // https://en.wikipedia.org/wiki/Tait_equation
    let pressure = f32::max(
        -0.1,
        eos_stiffness * (f32::powf(density / rest_density, eos_power) - 1.0),
    );
    // stress = -pressure * I + viscosity * (velocity_gradient +
    // velocity_gradient_transposed)
    let mut stress = Mat2::from_diagonal(&Vec2::new(-pressure, -pressure));

    // velocity gradient - CPIC eq. 17, where deriv of quadratic polynomial is
    // linear dudv = C_p;
    // strain = dudv;
    let mut strain = *affine_momentum;

    let trace = strain.trace();
    strain[(1, 0)] = trace;
    strain[(0, 1)] = trace;
    let viscosity_term = strain * dynamic_viscosity;
    stress += viscosity_term;

    // 4 \delta x is from magic simplification of term M_p.
    // Uses equation 16 in MLS-MPM paper.

    stress * (-volume * 4.0) * cell_size
}
