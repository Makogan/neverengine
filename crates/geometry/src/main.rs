use std::{collections::HashMap, f32::consts::PI};

use algebra::*;
use chicken_wire::wavefront_loader::ObjData;
use clap::Parser;

// TODO(low): Clean these imports.
pub mod b_spline;
pub use b_spline::*;
pub mod traits;
use itertools::Itertools;
pub use traits::*;
pub mod parametric_surfaces;
pub use parametric_surfaces::*;
pub mod differential_geometry;
pub use differential_geometry::*;
pub mod rotation_minimizing_frame;
pub use rotation_minimizing_frame::*;
pub mod triangulation;
pub use triangulation::*;
pub mod convex_hull;
pub use convex_hull::*;
pub mod vertex_enumeration;
pub use vertex_enumeration::*;
pub mod planar_bounding_primitives;
pub use planar_bounding_primitives::*;
pub mod euclidean;
pub use euclidean::*;
pub mod convex_polytope;
pub use convex_polytope::*;
pub mod skeleton_meshing;
pub mod utils;

use graphs::{
    compact_line_graph,
    depth_first_search::DFSIterator,
    find_root_of_tree,
    line_soup_to_graph,
};
use hedge::{
    mesh::{triangulate_convex_faces_symmetrically, GeomId, HalfMesh},
    remeshing::RemeshingParameters,
    smooth_vertices,
    HalfMeshLike,
};
pub use skeleton_meshing::*;
pub use utils::*;

const DECAY_RATE: f32 = 0.0;
const SMALLEST_RADIUS: f32 = 0.005;
const STARTING_RADIUS: f32 = 0.005;

#[derive(clap::ValueEnum, Clone, Copy, Debug)]
enum EdgeKind
{
    Circular,
    Quad,
    Corkscrew,
    Bulb,
}

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// Path to an input obj file containing the edges of the skeleton.
    #[arg(short, long)]
    input_path: String,

    /// Path in which the mesh file will be written to.
    #[arg(short, long)]
    output_path: String,

    /// General thickness of the edges and the nodes.
    #[arg(short, long, default_value_t = 0.8)]
    line_thickness: f32,

    /// Which kind of edge profile to use.
    #[arg(value_enum, short, long, default_value_t = EdgeKind::Quad)]
    edge_kind: EdgeKind,

    /// Whether to triangulate the mesh.
    #[arg(short, long, default_value_t)]
    triangulate: bool,
}

fn mesh_chain(
    vertices: &Vec<Vec3>,
    chain: &Vec<usize>,
    v1: &Vec3,
    v2: &Vec3,
    radius: f32,
    edge_kind: EdgeKind,
    close_end: bool,
    triangulate: bool,
) -> (Vec<Vec3>, Vec<Vec<usize>>, [usize; 2], [usize; 2])
{
    let mut points: Vec<_> = chain.iter().map(|&pid| vertices[pid as usize]).collect();
    let n = points.len();

    points[0] = *v1;
    points[n - 1] = *v2;

    let spline_order = if points.len() <= 3 { 2 } else { 3 };
    let spline = BSpline::new_uniform(points, spline_order);

    let resolution = match edge_kind
    {
        EdgeKind::Quad => 4,
        _ => 25,
    };
    let rmf = RotationMinimizedFrame::new_from_range_resolution([0., 1.0], 10, |t| {
        spline.sample(t)
    });

    let mesh_fun = |f, parameters| {
        if triangulate
        {
            let (vs, fs) = mesh_surface_triangle(f, parameters);

            (vs, fs.chunks(3).map(|a| a.to_vec()).collect_vec())
        }
        else
        {
            mesh_surface_quad(f, parameters)
        }
    };

    let (verts, faces) = mesh_fun(
        |u: f32, v| {
            let point = spline.sample(v);
            let [_x, y, z] = rmf.sample(v);

            match edge_kind
            {
                EdgeKind::Corkscrew =>
                {
                    let r: f32 = 0.5 + 0.3 * f32::cos(5_f32 * (u + 0.95_f32));

                    let coords = Vec2::new(u.cos() * radius * r, -u.sin() * radius * r);

                    let rotation = 7.1;
                    let coords = Vec2::new(
                        (v * rotation).cos() * coords.x - (v * rotation).sin() * coords.y,
                        (v * rotation).sin() * coords.x + (v * rotation).cos() * coords.y,
                    );

                    return point + y * coords.x + z * coords.y;
                }
                EdgeKind::Circular | EdgeKind::Quad =>
                {
                    return point + y * u.cos() * radius + z * -u.sin() * radius;
                }
                EdgeKind::Bulb =>
                {
                    return point
                        + (y * u.cos() * radius + z * -u.sin() * radius)
                            * ((v * PI * 2. * 4.).sin() + 2. + radius * 1.8)
                            * 0.5;
                }
            }
        },
        crate::Parameters {
            u_resolution: resolution,
            v_resolution: 20,
            u_range: [0.0, 2.0 * std::f32::consts::PI],
            // For some reason if the range is larger than this in rare cases we
            // get wrong results in the parametric curve.
            // TODO(high): figure out why sometimes meshes crash if this value
            // is the proper bound of 0 instead of 0.08. It's likely an issue with the
            // rotation minimization.
            v_range: [0.2, 0.8],
            topology: match close_end
            {
                true => TopologyKind::CappedVMax,
                false => TopologyKind::Cylinder,
            },
            ..Default::default()
        },
    );

    let n = verts.len();
    (verts, faces, [0, resolution], [n - resolution, n])
}

fn mesh_graph<N, E>(
    vertices: &Vec<Vec3>,
    graph: &Vec<Vec<u64>>,
    node_size: N,
    edge_size: E,
    args: &Args,
) -> (HalfMesh<Vec<Vec3>, (), ()>, usize)
where
    N: Fn(usize) -> f32,
    E: Fn(&[usize; 2]) -> f32,
{
    let root = find_root_of_tree(&graph) as u64;
    let (chains, sparse_graph, edge_chain_map) = compact_line_graph(root, graph);

    let mut depths = HashMap::new();
    for (node, depth, _parent) in
        DFSIterator::new(root as usize, |node, _depth, _parent| {
            sparse_graph
                .get(&(*node as u64))
                .expect(format!("{}", node).as_str())
                .iter()
                .map(|n| *n as usize)
        })
    {
        depths.insert(node, depth);
    }

    mesh_skeleton(
        root as usize,
        |i: usize| vertices[i],
        |i: usize, j: usize| {
            let mut key = [i as u64, j as u64];
            key.sort();
            let chain_id = *edge_chain_map.get(&key).unwrap();

            debug_assert!({
                let mut vals = [chains[chain_id][0], *chains[chain_id].last().unwrap()];
                vals.sort();
                vals == key
            });

            let start = chains[chain_id][0] as usize;
            let end = *chains[chain_id].last().unwrap() as usize;
            let dir = if i == chains[chain_id][0] as usize
            {
                let next = chains[chain_id][1];
                vertices[next as usize] - vertices[start]
            }
            else
            {
                let prev = chains[chain_id][chains[chain_id].len() - 2];
                vertices[prev as usize] - vertices[end]
            };

            assert!(dir.norm() >= f32::EPSILON * 10.0);

            dir
        },
        |&i: &usize, _, _| {
            sparse_graph
                .get(&(i as u64))
                .expect(format!("{}", i).as_str())
                .iter()
                .map(|n| *n as usize)
        },
        |&[v1, v2], [i, j]| {
            let mut key = [*i as u64, *j as u64];
            key.sort();

            let chain_id = edge_chain_map.get(&key).unwrap();
            let mut chain = chains[*chain_id].clone();

            let reverse_test = chain[0] != *i as u64;
            if reverse_test
            {
                chain.reverse()
            }

            let depth = depths.get(&(*i as usize)).unwrap();
            let base_radius = edge_size(&[*i, *j]);
            let area = (base_radius * base_radius * PI)
                / 2.0_f32.powf(*depth as f32 * DECAY_RATE);
            let radius = (area / PI).sqrt();
            let radius = radius.max(SMALLEST_RADIUS);
            // let radius = base_radius;
            mesh_chain(
                &vertices,
                &chain.iter().map(|&i| i as usize).collect(),
                &v1,
                &v2,
                radius,
                args.edge_kind,
                false,
                args.triangulate,
            )
        },
        |&[v1, v2], [i, j]| {
            let mut key = [*i as u64, *j as u64];
            key.sort();

            let chain_id = edge_chain_map.get(&key).unwrap();
            let mut chain = chains[*chain_id].clone();

            let reverse_test = chain[0] != *i as u64;
            if reverse_test
            {
                chain.reverse()
            }

            let depth = depths.get(&(*j as usize)).unwrap();

            let base_radius = edge_size(&[*i, *j]);
            let area = (base_radius * base_radius * PI)
                / 2.0_f32.powf(*depth as f32 * DECAY_RATE);
            let radius = (area / PI).sqrt();
            let radius = radius.max(SMALLEST_RADIUS);
            // let radius = base_radius;

            let (vs, fs, b1, _b2) = mesh_chain(
                &vertices,
                &chain.iter().map(|&i| i as usize).collect(),
                &v1,
                &v2,
                radius,
                args.edge_kind,
                true,
                args.triangulate,
            );

            (vs, fs, b1)
        },
        |joint_id| {
            let depth = depths.get(&(joint_id as usize)).unwrap();
            let base_radius = node_size(joint_id);
            let area = (base_radius * base_radius * PI)
                / 2.0_f32.powf(*depth as f32 * DECAY_RATE);
            let radius = (area / PI).sqrt();
            let radius = radius.max(SMALLEST_RADIUS);
            // let radius = base_radius;
            radius
        },
        crate::MeshSkeletonParameters {
            ..Default::default()
        },
    )
}

fn main()
{
    use env_logger::*;
    Builder::from_default_env()
        .filter_level(log::LevelFilter::Debug)
        .init();

    let args = Args::parse();

    let ObjData {
        vertices, lines, ..
    } = ObjData::from_disk_file(&args.input_path);
    let graph = line_soup_to_graph(&lines);

    let (mut mesh, polytope_vert_count) = mesh_graph(
        &vertices,
        &graph,
        |_| STARTING_RADIUS * 2.5,
        |_| STARTING_RADIUS,
        &args,
    );

    if args.triangulate
    {
        for _ in 0..5
        {
            smooth_vertices::<_, f32, _, _>(
                &mut mesh,
                |i| *i >= polytope_vert_count,
                0.3,
            );
        }

        ObjData::export(&mesh, &"pre_triangulation.obj");

        let _original_vert_count = mesh.vert_count();
        triangulate_convex_faces_symmetrically(&mut mesh);
        use hedge::remeshing::remeshing;

        // let mut sub_mesh = SubMesh::from_fn(&mesh, |v| v.0 <
        // polytope_vert_count as u64);

        // for _ in 0..3
        // {
        //     smooth_vertices::<_, f32, _>(&mut mesh, |i| false, 0.1);
        // }
        ObjData::export(&mesh, &"pre_remeshing.obj");
        remeshing(
            &mut mesh,
            RemeshingParameters {
                dihedral_angle_tolerance: PI,
                ..Default::default()
            },
            |_i, _j| 0.1,
            |&v| v,
        );

        ObjData::export(&mesh, &"pre_smoothing.obj");
        for _ in 0..1
        {
            mesh_laplacian_smoothing(
                mesh.iter_verts().map(|v| v.data()),
                |data, id| mesh.vert_handle(GeomId::from(id)).mutate_data(data),
                |id| {
                    mesh.vert_handle(GeomId::from(id))
                        .iter_verts()
                        .map(|v| (v.data(), v.id().0 as usize))
                },
                |id| {
                    let is_edge =
                        |id| id >= polytope_vert_count && id < _original_vert_count;

                    is_edge(id)
                        || mesh
                            .vert_handle(GeomId::from(id))
                            .iter_concentric_rings(1)
                            .any(|v| is_edge(v.id().0 as usize))

                    // !mesh
                    //     .vert_handle(GeomId::from(id))
                    //     .iter_concentric_rings(3)
                    //     .any(|v| (v.id().0 as usize) >= polytope_vert_count)
                },
                2,
                MeshWeights::Cotan,
            );
        }

        for _ in 0..10
        {
            smooth_vertices::<_, f32, _, _>(&mut mesh, |_i| false, 0.4);
        }
    }

    ObjData::export(&mesh, &args.output_path);
}
