pub use na::{Dim, RealField, UnitQuaternion};
pub use nalgebra as na;
use nalgebra::Vector3;
pub use nalgebra_glm::*;
use num::traits::float;

pub type Quatf = na::UnitQuaternion<f32>;

pub type Vec2 = na::Vector2<f32>;
pub type Vec3 = na::Vector3<f32>;
pub type Vec4 = na::Vector4<f32>;

pub type IVec2 = na::Vector2<i32>;
pub type IVec3 = na::Vector3<i32>;
pub type IVec4 = na::Vector4<i32>;

pub type UVec2 = na::Vector2<u32>;
pub type UVec3 = na::Vector3<u32>;
pub type UVec4 = na::Vector4<u32>;

pub type Mat2 = na::Matrix2<f32>;
pub type Mat3 = na::Matrix3<f32>;
pub type Mat4 = na::Matrix4<f32>;

pub type Quatd = na::UnitQuaternion<f64>;

pub type DVec2 = na::Vector2<f64>;
pub type DVec3 = na::Vector3<f64>;
pub type DVec4 = na::Vector4<f64>;

pub type DMat2 = na::Matrix2<f64>;
pub type DMat3 = na::Matrix3<f64>;
pub type DMat4 = na::Matrix4<f64>;

// pub mod eigen_decomposition;
pub mod krylov;
// use crate::krylov::krylov_schur_method;
pub mod sparse_vector;
pub mod utils;

pub fn eigen_decompose_sparse_symmetric_matrix(
    _mat: &faer::sparse::SparseColMat<usize, f64>,
    _eigen_value_count: usize,
) -> (Vec<f64>, faer::Mat<f64>) {
    // let guess =
    //     faer::Col::<f64>::ones(mat.nrows()) * (1.0 / (eigen_value_count as f64).sqrt());
    // krylov_schur_method(&mat, guess, eigen_value_count, 0.000001)

    // dbg hack
    (vec![], faer::Mat::<f64>::zeros(0, 0))
}

// +| Tests |+
// =================================================================
#[cfg(test)]
mod tests {
    use na::Matrix3x4;

    use crate::rref;
    use crate::*;

    #[test]
    pub fn rref_test() {
        let mat: Mat4 = Mat4::identity();
        let res = rref(&mat);

        assert_eq!(mat, res);

        let m = na::Matrix3x4::<f64>::new(
            1.0, 2.0, -1.0, -4.0, 2.0, 3.0, -1.0, -11.0, -2.0, 0.0, -3.0, 22.0,
        );

        let expected = Matrix3x4::<f64>::new(
            1.0, 0.0, 0.0, -8.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, -2.0,
        );

        let res = rref(&m);
        assert!((res - expected).norm() < 1.0e-5, "{}", (res - expected));
    }

    #[test]
    pub fn null_basis_test() {
        // T1 ===
        let m = na::Matrix2x4::<f64>::new(
            1.0, -1.0, -1.0, 3.0, //
            2.0, -2.0, 0.0, 4.0,
        );

        let basis = null_basis(&m);
        let expected = na::DMatrix::<f64>::from_vec(
            4,
            2,
            vec![
                1.0, 1.0, 0.0, 0.0, //
                -2.0, 0.0, 1.0, 1.0,
            ],
        );

        assert!(
            (basis.clone() - expected.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - expected.clone())
        );

        // T2 ===
        let m = na::Matrix4x2::<f64>::new(
            1.0, 1.0, //
            0.0, 1.0, //
            1.0, 0.0, //
            2.0, 1.0, //
        );

        let basis = null_basis(&m);
        assert!(basis.ncols() == 0);

        // T3 ===
        let m = na::Matrix4x2::<f64>::new(
            1.0, 2.0, //
            2.0, 4.0, //
            4.0, 8.0, //
            8.0, 16.0, //
        );

        let basis = null_basis(&m);
        let expected = na::DMatrix::<f64>::from_vec(2, 1, vec![-2.0, 1.0]);

        assert!(
            (basis.clone() - expected.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - expected.clone())
        );

        // T4 ===
        let m = na::Matrix2x3::<f64>::new(
            1.0, 0.0, 0.0, //
            0.0, 1.0, 0.0, //
        );

        let basis = null_basis(&m);

        let expected = na::DMatrix::<f64>::from_vec(
            3,
            1,
            vec![
                0.0, 0.0, 1.0, //
            ],
        );

        assert!(
            (basis.clone() - expected.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - expected.clone())
        );
    }

    #[test]
    pub fn column_basis_test() {
        // T1 ===
        let mat: Mat4 = Mat4::identity();
        let basis = column_basis(&mat);

        assert!(
            (basis.clone() - mat.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - mat.clone())
        );

        // T2 ===
        let m = na::DMatrix::<f64>::from_vec(
            4,
            2,
            vec![
                1.0, 1.0, 0.0, 0.0, //
                -2.0, 0.0, 1.0, 1.0,
            ],
        );
        let basis = column_basis(&m);
        let expected = na::DMatrix::<f64>::from_vec(
            4,
            2,
            vec![
                1.0, 1.0, 0.0, 0.0, //
                -2.0, 0.0, 1.0, 1.0,
            ],
        );

        assert!(
            (basis.clone() - expected.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - expected.clone())
        );

        // T3 ===
        let m = na::DMatrix::<f64>::from_vec(
            4,
            2,
            vec![
                1.0, 1.0, 0.0, 0.0, //
                -2.0, -2.0, 0.0, 0.0,
            ],
        );
        let basis = column_basis(&m);
        let expected = na::DMatrix::<f64>::from_vec(4, 1, vec![1.0, 1.0, 0.0, 0.0]);

        assert!(
            (basis.clone() - expected.clone()).norm() < 1.0e-5,
            "{}",
            (basis.clone() - expected.clone())
        );
    }
}

pub fn orthogonal_vector<S>(v: &Vector3<S>) -> Vector3<S>
where
    S: linear_isomorphic::RealField + float::TotalOrder,
{
    let o = S::from(1.).unwrap();
    let z = S::from(0.).unwrap();
    let v1 = Vector3::new(z, z, o);
    let v2 = Vector3::new(z, o, z);
    let v3 = Vector3::new(o, z, z);

    let scores = [v.dot(&v1).abs(), v.dot(&v2).abs(), v.dot(&v3).abs()];
    let dirs = [v1, v2, v3];

    let (min_index, _min_value) = scores
        .iter()
        .enumerate()
        .min_by(|(_, &x), (_, &y)| x.total_cmp(&y))
        .unwrap();

    dirs[min_index].cross(v)
}

pub fn slerp(v1: &Vec3, v2: &Vec3, t: f32) -> Vec3 {
    debug_assert!((v1.norm() - 1.).abs() < 1e-4);
    debug_assert!((v2.norm() - 1.).abs() < 1e-4);
    let dot = v1.dot(v2);
    let dot_clamped = dot.max(-1.0).min(1.0);

    // Check if vectors are close to colinear
    if (1.0 - dot_clamped.abs()).abs() < 1e-4 {
        return v1.clone(); // Return the first vector
    }

    let theta = dot_clamped.acos();
    let sin_theta = theta.sin();

    let weight_v1 = ((1.0 - t) * theta).sin() / sin_theta;
    let weight_v2 = (t * theta).sin() / sin_theta;

    (v1 * weight_v1 + v2 * weight_v2).normalize()
}
