use std::cell::RefCell;
use std::rc::Rc;

use crate::na::DMatrix;
use crate::na::RowDVector;
use algebra::Mat4;
use camera::*;
use chicken_wire::wavefront_loader::ObjData;
use core::f32::consts::PI;
use dem_core::{io::*, rendering::*};
use dem_gui::*;
use dem_peripherals::*;
use geometry::polytope_mesh_from_directions;
use geometry::segment_segment_distance;
use geometry::CharrogGregoryPatch;
use geometry::{mesh_surface_triangle, Parameters, RotationMinimizedFrame};

use hedge::mesh::verify_topology;
use hedge::FaceMetrics;
use hedge::HEdgeHandle;
use hedge::{
    mesh::{GeomId, HalfMesh},
    HalfMeshLike,
};

use std::collections::HashSet;
use vulkan_bindings::RenderContext;

fn line_cylinder(start: &Vec3, end: &Vec3) -> (Vec<Vec3>, Vec<usize>) {
    let rmf = RotationMinimizedFrame::new_from_range_resolution([0., 10.], 10, |t| {
        (1. - t) * start + t * end
    });

    mesh_surface_triangle(
        |u, v| {
            let [_t, n, b] = rmf.sample(v);
            b * u.cos() + n * u.sin() + (1. - v) * start + v * end
        },
        Parameters {
            u_range: [0., 2. * PI],
            v_range: [0., 1.],
            u_resolution: 100,
            v_resolution: 100,
            ..Default::default()
        },
    )
}

type HMesh = HalfMesh<Vec<Vec3>, (), ()>;
fn connect_cylinders<F, G>(
    cylinders: &mut HMesh,
    _joint: &Vec3,
    boundary_start_vertex_ids: &Vec<usize>,
    polytope: &HMesh,
    polytope_dual_edge_meshing: F,
    _polytope_dual_vertex_meshing: G,
    resolution: usize,
) where
    F: Fn(&HMesh, usize, usize, GeomId<0>, GeomId<0>) -> Vec3,
    G: Fn(&HMesh, usize, usize, GeomId<0>, &Vec3) -> Vec3,
{
    let prior_vert_count = cylinders.vert_count();
    let mut seen = HashSet::new();
    let mut face_section_map = Vec::new();
    // Construct the associations between boundary pieces of each cylinder. That is,
    // each cylinder boundary has a part of it that should be connected to one of its
    // neighbours. Find those pieces in each cylinder and establish their asociations.
    for face in polytope.iter_faces() {
        let f1 = face.id().0 as usize;
        seen.insert(f1);

        let b1 = cylinders
            .vert_handle(GeomId(boundary_start_vertex_ids[f1] as u64))
            .get_boundary_hedge()
            .unwrap();

        // Find the closest points between cylinders.
        let seeds: Vec<_> = face
            .hedge()
            .iter_loop()
            .map(|h| {
                let f2 = h.pair().face().id().0 as usize;
                let b2 = cylinders
                    .vert_handle(GeomId(boundary_start_vertex_ids[f2] as u64))
                    .get_boundary_hedge()
                    .unwrap();
                let [closest1, closest2] = find_closest_segments(&b1, &b2);
                [
                    cylinders.hedge_handle(closest1),
                    cylinders.hedge_handle(closest2),
                ]
            })
            .collect();

        let arclength: f32 = b1
            .iter_loop()
            .map(|h| (h.source().data() - h.dest().data()).norm())
            .sum();
        let boundary_point_count = b1.iter_loop().count();
        let face_valence = face.hedge().iter_loop().count();

        let mut face_sections = vec![(Vec::new(), 0., false); face_valence];
        let mut active_sections: Vec<_> = (0..face_sections.len()).collect();
        let mut current_section = 0;
        let mut occupied = HashSet::new();
        for (i, [s1, _s2]) in seeds.into_iter().enumerate() {
            occupied.insert(s1.id());
            face_sections[i].0.push(s1);
        }

        // Do a flood-fill growth of each section, starting at the found seeds.
        while !active_sections.is_empty() {
            let section = active_sections[current_section];
            let (list, length, update_back) = &mut face_sections[section];

            let hedge = if *update_back {
                list[0].prev().clone()
            } else {
                list.last().unwrap().next().clone()
            };

            if occupied.contains(&hedge.id()) {
                let e1 = &list[0];
                let e2 = &list.last().unwrap();
                if occupied.contains(&e1.next().id())
                    && occupied.contains(&e1.prev().id())
                    && occupied.contains(&e2.next().id())
                    && occupied.contains(&e2.prev().id())
                {
                    list.push(e2.next());
                    active_sections.remove(current_section);
                    current_section = current_section % active_sections.len().max(1);
                }

                *update_back = !*update_back;
                continue;
            }

            occupied.insert(hedge.id());
            let l = (hedge.source().data().clone() - hedge.dest().data().clone()).norm();

            *length += l;

            if *update_back {
                list.insert(0, hedge);
            } else {
                list.push(hedge);
            }
            *update_back = !*update_back;

            if *length > arclength / 3.
                && list.len() > (boundary_point_count as f64 / 3.).ceil() as usize
            {
                active_sections.remove(current_section);
                current_section = current_section % active_sections.len().max(1);
            } else {
                current_section = (current_section + 1) % active_sections.len().max(1);
            };
        }

        let face_sections: Vec<_> = face_sections.into_iter().map(|s| s.0).collect();
        face_section_map.push(face_sections);
    }

    // Create the associations between each boundary section, between each cylinder.
    let mut seen = HashSet::new();
    let mut stitching_pairs = Vec::new();
    for face in polytope.iter_faces() {
        let f1 = face.id().0 as usize;
        seen.insert(f1);

        for (i, hedge) in face.hedge().iter_loop().enumerate() {
            let f2 = hedge.pair().face().id().0 as usize;
            if seen.contains(&f2) {
                continue;
            }

            let id_f1_in_f2 = polytope
                .face_handle(GeomId(f2 as u64))
                .hedge()
                .iter_loop()
                .position(|h| h.pair().face().id().0 == f1 as u64)
                .unwrap();

            let [f1_sections, f2_sections] =
                get_disjoint_entries(&mut face_section_map, f1, f2);

            let s1 = &mut f1_sections[i];
            let s2 = &mut f2_sections[id_f1_in_f2];

            equalize_discretizations(s1, &cylinders, s2, &cylinders);
            stitching_pairs.push([s1.clone(), s2.clone()]);
        }
    }

    let start = cylinders.vert_count();
    let mut added_count = 0;
    let mut offsets = Vec::new();
    for [s1, s2] in &stitching_pairs {
        let n = s1.len();
        let s2: Vec<_> = s2.iter().cloned().rev().collect();

        offsets.push(added_count + start);
        for i in 0..n {
            for j in 0..resolution {
                let point = polytope_dual_edge_meshing(
                    cylinders,
                    i,
                    j,
                    s1[i].source().id(),
                    s2[i].source().id(),
                );

                cylinders.add_vert(point);
                added_count += 1;
            }
        }
    }

    for (i, [s1, s2]) in stitching_pairs.into_iter().enumerate() {
        let n = s1.len();
        let s2: Vec<_> = s2.iter().cloned().rev().collect();
        let offset = offsets[i];

        for i in 0..n - 1 {
            let p00 = GeomId::<0>((i * resolution + offset) as u64);
            let p10 = GeomId::<0>(((i + 1) * resolution + offset) as u64);

            cylinders.add_face(&[s1[i].source().id(), s1[i + 1].source().id(), p10], ());
            cylinders.add_face(&[s1[i].source().id(), p10, p00], ());

            for j in 0..resolution - 1 {
                let p00 = GeomId::<0>((i * resolution + j + offset) as u64);
                let p01 = GeomId::<0>((i * resolution + j + 1 + offset) as u64);
                let p10 = GeomId::<0>(((i + 1) * resolution + j + offset) as u64);
                let p11 = GeomId::<0>(((i + 1) * resolution + j + 1 + offset) as u64);

                cylinders.add_face(&[p00, p11, p01], ());
                cylinders.add_face(&[p00, p10, p11], ());
            }

            let last = resolution - 1;
            let p00 = GeomId::<0>((i * resolution + last + offset) as u64);
            let p10 = GeomId::<0>(((i + 1) * resolution + last + offset) as u64);

            cylinders.add_face(&[s2[i].source().id(), p10, s2[i + 1].source().id()], ());
            cylinders.add_face(&[s2[i].source().id(), p00, p10], ());
        }
    }

    let mut new_points = Vec::new();
    let mut new_faces = Vec::new();
    let mut stitching_faces = Vec::new();
    let mut offsets = Vec::new();
    // Close up the regions around the cylinder stitches.
    for vertex in polytope.iter_verts() {
        let faces: Vec<_> = vertex
            .iter_hedges()
            .map(|h| h.face().id().0 as usize)
            .collect();

        let f1 = faces[0];
        let f2 = faces[1];
        let id_f2_in_f1 = polytope
            .face_handle(GeomId(f1 as u64))
            .hedge()
            .iter_loop()
            .position(|h| h.pair().face().id().0 == f2 as u64)
            .unwrap();
        let section = &face_section_map[f1];
        let start_edge = cylinders
            .vert_handle(section[id_f2_in_f1].last().unwrap().source().id())
            .get_boundary_hedge()
            .unwrap();

        let create_bitangent = |h2: &HEdgeHandle<_, _, _>| {
            let h1 = h2.prev();

            let n1: Vec3 = h1.pair().face().normal();
            let n2: Vec3 = h2.pair().face().normal();
            let n = (n1 + n2).normalize();
            let dir: Vec3 = h2.dest().data() - h1.source().data();
            let t: Vec3 = n.cross(&dir);
            let t = t.normalize();
            assert!(t.x.is_finite());
            t
        };

        let create_smooth_bitangent = |h: &HEdgeHandle<_, _, _>| {
            let b1: Vec3 = create_bitangent(&h.prev());
            let b2: Vec3 = create_bitangent(&h);
            let b3: Vec3 = create_bitangent(&h.next());

            let v: Vec3 = (b1 + b2) * 0.5 + b3;
            -v.normalize()
        };
        let mut curves = Vec::new();
        let mut bitangents = Vec::new();
        let mut iter = start_edge.iter_loop();
        let h = iter.next().unwrap();
        let mut accumulator = vec![h.source().id()];
        let mut baccumulator = vec![create_bitangent(&h)];

        for h in iter {
            let vid = h.source().id();
            if (vid.0 as usize) < prior_vert_count {
                accumulator.push(h.source().id());
                baccumulator.push(create_smooth_bitangent(&h));
                assert!(accumulator.len() == baccumulator.len());

                curves.push(core::mem::take(&mut accumulator));
                bitangents.push(core::mem::take(&mut baccumulator));
            }
            accumulator.push(h.source().id());
            baccumulator.push(create_smooth_bitangent(&h));
        }
        curves.push(accumulator);
        bitangents.push(baccumulator);

        let mut added = Vec::new();
        for curve in &curves {
            let hedge = close_gaps(curve[0], cylinders, 0.6);
            debug_assert!(cylinders.hedge_handle(hedge).is_in_boundary_edge());
            added.push(hedge);
        }

        let mut new_curves = Vec::new();
        for i in 0..added.len() {
            let id = added[i];
            let nid = added[(i + 1) % added.len()];

            let v1 = cylinders.hedge_handle(id).source().id();
            let v2 = cylinders.hedge_handle(id).dest().id();
            let end = cylinders.hedge_handle(nid).source().id();

            let handle = cylinders.hedge_handle(id);
            debug_assert!(handle.is_boundary_hedge());
            match_subdivision_level(&mut vec![handle], &cylinders, 5);

            let c1: Vec<_> = cylinders
                .vert_handle(v1)
                .get_boundary_hedge()
                .unwrap()
                // .hedge_handle(h1)
                .iter_loop()
                .take_while(|h| h.prev().source().id() != v2)
                .map(|h| h.source().id())
                .collect();
            let c2 = cylinders
                .vert_handle(*c1.last().unwrap())
                .get_boundary_hedge()
                .unwrap()
                .iter_loop()
                .take_while(|h| h.prev().source().id() != end)
                .map(|h| h.source().id())
                .collect();

            new_curves.push(c1);
            new_curves.push(c2);
        }

        let curves = new_curves;

        let geom_curves: Vec<_> = curves
            .iter()
            .map(|list| {
                list.iter()
                    .map(|vid| cylinders.vert_handle(*vid).data().clone())
                    .collect::<Vec<_>>()
            })
            .collect();
        let sample_counts: Vec<_> = curves.iter().map(|c| c.len() - 1).collect();

        let charrot_gregory_patch =
            CharrogGregoryPatch::new(geom_curves, None /*Some(bitangents)*/, 0.0);
        let (vs, fs) =
            charrot_gregory_patch.mesh_with_per_curve_coeffs(&sample_counts, 0.70);

        offsets.push((new_points.len(), fs.len() / 3));
        for point in &vs {
            new_points.push(point.clone());
        }

        for face in fs.chunks(3) {
            new_faces.push(vec![(face[0]) as u32, (face[1]) as u32, (face[2]) as u32]);
        }

        let outer_boundary: Vec<_> = cylinders
            .vert_handle(curves[0][0])
            .get_boundary_hedge()
            .unwrap()
            .iter_loop()
            .collect();

        let outer_boundary: Vec<_> =
            outer_boundary.iter().map(|h| h.source().id()).collect();

        let mut stitches = Vec::new();
        for i in 0..outer_boundary.len() {
            let p00 = outer_boundary[i].0 as u32;
            let p01 = outer_boundary[(i + 1) % outer_boundary.len()].0 as u32;
            let p10 = i as u32;
            let p11 = ((i + 1) % outer_boundary.len()) as u32;

            stitches.push([p00, p01, p10, p11]);
        }
        stitching_faces.push(stitches);
    }

    let mut vs: Vec<_> = cylinders.iter_verts().map(|v| v.data().clone()).collect();
    let mut fs: Vec<_> = cylinders.get_topology();

    let mut accumulator = 0;
    let mut iter = 0;
    for (vert_offset, face_cutoff) in offsets {
        let offset = vert_offset as u32 + cylinders.vert_count() as u32;
        for i in 0..face_cutoff {
            new_faces[accumulator + i][0] += offset;
            new_faces[accumulator + i][1] += offset;
            new_faces[accumulator + i][2] += offset;
        }

        for [p00, p01, p10, p11] in &stitching_faces[iter] {
            let p10 = p10 + offset;
            let p11 = p11 + offset;

            new_faces.push(vec![*p00, *p01, p10]);
            new_faces.push(vec![*p01, p11, p10]);
        }
        accumulator += face_cutoff;
        iter += 1;
    }

    vs.extend(new_points);
    fs.extend(new_faces);
    *cylinders = HMesh::new(vs, (), (), fs);

    debug_assert!(verify_topology(cylinders).is_none());

    // ObjData::export(&new_points, "new_points.obj");
    ObjData::export(cylinders, "dbg.obj");

    // ObjData::export(&(&new_points, &new_faces), "dbg_plate.obj");
}

fn close_gaps(source: GeomId<0>, mesh: &mut HMesh, threshold: f32) -> GeomId<1> {
    let h = mesh.vert_handle(source).get_boundary_hedge().unwrap();
    let mut actives = [h.prev().source().id(), h.dest().id()];

    let mut first = true;
    loop {
        let v1 = mesh.vert_handle(actives[0]);
        let v2 = mesh.vert_handle(actives[1]);

        let p1 = v1.data().clone();
        let p2 = v2.data().clone();

        let d = (p2 - p1).norm();
        if d < threshold {
            if first {
                let h1 = v1.get_boundary_hedge().unwrap();

                mesh.add_face(&[v1.id(), h1.dest().id(), v2.id()], ());
                first = false;
            } else {
                let h1 = v1.get_boundary_hedge().unwrap();
                let h2 = h1.next();
                let h3 = h2.next();
                let h4 = h3.next();

                let p00 = h1.source().id();
                let p10 = h2.source().id();
                let p11 = h3.source().id();
                let p01 = h4.source().id();

                mesh.add_face(&[p00, p10, p11], ());
                mesh.add_face(&[p00, p11, p01], ());
            }

            actives[0] = v1.get_boundary_hedge().unwrap().prev().source().id();
            actives[1] = v2.get_boundary_hedge().unwrap().next().dest().id();
        } else {
            break;
        }
    }

    return mesh
        .vert_handle(actives[0])
        .get_boundary_hedge()
        .unwrap()
        .next()
        .id();
}

fn get_disjoint_entries<T>(arr: &mut [T], index1: usize, index2: usize) -> [&mut T; 2] {
    assert!(index1 != index2);
    assert!(index1 < arr.len());
    assert!(index2 < arr.len());

    unsafe {
        let ptr = arr.as_mut_ptr();
        // Get raw pointers to the elements
        let ptr1 = ptr.add(index1);
        let ptr2 = ptr.add(index2);

        // Create mutable references from raw pointers
        let first = &mut *ptr1;
        let second = &mut *ptr2;

        [first, second]
    }
}

fn find_closest_segments(
    b1: &HEdgeHandle<Vec<Vec3>, (), ()>,
    b2: &HEdgeHandle<Vec<Vec3>, (), ()>,
) -> [GeomId<1>; 2] {
    debug_assert!(b1.is_boundary_hedge() && b2.is_boundary_hedge());

    let mut best_pair = [GeomId::<1>(u64::MAX), GeomId::<1>(u64::MAX)];
    let mut best_distance = f32::MAX;
    for e1 in b1.iter_loop() {
        let p1 = e1.source().data();
        let p2 = e1.dest().data();
        for e2 in b2.iter_loop() {
            let q1 = e2.source().data();
            let q2 = e2.dest().data();

            let d = segment_segment_distance(&p1, &p2, &q1, &q2);

            if d < best_distance {
                best_distance = d;
                best_pair = [e1.id(), e2.id()];
            }
        }
    }

    best_pair
}

fn equalize_discretizations(
    l1: &mut Vec<HEdgeHandle<Vec<Vec3>, (), ()>>,
    c1: &HMesh,
    l2: &mut Vec<HEdgeHandle<Vec<Vec3>, (), ()>>,
    c2: &HMesh,
) {
    let (large, _m1, small, m2) = if l1.len() >= l2.len() {
        (l1, c1, l2, c2)
    } else {
        (l2, c2, l1, c1)
    };

    let s_start = small[0].source().id();
    let n = small.len();
    use containers::PQueue;
    let mut queue = PQueue::new();
    for hedge in small.iter() {
        let w = (hedge.source().data().clone() - hedge.dest().data().clone()).norm();
        queue.push(hedge.id(), w);
    }

    let mut created = 0;
    while created + n < large.len() {
        let (hedge, _c) = queue.pop().unwrap();

        let hedge = m2.hedge_handle(hedge).pair();

        let (_, _, [n1, n2]) = hedge.split_boundary::<f32>();

        let h1 = m2.hedge_handle(n1);
        let h2 = m2.hedge_handle(n2);
        let w1 = (h1.source().data().clone() - h1.dest().data().clone()).norm();
        let w2 = (h2.source().data().clone() - h2.dest().data().clone()).norm();

        queue.push(h1.pair().id(), w1);
        queue.push(h2.pair().id(), w2);

        created += 1;
    }

    *small = m2
        .vert_handle(s_start)
        .get_boundary_hedge()
        .unwrap()
        .iter_loop()
        .take(large.len())
        .collect();
}

fn match_subdivision_level(
    boundary: &mut Vec<HEdgeHandle<Vec<Vec3>, (), ()>>,
    mesh: &HMesh,
    target: usize,
) {
    let s_start = boundary[0].source().id();
    let n = boundary.len();
    use containers::PQueue;
    let mut queue = PQueue::new();
    for hedge in boundary.iter() {
        let w = (hedge.source().data().clone() - hedge.dest().data().clone()).norm();
        queue.push(hedge.id(), w);
    }

    let mut created = 0;
    while created + n < target {
        let (hedge, _c) = queue.pop().unwrap();

        let hedge = mesh
            .hedge_handle(hedge)
            .get_boundary_hedge()
            .unwrap()
            .pair();

        let (_, _, [n1, n2]) = hedge.split_boundary::<f32>();

        let h1 = mesh.hedge_handle(n1);
        let h2 = mesh.hedge_handle(n2);
        let w1 = (h1.source().data().clone() - h1.dest().data().clone()).norm();
        let w2 = (h2.source().data().clone() - h2.dest().data().clone()).norm();

        queue.push(h1.pair().id(), w1);
        queue.push(h2.pair().id(), w2);

        created += 1;
    }

    *boundary = mesh
        .vert_handle(s_start)
        .get_boundary_hedge()
        .unwrap()
        .iter_loop()
        .collect();
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct Mvp {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn main() {
    let mut io_context = IoContext::new("Joint Parametrization", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let phong_shader = render_context.add_shader(vec![
        "examples/11_joint_parametrization/scalar_shader/".to_string(),
    ]);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let mut gui = Some(DebugGui::init(
        &mut render_context,
        io_context.window().get_window_size(),
    ));

    use std::time::Instant;

    let dirs = [
        Vec3::new(1., 0., 0.),
        Vec3::new(0., 1., 0.),
        Vec3::new(0., 0., 1.),
        Vec3::new(-1. / 3_f32.sqrt(), -1. / 3_f32.sqrt(), -1. / 3_f32.sqrt()),
    ];

    let mut verts = Vec::new();
    let mut faces = Vec::new();
    let mut boundary_point_ids = Vec::<usize>::new();
    for dir in &dirs {
        let (vs, fs) = line_cylinder(&(dir * 1.5), &(dir * 10.0));
        let n = verts.len();
        verts.extend(vs.clone());
        faces.extend(fs.iter().map(|i| *i + n));

        boundary_point_ids.push(n);
    }

    let fs: Vec<_> = faces.chunks(3).map(|c| c.to_vec()).collect();
    let mut cylinders = HalfMesh::new(verts, (), (), fs);

    let vec1 = RowDVector::from_row_slice(&[dirs[0][0], dirs[0][1], dirs[0][2]]);
    let vec2 = RowDVector::from_row_slice(&[dirs[1][0], dirs[1][1], dirs[1][2]]);
    let vec3 = RowDVector::from_row_slice(&[dirs[2][0], dirs[2][1], dirs[2][2]]);
    let vec4 = RowDVector::from_row_slice(&[dirs[3][0], dirs[3][1], dirs[3][2]]);

    let mut mat = DMatrix::from_rows(&[vec1, vec2, vec3, vec4]);
    let (vs, fs) = polytope_mesh_from_directions(&mut mat, |_| 1.0, false);
    let polytope = HalfMesh::new(vs.clone(), (), (), fs.clone());

    ObjData::export(&polytope, "polytope.obj");

    let res = 20;
    let joint = Vec3::default();
    connect_cylinders(
        &mut cylinders,
        &joint,
        &boundary_point_ids,
        &polytope,
        |cylinders, _u, v, target1, target2| {
            let vhandle1 = cylinders.vert_handle(target1);
            let vhandle2 = cylinders.vert_handle(target2);
            let v1 = vhandle1.data().clone();
            let v2 = vhandle2.data().clone();

            use hedge::vert_handle::VertMetrics;
            use linear_isomorphic::InnerSpace;
            let h1 = vhandle1.get_boundary_hedge().unwrap();
            let d1 = (h1.dest().data().clone() - h1.prev().source().data().clone())
                .normalized();
            let n1 = vhandle1.normal();
            let t1 = n1.cross(&d1);

            let h2 = vhandle2.get_boundary_hedge().unwrap();
            let d2 = (h2.dest().data().clone() - h2.prev().source().data().clone())
                .normalized();
            let n2 = vhandle2.normal();
            let t2 = -n2.cross(&d2);

            let t = v as f32 / res as f32 + 0.5 / res as f32;

            // let blended = geometry::interpolation::lerp(&v1, &v2, t);
            let blended = geometry::interpolation::cubic_hermite(&v1, &t1, &v2, &t2, t);

            blended
        },
        |cylinders: &HMesh, _u, v, target1, centroid| {
            let v1 = cylinders.vert_handle(target1).data().clone();
            let v2 = centroid;

            let t = v as f32 / res as f32 + 0.5 / res as f32;
            (1. - t) * v1 + t * v2
        },
        res,
    );

    let vs = cylinders.get_verts();
    let fs: Vec<_> = cylinders.get_topology().into_iter().flatten().collect();
    let gpu_mesh = GraphicsInput::new()
        .add_attribute_buffer_from_slice(&vs, &mut render_context)
        .set_index_buffer_from_slice(&fs, &mut render_context);

    let mut render_request = RenderRequest::new(phong_shader)
        .vertex_input(gpu_mesh.clone())
        .add_ubo(0, Mat4::default())
        .build();

    let mut now = Instant::now();
    while io_context.poll_io(&mut gui) {
        render_context.start_frame(&io_context);
        let mvp = Mvp {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };
        let elapsed_time = now.elapsed().as_millis() as f32;
        now = Instant::now();

        render_request.set_ubo(0, mvp);
        render_context.draw(&render_request);

        gui.as_mut().unwrap().draw(&mut render_context, &mut |ctx| {
            dem_gui::Window::new("Program info:").show(ctx, |ui| {
                ui.label(format!(
                    "{:.0}ms ({:.0} FPS)",
                    elapsed_time,
                    1000.0 / elapsed_time
                ));
            });
        });
    }

    render_context.free_input(render_request.vertex_input);
}
