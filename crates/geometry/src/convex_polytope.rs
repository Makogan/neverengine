use crate::na::Vector3;
use algebra::{na::DMatrix, *};
use core::ops::Mul;
use faer::Entity;
use itertools::Itertools;
use num::traits::float;

use crate::vertex_enumeration_from_half_spaces;

// Potential improvements.
// https://dl.acm.org/doi/10.5555/3381089.3381142
// https://brsr.github.io/2021/05/02/spherical-triangle-centers.html
// https://mathweb.ucsd.edu/~sbuss/ResearchWeb/spheremean/paper.pdf
// https://www.redblobgames.com/x/1842-delaunay-voronoi-sphere/
pub fn close_polytope<T>(normals: &DMatrix<T>) -> DMatrix<T>
where
    T: Copy
        + Clone
        + Scalar
        + linear_isomorphic::RealField
        + simba::scalar::SubsetOf<f64>
        + simba::scalar::SubsetOf<f32>
        + algebra::na::SimdComplexField<SimdRealField = T>,
    f32: simba::scalar::SubsetOf<T>,
{
    debug_assert!(normals.nrows() > 0);

    let mut dirs = normals.clone();
    let mut worst_score = T::from(10.0).unwrap();

    loop {
        let row_count_before = dirs.nrows();
        for row in 0..dirs.nrows() {
            // Extremely convoluted because nalgebra is awful. All this code does is:
            // find -d, find two vectors orthogonal to v, find the closest point
            // according to the euclidean metric among the pre existing points.
            // If the closest point is further than a threshold, add the candidate.
            type RowVec<T> = na::Matrix<
                T,
                na::Const<1>,
                na::Dyn,
                na::VecStorage<T, na::Const<1>, na::Dyn>,
            >;
            let opposite = -dirs.row(row);
            let ortho1 = orthogonal_vector(&Vec3::new(
                opposite[0].to_f32().unwrap(),
                opposite[1].to_f32().unwrap(),
                opposite[2].to_f32().unwrap(),
            ));

            let ortho1 =
                RowVec::from_iterator(3, ortho1.iter().map(|v| T::from(*v).unwrap()))
                    .normalize();
            let ortho2 = opposite.cross(&ortho1).normalize();
            let candidates = [opposite, ortho1.clone(), ortho2.clone(), -ortho1, -ortho2];

            for candidate in candidates {
                let closest_angle = |candidate: &RowVec<T>| {
                    (0..dirs.nrows())
                        .map(|i| dirs.row(i))
                        .max_by(|x, y| {
                            candidate.dot(&x).partial_cmp(&candidate.dot(&y)).unwrap()
                        })
                        .map(|x| candidate.dot(&x))
                        .unwrap()
                };

                let score = closest_angle(&candidate);
                // This indicates the closest angle is very wide.
                if score <= T::from(f64::cos(f64::to_radians(30.0))).unwrap() {
                    debug_assert!(
                        (candidate.norm() - T::from(1.0).unwrap()).abs()
                            <= convert(0.001)
                    );
                    dirs = dirs.clone().insert_row(dirs.nrows(), convert(0.0));
                    dirs.row_mut(dirs.nrows() - 1).copy_from(&candidate);
                }
                worst_score = worst_score.min(score);
            }
        }

        if worst_score >= convert(0.9)
            || dirs.nrows() >= 30
            || row_count_before == dirs.nrows()
        {
            break;
        }
    }

    // If we end up here, add 2 extra directions to make sure the polytope closes.
    if dirs.nrows() == 3 {
        let v1 = dirs.row(0);
        let v2 = dirs.row(1);
        let v3 = dirs.row(2);

        let d1 = v2 - v1;
        let d2 = v3 - v1;

        let n1 = d1.cross(&d2).normalize();
        let n2 = -n1.clone();

        dirs = dirs.clone().insert_row(dirs.nrows(), convert(0.0));
        dirs.row_mut(3).copy_from(&n1);
        dirs = dirs.clone().insert_row(dirs.nrows(), convert(0.0));
        dirs.row_mut(4).copy_from(&n2);
    };

    debug_assert!(dirs.nrows() >= 4);

    dirs
}

pub fn close_polytope_by_discrepancy<T>(normals: &mut DMatrix<T>)
where
    T: Copy
        + Clone
        + Scalar
        + linear_isomorphic::RealField
        + simba::scalar::SubsetOf<f64>
        + simba::scalar::SubsetOf<f32>
        + algebra::na::SimdComplexField<SimdRealField = T>
        + Mul<Vector3<T>, Output = Vector3<T>>
        + Entity
        + float::TotalOrder,
    f32: simba::scalar::SubsetOf<T>,
{
    debug_assert!(normals.ncols() == 3);
    let mut discrepant = spherical_discrepancy(normals);

    let best_dot = |v: &Vector3<T>, normals: &DMatrix<T>| {
        let mut dot = T::from(10000.0).unwrap();
        for row in normals.row_iter() {
            let inner = (row * v)[(0, 0)];
            dot = dot.min(inner);
        }

        dot
    };

    use core::f64::consts::PI;
    while best_dot(&discrepant, &normals) < T::from((PI / 2.).cos()).unwrap()
        || normals.nrows() < 4
    {
        println!("{}", best_dot(&discrepant, &normals));
        println!("{}", normals);
        let n = normals.nrows();
        *normals = normals.clone().insert_row(n, T::from(0.0).unwrap());
        normals.row_mut(n).tr_copy_from(&discrepant);

        discrepant = spherical_discrepancy(&normals);
    }

    debug_assert!(normals.nrows() >= 4);
}

/// Do not use, incomplete
pub fn angular_convex_hull<T>(vectors: &Vec<Vector3<T>>)
where
    T: Copy
        + Clone
        + Scalar
        + linear_isomorphic::RealField
        + simba::scalar::SubsetOf<f64>
        + simba::scalar::SubsetOf<f32>
        + algebra::na::SimdComplexField<SimdRealField = T>
        + Mul<Vector3<T>, Output = Vector3<T>>
        + Entity
        + float::TotalOrder,
    f32: simba::scalar::SubsetOf<T>,
{
    let mut vectors = vectors.clone();
    let mut hull = vec![vectors.pop().unwrap()];

    let mut selected_index = usize::MAX;
    let mut best_dot = T::from(10.0).unwrap();
    for (i, position) in vectors.iter().enumerate() {
        let dot = hull[0].dot(&position);
        if dot < best_dot {
            best_dot = dot;
            selected_index = i;
        }
    }

    hull.push(vectors.remove(selected_index));

    let normal = hull[0].cross(&hull[1]).normalize();

    let plane_angle = |d: &Vector3<T>, n: &Vector3<T>| {
        let dot = d.dot(&n);

        use num::traits::FloatConst;
        T::from(f64::PI()).unwrap() - dot.acos()
    };

    let mut best_angle = T::from(-10000).unwrap();
    let mut selected_index = usize::MAX;

    for (i, vector) in vectors.iter().enumerate() {
        let angle = plane_angle(vector, &normal);
        if angle > best_angle {
            selected_index = i;
            best_angle = angle;
        }
    }

    hull.push(vectors.remove(selected_index));

    // Ensure that the orientation of the planes points outwards.
    let dir = (hull[0] + hull[1] + hull[2]).normalize();
    if dir.dot(&normal) > T::from(0).unwrap() {
        hull.swap(0, 1);
    }

    while !vectors.is_empty() {
        let candidate = vectors.pop().unwrap();

        let n = hull.len();
        for i in 0..hull.len() {
            let normal = hull[i].cross(&hull[(i + 1) % n]).normalize();
            if candidate.dot(&normal) >= T::from(0).unwrap() {
                /* add candidate to set */
                todo!();
            }
        }
    }
}

// https://newtraell.cs.uchicago.edu/files/ms_paper/csj.pdf
pub fn spherical_discrepancy<T>(vectors: &DMatrix<T>) -> na::Vector3<T>
where
    T: Copy
        + Clone
        + Scalar
        + linear_isomorphic::RealField
        + simba::scalar::SubsetOf<f64>
        + simba::scalar::SubsetOf<f32>
        + algebra::na::SimdComplexField<SimdRealField = T>
        + Mul<Vector3<T>, Output = Vector3<T>>
        + Entity
        + float::TotalOrder,
    f32: simba::scalar::SubsetOf<T>,
{
    let m = vectors.nrows();
    if m == 1 {
        return -vectors.row(0).fixed_columns::<3>(0).transpose();
    }

    if m == 2 {
        return -(vectors.row(0).fixed_columns::<3>(0) + vectors.fixed_columns::<3>(1))
            .normalize()
            .transpose();
    }

    let n = 3;
    let lambda = T::from((m.max(4) as f32 / n as f32).ln().sqrt()).unwrap();
    let delta = T::from(1. / (n * n * n) as f32).unwrap();
    let t_iters = T::from(2. * (n as f32 - 5.).max(2.)).unwrap() / delta * delta;

    let num1 = delta * delta * lambda * lambda; // detla^2 * lambda ^2
    let denom1 = 2. * (n as f64 - 5.);
    let fact1 = -num1 / T::from(denom1).unwrap();
    let fact2 = T::from(1.).unwrap() + lambda * delta * T::from(n).unwrap(); // 1 + lambda * delta * n
    let prod = fact1 * fact2;
    let rho = prod.exp();

    use crate::na::Vector3;
    let mut x = Vector3::<T>::zeros();
    let mut w = vec![(-lambda * lambda).exp(); m];
    let two = T::from(2.).unwrap();
    for _ in 0..t_iters.to_f64().unwrap() as usize {
        let i = (0..m).filter(|k| w[*k] >= two).collect_vec();
        // let i_not = (0..m).filter(|k| w[*k] < two).collect_vec();

        /* in the actual paper there is a matrix to compute here, not needed for n=3 */
        let mut p = vec![x];
        let mut y = vectors.row(0) * T::from(0.0).unwrap();
        for i in 0..m {
            y = y + vectors.row(i) * w[i];
        }
        p.push(Vector3::new(y[0], y[1], y[2]));

        for k in i {
            p.push(
                vectors
                    .row(k)
                    .fixed_columns::<3>(0)
                    .transpose()
                    .into_owned(),
            );
        }

        // There is technically a next part to add to p in the paper, but it's not needed for dimension 3.

        let ortho_complement = || {
            use crate::sparse_vector::gram_schmidt;
            let c = gram_schmidt(p.iter());

            if c.len() < 2 {
                return orthogonal_vector(&c[0]);
            }
            c[0].cross(&c[1]).normalize()
        };

        let y = ortho_complement();

        let y_t = Vector3::new(y.x, y.y, y.z);
        x = x + y_t * delta;

        for i in 0..m {
            let u = vectors.row(i);
            let u = Vector3::new(u[0], u[1], u[2]);
            w[i] = w[i] * (lambda * delta * u.dot(&y_t)).exp() * rho;
        }
    }

    debug_assert!(x.x.is_finite() && x.y.is_finite() && x.z.is_finite());

    x.normalize()
}

/// Produces a polytope mesh from a set of directions. The output faces
/// will be in the same order as the input directions + potentially additional
/// faces needed to close the polytope.
pub fn polytope_mesh_from_directions<D>(
    planes: &mut DMatrix<f32>,
    distance: D,
    close: bool,
) -> (Vec<Vec3>, Vec<Vec<usize>>)
where
    D: Fn(&Vec3) -> f32,
{
    debug_assert!(planes.ncols() == 3);

    let closed_polytope = if close {
        // close_polytope(planes)
        close_polytope_by_discrepancy(planes);
        planes.clone()
    } else {
        planes.clone()
    };

    let mut d_planes =
        DMatrix::<f64>::zeros(closed_polytope.nrows(), closed_polytope.ncols());
    for (r, _plane) in closed_polytope.row_iter().enumerate() {
        d_planes[(r, 0)] = closed_polytope[(r, 0)] as f64;
        d_planes[(r, 1)] = closed_polytope[(r, 1)] as f64;
        d_planes[(r, 2)] = closed_polytope[(r, 2)] as f64;
    }
    let closed_polytope = d_planes;

    let plane_matrix = DMatrix::from_fn(
        closed_polytope.nrows(),
        closed_polytope.ncols() + 1,
        |r, c| {
            if c < closed_polytope.ncols() {
                closed_polytope[(r, c)]
            } else {
                distance(&Vec3::new(
                    closed_polytope[(r, 0)] as f32,
                    closed_polytope[(r, 1)] as f32,
                    closed_polytope[(r, 2)] as f32,
                )) as f64
            }
        },
    );

    let (verts, indices) = vertex_enumeration_from_half_spaces(&plane_matrix);
    let verts = verts
        .iter()
        .map(|v| Vec3::new(v[0] as f32, v[1] as f32, v[2] as f32))
        .collect_vec();

    (verts, indices)
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests {
    use algebra::*;
    use approx::*;
    use chicken_wire::wavefront_loader::ObjData;

    use crate::convex_polytope::DMatrix;
    use crate::{close_polytope, polytope_mesh_from_directions};

    #[test]
    fn test_polytope_mesh_from_directions() {
        // Forms this: https://www.shadertoy.com/view/Ds3Bzj
        #[rustfmt::skip]
        let mut implicit_shape = na::DMatrix::from_vec(
            3,
            3,
            vec![
                0., -0.8479983, 0.52999896,
                0., -0.8479983, -0.52999896,
                0., -1., 0.,
            ],
        )
        .transpose();

        let center = Vec3::new(0.000000, 0.800000, 0.000000);
        let distance = |normal: &Vec3| (center + normal * 0.8).dot(&normal);
        let (verts, topology) =
            polytope_mesh_from_directions(&mut implicit_shape, distance, true);

        ObjData::export(
            &(&verts, &topology),
            "tmp/test_polytope_mesh_from_directions.obj",
        );
    }

    #[test]
    fn test_close_polytope() {
        // T1 ===
        let normals = vec![Vec3::new(1.0, 0.0, 0.0)];
        let matrix = na::DMatrix::from_fn(normals.len(), 3, |r, c| normals[r][c]);
        let mut sol = close_polytope(&matrix);

        let (vs, fs) = polytope_mesh_from_directions(&mut sol, |_| 1.0, true);
        ObjData::export(&(&vs, &fs), "tmp/test_close_polytope_1.obj");

        let ObjData {
            mut vertices,
            mut vertex_face_indices,
            ..
        } = ObjData::from_disk_file(
            &"test_data/close_polytope_tests/test_close_polytope_1.obj",
        );
        let epsilon = 0.01;
        for (vec3_1, vec3_2) in vs.iter().zip(vertices.iter()) {
            assert_relative_eq!(*vec3_1, *vec3_2, epsilon = epsilon);
        }

        assert_eq!(vertex_face_indices.len(), fs.len());
        for (f1, f2) in fs.iter().zip(vertex_face_indices.iter()) {
            assert_eq!(f1.len(), f2.len());
            for (i, j) in f1.iter().zip(f2.iter()) {
                assert_eq!(*i, *j as usize);
            }
        }

        // T2 ===
        let normals = vec![
            Vec3::new(1.0, 0.0, 0.0), //
            Vec3::new(0.0, 1.0, 0.0), //
        ];
        let matrix = na::DMatrix::from_fn(normals.len(), 3, |r, c| normals[r][c]);
        let mut sol = close_polytope(&matrix);

        let (vs, fs) = polytope_mesh_from_directions(&mut sol, |_| 1.0, true);
        ObjData::export(&(&vs, &fs), "tmp/test_close_polytope_2.obj");

        ObjData {
            vertices,
            vertex_face_indices,
            ..
        } = ObjData::from_disk_file(
            &"test_data/close_polytope_tests/test_close_polytope_2.obj",
        );

        let epsilon = 0.01;
        for (vec3_1, vec3_2) in vs.iter().zip(vertices.iter()) {
            assert_relative_eq!(*vec3_1, *vec3_2, epsilon = epsilon);
        }

        assert_eq!(vertex_face_indices.len(), fs.len());
        for (f1, f2) in fs.iter().zip(vertex_face_indices.iter()) {
            assert_eq!(f1.len(), f2.len());
            for (i, j) in f1.iter().zip(f2.iter()) {
                assert_eq!(*i, *j as usize);
            }
        }

        // T3 ===
        let normals = vec![
            Vec3::new(1.0, 0.0, 0.0),             //
            Vec3::new(1.0, 1.0, 0.0).normalize(), //
        ];
        let matrix = DMatrix::from_fn(normals.len(), 3, |r, c| normals[r][c]);
        let mut sol = close_polytope(&matrix);

        let (vs, fs) = polytope_mesh_from_directions(&mut sol, |_| 1.0, true);
        ObjData::export(&(&vs, &fs), "tmp/test_close_polytope_3.obj");

        let ObjData {
            vertices,
            vertex_face_indices,
            ..
        } = ObjData::from_disk_file(
            &"test_data/close_polytope_tests/test_close_polytope_3.obj",
        );

        let epsilon = 0.01;
        for (vec3_1, vec3_2) in vs.iter().zip(vertices.iter()) {
            assert_relative_eq!(*vec3_1, *vec3_2, epsilon = epsilon);
        }

        assert_eq!(vertex_face_indices.len(), fs.len());
        for (f1, f2) in fs.iter().zip(vertex_face_indices.iter()) {
            assert_eq!(f1.len(), f2.len());
            for (i, j) in f1.iter().zip(f2.iter()) {
                assert_eq!(*i, *j as usize);
            }
        }
    }
}
