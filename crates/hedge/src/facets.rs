use std::{
    borrow::BorrowMut,
    cell::RefMut,
    fmt::{Debug, Display},
};

#[allow(non_snake_case)]
pub const fn ABSENT<const DIM: usize>() -> GeomId<DIM> {
    GeomId::<DIM>(u64::MAX)
}

/// Typed id for the facets of a mesh. `DIM = 0` would be a vertex (i.e. a
/// 0-facet) `DIM = 1` would be an edge (a 1-facet) and `DIM=2` would be a
/// polygonal face (a 2 facet).
/// This is to avoid accidentally using a vertex id to fetch an edge or similar
/// issues.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub struct GeomId<const DIM: usize>(pub u64);

impl GeomId<1> {
    pub fn pair(&self) -> GeomId<1> {
        // Closest even.
        let n = self.0 - (self.0 & 1);
        GeomId::from(n + ((self.0 + 1) % 2))
    }

    /// Useful for methods that need a unique id for the edge.
    pub fn select_even(&self) -> GeomId<1> {
        // Closest even.
        let n = self.0 - (self.0 & 1);
        GeomId::from(n)
    }
}

impl<const DIM: usize> Debug for GeomId<DIM> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if *self != ABSENT() {
            write!(f, "{}", self.0)
        } else {
            write!(f, "ABSENT")
        }
    }
}

impl<const DIM: usize> Display for GeomId<DIM> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if *self != ABSENT() {
            write!(f, "{}", self.0)
        } else {
            write!(f, "ABSENT()")
        }
    }
}

impl<const DIM: usize> From<GeomId<DIM>> for usize {
    fn from(val: GeomId<DIM>) -> Self {
        val.0 as usize
    }
}

impl<const DIM: usize> From<usize> for GeomId<DIM> {
    fn from(val: usize) -> Self {
        Self(val as u64)
    }
}

impl<const DIM: usize> From<u64> for GeomId<DIM> {
    fn from(val: u64) -> Self {
        Self(val)
    }
}

impl<const DIM: usize> From<u32> for GeomId<DIM> {
    fn from(val: u32) -> Self {
        Self(val as u64)
    }
}

#[derive(Debug, Clone)]
pub struct Vert {
    pub(crate) enabled: bool,
    pub(crate) id: GeomId<0>,

    pub(crate) hedge_id: GeomId<1>,
}

#[derive(Debug, Clone)]
pub struct HEdge {
    pub(crate) enabled: bool,
    pub(crate) id: GeomId<1>,

    pub(crate) next_id: GeomId<1>,
    pub(crate) prev_id: GeomId<1>,
    pub(crate) pair_id: GeomId<1>,

    pub(crate) vert_id: GeomId<0>,

    pub(crate) face_id: GeomId<2>,
}

impl HEdge {
    pub(crate) fn new(self_id: u64, pair_id: u64, vert_id: u64) -> Self {
        debug_assert!((self_id as i64 - pair_id as i64).abs() == 1);
        return Self {
            enabled: true,
            id: GeomId(self_id),
            next_id: ABSENT(),
            prev_id: ABSENT(),
            pair_id: GeomId(pair_id),
            vert_id: GeomId(vert_id),
            face_id: ABSENT(),
        };
    }

    pub fn id(&self) -> GeomId<1> {
        return self.id;
    }
}

#[derive(Debug, Clone)]
pub struct Face {
    pub(crate) enabled: bool,
    pub(crate) id: GeomId<2>,

    pub(crate) hedge_id: GeomId<1>,
}

// +| Internal |+ ==============================================================
pub(crate) fn attach_face(
    face: &mut Face,
    array: &mut RefMut<Vec<HEdge>>,
    e1_id: GeomId<1>,
    e2_id: GeomId<1>,
    e3_id: GeomId<1>,
) {
    face.hedge_id = e1_id;

    array.borrow_mut()[e1_id.0 as usize].next_id = e2_id;
    array.borrow_mut()[e2_id.0 as usize].prev_id = e1_id;

    array.borrow_mut()[e2_id.0 as usize].next_id = e3_id;
    array.borrow_mut()[e3_id.0 as usize].prev_id = e2_id;

    array.borrow_mut()[e3_id.0 as usize].next_id = e1_id;
    array.borrow_mut()[e1_id.0 as usize].prev_id = e3_id;

    array.borrow_mut()[e1_id.0 as usize].face_id = face.id;
    array.borrow_mut()[e2_id.0 as usize].face_id = face.id;
    array.borrow_mut()[e3_id.0 as usize].face_id = face.id;
}
