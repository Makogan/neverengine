/*
#ifdef never
[[ne::topology(triangle list)]]
[[ne::line_width(1)]]
[[ne::polygon_mode(line)]]
[[ne::cull_mode(back)]]
#endif
*/

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use spirv_std::{glam::*, spirv};

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(output: &mut Vec4) { *output = Vec4::new(1., 1., 1., 1.); }

// Vertex shader
pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] camera_ubo: &CamUBO,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
)
{
    *screen_pos =
        camera_ubo.proj * camera_ubo.view * camera_ubo.model * in_position.extend(1.0);
}
