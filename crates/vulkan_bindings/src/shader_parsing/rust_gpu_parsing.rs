use std::path::PathBuf;
use std::process::Command;

use log::error;

pub fn compile_shader(path: &String) -> Vec<Vec<u8>>
{
    let spirv_data = rust_to_spirv(&(path.clone()).into());

    spirv_data
        .iter()
        .map(|buffer| buffer.as_slice().to_vec())
        .collect()
}

pub fn rust_to_spirv(path: &String) -> Vec<Vec<u8>>
{
    // Parse shader crate.
    let paths = compile_shader_crate(path);

    paths.iter().map(|p| read_binary_file(p)).collect()
}

use pathdiff::diff_paths;

pub fn compile_shader_crate(path: &String) -> Vec<PathBuf>
{
    let rust_builder_path = PathBuf::from(
        std::env::var("DEM_SPIRV_BUILDER")
            .expect("DEM_SPIRV_BUILDER env var has not been set."),
    );
    let cwd = PathBuf::from(std::env::current_dir().unwrap());

    let relative_rust_builder_path = diff_paths(&rust_builder_path, &cwd).unwrap();
    let absolute_path = PathBuf::from(path).canonicalize().unwrap();
    let relative_shader_path = diff_paths(&absolute_path, &cwd).unwrap();

    let output = Command::new("rustup")
        .arg("run")
        .arg("nightly-2024-01-08")
        .arg(relative_rust_builder_path)
        .arg("-i")
        .arg(relative_shader_path)
        .current_dir(cwd)
        .output()
        .unwrap();

    if !output.status.success()
    {
        let stdout = String::from_utf8(output.stdout).unwrap();
        let stderr = String::from_utf8(output.stderr).unwrap();
        error!("cargo:warning= {}", stdout);
        error!("cargo:warning= {}", stderr);
        panic!();
    }

    let stdout = String::from_utf8(output.stdout).unwrap();
    let mut paths: Vec<&str> = stdout.split("\n").collect();
    if *paths.last().unwrap() == ""
    {
        paths.pop();
    }

    return paths.iter().map(|s| PathBuf::from(s)).collect();
}
fn read_binary_file(filename: &PathBuf) -> Vec<u8>
{
    use std::fs;
    fs::read(filename)
        .expect(format!("File {} not found.", filename.display()).as_str())
        .into_boxed_slice()
        .into()
}

fn highlight(string: &String) -> String
{
    use synoptic::{Highlighter, TokOpt};
    let mut h = Highlighter::new(4);

    h.keyword("error", r"error\[.*\]");
    h.keyword("indicator_help", r"[^\|]*help:");
    h.keyword("indicator_err", r"[^\|]*error:");
    h.keyword("indicator_warn", r"[^\|]*warning:");
    h.keyword("indicator_raw", r"\^+");
    h.keyword("arrow", r"-->");
    h.keyword("num_bar", r"[0-9]+\s\|");
    h.keyword("bar", r"\s*\|");

    let message = string.split('\n').map(|line| line.to_string()).collect();
    h.run(&message);

    let mut result = "".to_string();
    let mut last_token_color = Fg::White;
    for (line_number, line) in message.iter().enumerate()
    {
        // Line returns tokens for the corresponding line
        for token in h.line(line_number, &line)
        {
            // Tokens can either require highlighting or not require highlighting
            match token
            {
                // This is some text that needs to be highlighted
                TokOpt::Some(text, kind) =>
                {
                    let colour = colour(&kind, &mut last_token_color);
                    result.push_str(&format!(
                        "{}{}{}{}{}",
                        colour,
                        bold(&kind),
                        text,
                        Fg::Reset,
                        lliw::BOLD_RESET,
                    ));
                }
                // This is just normal text with no highlighting
                TokOpt::None(text) => result.push_str(&text),
            }
        }
        // Insert a newline at the end of every line
        result.push_str("\n");
    }

    result
}

use lliw::Fg;
fn colour(name: &str, last_colour: &mut Fg) -> Fg
{
    // This function will take in the function name
    // And it will output the correct foreground colour
    match name
    {
        "error" =>
        {
            *last_colour = Fg::Red;
            *last_colour
        }
        "indicator_help" =>
        {
            *last_colour = Fg::Yellow;
            *last_colour
        }
        "indicator_err" =>
        {
            *last_colour = Fg::Red;
            *last_colour
        }
        "indicator_warn" =>
        {
            *last_colour = Fg::Yellow;
            *last_colour
        }
        "indicator_raw" => *last_colour,
        "arrow" => Fg::Blue,
        "num_bar" => Fg::Blue,
        "bar" => Fg::Blue,
        _ => panic!("unknown token name"),
    }
}

fn bold(name: &str) -> &str
{
    match name
    {
        "error" => lliw::BOLD,
        "indicator_help" => lliw::BOLD,
        "indicator_err" => lliw::BOLD,
        "indicator_warn" => lliw::BOLD,
        "indicator_raw" => lliw::BOLD,
        "arrow" => "",
        "num_bar" => "",
        "bar" => "",
        _ => panic!("unknown token name"),
    }
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_compile_shader()
    {
        compile_shader(&"./test_data/spinning_triangle_shader".to_string());
    }
}
