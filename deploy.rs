//! ```cargo
//! [dependencies]
//! walkdir = "2.3.2"
//! ```

use std::fs;
use std::path::Path;

use walkdir::WalkDir;

fn find_file(dir: &Path, target_file: &str) -> Option<String>
{
    for entry in WalkDir::new(dir).into_iter().filter_map(|e| e.ok())
    {
        if entry.file_name() == target_file
        {
            if let Some(src_path) = entry.path().to_str()
            {
                let src_file = Path::new(src_path);
                let dest_file = Path::new("./").join(src_file.file_name().unwrap());

                if let Err(err) = fs::copy(&src_file, &dest_file)
                {
                    eprintln!("Error copying file {}: {}", src_path, err);
                }
                else
                {
                    return Some(src_path.to_string());
                }
            }
        }
    }
    None
}

fn main()
{
    let dir = Path::new("./target/");
    let target_file = "librustc_codegen_spirv.so";

    match find_file(&dir, target_file)
    {
        Some(path) => println!(
            "Found rustc_codgen under {}.\nCopying to {}",
            path, target_file
        ),
        None => println!("File not found"),
    }
}
