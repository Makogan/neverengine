#import "@preview/polylux:0.2.0": *

#import themes.simple: *

#set text(font: "Inria Sans")

#show: simple-theme.with(
  footer: [Demiurge],
)

#let slide_block(body) = {
  set align(center)
  rect(
    fill: rgb("eeeeee"),
    inset: 14pt,
    radius: 8pt,
    [
      #set align(left)
      #body
    ]
  )
}

#title-slide[
  = Rust for Engine Development
  #v(2em)

  Makogan #footnote[Demiurge] #h(1em)

]

#slide[
  == Speaker background

  - 10 years of programming experience.
  - 5 in industry and academic positions.
  - Worked at startups and AAA videogame companies.
  - Have a masters on computational geometry.
  - Built multiple engines on C++.
]

#centered-slide[
  Language niceties
]

#slide[
  == C++ Ecosystem

  - Build systems / package managers.

    Meson vs Cmake.

    Conan vs vcpkg.

  - Unit testing.

    In file? Out of file? gtest? other library?

  - Documentation generation. Usually doxygen.
]

#centered-slide[
  == Rust provides native support for all of this!
]

#slide[
  == Cmake


  #set text(
    font: "Linux Libertine",
    size: 10pt,
  )

  #slide_block(
  ```cmake
  cmake_minimum_required(VERSION 3.10)

  project(CMakeExample)

  set(CMAKE_CXX_STANDARD 17)

  set(CMAKE_MODULE_PATH "${CMAKE_BINARY_DIR}" "${CMAKE_MODULE_PATH}")
  set(CMAKE_PREFIX_PATH "${CMAKE_BINARY_DIR}" "${CMAKE_PREFIX_PATH}")

  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")

  option(BUILD_TESTS "Build tests" ON)

  enable_testing()

  add_subdirectory(src)
  add_subdirectory(apps)

  if (BUILD_TESTS)
      add_subdirectory(tests)
  endif ()
  ```)
]

#slide[
  == Cargo

  #set text(
    font: "Linux Libertine",
    size: 10pt,
  )

  #slide_block(
  ```toml
    [package]
    name = "Demiurge"
    version = "0.1.0"
    edition = "2021"

    [lib]
    name = "Demiurge"
    path = "src/lib.rs"

    [profile.release-with-debug]
    inherits = "release"
    debug = true

    [dependencies]
    rand = "0.8.5"
    image = "0.24.3"

    camera = { path = "crates/camera" }
    dual_contouring = { path = "crates/dual_contouring/" }
    hedge = { path = "crates/hedge" }
    dem_core = { path = "crates/dem_core" }
    numerical_analysis = { path = "crates/numerical_analysis" }
    pga = { path = "crates/pga" }
    vulkan_bindings = { path = "crates/vulkan_bindings" }
  ```)
]

#slide[
  == Unit tests

  #set text(
    font: "Linux Libertine",
    size: 8pt,
  )

  #slide_block(
  ```rs
    #[cfg(test)]
    mod tests {
        use std::ops::Add;
        /* other global imports */

        use super::ThreadPool;
        /* other local imports */

        #[test]
        fn test_thread_pool() {
            let mut thread_pool = ThreadPool::new(4);
            let goal = Arc::new(Mutex::new(0));
            for i in 0..100 {
                let c_goal = goal.clone();
                thread_pool.enqueue_task(move |tasklist| {
                    println!(r"ran task {i}");
                    c_goal.lock().unwrap().add_assign(i);
                });
            }

            thread_pool.run();
            let result = goal.lock().unwrap().to_owned();
            assert_eq!(result, (99 * 99 + 99) / 2)
        }
    }
  ```)
]

#slide[
  == Documentation tests

  #set text(
    font: "Linux Libertine",
    size: 25pt,
  )

  #slide_block(
  ````rs
    /// ```
    /// /// Some documentation.
    /// # fn foo() {} // this function will be hidden
    /// println!("Hello, World!");
    /// ```
  ````)

  #linebreak()
  #set align(center)
  (You can also make benchmark tests)
]

#slide[
  == Syntax

  #set text(
    font: "Linux Libertine",
    size: 20pt,
  )
  #slide_block(
  ````rs
    // Vars are constant by default.
    // Types are often deduced.
    let mut var = 0;
    // `for` and `if` statements don't require parenthesis.
    for i in 0..100
    {
      var += 1;
    }

    // no need for explicit return a lot of the time.
    var
  ````)
]

#slide[
  == Iterators (1)

  #set text(
    font: "Linux Libertine",
    size: 15pt,
  )

  #slide_block(
  ```rs
  let list = /*list of vertices*/;
  let center_of_mass = list.sum() / list.len() as f32;

  let winding_number = list
    .map(|&vertex| compute_vertex_angle(vertex))
    .sum() / list.len() as f32;
  ```
  )

  #set text(
    font: "Linux Libertine",
    size: 24pt,
  )

  #linebreak()
  Simple to code but also, often leads to better memory usage by avoiding the creation of lists. Examples of complex iterations include depth first search, breadth first search, Dijkstra's shortest path, the fibonacci sequence...
]

#slide[
  == Iterators (2)

  #table(
    columns: (1fr, 1fr),
    stroke: none,
    [
    #set text(
      font: "Linux Libertine",
      size: 12pt,
    )
    #slide_block(
    ```rs
    struct Fibonacci {
      curr: u32,
      next: u32,
    }

    impl Iterator for Fibonacci {

        type Item = u32;

        fn next(&mut self) -> Option<Self::Item> {
            let current = self.curr;

            self.curr = self.next;
            self.next = current + self.next;

            Some(current)
        }
    }
    ```)
  ],
  [
    #set text(
      font: "Linux Libertine",
      size: 20pt,
    )

    #align(horizon,
    [
    - Simpler to write than std iterators.

    - Simpler to use than std iterators.

    - More versatile than std iterators.

    No more:

      `auto l = std::vector<uint>(n);`
      `std::iota(l.begin(), l.end(), 1);`

    Instead:

      `(0..n).iter().collect();`
    ])
  ])
]

#centered-slide[
  == Architecture design
]

#slide[

  == F@\$\# OOP

  #set text(
    font: "Linux Libertine",
    size: 20pt,
  )

  Yes, I hate it, you should to. Why?

  - class driven design leads to lots of indirection (have you heard of the cache?)

  - It promotes inheritance over function composition by default.

  - Incomprehensible abstract names. Ah yes, the

    `object_loader_manager_controller_observer_interface`

    so clear!

  - Neverending growing web of interdependent inheritance, implicit inherited variables, virtual tables.
]

#slide[
  == The correct approach to OOP

  #set align(center)
  #image("images/trash_oop.svg", width: 7cm)
]

#slide[

  #set align(center)

  #image("images/trash_oop_fire.svg", width: 9cm)

]

#slide[
  == Traits

  #set text(
    font: "Linux Libertine",
    size: 18pt,
  )

  They provide restrictions on generic types. Similar to abstract interfaces but
  they don't involve inheritance.

  - Types implementing the same trait have no knowledge or access to each other's members
  or methods.

  - Traits can have default implementations.

  - Traits can require other traits as dependencies.

  - It is always explicit which traits a generic function expects.

]

#slide[
  == Traits (Example)

  Assume we want the following design.

  #set align(center)
  #image("images/ral.svg", width: 10cm)

]

#slide[
  == Traits (Example)

#set text(
  font: "Linux Libertine",
  size: 10pt,
)

#slide_block(
```rs
pub trait GpuApi
{
    fn allocate_buffer(&mut self, data: *const u8, size: usize) -> BufferHandle;
    fn destroy_buffer(&mut self, buffer: BufferHandle);
    fn update_buffer(&mut self, buffer: &BufferHandle, data: *const u8, size: usize);
    fn update_buffer_from_slice<T>(&mut self, buffer: &BufferHandle, data: &[T])
    {
        self.update_buffer(
            buffer,
            data.as_ptr() as *const u8,
            data.len() * size_of::<T>(),
        );
    }

    fn draw(
        &mut self, render_request: &RenderRequest, uniform_container: &UniformBuffersData,
    );
    fn compute(
        &mut self, compute_request: &ComputeRequest, uniform_container: &UniformBuffersData,
    );
}
  ```)

]

#slide[
  == Trait driven design

 - Generic functions are type agnostic.

 - Modifying a trait cannot implicitly modify struct methods.

 - All incompatible differences are compile time.

 - No implicit indirection overhead.
]

#slide[
  == Macros

  - Extremely powerful meta programming tools (with great power comes great responsibility).

  - Makes reflection over structs straighforward to implement (ever tried on C++?).

  - Makes most traits trivial to use.
]

#slide[
  == Macros

  #set text(
    font: "Linux Libertine",
    size: 14pt,
  )

  #slide_block(
  ```rs
  /// Message object to issue a compute task to the gpu.
  #[derive(Debug, Default, Clone, Builder)]
  #[builder(|_|{})]
  pub struct ComputeRequest
  {
      pub shader: ShaderHandle,
      /// The amount of work per dimension. For example (250, 90, 1) will issue
      /// 250x90 work groups as an array spanning a grid of [0-249, 0,89].
      pub work_groups: (u32, u32, u32),
      /// Images that will be available either as samplers or image storage based
      /// on the shader declaration.
      #[optional(Vec::new())]
      pub image_inputs: Vec<GpuDataDescriptor>,
      /// (RWStructuredBuffer, StructuredBuffer, etc...) inputs.
      #[optional(Vec::new())]
      pub buffer_inputs: Vec<GpuDataDescriptor>,
  }
  ```
  )
]

#centered-slide[
  == My worst frenemy... The BC
]

#slide[
  == What does the Borrow Checker do?
  It enforces these rules at compile time (from the docs):

  - All variables are initialized before use.

  - Only one mutable reference or (exclusive) many immutable references exist at any one time.

  - Objects are only accessed while their lifetime is active.
]

#slide[
  == What does that do?

  - No use after free.

  - No data races.

  - No undefined behaviour (in theory).

  - Compile guaranteed safety.
]


#slide[
  == I hate it

  These are some nightmares I dealt with:

  - ECS.

  - Half Edge.

  - Threadpool.

  - In general if it behaves like al linked list you are about to have a miserable time.

  - Slows down iteration times (sometimes).
]

#slide[
  == Summary

  These are some nightmares I dealt with:

  - Really nice build system.

  - Best syntax of any language I have used, I love it.

  - The borrow checker giveth and taketh away.
]

#centered-slide[
  == Questions?
]