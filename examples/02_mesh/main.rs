use std::rc::Rc;
use std::{cell::RefCell, mem::size_of};

use algebra::*;
use camera::{CameraProperties, FlyCamera};
use chicken_wire::texture::Texture;
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use vulkan_bindings::RenderContext;

#[inline]
pub fn bytes_of<T>(t: &T) -> &[u8] {
    unsafe { std::slice::from_raw_parts(t as *const T as *const u8, size_of::<T>()) }
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct MVP {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn main() {
    let mut io_context = IoContext::new("Mesh", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let mesh_shader =
        render_context.add_shader(vec!["examples/02_mesh/mesh_shader/".to_string()]);

    let (width, height) = io_context.window().get_window_size();
    let mut camera = FlyCamera::new(width, height, 0.0);
    camera.base_camera.position.y += 200.0;

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                FlyCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();

        io_context
            .window()
            .add_key_callback(Box::new(move |key_states| {
                if key_states[KbKey::Space.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        -1.0,
                        0.0,
                    );
                }
                if key_states[KbKey::LeftControl.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        1.0,
                        0.0,
                    );
                }
                if key_states[KbKey::W.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        0.0,
                        1.0,
                    );
                }
                if key_states[KbKey::S.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        0.0,
                        -1.0,
                    );
                }
                if key_states[KbKey::A.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        1.0,
                        0.0,
                        0.0,
                    );
                }
                if key_states[KbKey::D.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        -1.0,
                        0.0,
                        0.0,
                    );
                }
            }));
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                FlyCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let meshes =
        chicken_wire::load_static_meshes_from_path("./Assets/Sponza/glTF/Sponza.gltf");
    let textures = Texture::from_path("./Assets/Sponza/glTF/Sponza.gltf");

    let mut gpu_meshes = Vec::new();
    for i in 0..meshes.len() {
        let gpu_mesh = GraphicsInput::new()
            .add_attribute_buffer_from_slice(&meshes[i].positions, &mut render_context)
            .add_attribute_buffer_from_slice(&meshes[i].normals, &mut render_context)
            .add_attribute_buffer_from_slice(&meshes[i].uvs, &mut render_context)
            .set_index_buffer_from_slice(&meshes[i].indices, &mut render_context);

        gpu_meshes.push(gpu_mesh);
    }

    let mut gpu_textures = Vec::new();
    for texture in textures {
        let raw_image_data = RawImageData {
            width: texture.width as u32,
            height: texture.height as u32,
            depth: 0,
            format: texture.format,
            data: Some(texture.data.as_ptr() as *const u8),
        };
        let image_handle = render_context.allocate_image(&raw_image_data);
        gpu_textures.push(image_handle);
    }

    while io_context.poll_io(&mut None) {
        render_context.start_frame(&io_context);

        let mvp = MVP {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };

        let mut first = true;
        for mesh in &gpu_meshes {
            let render_request = RenderRequest::new(mesh_shader)
                .vertex_input(mesh.clone())
                .add_images_from_iterator(1, gpu_textures.iter().copied())
                .should_clear(first)
                .add_ubo(0, mvp)
                .build();

            first = false;

            render_context.draw(&render_request);
        }
    }
}
