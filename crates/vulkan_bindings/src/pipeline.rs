#![allow(dead_code)]

use std::collections::{BTreeMap, HashSet};
use std::ffi::{c_void, CString};

use ash::vk::Handle;
use ash::*;
use dem_core::dem_log::dem_debug_assert;
use dem_core::rendering::*;
use log::error;

use crate::hardware_interface::*;
use crate::image::*;
use crate::memory::*;
use crate::shader_program::*;
use crate::swapchain::*;

const MAXIMUM_SHADER_STAGE_COUNT: u32 = 8;
const MAXIMUM_DESCRIPTOR_SIZE: u32 = 1024;

// +| Public interface |+ ======================================================
pub(crate) struct Pipeline
{
    hardware_interface: *mut HardwareInterface,
    memory: *mut Memory,

    pub(crate) pipeline: vk::Pipeline,
    pub(crate) pipeline_layout: vk::PipelineLayout,
    pub(crate) descriptor_pool: vk::DescriptorPool,
    pub(crate) descriptor_set_layouts: Vec<vk::DescriptorSetLayout>,
    pub(crate) descriptor_sets: Vec<vk::DescriptorSet>,

    binding_uniform_buffers_map: BTreeMap<u32, vk::Buffer>,
    binding_sampler_map: BTreeMap<u32, Vec<vk::Sampler>>,
    image_storage_bindings: HashSet<u32>,
    sampled_image_bindings: HashSet<u32>,

    program: ShaderProgram,
}

impl Pipeline
{
    pub(crate) fn update_uniforms(
        &self,
        cmd: &vk::CommandBuffer,
        uniform_block_data: &Vec<UboData>,
        uniform_image_data: Vec<(*mut VulkanImage, usize, usize)>,
        uniform_buffer_inputs: &Vec<GpuDataDescriptor>,
        pipeline_binding_point: vk::PipelineBindPoint,
    )
    {
        let mut descriptor_writes = Vec::<vk::WriteDescriptorSet>::with_capacity(
            uniform_block_data.len()
                + uniform_image_data.len()
                + uniform_buffer_inputs.len(),
        );

        let mut buffer_infos = Vec::<vk::DescriptorBufferInfo>::with_capacity(
            uniform_block_data.len() + uniform_buffer_inputs.len(),
        );

        for ubo in uniform_block_data
        {
            let (binding, data_ptr, data_size) = ubo.get_raw_data();
            debug_assert!(
                self.program
                    .get_uniform_binding_size_map()
                    .contains_key(&binding),
                "Binding {} not found in program.",
                binding
            );

            dem_debug_assert!(
                data_size
                    == *self
                    .program
                    .get_uniform_binding_size_map()
                    .get(&binding)
                    .expect("No binding."),
                "Binding sizes don't match, expected {} bytes, got {} for uniform binding {} in shader {}.",
                self.program
                    .get_uniform_binding_size_map()
                    .get(&binding)
                    .unwrap(),
                    data_size,
                binding,
                self.program.get_name()
            );

            unsafe {
                let expected_size =
                    match self.program.get_uniform_binding_size_map().get(&binding)
                    {
                        Some(x) => *x,
                        None =>
                        {
                            error!("No uniform at binding {}.", binding);
                            panic!();
                        }
                    };

                dem_debug_assert!(expected_size >= data_size, "");
                self.memory().update_buffer(
                    *self.binding_uniform_buffers_map.get(&binding).unwrap(),
                    data_ptr,
                    data_size,
                );
            }

            dem_debug_assert!(buffer_infos.len() < buffer_infos.capacity(), "");
            buffer_infos.push(vk::DescriptorBufferInfo {
                buffer: *self.binding_uniform_buffers_map.get(&binding).unwrap(),
                offset: 0,
                range: *self
                    .program
                    .get_uniform_binding_size_map()
                    .get(&binding)
                    .unwrap() as u64,
            });

            descriptor_writes.push(vk::WriteDescriptorSet {
                dst_set: self.descriptor_sets[1],
                dst_binding: binding,
                dst_array_element: 0,
                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
                p_buffer_info: buffer_infos.last().unwrap(),
                ..Default::default()
            });
        }

        for GpuDataDescriptor {
            handle,
            binding,
            index: _,
        } in uniform_buffer_inputs
        {
            debug_assert!(buffer_infos.len() < buffer_infos.capacity());
            buffer_infos.push(vk::DescriptorBufferInfo {
                buffer: vk::Buffer::from_raw(handle.to_buffer_handle().to_raw()),
                offset: 0,
                range: vk::WHOLE_SIZE,
            });

            descriptor_writes.push(vk::WriteDescriptorSet {
                dst_set: self.descriptor_sets[1],
                dst_binding: *binding as u32,
                dst_array_element: 0,
                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::STORAGE_BUFFER,
                p_buffer_info: buffer_infos.last().unwrap(),
                ..Default::default()
            });
        }

        // There might be separate samplers for each image, thus allocate up to 2x
        // slots to prevent reallocation.
        let mut image_infos =
            Vec::<vk::DescriptorImageInfo>::with_capacity(uniform_image_data.len() * 2);
        let mut samplers_combined_with_images = HashSet::new();
        for (image, binding, index) in uniform_image_data
        {
            debug_assert!(image_infos.len() < image_infos.capacity());
            image_infos.push(vk::DescriptorImageInfo {
                ..Default::default()
            });
            let image_info = image_infos.last_mut().unwrap();

            debug_assert!(descriptor_writes.len() < descriptor_writes.capacity());
            descriptor_writes.push(vk::WriteDescriptorSet::default());

            let descriptor_write = descriptor_writes.last_mut().unwrap();

            // Image storage.
            if self.image_storage_bindings.contains(&(binding as u32))
            {
                unsafe {
                    (*image).transition_layout(
                        vk::ImageLayout::GENERAL,
                        vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                    );
                    image_info.image_view = (*image).image_view;
                    image_info.image_layout = (*image).layout;
                }

                descriptor_write.descriptor_type = vk::DescriptorType::STORAGE_IMAGE;
            }
            // Sampled Image
            else if self.sampled_image_bindings.contains(&(binding as u32))
            {
                unsafe {
                    (*image).transition_layout(
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                        vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                    );

                    image_info.image_view = (*image).image_view;
                    image_info.image_layout = (*image).layout;
                }

                descriptor_write.descriptor_type = vk::DescriptorType::SAMPLED_IMAGE;
            }
            // Combined image samplers.
            else
            {
                debug_assert!(
                    self.binding_sampler_map.contains_key(&(binding as u32)),
                    "Binding {} does not exist in this shader.",
                    binding
                );
                debug_assert!(
                    index
                        < self
                            .binding_sampler_map
                            .get(&(binding as u32))
                            .unwrap()
                            .len(),
                    "Trying to index binding {} with value {}. Binding has length {}",
                    binding,
                    index,
                    self.binding_sampler_map
                        .get(&(binding as u32))
                        .unwrap()
                        .len()
                );
                unsafe {
                    (*image).transition_layout(
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                        vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                    );

                    image_info.sampler =
                        self.binding_sampler_map.get(&(binding as u32)).expect(
                            format!("No sampler at binding {}", binding).as_str(),
                        )[index];
                    image_info.image_view = (*image).image_view;
                    image_info.image_layout = (*image).layout;
                }

                samplers_combined_with_images.insert(binding);

                descriptor_write.descriptor_type =
                    vk::DescriptorType::COMBINED_IMAGE_SAMPLER;
            }

            descriptor_write.dst_set = self.descriptor_sets[1];
            descriptor_write.dst_binding = binding as u32;
            descriptor_write.dst_array_element = index as u32;
            descriptor_write.descriptor_count = 1;
            descriptor_write.p_image_info = image_info;
        }

        // In HLSL samplers and images are independent. We don't want to expose sampler
        // creation to the user, so we will need to make the missing samplers for
        // this pipeline.
        for (&binding, samplers) in &self.binding_sampler_map
        {
            if !samplers_combined_with_images.contains(&(binding as usize))
            {
                for (index, sampler) in samplers.iter().enumerate()
                {
                    debug_assert!(
                        image_infos.len() <= image_infos.capacity(),
                        "Image infos length {} is not smaller than the image_infos capacity {}",
                        image_infos.len(),
                        image_infos.capacity());

                    image_infos.push(vk::DescriptorImageInfo {
                        ..Default::default()
                    });
                    let image_info = image_infos.last_mut().unwrap();
                    image_info.sampler = *sampler;
                    image_info.image_view = vk::ImageView::null();

                    descriptor_writes.push(vk::WriteDescriptorSet::default());
                    let descriptor_write = descriptor_writes.last_mut().unwrap();

                    descriptor_write.descriptor_type = vk::DescriptorType::SAMPLER;
                    descriptor_write.descriptor_count = 1;
                    descriptor_write.dst_array_element = index as u32;
                    descriptor_write.dst_binding = binding as u32;
                    descriptor_write.dst_set = self.descriptor_sets[1];
                    descriptor_write.p_image_info = image_info;
                }
            }
        }

        unsafe {
            let device = &self.hardware_interface().device;
            device.update_descriptor_sets(&descriptor_writes, &[]);

            device.cmd_bind_descriptor_sets(
                *cmd,
                pipeline_binding_point,
                self.pipeline_layout,
                0,
                self.descriptor_sets.as_slice(),
                &[],
            );

            device.cmd_bind_pipeline(*cmd, pipeline_binding_point, self.pipeline);
        };
    }

    pub(crate) fn new(
        hardware_interface: *mut HardwareInterface,
        memory: *mut Memory,
        swapchain: *mut Swapchain,
        program: ShaderProgram,
    ) -> Pipeline
    {
        let device = unsafe { &(*hardware_interface).device };
        let descriptor_pool = create_descriptor_pool(device);
        let descriptor_set_layouts = create_descriptor_set_layouts(device, &program);

        let pipeline_layout = create_pipeline_layout(device, &descriptor_set_layouts);
        let pipeline = unsafe {
            create_pipeline(
                device,
                &pipeline_layout,
                &program,
                (*swapchain).selected_format,
            )
        };

        let alloc_info = vk::DescriptorSetAllocateInfo {
            descriptor_pool: descriptor_pool,
            descriptor_set_count: descriptor_set_layouts.len() as u32,
            p_set_layouts: descriptor_set_layouts.as_ptr(),
            ..Default::default()
        };

        let descriptor_sets = unsafe {
            device
                .allocate_descriptor_sets(&alloc_info)
                .expect("Could not allocate descriptor set.")
        };

        let mut binding_uniform_buffers_map: BTreeMap<u32, vk::Buffer> = BTreeMap::new();
        for (binding, byte_size) in program.get_uniform_binding_size_map()
        {
            binding_uniform_buffers_map.insert(*binding, unsafe {
                (*memory).create_buffer(*byte_size, vk::BufferUsageFlags::UNIFORM_BUFFER)
            });
        }

        let binding_layouts = program.get_uniform_layouts();
        let mut binding_sampler_map: BTreeMap<u32, Vec<vk::Sampler>> = BTreeMap::new();
        let mut image_storage_bindings = HashSet::<u32>::new();
        let mut sampled_image_bindings = HashSet::<u32>::new();

        for uniform_descriptor in binding_layouts
        {
            type DT = vk::DescriptorType;
            match uniform_descriptor.descriptor_type
            {
                DT::COMBINED_IMAGE_SAMPLER | DT::SAMPLER =>
                {
                    binding_sampler_map.insert(uniform_descriptor.binding, Vec::new());
                    let sampler_list: &mut Vec<vk::Sampler> = binding_sampler_map
                        .get_mut(&uniform_descriptor.binding)
                        .unwrap();
                    sampler_list.reserve(uniform_descriptor.descriptor_count as usize);
                    for _ in 0..uniform_descriptor.descriptor_count
                    {
                        sampler_list.push(create_texture_sampler(
                            &device,
                            &vk::SamplerCreateInfo {
                                ..Default::default()
                            },
                        ));
                    }
                }
                DT::STORAGE_IMAGE =>
                {
                    image_storage_bindings.insert(uniform_descriptor.binding);
                }
                DT::SAMPLED_IMAGE =>
                {
                    sampled_image_bindings.insert(uniform_descriptor.binding);
                }
                // Skip all other uniform types.
                _ =>
                {}
            }
        }

        Pipeline {
            hardware_interface,
            memory,
            pipeline,
            pipeline_layout,
            descriptor_pool,
            descriptor_set_layouts,
            descriptor_sets,
            binding_uniform_buffers_map,
            binding_sampler_map,
            sampled_image_bindings,
            image_storage_bindings,
            program,
        }
    }

    fn memory(&self) -> &mut Memory
    {
        unsafe {
            return &mut (*self.memory);
        }
    }

    fn hardware_interface(&self) -> &HardwareInterface
    {
        unsafe {
            return &(*self.hardware_interface);
        }
    }
}

impl Drop for Pipeline
{
    fn drop(&mut self)
    {
        unsafe {
            for module in self.program.get_shader_modules()
            {
                self.hardware_interface()
                    .device
                    .destroy_shader_module(*module, None);
            }
            for (_, sampler_list) in &self.binding_sampler_map
            {
                for sampler in sampler_list
                {
                    self.hardware_interface()
                        .device
                        .destroy_sampler(*sampler, None);
                }
            }
            for (_, buffer) in &self.binding_uniform_buffers_map
            {
                self.memory().destroy_buffer(*buffer);
            }

            self.hardware_interface()
                .device
                .destroy_pipeline_layout(self.pipeline_layout, None);
            self.hardware_interface()
                .device
                .destroy_pipeline(self.pipeline, None);

            let device = self.hardware_interface().device.clone();
            for descriptor_set_layout in self.descriptor_set_layouts.drain(..)
            {
                device.destroy_descriptor_set_layout(descriptor_set_layout, None);
            }

            self.hardware_interface()
                .device
                .destroy_descriptor_pool(self.descriptor_pool, None);
        }
    }
}

// +| Internal |+ ==============================================================
fn create_texture_sampler(
    device: &ash::Device,
    sampler_info: &vk::SamplerCreateInfo,
) -> vk::Sampler
{
    unsafe {
        device
            .create_sampler(&sampler_info, None)
            .expect("Could not create sampler")
    }
}

fn create_pipeline(
    device: &ash::Device,
    pipeline_layout: &vk::PipelineLayout,
    program: &ShaderProgram,
    fallback_format: vk::Format,
) -> vk::Pipeline
{
    match program.is_compute()
    {
        false =>
        {
            create_graphics_pipeline(device, pipeline_layout, program, fallback_format)
        }
        true => create_compute_pipeline(device, pipeline_layout, program),
    }
}

fn create_graphics_pipeline(
    device: &ash::Device,
    pipeline_layout: &vk::PipelineLayout,
    program: &ShaderProgram,
    fallback_format: vk::Format,
) -> vk::Pipeline
{
    debug_assert!(program.get_shader_stages().len() as u32 <= MAXIMUM_SHADER_STAGE_COUNT);
    let mut shader_infos: [vk::PipelineShaderStageCreateInfo;
        MAXIMUM_SHADER_STAGE_COUNT as usize] =
        [vk::PipelineShaderStageCreateInfo::default();
            MAXIMUM_SHADER_STAGE_COUNT as usize];

    let modules = program.get_shader_modules();

    let shader_entry_names: Vec<_> = program
        .get_shader_entry_names()
        .iter()
        .map(|name| CString::new(name.clone()).unwrap())
        .collect();

    for (i, stage) in program.get_shader_stages().iter().enumerate()
    {
        let main_function_name = &shader_entry_names[i];
        shader_infos[i].stage = *stage;
        shader_infos[i].module = modules[i];
        shader_infos[i].p_name = main_function_name.as_ptr();
    }

    let vertex_input_info = vk::PipelineVertexInputStateCreateInfo {
        vertex_binding_description_count: program.get_vertex_bindings().len() as u32,
        p_vertex_binding_descriptions: program.get_vertex_bindings().as_ptr(),
        vertex_attribute_description_count: program.get_attributes().len() as u32,
        p_vertex_attribute_descriptions: program.get_attributes().as_ptr(),
        ..Default::default()
    };

    let input_assembly = vk::PipelineInputAssemblyStateCreateInfo {
        topology: program.get_config().topology,
        primitive_restart_enable: vk::FALSE,
        ..Default::default()
    };

    let rasterizer = vk::PipelineRasterizationStateCreateInfo {
        depth_clamp_enable: vk::FALSE,
        rasterizer_discard_enable: vk::FALSE,
        polygon_mode: program.get_config().polygon_mode,
        cull_mode: program.get_config().cull_mode,
        front_face: program.get_config().front_face,
        depth_bias_enable: vk::FALSE,
        depth_bias_constant_factor: 0.0,
        depth_bias_clamp: 0.0,
        depth_bias_slope_factor: 0.0,
        line_width: program.get_config().line_width,
        ..Default::default()
    };

    let multisampling = vk::PipelineMultisampleStateCreateInfo {
        sample_shading_enable: vk::FALSE,
        rasterization_samples: vk::SampleCountFlags::TYPE_1,
        ..Default::default()
    };

    let mut color_blends = Vec::<vk::PipelineColorBlendAttachmentState>::with_capacity(
        program.attachment_count() as usize,
    );

    for _ in 0..program.attachment_count()
    {
        color_blends.push(vk::PipelineColorBlendAttachmentState {
            blend_enable: vk::TRUE,
            src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
            dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
            color_blend_op: vk::BlendOp::ADD,
            src_alpha_blend_factor: vk::BlendFactor::ONE_MINUS_DST_ALPHA,
            dst_alpha_blend_factor: vk::BlendFactor::ONE,
            alpha_blend_op: vk::BlendOp::ADD,
            color_write_mask: vk::ColorComponentFlags::R
                | vk::ColorComponentFlags::G
                | vk::ColorComponentFlags::B
                | vk::ColorComponentFlags::A,
        });
    }

    let color_blending = vk::PipelineColorBlendStateCreateInfo {
        logic_op_enable: vk::FALSE,
        logic_op: vk::LogicOp::COPY,
        attachment_count: color_blends.len() as u32,
        p_attachments: color_blends.as_ptr(),
        blend_constants: [0.0, 0.0, 0.0, 0.0],
        ..Default::default()
    };

    let dynamic_state_enables: [vk::DynamicState; 2] =
        [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
    let dynamic_state = vk::PipelineDynamicStateCreateInfo {
        dynamic_state_count: dynamic_state_enables.len() as u32,
        p_dynamic_states: dynamic_state_enables.as_ptr(),
        ..Default::default()
    };

    let depth_stencil = vk::PipelineDepthStencilStateCreateInfo {
        depth_test_enable: program.get_config().depth_test as u32,
        depth_write_enable: program.get_config().depth_write as u32,
        depth_compare_op: program.get_config().depth_compare,
        depth_bounds_test_enable: vk::FALSE,
        stencil_test_enable: vk::FALSE,
        min_depth_bounds: 0.0,
        max_depth_bounds: 1.0,
        ..Default::default()
    };

    let viewport_state = vk::PipelineViewportStateCreateInfo {
        viewport_count: 1,
        scissor_count: 1,
        ..Default::default()
    };

    let mut pipeline_info = vk::GraphicsPipelineCreateInfo {
        stage_count: program.get_shader_stages().len() as u32,
        p_stages: shader_infos.as_ptr(),
        p_vertex_input_state: &vertex_input_info,
        p_input_assembly_state: &input_assembly,
        p_viewport_state: &viewport_state,
        p_rasterization_state: &rasterizer,
        p_multisample_state: &multisampling,
        p_color_blend_state: &color_blending,
        layout: *pipeline_layout,
        p_dynamic_state: &dynamic_state,
        p_depth_stencil_state: &depth_stencil,
        ..Default::default()
    };

    let mut attachment_formats = program.get_attachment_formats().clone();
    for format in &mut attachment_formats
    {
        if *format == vk::Format::UNDEFINED
        {
            *format = fallback_format;
        }
    }

    debug_assert!(
        attachment_formats.len() == program.attachment_count() as usize,
        "{} {}",
        attachment_formats.len(),
        program.attachment_count()
    );

    let pipeline_rendering_create_info = vk::PipelineRenderingCreateInfoKHR {
        color_attachment_count: attachment_formats.len() as u32,
        p_color_attachment_formats: attachment_formats.as_ptr(),
        depth_attachment_format: vk::Format::D32_SFLOAT_S8_UINT,
        stencil_attachment_format: vk::Format::D32_SFLOAT_S8_UINT,
        ..Default::default()
    };

    pipeline_info.p_next = (&pipeline_rendering_create_info) as *const _ as *const c_void;

    unsafe {
        device
            .create_graphics_pipelines(vk::PipelineCache::null(), &[pipeline_info], None)
            .expect("Could not create graphics pipeline.")[0]
    }
}

fn create_compute_pipeline(
    device: &ash::Device,
    pipeline_layout: &vk::PipelineLayout,
    program: &ShaderProgram,
) -> vk::Pipeline
{
    debug_assert!(program.get_shader_modules().len() == 1);
    debug_assert!(program.get_shader_stages().len() == 1);

    let main_function_name =
        CString::new(program.get_shader_entry_names()[0].clone()).unwrap();
    let shader_stage_info = vk::PipelineShaderStageCreateInfo {
        stage: program.get_shader_stages()[0],
        module: program.get_shader_modules()[0],
        p_name: main_function_name.as_ptr(),
        ..Default::default()
    };

    let create_info = vk::ComputePipelineCreateInfo {
        stage: shader_stage_info,
        layout: *pipeline_layout,
        ..Default::default()
    };

    return unsafe {
        device
            .create_compute_pipelines(vk::PipelineCache::null(), &[create_info], None)
            .expect("Could not create compute pipeline.")[0]
    };
}

fn create_descriptor_pool(device: &ash::Device) -> vk::DescriptorPool
{
    let mut pool_sizes: [vk::DescriptorPoolSize; 4] =
        [vk::DescriptorPoolSize::default(); 4];
    pool_sizes[0] = vk::DescriptorPoolSize {
        ty: vk::DescriptorType::UNIFORM_BUFFER,
        descriptor_count: MAXIMUM_DESCRIPTOR_SIZE,
    };
    pool_sizes[1] = vk::DescriptorPoolSize {
        ty: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
        descriptor_count: MAXIMUM_DESCRIPTOR_SIZE,
    };
    pool_sizes[2] = vk::DescriptorPoolSize {
        ty: vk::DescriptorType::STORAGE_IMAGE,
        descriptor_count: MAXIMUM_DESCRIPTOR_SIZE,
    };
    pool_sizes[3] = vk::DescriptorPoolSize {
        ty: vk::DescriptorType::STORAGE_BUFFER,
        descriptor_count: MAXIMUM_DESCRIPTOR_SIZE,
    };

    let c_flags = vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET;
    let pool_info = vk::DescriptorPoolCreateInfo {
        flags: c_flags,
        max_sets: MAXIMUM_DESCRIPTOR_SIZE,
        pool_size_count: pool_sizes.len() as u32,
        p_pool_sizes: pool_sizes.as_ptr(),
        ..Default::default()
    };

    return unsafe {
        device
            .create_descriptor_pool(&pool_info, None)
            .expect("Could not create pool info.")
    };
}

fn create_descriptor_set_layouts(
    device: &ash::Device,
    program: &ShaderProgram,
) -> Vec<vk::DescriptorSetLayout>
{
    let mut binding_layouts = program.get_uniform_layouts().clone();

    // This exists to create an empty DS for rustgpu.
    let dummy = vk::DescriptorSetLayoutCreateInfo {
        ..Default::default()
    };

    let flags = binding_layouts
        .iter_mut()
        .map(|layout| {
            // In cases of unbound uniform arrays, such as an unbound texture array,
            // the descriptor count is flagged as 0. Vulkan demands we give an upper bound
            // for the descriptor set, so set the value to a large number.
            type BF = vk::DescriptorBindingFlags;
            if layout.descriptor_count == 0
            {
                layout.descriptor_count = MAXIMUM_DESCRIPTOR_SIZE;
                BF::VARIABLE_DESCRIPTOR_COUNT | BF::PARTIALLY_BOUND
            }
            else
            {
                vk::DescriptorBindingFlags::default()
            }
        })
        .collect::<Vec<_>>();

    let mut create_flags = vk::DescriptorSetLayoutBindingFlagsCreateInfo {
        binding_count: flags.len() as u32,
        p_binding_flags: flags.as_ptr(),
        ..Default::default()
    };

    let info = vk::DescriptorSetLayoutCreateInfo {
        binding_count: binding_layouts.len() as u32,
        p_bindings: binding_layouts.as_ptr(),
        ..Default::default()
    }
    .push_next(&mut create_flags);

    return unsafe {
        vec![
            device
                .create_descriptor_set_layout(&dummy, None)
                .expect("Could not create descriptor set."),
            device
                .create_descriptor_set_layout(&info, None)
                .expect("Could not create descriptor set."),
        ]
    };
}

fn create_pipeline_layout(
    device: &ash::Device,
    descriptor_set_layouts: &[vk::DescriptorSetLayout],
) -> vk::PipelineLayout
{
    let pipeline_layout_info = vk::PipelineLayoutCreateInfo {
        set_layout_count: descriptor_set_layouts.len() as u32,
        p_set_layouts: descriptor_set_layouts.as_ptr(),
        ..Default::default()
    };

    unsafe {
        device
            .create_pipeline_layout(&pipeline_layout_info, None)
            .expect("Could not create pipeline layout.")
    }
}
