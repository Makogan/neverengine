use core::iter::Sum;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::ops::{Add, Mul};

use algebra::*;
use linear_isomorphic::{InnerSpace, RealField, VectorSpace};
use std::collections::BTreeMap;

use crate::face_handle::*;
pub use crate::facets::GeomId;
use crate::facets::*;
use crate::hedge_handle::*;
pub use crate::traits::HalfMeshLike;
pub use crate::traits::*;
use crate::vert_handle::*;

#[derive(Debug, Clone)]
pub struct HalfMesh<Verts, Edges, Faces>
where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    pub(crate) verts: RefCell<Verts>,
    pub(crate) edges: RefCell<Edges>,
    pub(crate) faces: RefCell<Faces>,

    pub(crate) verts_meta: RefCell<Vec<Vert>>,
    pub(crate) hedges_meta: RefCell<Vec<HEdge>>,
    pub(crate) faces_meta: RefCell<Vec<Face>>,
}

impl<Verts, Edges, Faces> HalfMeshLike for HalfMesh<Verts, Edges, Faces>
where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    type EdgeData = Edges;
    type EdgeIterator = FullEdgeIter<Verts, Edges, Faces>;
    type FaceData = Faces;
    type FaceIterator = FullFaceIter<Verts, Edges, Faces>;
    type VertData = Verts;
    type VertIterator = FullVertIter<Verts, Edges, Faces>;

    fn last_vert_id(&self) -> GeomId<0> {
        GeomId::from(self.vert_count())
    }

    fn vert_handle(&self, id: GeomId<0>) -> VertHandle<Verts, Edges, Faces> {
        debug_assert!(
            id.0 < self.vert_count() as u64,
            "Requested vert {} out of {} in the mesh.",
            id.0,
            self.vert_count()
        );
        VertHandle::<Verts, Edges, Faces> {
            mesh: self as *const HalfMesh<Verts, Edges, Faces> as *mut _,
            id,
        }
    }

    fn hedge_handle(&self, id: GeomId<1>) -> HEdgeHandle<Verts, Edges, Faces> {
        debug_assert!(
            id.0 < self.hedge_count() as u64,
            "Id {} is less than the edge count {}.",
            id.0,
            self.hedge_count()
        );
        HEdgeHandle::<Verts, Edges, Faces> {
            mesh: self as *const HalfMesh<Verts, Edges, Faces>
                as *mut HalfMesh<Verts, Edges, Faces>,
            id,
        }
    }

    fn face_handle(&self, id: GeomId<2>) -> FaceHandle<Verts, Edges, Faces> {
        debug_assert!(
            id.0 < self.face_count() as u64,
            "{} / {}",
            id,
            self.face_count()
        );
        FaceHandle::<Verts, Edges, Faces> {
            mesh: self as *const HalfMesh<Verts, Edges, Faces>
                as *mut HalfMesh<Verts, Edges, Faces>,
            id,
        }
    }

    fn iter_faces(&self) -> FullFaceIter<Verts, Edges, Faces> {
        FullFaceIter {
            mesh: self as *const HalfMesh<Verts, Edges, Faces>
                as *mut HalfMesh<Verts, Edges, Faces>,
            current_id: GeomId(0),
            end_id: GeomId(self.face_count() as u64),
        }
    }

    /// Returns an iterator over the *edges* of the mesh i.e. it skips every
    /// other half edge.
    fn iter_edges(&self) -> FullEdgeIter<Verts, Edges, Faces> {
        FullEdgeIter {
            mesh: self as *const HalfMesh<Verts, Edges, Faces>
                as *mut HalfMesh<Verts, Edges, Faces>,
            current_id: GeomId(0),
            end_id: GeomId(self.hedge_count() as u64),
        }
    }

    fn iter_verts(&self) -> FullVertIter<Verts, Edges, Faces> {
        FullVertIter::<Verts, Edges, Faces> {
            mesh: self as *const HalfMesh<Verts, Edges, Faces>
                as *mut HalfMesh<Verts, Edges, Faces>,
            current_id: GeomId(0),
            end_id: GeomId(self.verts.borrow().len() as u64),
        }
    }

    fn vert_count(&self) -> usize {
        self.verts_meta.borrow().len()
    }

    fn hedge_count(&self) -> usize {
        self.hedges_meta.borrow().len()
    }

    fn edge_count(&self) -> usize {
        self.hedges_meta.borrow().len() / 2
    }

    fn face_count(&self) -> usize {
        self.faces_meta.borrow().len()
    }

    fn flip_edge(&mut self, id: GeomId<1>) {
        self.hedge_handle(id).flip()
    }

    fn split_edge<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 2], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float,
    {
        self.hedge_handle(id).split()
    }

    fn split_edge_boundary<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 1], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float,
    {
        let edge_handle = self.hedge_handle(id);
        let boundary_edge = edge_handle.get_non_boundary_hedge().unwrap();
        boundary_edge.split_boundary()
    }

    fn is_in_boundary_edge(&self, id: GeomId<1>) -> bool {
        self.hedge_handle(id).is_in_boundary_edge()
    }
}

impl<Verts, Edges, Faces> HalfMesh<Verts, Edges, Faces>
where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    pub fn add_vert(&mut self, data: Verts::PrimitiveData) -> GeomId<0> {
        let verts = self.verts.get_mut();
        let verts_meta = self.verts_meta.get_mut();
        let id = verts.len();

        verts.push(data);
        verts_meta.push(Vert {
            enabled: true,
            id: GeomId(id as u64),
            hedge_id: GeomId(!0),
        });
        return GeomId(id as u64);
    }

    /// The first id corresponds to the half edge with source at `p1`, the
    /// second id corresponds to its pair.
    pub fn add_edge(
        &mut self,
        p1: GeomId<0>,
        p2: GeomId<0>,
        data_1: Edges::PrimitiveData,
        data_2: Edges::PrimitiveData,
    ) -> (GeomId<1>, GeomId<1>) {
        let hedge_count = self.hedge_count();
        let hedges = self.edges.get_mut();
        let hedges_meta = self.hedges_meta.get_mut();
        let eid1 = GeomId(hedge_count as u64);
        let eid2 = GeomId((hedge_count + 1) as u64);

        // Order is extremely important!
        hedges.push(data_1);
        hedges.push(data_2);

        hedges_meta.push(HEdge {
            enabled: true,
            id: eid1,
            next_id: ABSENT(),
            prev_id: ABSENT(),
            pair_id: eid2,
            vert_id: p1,
            face_id: ABSENT(),
        });
        hedges_meta.push(HEdge {
            enabled: true,
            id: eid2,
            next_id: ABSENT(),
            prev_id: ABSENT(),
            pair_id: eid1,
            vert_id: p2,
            face_id: ABSENT(),
        });

        self.verts_meta.borrow_mut()[usize::from(p1)].hedge_id = eid1;
        self.verts_meta.borrow_mut()[usize::from(p2)].hedge_id = eid2;

        return (eid1, eid2);
    }

    pub fn add_face(
        &mut self,
        verts: &[GeomId<0>],
        data: Faces::PrimitiveData,
    ) -> GeomId<2> {
        let mut hedges = BTreeMap::<[GeomId<0>; 2], GeomId<1>>::new();

        // For each vertex in the input, collect all boundary hedges (if any) and
        // sort them by inbound or outbound.
        // Additionally, store all edges touching the input vertices.
        let mut inbound_boundaries = BTreeMap::<GeomId<0>, GeomId<1>>::new();
        let mut outbound_boundaries = BTreeMap::<GeomId<0>, GeomId<1>>::new();
        for v in verts {
            for h in self.vert_handle(*v).iter_hedges() {
                hedges.insert([h.source().id(), h.dest().id()], h.id());
                let p = h.pair();
                hedges.insert([p.source().id(), p.dest().id()], p.id());

                if h.is_boundary_hedge() {
                    outbound_boundaries.insert(*v, h.id());
                }

                if h.pair().is_boundary_hedge() {
                    inbound_boundaries.insert(*v, h.pair().id());
                }
            }
        }

        // For each pair of points in the input, assert that they
        // are connected by an edge. So make one if not present.
        for i in 0..verts.len() {
            let v1 = verts[i];
            let v2 = verts[(i + 1) % verts.len()];

            if !hedges.contains_key(&[v1, v2]) {
                let (n1, n2) = self.add_edge(
                    v1,
                    v2,
                    Edges::PrimitiveData::default(),
                    Edges::PrimitiveData::default(),
                );

                hedges.insert([v1, v2], n1);
                hedges.insert([v2, v1], n2);
            }
        }

        // Add the face.
        self.faces.borrow_mut().push(data);
        let mut faces = self.faces_meta.borrow_mut();
        let fid = GeomId(faces.len() as u64);
        faces.push(Face {
            enabled: true,
            id: fid,
            hedge_id: ABSENT(),
        });
        drop(faces);

        // Attach in face hedges.
        for i in 0..verts.len() {
            let v1 = verts[i];
            let v2 = verts[(i + 1) % verts.len()];
            let v3 = verts[(i + 2) % verts.len()];

            let h1 = *hedges.get(&[v1, v2]).unwrap();
            let h2 = *hedges.get(&[v2, v3]).unwrap();

            let mut data = self.hedges_meta.borrow_mut();
            data[h1.0 as usize].next_id = h2;
            data[h2.0 as usize].prev_id = h1;
            data[h1.0 as usize].face_id = fid;
            drop(data);

            let mut data = self.faces_meta.borrow_mut();
            data[fid.0 as usize].hedge_id = h1;
            drop(data);

            let mut data = self.verts_meta.borrow_mut();
            data[v1.0 as usize].hedge_id = h1;

            drop(data);
        }

        // Remove the hedges we just added from the hedge map.
        for i in 0..verts.len() {
            let v1 = verts[i];
            let v2 = verts[(i + 1) % verts.len()];

            hedges.remove(&[v1, v2]);
        }

        // Attach face boundary.
        for i in 0..verts.len() {
            let v1 = verts[i];
            let v2 = verts[(i + 1) % verts.len()];
            let v3 = verts[(i + 2) % verts.len()];

            let pair = *hedges.get(&[v2, v1]).unwrap();
            let prior = *hedges.get(&[v3, v2]).unwrap();

            if !self.hedge_handle(pair).is_boundary_hedge() {
                continue;
            }

            if !self.hedge_handle(prior).is_boundary_hedge() {
                continue;
            }

            let mut data = self.hedges_meta.borrow_mut();
            data[prior.0 as usize].next_id = pair;
            data[pair.0 as usize].prev_id = prior;
        }

        for i in 0..verts.len() {
            let v1 = verts[i];
            let v2 = verts[(i + 1) % verts.len()];

            hedges.remove(&[v2, v1]);
        }

        // Attach outbound vertices.
        for i in 0..verts.len() {
            let h = self.vert_handle(verts[i]).hedge().unwrap().id();
            let outbound = outbound_boundaries.get(&verts[i]);
            if outbound.is_none() {
                continue;
            }
            let outbound = *outbound.unwrap();

            let outbound_handle = self.hedge_handle(outbound);

            if !outbound_handle.is_boundary_hedge() {
                continue;
            }

            let pair = self.hedge_handle(h).pair().id();

            if !self.hedge_handle(h).pair().is_boundary_hedge() {
                continue;
            }

            let mut data = self.hedges_meta.borrow_mut();
            data[pair.0 as usize].next_id = outbound;
            data[outbound.0 as usize].prev_id = pair;
        }

        // Attach inbound vertices.
        for i in 0..verts.len() {
            let h = self.vert_handle(verts[i]).hedge().unwrap().id();
            let inbound = inbound_boundaries.get(&verts[i]);
            if inbound.is_none() {
                continue;
            }
            let inbound = *inbound.unwrap();

            let inbound_handle = self.hedge_handle(inbound);

            if !inbound_handle.is_boundary_hedge() {
                continue;
            }

            let pair = self.hedge_handle(h).prev().pair().id();

            if !self.hedge_handle(h).prev().pair().is_boundary_hedge() {
                continue;
            }

            let mut data = self.hedges_meta.borrow_mut();
            data[inbound.0 as usize].next_id = pair;
            data[pair.0 as usize].prev_id = inbound;
        }

        return fid;
    }

    pub fn add_face_through_edge(
        &mut self,
        he_id: GeomId<1>,
        data: Faces::PrimitiveData,
    ) -> GeomId<2> {
        let face_count = self.face_count();
        let faces = self.faces.get_mut();
        let faces_meta = self.faces_meta.get_mut();
        let fid = GeomId(face_count as u64);

        faces.push(data);
        faces_meta.push(Face {
            enabled: true,
            id: fid,
            hedge_id: he_id,
        });
        let hedge = &mut self.hedges_meta.borrow_mut()[he_id.0 as usize];

        hedge.face_id = fid;

        return fid;
    }

    /// Remove a face from the mesh.
    /// WARNING: this swaps the target and last faces, so all handles to faces
    /// made before this point will become invalid. Same for iterators.
    pub fn remove_face(&mut self, face_id: GeomId<2>) {
        let src_index = face_id.0 as usize;
        // Face does not exist so it's trivially removed.
        if src_index >= self.face_count() {
            return;
        }

        let last_face = GeomId(self.face_count() as u64 - 1);

        // Update all edges in the face to remove, to point to absent.
        for hedge in self.face_handle(face_id).hedge().iter_loop() {
            self.hedges_meta.borrow_mut()[hedge.id().0 as usize].face_id = ABSENT();
        }

        // Update all edges in the last face to point to the index at which the
        // last face will be moved.
        for hedge in self.face_handle(last_face).hedge().iter_loop() {
            self.hedges_meta.borrow_mut()[hedge.id().0 as usize].face_id = face_id;
        }

        self.faces_meta.borrow_mut().swap_remove(src_index);
        self.faces.borrow_mut().swap_remove(src_index as u64);

        // Update the id of the face, since it was moved from the end into this new
        // index.
        if src_index < self.face_count() {
            self.faces_meta.borrow_mut()[src_index].id = face_id;
        }
    }

    /// Remove an edge from the mesh.
    /// WARNING: this moves faces and edges around. All handles to faces and
    /// edges made before this point will become invalid. Same for
    /// iterators.
    pub fn remove_edge(&mut self, hedge_id: GeomId<1>) {
        debug_assert!((hedge_id.0 as usize) < self.hedge_count());

        let e0 = self.hedge_handle(hedge_id);
        let e0 = e0.even_hedge();
        let e1 = e0.pair();
        let e2 = e1.prev();
        let e3 = e1.next();
        let e4 = e0.next();
        let e5 = e0.prev();

        let v1 = e0.dest();
        let v2 = e0.source();

        let f1 = e0.face();
        let mut f2 = e1.face();

        self.verts_meta.borrow_mut()[v1.id().0 as usize].hedge_id = e4.id();
        self.verts_meta.borrow_mut()[v2.id().0 as usize].hedge_id = e3.id();

        if f1.id != ABSENT() {
            self.remove_face(f1.id);
        }
        // Deleting the above face will move the last face into the index of f1. So
        // check and correct accordingly.
        if f2.id.0 == self.face_count() as u64 {
            f2 = f1;
        }
        if f2.id != ABSENT() {
            self.remove_face(f2.id);
        }

        self.hedges_meta.borrow_mut()[e2.id().0 as usize].next_id = e4.id();
        self.hedges_meta.borrow_mut()[e4.id().0 as usize].prev_id = e2.id();

        self.hedges_meta.borrow_mut()[e5.id().0 as usize].next_id = e3.id();
        self.hedges_meta.borrow_mut()[e3.id().0 as usize].prev_id = e5.id();

        let e_0_id = e0.id();
        let e_1_id = e1.id();

        let hedge_count = self.hedge_count();
        let f0_id = self.hedges_meta.borrow()[hedge_count - 2].face_id;
        let f1_id = self.hedges_meta.borrow()[hedge_count - 1].face_id;

        self.edges.borrow_mut().swap_remove(e1.id().0);
        self.edges.borrow_mut().swap_remove(e0.id().0);
        self.hedges_meta
            .borrow_mut()
            .swap_remove(e1.id().0 as usize);
        self.hedges_meta
            .borrow_mut()
            .swap_remove(e0.id().0 as usize);

        self.hedges_meta.borrow_mut()[e0.id().0 as usize].id = e_0_id;
        self.hedges_meta.borrow_mut()[e1.id().0 as usize].id = e_1_id;

        let new_e0 = self.hedge_handle(e_0_id);
        let new_e1 = self.hedge_handle(e_1_id);

        let update_hedge = |handle: &HEdgeHandle<Verts, Edges, Faces>, fid: GeomId<2>| {
            let next_id = handle.next().id().0 as usize;
            let prev_id = handle.prev().id().0 as usize;

            self.hedges_meta.borrow_mut()[next_id].prev_id = handle.id();
            self.hedges_meta.borrow_mut()[prev_id].next_id = handle.id();

            if fid != ABSENT() && (fid.0 as usize) < self.face_count() {
                debug_assert!(self.faces_meta.borrow()[fid.0 as usize].id == fid);
                self.faces_meta.borrow_mut()[fid.0 as usize].hedge_id = handle.id();
            }

            let source_id = handle.source().id().0 as usize;
            self.verts_meta.borrow_mut()[source_id].hedge_id = handle.id();
        };

        update_hedge(&new_e0, f0_id);
        update_hedge(&new_e1, f1_id);

        self.hedges_meta.borrow_mut()[new_e0.id().0 as usize].pair_id = new_e1.id();
        self.hedges_meta.borrow_mut()[new_e1.id().0 as usize].pair_id = new_e0.id();
    }

    /// Remove a vertex from the mesh.
    /// WARNING: this moves faces, edges and vertices around. All handles to
    /// faces and edges made before this point will become invalid. Same for
    /// iterators.
    pub fn remove_vertex(&mut self, vert_id: GeomId<0>) {
        let old_hedge_ids: Vec<_> = self
            .vert_handle(vert_id)
            .iter_hedges()
            .map(|x| x.id)
            .collect();

        // Each time an edge is deleted 2 hedges from the end of the list are swapped
        // into their place to keep continuity. So we need to keep track of the
        // permutations to make sure we delete the correct hedge each time.
        let mut old_new_id_map = HashMap::new();
        for oid in &old_hedge_ids {
            let handle = self.hedge_handle(*oid);
            old_new_id_map.insert(handle.id(), handle.id());
            old_new_id_map.insert(handle.pair().id(), handle.pair().id());
        }

        for oid in old_hedge_ids {
            // Make even.
            let nid_0 = *old_new_id_map.get(&GeomId((oid.0 / 2) * 2)).unwrap();
            let nid_1 = *old_new_id_map.get(&GeomId(nid_0.0 + 1)).unwrap();
            let last_id_0 = GeomId(self.hedge_count() as u64 - 2);
            let last_id_1 = GeomId(self.hedge_count() as u64 - 1);

            self.remove_edge(nid_0);

            *old_new_id_map.entry(last_id_0).or_insert(nid_0) = nid_0;
            *old_new_id_map.entry(last_id_1).or_insert(nid_1) = nid_1;
        }

        self.verts.borrow_mut().swap_remove(vert_id.0);
        self.verts_meta.borrow_mut().swap_remove(vert_id.0 as usize);
        // If this is true we just removed the last element, there's nothing to update.
        if vert_id.0 as usize >= self.vert_count() {
            return;
        }

        let v_handle = self.vert_handle(vert_id);

        self.verts_meta.borrow_mut()[vert_id.0 as usize].id = vert_id;
        for hedge in v_handle.iter_hedges() {
            self.hedges_meta.borrow_mut()[hedge.id().0 as usize].vert_id = vert_id;
        }
    }

    /// Warning: Can leave the mesh in a degenerate state.
    pub(crate) fn disable_face(&mut self, face_id: GeomId<2>) {
        self.faces_meta.borrow_mut()[face_id.0 as usize].enabled = false;
        let eids: Vec<_> = self
            .face_handle(face_id)
            .hedge()
            .iter_loop()
            .map(|e| e.id())
            .collect();

        let vids: Vec<_> = self
            .face_handle(face_id)
            .hedge()
            .iter_loop()
            .map(|e| e.source().id())
            .collect();

        let dids: Vec<_> = self
            .face_handle(face_id)
            .hedge()
            .iter_loop()
            .filter(|e| e.pair().is_boundary_hedge())
            .map(|e| e.id())
            .collect();

        for vid in vids {
            let vert = self.vert_handle(vid);
            let hedge_enabled =
                self.hedges_meta.borrow()[vert.hedge().unwrap().id().0 as usize].enabled;
            if !hedge_enabled {
                let good_hedge = vert.iter_hedges().find(|e| !dids.contains(&e.id()));
                match good_hedge {
                    Some(hedge) => {
                        self.verts_meta.borrow_mut()[vert.id().0 as usize].hedge_id =
                            hedge.id();
                    }
                    None => {
                        self.verts_meta.borrow_mut()[vert.id().0 as usize].hedge_id =
                            ABSENT()
                    }
                }
            }
        }

        for eid in eids {
            self.hedges_meta.borrow_mut()[eid.0 as usize].face_id = ABSENT();
        }

        for did in dids {
            self.hedges_meta.borrow_mut()[did.0 as usize].enabled = false;
            disable_hedge(did, self);
        }

        self.faces_meta.borrow_mut()[face_id.0 as usize].hedge_id = ABSENT();
    }

    /// Warning: Can leave the mesh in a degenerate state.
    pub(crate) fn disable_edge(&mut self, hedge_id: GeomId<1>) {
        debug_assert!((hedge_id.0 as usize) < self.hedge_count());
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].enabled = false;

        let mut hedge = self.hedge_handle(hedge_id);

        let v1 = hedge.source();
        let v2 = hedge.dest();

        let n1 = hedge.next();
        let p1 = hedge.pair().prev();

        let pair_id = hedge.pair().id();

        let n2 = hedge.pair().next();
        let p2 = hedge.prev();

        let f1 = hedge.face();
        let f2 = hedge.pair().face();

        let start_id = hedge.id();
        loop {
            hedge = hedge.next();

            self.hedges_meta.borrow_mut()[hedge.id().0 as usize].face_id = ABSENT();

            if start_id == hedge.id() {
                break;
            }
        }

        link_hedges(&p1, &n1, self);
        link_hedges(&p2, &n2, self);

        if f1.id != ABSENT() {
            self.faces_meta.borrow_mut()[f1.id().0 as usize].enabled = false;
            self.faces_meta.borrow_mut()[f1.id().0 as usize].hedge_id = ABSENT();
        }

        if f2.id != ABSENT() {
            self.faces_meta.borrow_mut()[f2.id().0 as usize].enabled = false;
            self.faces_meta.borrow_mut()[f2.id().0 as usize].hedge_id = ABSENT();
        }

        self.verts_meta.borrow_mut()[v1.id().0 as usize].hedge_id = n1.id();
        self.verts_meta.borrow_mut()[v2.id().0 as usize].hedge_id = n2.id();

        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].enabled = false;
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].next_id = ABSENT();
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].prev_id = ABSENT();
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].pair_id = ABSENT();
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].face_id = ABSENT();
        self.hedges_meta.borrow_mut()[hedge_id.0 as usize].vert_id = ABSENT();

        self.hedges_meta.borrow_mut()[pair_id.0 as usize].enabled = false;
        self.hedges_meta.borrow_mut()[pair_id.0 as usize].next_id = ABSENT();
        self.hedges_meta.borrow_mut()[pair_id.0 as usize].prev_id = ABSENT();
        self.hedges_meta.borrow_mut()[pair_id.0 as usize].pair_id = ABSENT();
        self.hedges_meta.borrow_mut()[pair_id.0 as usize].face_id = ABSENT();
        self.hedges_meta.borrow_mut()[pair_id.0 as usize].vert_id = ABSENT();
    }

    /// Warning: Can leave the mesh in a degenerate state.
    pub(crate) fn disable_vertex(&mut self, vert_id: GeomId<0>) {
        let hedge_id = self.verts_meta.borrow_mut()[vert_id.0 as usize].hedge_id;
        debug_assert!(hedge_id != ABSENT());

        if !self.hedges_meta.borrow_mut()[hedge_id.0 as usize].enabled {
            self.verts_meta.borrow_mut()[vert_id.0 as usize].hedge_id = ABSENT();
            return;
        }

        let old_hedge_ids: Vec<_> = self
            .vert_handle(vert_id)
            .iter_hedges()
            .map(|x| x.id)
            .collect();

        let old_face_ids: Vec<_> = self
            .vert_handle(vert_id)
            .iter_hedges()
            .map(|x| x.face().id)
            .collect();

        for oid in old_face_ids {
            if oid == ABSENT() {
                continue;
            }
            self.faces_meta.borrow_mut()[oid.0 as usize].enabled = false;
            self.faces_meta.borrow_mut()[oid.0 as usize].hedge_id = ABSENT();
        }

        for oid in old_hedge_ids {
            disable_hedge_on_vert(oid, self);
        }

        self.verts_meta.borrow_mut()[vert_id.0 as usize].enabled = false;
        self.verts_meta.borrow_mut()[vert_id.0 as usize].hedge_id = ABSENT();
    }

    pub fn find_boundary(&self) -> Option<HEdgeHandle<Verts, Edges, Faces>> {
        self.iter_edges()
            .find(|e| e.is_in_boundary_edge())
            .map(|h| h.get_boundary_hedge().unwrap())
    }

    pub fn new<I>(
        verts: Verts,
        edges: Edges,
        faces: Faces,
        topology: Vec<Vec<I>>,
    ) -> HalfMesh<Verts, Edges, Faces>
    where
        u64: TryFrom<I>,
        I: Copy,
    {
        let mesh = HalfMesh::<Verts, Edges, Faces> {
            verts: RefCell::new(verts),
            edges: RefCell::new(edges),
            faces: RefCell::new(faces),

            verts_meta: RefCell::new(Vec::new()),
            hedges_meta: RefCell::new(Vec::new()),
            faces_meta: RefCell::new(Vec::new()),
        };

        debug_assert!(
            verify_winding_order(&topology),
            "Winding order is inconsistent, check the order of the indices. \
            In general there should be no duplicate edge pairs."
        );
        let mut half_edges = HashMap::<(u64, u64), usize>::new();

        let vert_count = mesh.verts.borrow().len();
        for vert_id in 0..vert_count {
            mesh.verts_meta.borrow_mut().push(Vert {
                enabled: true,
                id: GeomId(vert_id as u64),
                hedge_id: ABSENT(),
            });
        }

        for face in topology.iter() {
            for he_id in 0..face.len() {
                let v0 = u64::try_from(face[he_id]).unwrap_or(0);
                let v1 = u64::try_from(face[(he_id + 1) % face.len()]).unwrap_or(0);
                debug_assert!(v0 < (vert_count as u64), "{v0} / {}", vert_count);
                debug_assert!(v1 < (vert_count as u64), "{v0} / {}", vert_count);
                if half_edges.contains_key(&(v0, v1)) {
                    continue;
                }

                let n = mesh.hedges_meta.borrow().len() as u64;
                mesh.hedges_meta
                    .borrow_mut()
                    .push(HEdge::new(n + 0, n + 1, v0));
                mesh.hedges_meta
                    .borrow_mut()
                    .push(HEdge::new(n + 1, n + 0, v1));

                half_edges.insert((v0, v1), (n + 0) as usize);
                half_edges.insert((v1, v0), (n + 1) as usize);
            }
        }

        for (face_id, face) in topology.iter().enumerate() {
            let mut hedge_id = !0;
            for he_id in 0..face.len() {
                let face_id = face_id as u64;

                let prev_id = he_id as i64 - 1 + face.len() as i64;
                let v0 = u64::try_from(face[he_id]).unwrap_or(0);
                let v1 = u64::try_from(face[(he_id + 1) % face.len()]).unwrap_or(0);
                let v2 = u64::try_from(face[(he_id + 2) % face.len()]).unwrap_or(0);
                let vp =
                    u64::try_from(face[(prev_id as usize) % face.len()]).unwrap_or(0);

                let current_id = *half_edges.get(&(v0, v1)).unwrap();
                let next_id = *half_edges.get(&(v1, v2)).unwrap();
                let prev_id = *half_edges.get(&(vp, v0)).unwrap();

                mesh.hedges_meta.borrow_mut()[current_id].next_id =
                    GeomId(next_id as u64);
                mesh.hedges_meta.borrow_mut()[current_id].prev_id =
                    GeomId(prev_id as u64);
                mesh.hedges_meta.borrow_mut()[current_id].face_id =
                    GeomId(face_id as u64);

                mesh.verts_meta.borrow_mut()[v0 as usize].hedge_id =
                    GeomId(current_id as u64);

                hedge_id = current_id as u64;
            }

            mesh.faces_meta.borrow_mut().push(Face {
                enabled: true,
                id: GeomId(face_id as u64),
                hedge_id: GeomId(hedge_id),
            });
        }

        // Identify each boundary half edge by its source vertex.
        let mut boundary_half_edges = HashMap::new();
        for hedge in mesh.hedges_meta.borrow_mut().iter() {
            if hedge.face_id == ABSENT() {
                let source = &mut mesh.verts_meta.borrow_mut()[hedge.vert_id.0 as usize];
                boundary_half_edges.insert(source.id, hedge.id);
            }
        }

        // Connect the boundary edges.
        for (_v_id, he_id) in &boundary_half_edges {
            let he_id = usize::from(*he_id);
            let pair_id = if he_id % 2 == 0 { he_id + 1 } else { he_id - 1 };
            let next_id = usize::from(
                *boundary_half_edges
                    .get(&mesh.hedges_meta.borrow()[pair_id].vert_id)
                    .unwrap(),
            );

            let hedge_id = mesh.hedges_meta.borrow()[he_id].id();
            let next_hedge_id = mesh.hedges_meta.borrow()[next_id].id();

            mesh.hedges_meta.borrow_mut()[he_id].next_id = next_hedge_id;
            mesh.hedges_meta.borrow_mut()[next_id].prev_id = hedge_id;
        }

        #[cfg(debug_assertions)]
        verify_topology(&mesh);

        mesh
    }

    pub fn get_verts(&self) -> Vec<Verts::PrimitiveData> {
        self.iter_verts().map(|v| v.data().clone()).collect()
    }

    pub fn get_topology(&self) -> Vec<Vec<u32>> {
        let mut topology = Vec::<Vec<u32>>::new();
        for face in self.faces_meta.borrow().iter() {
            let mut face_topology = Vec::<u32>::new();
            for edge in self.hedge_handle(face.hedge_id).iter_loop() {
                face_topology.push(edge.source().id.0 as u32);
            }
            topology.push(face_topology);
        }

        topology
    }

    /// Get the vertices of the mesh.
    pub fn collect_verts(&self) -> Verts {
        self.verts.borrow().clone()
    }

    pub fn verts_mut(&self) -> std::cell::RefMut<Verts> {
        self.verts.borrow_mut()
    }

    pub fn hedges_mut(&self) -> std::cell::RefMut<Edges> {
        self.edges.borrow_mut()
    }

    pub fn faces_mut(&self) -> std::cell::RefMut<Faces> {
        self.faces.borrow_mut()
    }
}

/// Test that all triangles have consistent winding order. If they don't returns
/// false.
pub fn verify_winding_order<I>(topology: &Vec<Vec<I>>) -> bool
where
    u64: TryFrom<I>,
    I: Copy,
{
    let mut half_edge_set = HashSet::new();
    for face in topology {
        for i in 0..face.len() {
            let p1 = u64::try_from(face[i]).unwrap_or(0);
            let p2 = u64::try_from(face[(i + 1) % face.len()]).unwrap_or(0);
            if !half_edge_set.insert((p1, p2)) {
                return false;
            }
        }
    }

    return true;
}

/// Always returns true, the return type is just so that this can be
/// called in asserts.
pub fn verify_topology<MeshType>(omesh: &MeshType) -> Option<String>
where
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let mesh = unsafe { &*omesh.inner_mesh() };
    for vert_index in 0..mesh.verts.borrow().len() {
        if !mesh.verts_meta.borrow()[vert_index].enabled
            // isolated vertices are topologically valid
            || mesh.verts_meta.borrow()[vert_index].hedge_id == ABSENT()
        {
            continue;
        }

        let vert_id = GeomId(vert_index as u64);
        let edge_id = mesh.verts_meta.borrow()[vert_index].hedge_id;
        let edge = &mesh.hedges_meta.borrow()[edge_id.0 as usize];

        if vert_id != edge.vert_id {
            return Some(format!(
                "vert_id {} does not match edge.vert_id {}",
                vert_id, edge.vert_id,
            ));
        }

        if vert_id != mesh.verts_meta.borrow()[vert_index].id {
            return Some(format!(
                "vertex index {} does not match its id {}",
                vert_id,
                mesh.verts_meta.borrow()[vert_index].id
            ));
        };
        if !mesh.hedges_meta.borrow()[edge_id.0 as usize].enabled {
            return Some(format!(
                "vertex {} points to a disabled edge {}",
                vert_id, edge_id
            ));
        }

        let v_handle = mesh.vert_handle(GeomId(vert_index as u64));
        if v_handle.id() != v_handle.hedge().unwrap().source().id() {
            return Some(
                "Vertex handle does not match source of hedge handle.".to_string(),
            );
        };
    }

    for hedge_index in 0..mesh.hedges_meta.borrow().len() {
        if !mesh.hedges_meta.borrow()[hedge_index].enabled {
            continue;
        }

        let hedge_id = GeomId(hedge_index as u64);
        let hedge = &mesh.hedges_meta.borrow()[hedge_index];

        if hedge.pair_id.0 as usize > mesh.hedges_meta.borrow().len()
            || hedge.next_id.0 as usize > mesh.hedges_meta.borrow().len()
            || hedge.prev_id.0 as usize > mesh.hedges_meta.borrow().len()
        {
            return Some(
                format!(
                    "An enabled hedge index is out of bounds pair {} prev {} next {}.",
                    hedge.pair_id.0, hedge.prev_id.0, hedge.next_id.0
                )
                .to_string(),
            );
        }

        let pair = &mesh.hedges_meta.borrow()[hedge.pair_id.0 as usize];
        let next = &mesh.hedges_meta.borrow()[hedge.next_id.0 as usize];
        let prev = &mesh.hedges_meta.borrow()[hedge.prev_id.0 as usize];

        if pair.pair_id != hedge.id {
            return Some(format!("Invalid pair {} of {}.", pair.pair_id, hedge.id));
        }
        if next.prev_id != hedge.id {
            return Some(format!(
                "Invalid next {} of {} pointing to {}.",
                next.id, hedge.id, next.prev_id
            ));
        }
        if prev.next_id != hedge.id {
            return Some(format!(
                "Invalid prev {} of {} pointing to {}.",
                prev.id, hedge.id, prev.next_id
            ));
        }
        if pair.vert_id != next.vert_id {
            return Some("Invalid vert_id".to_string());
        }

        if hedge.vert_id == pair.vert_id {
            return Some("Two hedges point to the same vertex.".to_string());
        };
        if hedge.vert_id == next.vert_id {
            return Some("Two hedges point to the same vertex.".to_string());
        };
        if hedge.vert_id == prev.vert_id {
            return Some("Two hedges point to the same vertex.".to_string());
        };

        if hedge_id != mesh.hedges_meta.borrow()[hedge_index].id {
            return Some("Hedge index does not match its id.".to_string());
        }
    }

    for face_index in 0..mesh.faces_meta.borrow().len() {
        if !mesh.faces_meta.borrow()[face_index].enabled {
            continue;
        }

        let face_id = GeomId(face_index as u64);
        let face = &mesh.faces_meta.borrow()[face_index];

        let edge = &mesh.hedges_meta.borrow()[face.hedge_id.0 as usize];

        if edge.face_id != face.id {
            return Some(format!(
                "edge.face_id {:?} does not match face.id {:?}",
                edge.face_id, face.id
            ));
        }
        if face_id != mesh.hedges_meta.borrow()[face_index].id {
            return Some("Face index does not match its id.".to_string());
        };
    }

    None
}

/// Always returns true, the return type is just so that this can be
/// called in asserts.
/// It will check for no duplicate points.
pub fn verify_geometry<MeshType, S>(mesh: &MeshType) -> bool
where
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
    <<MeshType as HalfMeshLike>::VertData as PrimitiveContainer>::PrimitiveData:
        InnerSpace<S>,
    S: RealField,
{
    for v_handle in mesh.iter_verts() {
        for o_handle in mesh.iter_verts() {
            if v_handle.id() == o_handle.id() {
                continue;
            }

            assert!(
                (v_handle.data() - o_handle.data()).norm()
                    >= S::from(f32::EPSILON * 10.0).unwrap()
            );
        }
    }

    for hedge in mesh.iter_edges() {
        let epsilon = S::from(f32::EPSILON * 10.0).unwrap();
        assert!((hedge.source().data() - hedge.dest().data()).norm() >= epsilon);
        assert!((hedge.source().data() - hedge.prev().source().data()).norm() >= epsilon);
        assert!((hedge.source().data() - hedge.next().source().data()).norm() >= epsilon);
        assert!(
            (hedge.pair().source().data() - hedge.pair().dest().data()).norm() >= epsilon
        );
    }

    // for face in mesh.iter_faces()
    // {
    //     for e in face.hedge().iter_loop()
    //     {
    //         let d1 = e.dir().normalized();
    //         let d2 = e.prev().dir().normalized() * -1.0;
    //         assert!(
    //             d1.dot(&d2).abs() <= 1.0 - f32::EPSILON,
    //             "{}",
    //             d1.dot(&d2).abs()
    //         )
    //     }
    // }

    true
}

pub fn mesh_is_closed<Verts, Edges, Faces>(mesh: &HalfMesh<Verts, Edges, Faces>) -> bool
where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    for hedge_index in 0..mesh.hedges_meta.borrow().len() {
        if mesh.hedges_meta.borrow()[hedge_index].face_id == ABSENT() {
            return false;
        }
    }

    return true;
}

// === Internal ===
fn disable_hedge<Verts, Edges, Faces>(
    eid: GeomId<1>,
    mesh: &mut HalfMesh<Verts, Edges, Faces>,
) where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    let enid = mesh.hedges_meta.borrow()[eid.0 as usize].next_id;
    let epid = mesh.hedges_meta.borrow()[eid.0 as usize].prev_id;

    let pid = mesh.hedges_meta.borrow()[eid.0 as usize].pair_id;
    let ppid = mesh.hedges_meta.borrow()[pid.0 as usize].prev_id;
    let pnid = mesh.hedges_meta.borrow()[pid.0 as usize].next_id;

    let veid = mesh.hedges_meta.borrow()[eid.0 as usize].vert_id;
    let vpid = mesh.hedges_meta.borrow()[pid.0 as usize].vert_id;

    mesh.verts_meta.borrow_mut()[vpid.0 as usize].hedge_id = enid;
    mesh.verts_meta.borrow_mut()[veid.0 as usize].hedge_id = pnid;

    mesh.hedges_meta.borrow_mut()[enid.0 as usize].vert_id = vpid;
    mesh.hedges_meta.borrow_mut()[pnid.0 as usize].vert_id = veid;

    mesh.hedges_meta.borrow_mut()[enid.0 as usize].prev_id = ppid;
    mesh.hedges_meta.borrow_mut()[ppid.0 as usize].next_id = enid;

    mesh.hedges_meta.borrow_mut()[epid.0 as usize].next_id = pnid;
    mesh.hedges_meta.borrow_mut()[pnid.0 as usize].prev_id = epid;

    mesh.hedges_meta.borrow_mut()[enid.0 as usize].face_id = ABSENT();
    mesh.hedges_meta.borrow_mut()[ppid.0 as usize].face_id = ABSENT();

    let erase = |e: GeomId<1>| {
        mesh.hedges_meta.borrow_mut()[e.0 as usize].enabled = false;
        mesh.hedges_meta.borrow_mut()[e.0 as usize].pair_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].next_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].prev_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].vert_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].face_id = ABSENT();
    };

    erase(eid);
    erase(pid);
}

fn disable_hedge_on_vert<Verts, Edges, Faces>(
    eid: GeomId<1>,
    mesh: &mut HalfMesh<Verts, Edges, Faces>,
) where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    let enid = mesh.hedges_meta.borrow()[eid.0 as usize].next_id;

    let pid = mesh.hedges_meta.borrow()[eid.0 as usize].pair_id;
    let ppid = mesh.hedges_meta.borrow()[pid.0 as usize].prev_id;

    let vpid = mesh.hedges_meta.borrow()[pid.0 as usize].vert_id;

    mesh.verts_meta.borrow_mut()[vpid.0 as usize].hedge_id = enid;

    mesh.hedges_meta.borrow_mut()[enid.0 as usize].prev_id = ppid;
    mesh.hedges_meta.borrow_mut()[ppid.0 as usize].next_id = enid;

    mesh.hedges_meta.borrow_mut()[enid.0 as usize].face_id = ABSENT();
    mesh.hedges_meta.borrow_mut()[ppid.0 as usize].face_id = ABSENT();

    let erase = |e: GeomId<1>| {
        mesh.hedges_meta.borrow_mut()[e.0 as usize].enabled = false;
        mesh.hedges_meta.borrow_mut()[e.0 as usize].pair_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].next_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].prev_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].vert_id = ABSENT();
        mesh.hedges_meta.borrow_mut()[e.0 as usize].face_id = ABSENT();
    };

    erase(eid);
    erase(pid);
}

fn link_hedges<Verts, Edges, Faces>(
    prev: &HEdgeHandle<Verts, Edges, Faces>,
    next: &HEdgeHandle<Verts, Edges, Faces>,
    mesh: &mut HalfMesh<Verts, Edges, Faces>,
) where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    mesh.hedges_meta.borrow_mut()[prev.id().0 as usize].next_id = next.id();
    mesh.hedges_meta.borrow_mut()[next.id().0 as usize].prev_id = prev.id();
}

impl<Verts, Edges, Faces> InternalMeshAccess<Verts, Edges, Faces>
    for HalfMesh<Verts, Edges, Faces>
where
    Verts: PrimitiveContainer,
    Edges: PrimitiveContainer,
    Faces: PrimitiveContainer,
{
    unsafe fn inner_mesh(&self) -> *mut HalfMesh<Verts, Edges, Faces>
    where
        Verts: PrimitiveContainer,
        Edges: PrimitiveContainer,
        Faces: PrimitiveContainer,
    {
        self as *const _ as *mut HalfMesh<Verts, Edges, Faces>
    }
}

/// Can only be used on convex faces!
// TODO(low): This can be made more optimal by modifying the pointers of the
// half edge instead of copying and reconstructing.
pub fn triangulate_faces<V>(mesh: &mut HalfMesh<V, (), ()>)
where
    V: PrimitiveContainer,
    V::PrimitiveData: VectorSpace<Scalar = f32>,
{
    let faces = mesh.get_topology();

    let mut new_faces = Vec::new();
    for face in faces {
        let mut face_ids = Vec::new();
        let points: Vec<GeomId<0>> =
            face.iter().map(|vid| GeomId::from(*vid as usize)).collect();

        let mut connectivity = Vec::new();
        for i in 1..points.len() - 1 {
            connectivity.push(0);
            connectivity.push(i);
            connectivity.push(i + 1);
        }

        for vid in connectivity {
            face_ids.push(points[vid].0);
        }

        new_faces.push(face_ids);
    }

    let verts = mesh.collect_verts();

    *mesh = HalfMesh::new(verts, (), (), new_faces);
}

/// Adds a new point to the center of each non triangular convex face and
/// create the associated fan.
pub fn triangulate_convex_faces_symmetrically<V>(mesh: &mut HalfMesh<V, (), ()>)
where
    V: PrimitiveContainer,
    V::PrimitiveData: VectorSpace<Scalar = f32> + Sum,
{
    let faces = mesh.get_topology();

    let mut new_faces = Vec::new();
    let mut verts = mesh.collect_verts();

    let mut dbg_centers = Vec::new();
    for face in faces {
        let points: Vec<GeomId<0>> =
            face.iter().map(|vid| GeomId::from(*vid as usize)).collect();

        if points.len() == 3 {
            new_faces.push(vec![points[0].0, points[1].0, points[2].0]);
            continue;
        }

        let mut center: V::PrimitiveData =
            points.iter().map(|id| mesh.vert_handle(*id).data()).sum();
        center = center * (1.0 / points.len() as f32);

        dbg_centers.push(Vec3::new(center[0], center[1], center[2]));

        let nv = verts.len();
        verts.push(center);
        for i in 0..points.len() {
            new_faces.push(vec![
                nv as u64,
                points[i].0,
                points[(i + 1) % points.len()].0,
            ])
        }
    }

    *mesh = HalfMesh::new(verts, (), (), new_faces);
}
