use spirv_std::glam::*;
extern crate bytemuck;

#[derive(Default, Clone, Copy)]
#[repr(C)]
pub struct Motor
{
    s: f32,
    e01: f32,
    e02: f32,
    e03: f32,
    e12: f32,
    e31: f32,
    e23: f32,
    i: f32,
}

impl Motor
{
    pub fn scalar_mul(mut self, rhs: f32) -> Motor
    {
        self.s *= rhs;
        self.e01 *= rhs;
        self.e02 *= rhs;
        self.e03 *= rhs;
        self.e12 *= rhs;
        self.e31 *= rhs;
        self.e23 *= rhs;
        self.i *= rhs;

        self
    }

    pub fn add(mut self, rhs: Motor) -> Motor
    {
        self.s += rhs.s;
        self.e01 += rhs.e01;
        self.e02 += rhs.e02;
        self.e03 += rhs.e03;
        self.e12 += rhs.e12;
        self.e31 += rhs.e31;
        self.e23 += rhs.e23;
        self.i += rhs.i;

        self
    }

    pub fn reverse(mut self) -> Motor
    {
        self.e01 *= -1.;
        self.e02 *= -1.;
        self.e03 *= -1.;
        self.e12 *= -1.;
        self.e31 *= -1.;
        self.e23 *= -1.;

        self
    }

    pub fn mul(&self, b: &Motor) -> Motor
    {
        let &Motor {
            s,
            e01,
            e02,
            e03,
            e12,
            e31,
            e23,
            i,
        } = self;

        let mut ret = Motor::default();
        ret.s = b.s * s - b.e12 * e12 - b.e31 * e31 - b.e23 * e23;

        ret.e01 = b.e01 * s + b.s * e01 - b.e12 * e02 + b.e31 * e03 + b.e02 * e12
            - b.e03 * e31
            - b.i * e23
            - b.e23 * i;
        ret.e02 =
            b.e02 * s + b.e12 * e01 + b.s * e02 - b.e23 * e03 - b.e01 * e12 - b.i * e31
                + b.e03 * e23
                - b.e31 * i;
        ret.e03 = b.e03 * s - b.e31 * e01 + b.e23 * e02 + b.s * e03 - b.i * e12
            + b.e01 * e31
            - b.e02 * e23
            - b.e12 * i;

        ret.e12 = b.e12 * s + b.s * e12 + b.e23 * e31 - b.e31 * e23;
        ret.e31 = b.e31 * s - b.e23 * e12 + b.s * e31 + b.e12 * e23;
        ret.e23 = b.e23 * s + b.e31 * e12 - b.e12 * e31 + b.s * e23;

        ret.i = b.i * s
            + b.e23 * e01
            + b.e31 * e02
            + b.e12 * e03
            + b.e03 * e12
            + b.e02 * e31
            + b.e01 * e23
            + b.s * i;

        ret
    }

    // pub fn motor_to_mat4_covariant(mut self) -> Mat4
    // {
    //     let Motor {
    //         s,
    //         e01,
    //         e02,
    //         e03,
    //         e12,
    //         e31,
    //         e23,
    //         i,
    //     } = self;

    //     let r44 = e12 * e12;
    //     let r55 = e31 * e31;
    //     let r66 = e23 * e23;

    //     let r45 = e12 * e31;
    //     let r46 = e12 * e23;
    //     let r56 = e31 * e23;

    //     let r04 = s * e12;
    //     let r05 = s * e31;
    //     let r06 = s * e23;

    //     let mut res = Mat4::default();
    //     res.col_mut(0)[0] = 1. + 2. * (-r44 - r55);
    //     res.col_mut(1)[0] = 2. * (r04 + r56);
    //     res.col_mut(2)[0] = 2. * (r46 - r05);
    //     res.col_mut(3)[0] = 2. * (e03 * e31 - e02 * e12 - s * e01 - i * e23);

    //     res.col_mut(0)[1] = 2. * (r56 - r04);
    //     res.col_mut(1)[1] = 1. + 2. * (-r44 - r66);
    //     res.col_mut(2)[1] = 2. * (r06 + r45);
    //     res.col_mut(3)[1] = 2. * (e01 * e12 - e03 * e23 - s * e02 - i * e31);

    //     res.col_mut(0)[2] = 2. * (r05 + r46);
    //     res.col_mut(1)[2] = 2. * (r45 - r06);
    //     res.col_mut(2)[2] = 1. + 2. * (-r55 - r66);
    //     res.col_mut(3)[2] = 2. * (e02 * e23 - e01 * e31 - s * e03 - i * e12);

    //     res.col_mut(0)[3] = 0.;
    //     res.col_mut(1)[3] = 0.;
    //     res.col_mut(2)[3] = 0.;
    //     res.col_mut(3)[3] = 1.;

    //     res
    // }

    // pub fn exp(mut self) -> Motor
    // {
    //     let Motor {
    //         s,
    //         e01,
    //         e02,
    //         e03,
    //         e12,
    //         e31,
    //         e23,
    //         i,
    //     } = self;

    //     let l = e12 * e12 + e31 * e31 + e23 * e23;
    //     let m = e12 * e03 + e23 * e01 + e31 * e02;
    //     let a = l.sqrt();
    //     let b = m / l;
    //     let c = a.cos();
    //     let s = if a == 0. { 1. } else { a.sin() / a };

    //     // if output is a translation, l=0, therefore c=1, s=0
    //     let t = b * (c - s); // 0 if translation

    //     let mut new_motor = Motor::default();
    //     new_motor.s = c;
    //     new_motor.e01 = s * e01 + t * e23;
    //     new_motor.e02 = s * e02 + t * e31;
    //     new_motor.e03 = s * e03 + t * e12;
    //     new_motor.e12 = s * e12;
    //     new_motor.e31 = s * e31;
    //     new_motor.e23 = s * e23;
    //     new_motor.i = s * m;

    //     new_motor
    // }

    // pub fn log(mut self) -> Motor
    // {
    //     let Motor {
    //         s,
    //         e01,
    //         e02,
    //         e03,
    //         e12,
    //         e31,
    //         e23,
    //         i,
    //     } = self;

    //     let acosScalar = s.acos(); // 0 if s == 1.
    //     let A = if acosScalar == 0.
    //     {
    //         1.
    //     }
    //     else
    //     {
    //         acosScalar.sin() / acosScalar
    //     }; // 1
    //     let mn = A * acosScalar; // 0
    //     let b = 1. / A; // 1
    //     let cN = i * (mn - s * acosScalar); // 0
    //     let cD = mn * mn * mn; // 0
    //     let c = cN / (if cD == 0. { 1. } else { cD });
    //     //if cD is 0, cN is 0 anyway

    //     let mut new_motor = Motor::default();
    //     new_motor.s = 0.;
    //     new_motor.e01 = c * e23 + b * e01;
    //     new_motor.e02 = c * e31 + b * e02;
    //     new_motor.e03 = c * e12 + b * e03;
    //     new_motor.e12 = e12 * b;
    //     //if s == 1, e12, e31, e23 would all be 0 anyway
    //     new_motor.e31 = e31 * b;
    //     new_motor.e23 = e23 * b;
    //     new_motor.i = 0.;

    //     new_motor
    // }

    pub fn sandwich(&self, b: &Vec4) -> Vec4
    {
        let &Motor {
            s,
            e01,
            e02,
            e03,
            e12,
            e31,
            e23,
            i,
        } = self;

        let rr0 = e12 * b.z + e31 * b.y + e23 * b.x - i * 1.0;
        let rr1 = -e23 * 1.0;
        let rr2 = -e31 * 1.0;
        let rr3 = -e12 * 1.0;
        let rr4 = s * b.z - e03 * 1.0 + e31 * b.x - e23 * b.y;
        let rr5 = s * b.y - e02 * 1.0 - e12 * b.x + e23 * b.z;
        let rr6 = s * b.x - e01 * 1.0 + e12 * b.y - e31 * b.z;
        let rr7 = s * 1.0;

        let mut ret = Vec4::default();
        ret.x =
            rr0 * e23 + rr1 * i - rr2 * e03 + rr3 * e02 - rr4 * e31 + rr5 * e12 + rr6 * s
                - rr7 * e01;
        ret.y = rr0 * e31 + rr1 * e03 + rr2 * i - rr3 * e01 + rr4 * e23 + rr5 * s
            - rr6 * e12
            - rr7 * e02;
        ret.z = rr0 * e12 - rr1 * e02 + rr2 * e01 + rr3 * i + rr4 * s - rr5 * e23
            + rr6 * e31
            - rr7 * e03;

        let div = -rr1 * e23 - rr2 * e31 - rr3 * e12 + rr7 * s;

        ret.x = ret.x / div;
        ret.y = ret.y / div;
        ret.z = ret.z / div;

        ret.w = 1.;

        ret
    }
}

pub fn zero_motor() -> Motor
{
    let mut ret = Motor::default();
    ret.s = 0.;
    ret.e01 = 0.;
    ret.e02 = 0.;
    ret.e03 = 0.;
    ret.e12 = 0.;
    ret.e31 = 0.;
    ret.e23 = 0.;
    ret.i = 0.;

    ret
}

pub fn low_quality_blend(x: Motor, y: Motor, z: Motor, w: Motor, weights: Vec4) -> Motor
{
    let mut new_motor = Motor::default();

    new_motor.s = weights.x * x.s + weights.y * y.s + weights.z * z.s + weights.w * w.s;
    new_motor.e01 =
        weights.x * x.e01 + weights.y * y.e01 + weights.z * z.e01 + weights.w * w.e01;
    new_motor.e02 =
        weights.x * x.e02 + weights.y * y.e02 + weights.z * z.e02 + weights.w * w.e02;
    new_motor.e03 =
        weights.x * x.e03 + weights.y * y.e03 + weights.z * z.e03 + weights.w * w.e03;
    new_motor.e12 =
        weights.x * x.e12 + weights.y * y.e12 + weights.z * z.e12 + weights.w * w.e12;
    new_motor.e31 =
        weights.x * x.e31 + weights.y * y.e31 + weights.z * z.e31 + weights.w * w.e31;
    new_motor.e23 =
        weights.x * x.e23 + weights.y * y.e23 + weights.z * z.e23 + weights.w * w.e23;
    new_motor.i = weights.x * x.i + weights.y * y.i + weights.z * z.i + weights.w * w.i;

    new_motor
}
