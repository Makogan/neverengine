use std::mem::size_of;

use crate::rendering::*;

pub trait GpuApi
{
    fn allocate_buffer<T: IntoPtrSize>(&mut self, data: &T) -> GpuMemoryHandle;

    fn allocate_image(&mut self, image: &RawImageData) -> GpuMemoryHandle;
    fn destroy_image(&mut self, image: GpuMemoryHandle);
    fn add_shader<T>(&mut self, shader_data: T) -> ShaderHandle
    where
        ShaderRepresentation: From<T>;
    fn destroy_buffer(&mut self, buffer: GpuMemoryHandle);
    fn update_buffer(&mut self, buffer: &GpuMemoryHandle, data: *const u8, size: usize);
    // TODO(low): merge all update buffer functions using traits.
    fn update_buffer_from_slice<T>(&mut self, buffer: &GpuMemoryHandle, data: &[T])
    {
        self.update_buffer(
            buffer,
            data.as_ptr() as *const u8,
            data.len() * size_of::<T>(),
        );
    }

    fn draw(&mut self, render_request: &RenderRequest);
    fn compute(&mut self, compute_request: &ComputeRequest);
    fn read_memory(&mut self, resource: &GpuMemoryHandle) -> Vec<u8>;

    fn free_input(&mut self, mut input: GraphicsInput)
    {
        for buffer in &mut input.buffers
        {
            self.destroy_buffer(GpuMemoryHandle::Buffer(*buffer));
        }

        if input.index_buffer.0 != 0
        {
            self.destroy_buffer(GpuMemoryHandle::Buffer(input.index_buffer));
        }
    }
}

pub enum ShaderRepresentation
{
    Paths
    {
        data_paths: Vec<String>
    },
    Sources
    {
        names: Vec<String>,
        sources: Vec<String>,
    },
    Raw
    {
        names: Vec<String>,
        raw_sources: Vec<Vec<u8>>,
        metadata: String,
    },
}

/// Use when you have direct paths to the shader crate (rustgpu) or files
/// (HLSL/GLSL).
impl<T> From<Vec<T>> for ShaderRepresentation
where
    String: From<T>,
{
    fn from(paths: Vec<T>) -> Self
    {
        Self::Paths {
            data_paths: paths.into_iter().map(String::from).collect(),
        }
    }
}

/// Use when you have source files already in RAM, the first array are names
/// you want to use to identify the sources, the second are the sources.
impl<T> From<(Vec<T>, Vec<T>)> for ShaderRepresentation
where
    String: From<T>,
{
    fn from(names_and_sources: (Vec<T>, Vec<T>)) -> Self
    {
        Self::Sources {
            names: names_and_sources.0.into_iter().map(String::from).collect(),
            sources: names_and_sources.1.into_iter().map(String::from).collect(),
        }
    }
}

impl From<(Vec<String>, Vec<Vec<u8>>, String)> for ShaderRepresentation
{
    fn from(names_and_byte_data: (Vec<String>, Vec<Vec<u8>>, String)) -> Self
    {
        Self::Raw {
            names: names_and_byte_data.0,
            raw_sources: names_and_byte_data.1,
            metadata: names_and_byte_data.2,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct GraphicsInput
{
    pub buffers: Vec<BufferHandle>,
    /// Set to a 0 initialized buffer to avoid using indexing. But in that case,
    /// set `index_count` to the vertex count.
    pub index_buffer: BufferHandle,
    pub index_count: usize,
}

impl GraphicsInput
{
    pub fn new() -> Self { Self::default() }

    pub fn get_attribute_buffer_handle(&self, i: usize) -> BufferHandle
    {
        self.buffers[i]
    }

    /// Add data as a buffer. For example vertex positions. Can be interleaved
    /// data.
    pub fn add_attribute_buffer_from_slice<T, RHI>(
        mut self,
        data: &[T],
        api: &mut RHI,
    ) -> Self
    where
        RHI: GpuApi,
    {
        let buffer = api.allocate_buffer(&data).to_buffer_handle();
        self.buffers.push(buffer);

        if self.index_count == 0
        {
            self.index_count = data.len();
        }

        self
    }

    /// Add data as a buffer. For example vertex positions. Can be interleaved
    /// data.
    pub fn add_attribute_buffer_from_ptr<T, RHI>(
        mut self,
        data: *const T,
        size: usize,
        api: &mut RHI,
    ) -> Self
    where
        RHI: GpuApi,
    {
        let buffer = api.allocate_buffer(&(data, size)).to_buffer_handle();
        self.buffers.push(buffer);

        if self.index_count == 0
        {
            self.index_count = size / size_of::<T>();
        }

        self
    }

    /// Replace the existing buffer (will call a de-allocation).
    pub fn set_index_buffer_from_slice<RHI>(mut self, data: &[u32], api: &mut RHI) -> Self
    where
        RHI: GpuApi,
    {
        // Clean up to avoid memleaks.
        if self.index_buffer != BufferHandle::from_raw(0)
        {
            api.destroy_buffer(GpuMemoryHandle::Buffer(self.index_buffer));
        }

        let buffer = api.allocate_buffer(&data).to_buffer_handle();
        self.index_buffer = buffer;
        self.index_count = data.len();

        self
    }

    /// Replace the existing buffer (will call a de-allocation).
    pub fn set_index_buffer_from_ptr<T, RHI>(
        mut self,
        data: *const T,
        size: usize,
        api: &mut RHI,
    ) -> Self
    where
        RHI: GpuApi,
    {
        // Clean up to avoid memleaks.
        if self.index_buffer != BufferHandle::from_raw(0)
        {
            api.destroy_buffer(GpuMemoryHandle::Buffer(self.index_buffer));
        }

        let buffer = api.allocate_buffer(&(data, size)).to_buffer_handle();
        self.index_buffer = buffer;
        self.index_count = size / size_of::<T>();

        self
    }

    pub fn set_count(mut self, count: usize) -> Self
    {
        self.index_count = count;

        self
    }
}

#[derive(Debug, Default, Clone)]
pub struct GraphicsInputModifiers
{
    // TODO(medium): this should be an array instead of a single one.
    /// Optional.
    pub instance_buffer: BufferHandle,
    /// Optional, 0 means not instanced.
    pub instance_count: usize,
    /// Optional, inner offset of the vertex data.
    pub vertex_offset: usize,
    /// Optional, inner offset of the index data.
    pub index_offset: usize,
    /// Optional, number of subvertices to render.
    pub element_count: usize,
}

pub trait IntoPtrSize
{
    fn into_ptr_size(&self) -> (*const u8, usize);
}

impl<T> IntoPtrSize for &[T]
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.as_ptr() as *const u8, self.len() * size_of::<T>())
    }
}

impl<T> IntoPtrSize for [T]
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.as_ptr() as *const u8, self.len() * size_of::<T>())
    }
}

impl<T, const N: usize> IntoPtrSize for [T; N]
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.as_ptr() as *const u8, self.len() * size_of::<T>())
    }
}

impl<T> IntoPtrSize for Vec<T>
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.as_ptr() as *const u8, self.len() * size_of::<T>())
    }
}

impl<T> IntoPtrSize for &Vec<T>
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.as_ptr() as *const u8, self.len() * size_of::<T>())
    }
}

impl<T> IntoPtrSize for (*const T, usize)
{
    fn into_ptr_size(&self) -> (*const u8, usize)
    {
        (self.0 as *const u8, self.1 * size_of::<T>())
    }
}
