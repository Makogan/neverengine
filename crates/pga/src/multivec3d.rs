use std::fmt;
use std::ops::{Add, BitAnd, BitXor, Div, Index, IndexMut, Neg, Sub};

use algebra::*;

use crate::*;

#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct MultiVec3D
{
    coeffs: [f32; BASIS_COUNT],
}

impl MultiVec3D
{
    pub const fn zero() -> Self
    {
        Self {
            coeffs: [0.0; BASIS_COUNT],
        }
    }

    pub const fn identity() -> Self
    {
        Self {
            coeffs: [
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0,
            ],
        }
    }

    pub const fn new(f: f32, idx: usize) -> Self
    {
        let mut ret = Self::zero();
        ret.coeffs[idx] = f;
        ret
    }

    pub fn new_upper_grade2(a: f32, b: f32, c: f32, d: f32) -> Self
    {
        let mut ret = Self::zero();
        *ret.s_mut() = a;
        *ret.e23_mut() = b;
        *ret.e31_mut() = c;
        *ret.e12_mut() = d;
        ret
    }

    pub fn from_vec3(vec: &Vec3) -> Self
    {
        let mut ret = Self::zero();
        *ret.e032_mut() = vec.x;
        *ret.e013_mut() = vec.y;
        *ret.e021_mut() = vec.z;
        *ret.e123_mut() = 1.0;
        ret
    }

    pub fn from_motor(motor: &Motor) -> Self
    {
        let mut ret = Self::zero();
        *ret.s_mut() = motor.s();
        *ret.e01_mut() = motor.e01();
        *ret.e02_mut() = motor.e02();
        *ret.e03_mut() = motor.e03();
        *ret.e12_mut() = motor.e12();
        *ret.e31_mut() = motor.e31();
        *ret.e23_mut() = motor.e23();
        *ret.e0123_mut() = motor.e0123();

        ret
    }
}

impl Index<usize> for MultiVec3D
{
    type Output = f32;

    fn index<'a>(&'a self, index: usize) -> &'a Self::Output { &self.coeffs[index] }
}

impl IndexMut<usize> for MultiVec3D
{
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut Self::Output
    {
        &mut self.coeffs[index]
    }
}

impl fmt::Display for MultiVec3D
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        let mut n = 0;
        let ret = self
            .coeffs
            .iter()
            .enumerate()
            .filter_map(|(i, &coeff)| {
                if coeff.abs() != 0.000000
                {
                    n = 1;
                    Some(format!(
                        "{}{}",
                        format!("{:.*}", 16, coeff)
                            .trim_end_matches('0')
                            .trim_end_matches('.'),
                        if i > 0 { BASIS[i] } else { "" }
                    ))
                }
                else
                {
                    None
                }
            })
            .collect::<Vec<String>>()
            .join(" + ");
        if n == 0
        {
            write!(f, "0")
        }
        else
        {
            write!(f, "{}", ret)
        }
    }
}

impl Mul for MultiVec3D
{
    type Output = MultiVec3D;

    fn mul(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() =
            b.s() * a.s() + b.e1() * a.e1() + b.e2() * a.e2() + b.e3() * a.e3()
                - b.e12() * a.e12()
                - b.e31() * a.e31()
                - b.e23() * a.e23()
                - b.e123() * a.e123();
        *res.e0_mut() = b.e0() * a.s() + b.s() * a.e0()
            - b.e01() * a.e1()
            - b.e02() * a.e2()
            - b.e03() * a.e3()
            + b.e1() * a.e01()
            + b.e2() * a.e02()
            + b.e3() * a.e03()
            + b.e021() * a.e12()
            + b.e013() * a.e31()
            + b.e032() * a.e23()
            + b.e12() * a.e021()
            + b.e31() * a.e013()
            + b.e23() * a.e032()
            + b.e0123() * a.e123()
            - b.e123() * a.e0123();
        *res.e1_mut() = b.e1() * a.s() + b.s() * a.e1() - b.e12() * a.e2()
            + b.e31() * a.e3()
            + b.e2() * a.e12()
            - b.e3() * a.e31()
            - b.e123() * a.e23()
            - b.e23() * a.e123();
        *res.e2_mut() = b.e2() * a.s() + b.e12() * a.e1() + b.s() * a.e2()
            - b.e23() * a.e3()
            - b.e1() * a.e12()
            - b.e123() * a.e31()
            + b.e3() * a.e23()
            - b.e31() * a.e123();
        *res.e3_mut() =
            b.e3() * a.s() - b.e31() * a.e1() + b.e23() * a.e2() + b.s() * a.e3()
                - b.e123() * a.e12()
                + b.e1() * a.e31()
                - b.e2() * a.e23()
                - b.e12() * a.e123();
        *res.e01_mut() =
            b.e01() * a.s() + b.e1() * a.e0() - b.e0() * a.e1() - b.e021() * a.e2()
                + b.e013() * a.e3()
                + b.s() * a.e01()
                - b.e12() * a.e02()
                + b.e31() * a.e03()
                + b.e02() * a.e12()
                - b.e03() * a.e31()
                - b.e0123() * a.e23()
                - b.e2() * a.e021()
                + b.e3() * a.e013()
                + b.e123() * a.e032()
                - b.e032() * a.e123()
                - b.e23() * a.e0123();
        *res.e02_mut() = b.e02() * a.s() + b.e2() * a.e0() + b.e021() * a.e1()
            - b.e0() * a.e2()
            - b.e032() * a.e3()
            + b.e12() * a.e01()
            + b.s() * a.e02()
            - b.e23() * a.e03()
            - b.e01() * a.e12()
            - b.e0123() * a.e31()
            + b.e03() * a.e23()
            + b.e1() * a.e021()
            + b.e123() * a.e013()
            - b.e3() * a.e032()
            - b.e013() * a.e123()
            - b.e31() * a.e0123();
        *res.e03_mut() = b.e03() * a.s() + b.e3() * a.e0() - b.e013() * a.e1()
            + b.e032() * a.e2()
            - b.e0() * a.e3()
            - b.e31() * a.e01()
            + b.e23() * a.e02()
            + b.s() * a.e03()
            - b.e0123() * a.e12()
            + b.e01() * a.e31()
            - b.e02() * a.e23()
            + b.e123() * a.e021()
            - b.e1() * a.e013()
            + b.e2() * a.e032()
            - b.e021() * a.e123()
            - b.e12() * a.e0123();
        *res.e12_mut() = b.e12() * a.s() + b.e2() * a.e1() - b.e1() * a.e2()
            + b.e123() * a.e3()
            + b.s() * a.e12()
            + b.e23() * a.e31()
            - b.e31() * a.e23()
            + b.e3() * a.e123();
        *res.e31_mut() =
            b.e31() * a.s() - b.e3() * a.e1() + b.e123() * a.e2() + b.e1() * a.e3()
                - b.e23() * a.e12()
                + b.s() * a.e31()
                + b.e12() * a.e23()
                + b.e2() * a.e123();
        *res.e23_mut() = b.e23() * a.s() + b.e123() * a.e1() + b.e3() * a.e2()
            - b.e2() * a.e3()
            + b.e31() * a.e12()
            - b.e12() * a.e31()
            + b.s() * a.e23()
            + b.e1() * a.e123();
        *res.e021_mut() = b.e021() * a.s() - b.e12() * a.e0() + b.e02() * a.e1()
            - b.e01() * a.e2()
            + b.e0123() * a.e3()
            - b.e2() * a.e01()
            + b.e1() * a.e02()
            - b.e123() * a.e03()
            - b.e0() * a.e12()
            + b.e032() * a.e31()
            - b.e013() * a.e23()
            + b.s() * a.e021()
            + b.e23() * a.e013()
            - b.e31() * a.e032()
            + b.e03() * a.e123()
            - b.e3() * a.e0123();
        *res.e013_mut() = b.e013() * a.s() - b.e31() * a.e0() - b.e03() * a.e1()
            + b.e0123() * a.e2()
            + b.e01() * a.e3()
            + b.e3() * a.e01()
            - b.e123() * a.e02()
            - b.e1() * a.e03()
            - b.e032() * a.e12()
            - b.e0() * a.e31()
            + b.e021() * a.e23()
            - b.e23() * a.e021()
            + b.s() * a.e013()
            + b.e12() * a.e032()
            + b.e02() * a.e123()
            - b.e2() * a.e0123();
        *res.e032_mut() =
            b.e032() * a.s() - b.e23() * a.e0() + b.e0123() * a.e1() + b.e03() * a.e2()
                - b.e02() * a.e3()
                - b.e123() * a.e01()
                - b.e3() * a.e02()
                + b.e2() * a.e03()
                + b.e013() * a.e12()
                - b.e021() * a.e31()
                - b.e0() * a.e23()
                + b.e31() * a.e021()
                - b.e12() * a.e013()
                + b.s() * a.e032()
                + b.e01() * a.e123()
                - b.e1() * a.e0123();
        *res.e123_mut() = b.e123() * a.s()
            + b.e23() * a.e1()
            + b.e31() * a.e2()
            + b.e12() * a.e3()
            + b.e3() * a.e12()
            + b.e2() * a.e31()
            + b.e1() * a.e23()
            + b.s() * a.e123();
        *res.e0123_mut() = b.e0123() * a.s()
            + b.e123() * a.e0()
            + b.e032() * a.e1()
            + b.e013() * a.e2()
            + b.e021() * a.e3()
            + b.e23() * a.e01()
            + b.e31() * a.e02()
            + b.e12() * a.e03()
            + b.e03() * a.e12()
            + b.e02() * a.e31()
            + b.e01() * a.e23()
            - b.e3() * a.e021()
            - b.e2() * a.e013()
            - b.e1() * a.e032()
            - b.e0() * a.e123()
            + b.s() * a.e0123();
        res
    }
}

impl Mul<Rotor> for MultiVec3D
{
    type Output = MultiVec3D;

    fn mul(self: MultiVec3D, b: Rotor) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() =
            b.s() * a.s() - b.e12() * a.e12() - b.e31() * a.e31() - b.e23() * a.e23();
        *res.e0_mut() =
            b.s() * a.e0() + b.e12() * a.e021() + b.e31() * a.e013() + b.e23() * a.e032();
        *res.e1_mut() =
            b.s() * a.e1() - b.e12() * a.e2() + b.e31() * a.e3() - b.e23() * a.e123();
        *res.e2_mut() =
            b.e12() * a.e1() + b.s() * a.e2() - b.e23() * a.e3() - b.e31() * a.e123();
        *res.e3_mut() =
            -b.e31() * a.e1() + b.e23() * a.e2() + b.s() * a.e3() - b.e12() * a.e123();
        *res.e01_mut() =
            b.s() * a.e01() - b.e12() * a.e02() + b.e31() * a.e03() - b.e23() * a.e0123();
        *res.e02_mut() =
            b.e12() * a.e01() + b.s() * a.e02() - b.e23() * a.e03() - b.e31() * a.e0123();
        *res.e03_mut() = -b.e31() * a.e01() + b.e23() * a.e02() + b.s() * a.e03()
            - b.e12() * a.e0123();
        *res.e12_mut() =
            b.e12() * a.s() + b.s() * a.e12() + b.e23() * a.e31() - b.e31() * a.e23();
        *res.e31_mut() =
            b.e31() * a.s() - b.e23() * a.e12() + b.s() * a.e31() + b.e12() * a.e23();
        *res.e23_mut() =
            b.e23() * a.s() + b.e31() * a.e12() - b.e12() * a.e31() + b.s() * a.e23();
        *res.e021_mut() = -b.e12() * a.e0() + b.s() * a.e021() + b.e23() * a.e013()
            - b.e31() * a.e032();
        *res.e013_mut() = -b.e31() * a.e0() - b.e23() * a.e021()
            + b.s() * a.e013()
            + b.e12() * a.e032();
        *res.e032_mut() = -b.e23() * a.e0() + b.e31() * a.e021() - b.e12() * a.e013()
            + b.s() * a.e032();
        *res.e123_mut() =
            b.e23() * a.e1() + b.e31() * a.e2() + b.e12() * a.e3() + b.s() * a.e123();
        *res.e0123_mut() =
            b.e23() * a.e01() + b.e31() * a.e02() + b.e12() * a.e03() + b.s() * a.e0123();
        res
    }
}

impl<T> Mul<T> for MultiVec3D
where
    T: Default + CPointLike,
{
    type Output = MultiVec3D;

    fn mul(self: MultiVec3D, b: T) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = -a.e123();
        *res.e0_mut() =
            b.e021() * a.e12() + b.e013() * a.e31() + b.e032() * a.e23() - a.e0123();
        *res.e1_mut() = -a.e23();
        *res.e2_mut() = -a.e31();
        *res.e3_mut() = -a.e12();
        *res.e01_mut() =
            -b.e021() * a.e2() + b.e013() * a.e3() + a.e032() - b.e032() * a.e123();
        *res.e02_mut() =
            b.e021() * a.e1() - b.e032() * a.e3() + a.e013() - b.e013() * a.e123();
        *res.e03_mut() =
            -b.e013() * a.e1() + b.e032() * a.e2() + a.e021() - b.e021() * a.e123();
        *res.e12_mut() = a.e3();
        *res.e31_mut() = a.e2();
        *res.e23_mut() = a.e1();
        *res.e021_mut() =
            b.e021() * a.s() - a.e03() + b.e032() * a.e31() - b.e013() * a.e23();
        *res.e013_mut() =
            b.e013() * a.s() - a.e02() - b.e032() * a.e12() + b.e021() * a.e23();
        *res.e032_mut() =
            b.e032() * a.s() - a.e01() + b.e013() * a.e12() - b.e021() * a.e31();
        *res.e123_mut() = a.s();
        *res.e0123_mut() =
            a.e0() + b.e032() * a.e1() + b.e013() * a.e2() + b.e021() * a.e3();
        res
    }
}

// TODO this is bugged, results on normals don't look right.
impl Mul<Point> for MultiVec3D
{
    type Output = MultiVec3D;

    fn mul(self: MultiVec3D, b: Point) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = -a.e123() * b.e123();
        *res.e0_mut() = b.e021() * a.e12() + b.e013() * a.e31() + b.e032() * a.e23()
            - a.e0123() * b.e123();
        *res.e1_mut() = -b.e123() * a.e23();
        *res.e2_mut() = -b.e123() * a.e31();
        *res.e3_mut() = -b.e123() * a.e12();
        *res.e01_mut() = -b.e021() * a.e2() + b.e013() * a.e3() + b.e123() * a.e032()
            - b.e032() * a.e123();
        *res.e02_mut() = b.e021() * a.e1() - b.e032() * a.e3() + b.e123() * a.e013()
            - b.e013() * a.e123();
        *res.e03_mut() = -b.e013() * a.e1() + b.e032() * a.e2() + b.e123() * a.e021()
            - b.e021() * a.e123();
        *res.e12_mut() = b.e123() * a.e3();
        *res.e31_mut() = b.e123() * a.e2();
        *res.e23_mut() = b.e123() * a.e1();
        *res.e021_mut() = b.e021() * a.s() - b.e123() * a.e03() + b.e032() * a.e31()
            - b.e013() * a.e23();
        *res.e013_mut() = b.e013() * a.s() - b.e123() * a.e02() - b.e032() * a.e12()
            + b.e021() * a.e23();
        *res.e032_mut() = b.e032() * a.s() - b.e123() * a.e01() + b.e013() * a.e12()
            - b.e021() * a.e31();
        *res.e123_mut() = a.s();
        *res.e0123_mut() =
            b.e123() * a.e0() + b.e032() * a.e1() + b.e013() * a.e2() + b.e021() * a.e3();
        res
    }
}

// smul
// scalar/multivector multiplication
impl Mul<MultiVec3D> for f32
{
    type Output = MultiVec3D;

    fn mul(self: f32, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a * b.s();
        *res.e0_mut() = a * b.e0();
        *res.e1_mut() = a * b.e1();
        *res.e2_mut() = a * b.e2();
        *res.e3_mut() = a * b.e3();
        *res.e01_mut() = a * b.e01();
        *res.e02_mut() = a * b.e02();
        *res.e03_mut() = a * b.e03();
        *res.e12_mut() = a * b.e12();
        *res.e31_mut() = a * b.e31();
        *res.e23_mut() = a * b.e23();
        *res.e021_mut() = a * b.e021();
        *res.e013_mut() = a * b.e013();
        *res.e032_mut() = a * b.e032();
        *res.e123_mut() = a * b.e123();
        *res.e0123_mut() = a * b.e0123();
        res
    }
}


// muls
// multivector/scalar multiplication
impl Mul<f32> for MultiVec3D
{
    type Output = MultiVec3D;

    fn mul(self: MultiVec3D, b: f32) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s() * b;
        *res.e0_mut() = a.e0() * b;
        *res.e1_mut() = a.e1() * b;
        *res.e2_mut() = a.e2() * b;
        *res.e3_mut() = a.e3() * b;
        *res.e01_mut() = a.e01() * b;
        *res.e02_mut() = a.e02() * b;
        *res.e03_mut() = a.e03() * b;
        *res.e12_mut() = a.e12() * b;
        *res.e31_mut() = a.e31() * b;
        *res.e23_mut() = a.e23() * b;
        *res.e021_mut() = a.e021() * b;
        *res.e013_mut() = a.e013() * b;
        *res.e032_mut() = a.e032() * b;
        *res.e123_mut() = a.e123() * b;
        *res.e0123_mut() = a.e0123() * b;
        res
    }
}

// Wedge
// The outer product. (MEET)
impl BitXor for MultiVec3D
{
    type Output = MultiVec3D;

    fn bitxor(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = b.s() * a.s();
        *res.e0_mut() = b.e0() * a.s() + b.s() * a.e0();
        *res.e1_mut() = b.e1() * a.s() + b.s() * a.e1();
        *res.e2_mut() = b.e2() * a.s() + b.s() * a.e2();
        *res.e3_mut() = b.e3() * a.s() + b.s() * a.e3();
        *res.e01_mut() =
            b.e01() * a.s() + b.e1() * a.e0() - b.e0() * a.e1() + b.s() * a.e01();
        *res.e02_mut() =
            b.e02() * a.s() + b.e2() * a.e0() - b.e0() * a.e2() + b.s() * a.e02();
        *res.e03_mut() =
            b.e03() * a.s() + b.e3() * a.e0() - b.e0() * a.e3() + b.s() * a.e03();
        *res.e12_mut() =
            b.e12() * a.s() + b.e2() * a.e1() - b.e1() * a.e2() + b.s() * a.e12();
        *res.e31_mut() =
            b.e31() * a.s() - b.e3() * a.e1() + b.e1() * a.e3() + b.s() * a.e31();
        *res.e23_mut() =
            b.e23() * a.s() + b.e3() * a.e2() - b.e2() * a.e3() + b.s() * a.e23();
        *res.e021_mut() = b.e021() * a.s() - b.e12() * a.e0() + b.e02() * a.e1()
            - b.e01() * a.e2()
            - b.e2() * a.e01()
            + b.e1() * a.e02()
            - b.e0() * a.e12()
            + b.s() * a.e021();
        *res.e013_mut() = b.e013() * a.s() - b.e31() * a.e0() - b.e03() * a.e1()
            + b.e01() * a.e3()
            + b.e3() * a.e01()
            - b.e1() * a.e03()
            - b.e0() * a.e31()
            + b.s() * a.e013();
        *res.e032_mut() = b.e032() * a.s() - b.e23() * a.e0() + b.e03() * a.e2()
            - b.e02() * a.e3()
            - b.e3() * a.e02()
            + b.e2() * a.e03()
            - b.e0() * a.e23()
            + b.s() * a.e032();
        *res.e123_mut() = b.e123() * a.s()
            + b.e23() * a.e1()
            + b.e31() * a.e2()
            + b.e12() * a.e3()
            + b.e3() * a.e12()
            + b.e2() * a.e31()
            + b.e1() * a.e23()
            + b.s() * a.e123();
        *res.e0123_mut() = b.e0123() * a.s()
            + b.e123() * a.e0()
            + b.e032() * a.e1()
            + b.e013() * a.e2()
            + b.e021() * a.e3()
            + b.e23() * a.e01()
            + b.e31() * a.e02()
            + b.e12() * a.e03()
            + b.e03() * a.e12()
            + b.e02() * a.e31()
            + b.e01() * a.e23()
            - b.e3() * a.e021()
            - b.e2() * a.e013()
            - b.e1() * a.e032()
            - b.e0() * a.e123()
            + b.s() * a.e0123();
        res
    }
}


// Vee
// The regressive product. (JOIN)
impl BitAnd for MultiVec3D
{
    type Output = MultiVec3D;

    fn bitand(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        (self.dual() ^ b.dual()).dual()
    }
}

// Vee
// The regressive product. (JOIN)
impl BitAnd<CPoint> for MultiVec3D
{
    type Output = MultiVec3D;

    fn bitand(self: MultiVec3D, b: CPoint) -> MultiVec3D
    {
        (self.dual() ^ b.dual()).dual()
    }
}

// sadd
// scalar/multivector addition
impl Add<MultiVec3D> for f32
{
    type Output = MultiVec3D;

    fn add(self: f32, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a + b.s();
        *res.e0_mut() = b.e0();
        *res.e1_mut() = b.e1();
        *res.e2_mut() = b.e2();
        *res.e3_mut() = b.e3();
        *res.e01_mut() = b.e01();
        *res.e02_mut() = b.e02();
        *res.e03_mut() = b.e03();
        *res.e12_mut() = b.e12();
        *res.e31_mut() = b.e31();
        *res.e23_mut() = b.e23();
        *res.e021_mut() = b.e021();
        *res.e013_mut() = b.e013();
        *res.e032_mut() = b.e032();
        *res.e123_mut() = b.e123();
        *res.e0123_mut() = b.e0123();
        res
    }
}

// adds
// multivector/scalar addition
impl Add<f32> for MultiVec3D
{
    type Output = MultiVec3D;

    fn add(self: MultiVec3D, b: f32) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s() + b;
        *res.e0_mut() = a.e0();
        *res.e1_mut() = a.e1();
        *res.e2_mut() = a.e2();
        *res.e3_mut() = a.e3();
        *res.e01_mut() = a.e01();
        *res.e02_mut() = a.e02();
        *res.e03_mut() = a.e03();
        *res.e12_mut() = a.e12();
        *res.e31_mut() = a.e31();
        *res.e23_mut() = a.e23();
        *res.e021_mut() = a.e021();
        *res.e013_mut() = a.e013();
        *res.e032_mut() = a.e032();
        *res.e123_mut() = a.e123();
        *res.e0123_mut() = a.e0123();
        res
    }
}


// Add
// Multivector addition
impl Add for MultiVec3D
{
    type Output = MultiVec3D;

    fn add(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s() + b.s();
        *res.e0_mut() = a.e0() + b.e0();
        *res.e1_mut() = a.e1() + b.e1();
        *res.e2_mut() = a.e2() + b.e2();
        *res.e3_mut() = a.e3() + b.e3();
        *res.e01_mut() = a.e01() + b.e01();
        *res.e02_mut() = a.e02() + b.e02();
        *res.e03_mut() = a.e03() + b.e03();
        *res.e12_mut() = a.e12() + b.e12();
        *res.e31_mut() = a.e31() + b.e31();
        *res.e23_mut() = a.e23() + b.e23();
        *res.e021_mut() = a.e021() + b.e021();
        *res.e013_mut() = a.e013() + b.e013();
        *res.e032_mut() = a.e032() + b.e032();
        *res.e123_mut() = a.e123() + b.e123();
        *res.e0123_mut() = a.e0123() + b.e0123();
        res
    }
}

impl Neg for MultiVec3D
{
    type Output = MultiVec3D;

    fn neg(self) -> Self::Output
    {
        let mut res = MultiVec3D::zero();
        let b = self;
        *res.s_mut() = -b.s();
        *res.e0_mut() = -b.e0();
        *res.e1_mut() = -b.e1();
        *res.e2_mut() = -b.e2();
        *res.e3_mut() = -b.e3();
        *res.e01_mut() = -b.e01();
        *res.e02_mut() = -b.e02();
        *res.e03_mut() = -b.e03();
        *res.e12_mut() = -b.e12();
        *res.e31_mut() = -b.e31();
        *res.e23_mut() = -b.e23();
        *res.e021_mut() = -b.e021();
        *res.e013_mut() = -b.e013();
        *res.e032_mut() = -b.e032();
        *res.e123_mut() = -b.e123();
        *res.e0123_mut() = -b.e0123();
        res
    }
}

// sub
// scalar/multivector subtraction
impl Sub<MultiVec3D> for MultiVec3D
{
    type Output = MultiVec3D;

    fn sub(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s() - b.s();
        *res.e0_mut() = a.e0() - b.e0();
        *res.e1_mut() = a.e1() - b.e1();
        *res.e2_mut() = a.e2() - b.e2();
        *res.e3_mut() = a.e3() - b.e3();
        *res.e01_mut() = a.e01() - b.e01();
        *res.e02_mut() = a.e02() - b.e02();
        *res.e03_mut() = a.e03() - b.e03();
        *res.e12_mut() = a.e12() - b.e12();
        *res.e31_mut() = a.e31() - b.e31();
        *res.e23_mut() = a.e23() - b.e23();
        *res.e021_mut() = a.e021() - b.e021();
        *res.e013_mut() = a.e013() - b.e013();
        *res.e032_mut() = a.e032() - b.e032();
        *res.e123_mut() = a.e123() - b.e123();
        *res.e0123_mut() = a.e0123() - b.e0123();
        res
    }
}

// ssub
// scalar/multivector subtraction
impl Sub<MultiVec3D> for f32
{
    type Output = MultiVec3D;

    fn sub(self: f32, b: MultiVec3D) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a - b.s();
        *res.e0_mut() = -b.e0();
        *res.e1_mut() = -b.e1();
        *res.e2_mut() = -b.e2();
        *res.e3_mut() = -b.e3();
        *res.e01_mut() = -b.e01();
        *res.e02_mut() = -b.e02();
        *res.e03_mut() = -b.e03();
        *res.e12_mut() = -b.e12();
        *res.e31_mut() = -b.e31();
        *res.e23_mut() = -b.e23();
        *res.e021_mut() = -b.e021();
        *res.e013_mut() = -b.e013();
        *res.e032_mut() = -b.e032();
        *res.e123_mut() = -b.e123();
        *res.e0123_mut() = -b.e0123();
        res
    }
}

// subs
// multivector/scalar subtraction
impl Sub<f32> for MultiVec3D
{
    type Output = MultiVec3D;

    fn sub(self: MultiVec3D, b: f32) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s() - b;
        *res.e0_mut() = a.e0();
        *res.e1_mut() = a.e1();
        *res.e2_mut() = a.e2();
        *res.e3_mut() = a.e3();
        *res.e01_mut() = a.e01();
        *res.e02_mut() = a.e02();
        *res.e03_mut() = a.e03();
        *res.e12_mut() = a.e12();
        *res.e31_mut() = a.e31();
        *res.e23_mut() = a.e23();
        *res.e021_mut() = a.e021();
        *res.e013_mut() = a.e013();
        *res.e032_mut() = a.e032();
        *res.e123_mut() = a.e123();
        *res.e0123_mut() = a.e0123();
        res
    }
}

// div
// multivector division/inversion
impl Div<MultiVec3D> for MultiVec3D
{
    type Output = MultiVec3D;

    fn div(self: MultiVec3D, b: MultiVec3D) -> MultiVec3D
    {
        self * (b.reverse() * (1.0 / b.norm_squared()))
    }
}

// div
// multivector division/inversion
impl Div<f32> for MultiVec3D
{
    type Output = MultiVec3D;

    fn div(self: MultiVec3D, b: f32) -> MultiVec3D { self * (1.0 / b) }
}

// Conjugate
// Clifford Conjugation
impl MultiVec3D
{
    pub fn conjugate(self: Self) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s();
        *res.e0_mut() = -a.e0();
        *res.e1_mut() = -a.e1();
        *res.e2_mut() = -a.e2();
        *res.e3_mut() = -a.e3();
        *res.e01_mut() = -a.e01();
        *res.e02_mut() = -a.e02();
        *res.e03_mut() = -a.e03();
        *res.e12_mut() = -a.e12();
        *res.e31_mut() = -a.e31();
        *res.e23_mut() = -a.e23();
        *res.e021_mut() = a.e021();
        *res.e013_mut() = a.e013();
        *res.e032_mut() = a.e032();
        *res.e123_mut() = a.e123();
        *res.e0123_mut() = a.e0123();
        res
    }

    pub fn dual(self: Self) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.e0123();
        *res.e0_mut() = a.e123();
        *res.e1_mut() = a.e032();
        *res.e2_mut() = a.e013();
        *res.e3_mut() = a.e021();
        *res.e01_mut() = a.e23();
        *res.e02_mut() = a.e31();
        *res.e03_mut() = a.e12();
        *res.e12_mut() = a.e03();
        *res.e31_mut() = a.e02();
        *res.e23_mut() = a.e01();
        *res.e021_mut() = a.e3();
        *res.e013_mut() = a.e2();
        *res.e032_mut() = a.e1();
        *res.e123_mut() = a.e0();
        *res.e0123_mut() = a.s();
        res
    }

    pub fn reverse(self: Self) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.s_mut() = a.s();
        *res.e0_mut() = a.e0();
        *res.e1_mut() = a.e1();
        *res.e2_mut() = a.e2();
        *res.e3_mut() = a.e3();
        *res.e01_mut() = -a.e01();
        *res.e02_mut() = -a.e02();
        *res.e03_mut() = -a.e03();
        *res.e12_mut() = -a.e12();
        *res.e31_mut() = -a.e31();
        *res.e23_mut() = -a.e23();
        *res.e021_mut() = -a.e021();
        *res.e013_mut() = -a.e013();
        *res.e032_mut() = -a.e032();
        *res.e123_mut() = -a.e123();
        *res.e0123_mut() = a.e0123();
        res
    }

    pub fn grade(&self, grade_val: usize) -> MultiVec3D
    {
        let a = self;
        let mut res = MultiVec3D::zero();
        match grade_val
        {
            0 =>
            {
                *res.s_mut() = a.s();
            }
            1 =>
            {
                *res.e0_mut() = a.e0();
                *res.e1_mut() = a.e1();
                *res.e2_mut() = a.e2();
                *res.e3_mut() = a.e3();
            }
            2 =>
            {
                *res.e01_mut() = a.e01();
                *res.e02_mut() = a.e02();
                *res.e03_mut() = a.e03();
                *res.e12_mut() = a.e12();
                *res.e31_mut() = a.e31();
                *res.e23_mut() = a.e23();
            }
            3 =>
            {
                *res.e123_mut() = a.e123();
                *res.e032_mut() = a.e032();
                *res.e013_mut() = a.e013();
                *res.e021_mut() = a.e021();
            }
            4 =>
            {
                *res.e0123_mut() = a.e0123();
            }
            _ => panic!(),
        }

        return res;
    }

    pub fn exp(self: Self) -> MultiVec3D
    {
        // exp(x) = lim (1+x/n)^n for n->infinity
        let n = 15.0;
        let mut res = 1.0 + self / f32::powf(2.0, n);

        for _ in 0..(n as u32)
        {
            res = res * res;
        }

        res
    }

    pub fn log(self) -> MultiVec3D
    {
        // log(x) = lim \omega(z^{1/\omega} - 1)
        let mut res = self;

        let iter_num = 15;
        for _ in 0..iter_num
        {
            res = MultiVec3D::sqrt(res)
        }

        let omega = (1 << iter_num) as f32;

        omega * (res - 1.0)
    }

    pub fn sqrt(self) -> MultiVec3D
    {
        // Not sure if this works for arbitrary multivectors or just motors
        (self.normalized() + 1.0).normalized()
    }

    pub fn norm(self) -> f32
    {
        let scalar_part = (self * self.conjugate())[0];

        scalar_part.abs().sqrt()
    }

    pub fn norm_squared(self) -> f32
    {
        let scalar_part = (self * self.conjugate())[0];

        scalar_part.abs()
    }

    pub fn inorm(self) -> f32 { self.dual().norm() }

    pub fn normalized(self) -> Self { self * (1.0 / self.norm()) }

    // A rotor (Euclidean line) and translator (Ideal line)
    pub fn rotor(angle: f32, line: Self) -> Self
    {
        (angle / 2.0).cos() + (angle / 2.0).sin() * line.normalized()
    }

    pub fn translator(dist: f32, line: Self) -> Self { 1.0 + dist / 2.0 * line }

    // https://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/index.htm
    // TODO: this only works for pure rotors.
    pub fn slerp(qa: &MultiVec3D, qb: &MultiVec3D, t: f32) -> MultiVec3D
    {
        let qb = if (*qa * qb.conjugate()).s() < 0.0
        {
            -*qb
        }
        else
        {
            *qb
        };

        // quaternion to return
        let mut qm = MultiVec3D::identity();
        // Calculate angle between them.
        let cos_half_theta = qa.s() * qb.s()
            + qa.e12() * qb.e12()
            + qa.e31() * qb.e31()
            + qa.e23() * qb.e23();
        // if qa=qb or qa=-qb then theta = 0 and we can return qa
        if f32::abs(cos_half_theta) >= 1.0
        {
            *qm.s_mut() = qa.s();
            *qm.e12_mut() = qa.e12();
            *qm.e31_mut() = qa.e31();
            *qm.e23_mut() = qa.e23();
            return qm;
        }
        // Calculate temporary values.
        let half_theta = f32::acos(cos_half_theta);
        let sin_half_theta = f32::sqrt(1.0 - cos_half_theta * cos_half_theta);
        // if theta = 180 degrees then result is not fully defined
        // we could rotate around any axis normal to qa or qb
        if f32::abs(sin_half_theta) < 0.001
        // fabs is floating point absolute
        {
            *qm.s_mut() = qa.s() * 0.5 + qb.s() * 0.5;
            *qm.e12_mut() = qa.e12() * 0.5 + qb.e12() * 0.5;
            *qm.e31_mut() = qa.e31() * 0.5 + qb.e31() * 0.5;
            *qm.e23_mut() = qa.e23() * 0.5 + qb.e23() * 0.5;
            return qm;
        }
        let ratio_a = f32::sin((1.0 - t) * half_theta) / sin_half_theta;
        let ratio_b = f32::sin(t * half_theta) / sin_half_theta;
        //calculate Quaternion.
        *qm.s_mut() = qa.s() * ratio_a + qb.s() * ratio_b;
        *qm.e12_mut() = qa.e12() * ratio_a + qb.e12() * ratio_b;
        *qm.e31_mut() = qa.e31() * ratio_a + qb.e31() * ratio_b;
        *qm.e23_mut() = qa.e23() * ratio_a + qb.e23() * ratio_b;
        return qm;
    }

    pub fn s(&self) -> f32 { self[0] }

    pub fn e0(&self) -> f32 { self[1] }

    pub fn e1(&self) -> f32 { self[2] }

    pub fn e2(&self) -> f32 { self[3] }

    pub fn e3(&self) -> f32 { self[4] }

    pub fn e01(&self) -> f32 { self[5] }

    pub fn e02(&self) -> f32 { self[6] }

    pub fn e03(&self) -> f32 { self[7] }

    pub fn e12(&self) -> f32 { self[8] }

    pub fn e31(&self) -> f32 { self[9] }

    pub fn e23(&self) -> f32 { self[10] }

    pub fn e021(&self) -> f32 { self[11] }

    pub fn e013(&self) -> f32 { self[12] }

    pub fn e032(&self) -> f32 { self[13] }

    pub fn e123(&self) -> f32 { self[14] }

    pub fn e0123(&self) -> f32 { self[15] }

    pub fn s_mut(&mut self) -> &mut f32 { &mut self[0] }

    pub fn e0_mut(&mut self) -> &mut f32 { &mut self[1] }

    pub fn e1_mut(&mut self) -> &mut f32 { &mut self[2] }

    pub fn e2_mut(&mut self) -> &mut f32 { &mut self[3] }

    pub fn e3_mut(&mut self) -> &mut f32 { &mut self[4] }

    pub fn e01_mut(&mut self) -> &mut f32 { &mut self[5] }

    pub fn e02_mut(&mut self) -> &mut f32 { &mut self[6] }

    pub fn e03_mut(&mut self) -> &mut f32 { &mut self[7] }

    pub fn e12_mut(&mut self) -> &mut f32 { &mut self[8] }

    pub fn e31_mut(&mut self) -> &mut f32 { &mut self[9] }

    pub fn e23_mut(&mut self) -> &mut f32 { &mut self[10] }

    pub fn e021_mut(&mut self) -> &mut f32 { &mut self[11] }

    pub fn e013_mut(&mut self) -> &mut f32 { &mut self[12] }

    pub fn e032_mut(&mut self) -> &mut f32 { &mut self[13] }

    pub fn e123_mut(&mut self) -> &mut f32 { &mut self[14] }

    pub fn e0123_mut(&mut self) -> &mut f32 { &mut self[15] }

    pub fn to_point(&self) -> CPoint
    {
        CPoint::new(self.e032(), self.e013(), self.e021()) / self.e123()
    }

    pub fn to_vec(&self) -> Vec3
    {
        Vec3::new(self.e032(), self.e013(), self.e021()) / self.e123()
    }

    pub fn from_implicit_plane(a: f32, b: f32, c: f32, d: f32) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();

        *res.e1_mut() = a;
        *res.e2_mut() = b;
        *res.e3_mut() = c;
        *res.s_mut() = d;

        res
    }

    pub fn get_line_dir(&self) -> Vec3 { Vec3::new(self.e23(), self.e31(), self.e12()) }
}
