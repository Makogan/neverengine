use std::fmt::Debug;

use nalgebra as na;

pub(crate) trait FromJson<T>
{
    fn from_json(_name: &String, val: &serde_json::Value) -> T;
}

impl FromJson<usize> for usize
{
    fn from_json(_name: &String, val: &serde_json::Value) -> usize
    {
        return val.as_u64().unwrap_or(0) as usize;
    }
}

impl FromJson<bool> for bool
{
    fn from_json(_name: &String, val: &serde_json::Value) -> bool
    {
        return val.as_bool().unwrap_or(false) as bool;
    }
}


impl FromJson<f32> for f32
{
    fn from_json(_name: &String, val: &serde_json::Value) -> f32
    {
        return val.as_f64().unwrap_or_default() as f32;
    }
}

impl<T> FromJson<Vec<T>> for Vec<T>
where
    T: FromJson<T>,
{
    fn from_json(_name: &String, array: &serde_json::Value) -> Vec<T>
    {
        if array.is_null()
        {
            return Vec::new();
        }

        let mut narray: Vec<T> = Vec::new();

        for val in array.as_array().unwrap()
        {
            narray.push(T::from_json(_name, &val));
        }

        return narray;
    }
}

impl FromJson<String> for String
{
    fn from_json(_name: &String, json: &serde_json::Value) -> String
    {
        match json.as_str()
        {
            Some(x) =>
            {
                return x.to_string();
            }
            None =>
            {
                return "".to_string();
            }
        }
    }
}

impl FromJson<na::UnitQuaternion<f32>> for na::UnitQuaternion<f32>
{
    fn from_json(_name: &String, json: &serde_json::Value) -> na::UnitQuaternion<f32>
    {
        let values = json.as_array();
        match values
        {
            Some(vec) =>
            {
                debug_assert!(vec.len() == 4);
                let x = vec[0].as_f64().unwrap() as f32;
                let y = vec[1].as_f64().unwrap() as f32;
                let z = vec[2].as_f64().unwrap() as f32;
                let w = vec[3].as_f64().unwrap() as f32;

                return na::UnitQuaternion::from_quaternion(na::Quaternion::<f32>::new(
                    w, x, y, z,
                ));
            }
            None =>
            {
                return na::UnitQuaternion::from_quaternion(na::Quaternion::<f32>::new(
                    1.0, 0.0, 0.0, 0.0,
                ));
            }
        }
    }
}

impl<T> FromJson<Option<T>> for Option<T>
where
    T: FromJson<T> + Debug,
{
    fn from_json(name: &String, json: &serde_json::Value) -> Option<T>
    {
        let ret = match json
        {
            serde_json::Value::Null => None,
            _ => Some(T::from_json(name, json)),
        };

        return ret;
    }
}

impl FromJson<na::Vector3<f32>> for na::Vector3<f32>
{
    fn from_json(name: &String, json: &serde_json::Value) -> na::Vector3<f32>
    {
        let values = json.as_array();
        match values
        {
            Some(vec) =>
            {
                assert!(vec.len() == 3);
                let x = vec[0].as_f64().unwrap() as f32;
                let y = vec[1].as_f64().unwrap() as f32;
                let z = vec[2].as_f64().unwrap() as f32;

                return na::Vector3::<f32>::new(x, y, z);
            }
            None => match name.as_str()
            {
                "translation" =>
                {
                    return na::Vector3::<f32>::new(0.0, 0.0, 0.0);
                }
                "scale" =>
                {
                    return na::Vector3::<f32>::new(1.0, 1.0, 1.0);
                }
                _ =>
                {
                    return na::Vector3::<f32>::new(0.0, 0.0, 0.0);
                }
            },
        }
    }
}
