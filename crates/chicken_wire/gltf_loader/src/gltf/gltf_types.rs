use std::format;
use std::fs;
use std::path::Path;

use data_url::DataUrl;
use gltf_reflect::GltfReflect;
use nalgebra as na;
use serde::ser::SerializeStruct;

use crate::gltf::serde_helpers::*;

#[rustfmt::skip]

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum ComponentType
{
    SBYTE,      // 8 bits
    UBYTE,      // 8 bits
    SSHORT,     // 16 bits
    USHORT,     // 16 bits
    UINT,       // 32 bits
    F32         // 32 bits
}

impl serde::Serialize for ComponentType
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let code = match self
        {
            ComponentType::SBYTE => 5120,
            ComponentType::UBYTE => 5121,
            ComponentType::SSHORT => 5122,
            ComponentType::USHORT => 5123,
            ComponentType::UINT => 5125,
            ComponentType::F32 => 5126,
        };

        serializer.serialize_i32(code)
    }
}

impl ComponentType
{
    pub(crate) fn from_json(_name: &String, int: &serde_json::Value) -> ComponentType
    {
        match int.as_u64().unwrap() as usize
        {
            5120 => return ComponentType::SBYTE,
            5121 => return ComponentType::UBYTE,
            5122 => return ComponentType::SSHORT,
            5123 => return ComponentType::USHORT,
            5125 => return ComponentType::UINT,
            5126 => return ComponentType::F32,
            _ => panic!("Unrecognized data type {}.", int),
        }
    }
}


#[derive(PartialEq, Debug, Clone, Copy, serde::Serialize)]
pub enum DataType
{
    SCALAR,
    VEC2,
    VEC3,
    VEC4,
    MAT2,
    MAT3,
    MAT4,
}

impl DataType
{
    pub(crate) fn from_json(_name: &String, val: &serde_json::Value) -> DataType
    {
        match val.as_str()
        {
            Some(x) => match x
            {
                "SCALAR" => return DataType::SCALAR,
                "VEC2" => return DataType::VEC2,
                "VEC3" => return DataType::VEC3,
                "VEC4" => return DataType::VEC4,
                "MAT2" => return DataType::MAT2,
                "MAT3" => return DataType::MAT3,
                "MAT4" => return DataType::MAT4,
                _ => panic!("Unrecognized data type."),
            },
            None => panic!(""),
        }
    }
}

pub trait DataIntrospection
{
    fn get_component_type() -> ComponentType;
    fn get_data_type() -> DataType;
    fn get_component_count() -> i16;
}

impl DataIntrospection for f32
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

impl DataIntrospection for na::Vector2<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::VEC2; }

    fn get_component_count() -> i16 { return 2; }
}

impl DataIntrospection for na::Vector3<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::VEC3; }

    fn get_component_count() -> i16 { return 3; }
}

impl DataIntrospection for na::Vector4<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::VEC4; }

    fn get_component_count() -> i16 { return 4; }
}

impl DataIntrospection for na::Vector4<u8>
{
    fn get_component_type() -> ComponentType { return ComponentType::UBYTE; }

    fn get_data_type() -> DataType { return DataType::VEC4; }

    fn get_component_count() -> i16 { return 4; }
}

impl DataIntrospection for na::Vector4<u16>
{
    fn get_component_type() -> ComponentType { return ComponentType::USHORT; }

    fn get_data_type() -> DataType { return DataType::VEC4; }

    fn get_component_count() -> i16 { return 4; }
}

impl DataIntrospection for na::Matrix2<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::MAT2; }

    fn get_component_count() -> i16 { return 4; }
}

impl DataIntrospection for na::Matrix3<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::MAT3; }

    fn get_component_count() -> i16 { return 9; }
}

impl DataIntrospection for na::Matrix4<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::MAT4; }

    fn get_component_count() -> i16 { return 16; }
}

impl DataIntrospection for na::UnitQuaternion<f32>
{
    fn get_component_type() -> ComponentType { return ComponentType::F32; }

    fn get_data_type() -> DataType { return DataType::VEC4; }

    fn get_component_count() -> i16 { return 4; }
}

impl DataIntrospection for u32
{
    fn get_component_type() -> ComponentType { return ComponentType::UINT; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

impl DataIntrospection for u16
{
    fn get_component_type() -> ComponentType { return ComponentType::USHORT; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

impl DataIntrospection for i16
{
    fn get_component_type() -> ComponentType { return ComponentType::SSHORT; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

impl DataIntrospection for i8
{
    fn get_component_type() -> ComponentType { return ComponentType::SBYTE; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

impl DataIntrospection for u8
{
    fn get_component_type() -> ComponentType { return ComponentType::UBYTE; }

    fn get_data_type() -> DataType { return DataType::SCALAR; }

    fn get_component_count() -> i16 { return 1; }
}

#[derive(PartialEq, Debug, Clone, serde::Serialize)]
pub enum InterpolationMode
{
    NEAREST,
    LINEAR,
}


impl InterpolationMode
{
    pub(crate) fn from_json(_name: &String, val: &serde_json::Value)
        -> InterpolationMode
    {
        match val.as_str()
        {
            Some(x) => match x
            {
                "NEAREST" => return InterpolationMode::NEAREST,
                "LINEAR" => return InterpolationMode::LINEAR,
                _ => panic!("Unrecognized data type."),
            },
            None => return InterpolationMode::NEAREST,
        }
    }
}

#[derive(PartialEq, Debug, Clone, Copy, serde::Serialize)]
#[serde(rename_all = "lowercase")]
pub enum ChannelPath
{
    TRANSLATION,
    ROTATION,
    SCALE,
}

impl ChannelPath
{
    pub(crate) fn from_json(_name: &String, val: &serde_json::Value) -> ChannelPath
    {
        match val.as_str()
        {
            Some(x) => match x
            {
                "translation" => return ChannelPath::TRANSLATION,
                "rotation" => return ChannelPath::ROTATION,
                "scale" => return ChannelPath::SCALE,
                _ => panic!("Unrecognized channel path. {}", x),
            },
            None => panic!("No path specified."),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Attributes
{
    pub position: usize,
    pub normal: usize,
    pub tex_coords: usize,
    pub joint0: usize,
    pub weights0: usize,
}

impl serde::Serialize for Attributes
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("attributes", 5)?;
        if self.position != !0
        {
            state.serialize_field("POSITION", &self.position).unwrap();
        }
        if self.normal != !0
        {
            state.serialize_field("NORMAL", &self.normal).unwrap();
        }
        if self.tex_coords != !0
        {
            state
                .serialize_field("TEXCOORD_0", &self.tex_coords)
                .unwrap();
        }
        if self.joint0 != !0
        {
            state.serialize_field("JOINTS_0", &self.joint0).unwrap();
        }
        if self.weights0 != !0
        {
            state.serialize_field("WEIGHTS_0", &self.weights0).unwrap();
        }

        state.end()
    }
}

impl FromJson<Attributes> for Attributes
{
    fn from_json(_name: &String, val: &serde_json::Value) -> Attributes
    {
        return Attributes {
            position: val["POSITION"].as_u64().unwrap() as usize,
            normal: val["NORMAL"].as_u64().unwrap_or(!0) as usize,
            tex_coords: val["TEXCOORD_0"].as_u64().unwrap_or(!0) as usize,
            joint0: val["JOINTS_0"].as_u64().unwrap_or(!0) as usize,
            weights0: val["WEIGHTS_0"].as_u64().unwrap_or(!0) as usize,
        };
    }
}

pub fn parse_buffers(json: &serde_json::Value, path_to_data: &str) -> Vec<Vec<u8>>
{
    let mut raw_buffers: Vec<Vec<u8>> = Vec::new();

    for buffer in json["buffers"].as_array().unwrap()
    {
        let data_name = &buffer["uri"].as_str().unwrap();
        let potential_data_path =
            Path::new(path_to_data).parent().unwrap().join(data_name);

        let body = if potential_data_path.exists()
        {
            fs::read(potential_data_path).unwrap()
        }
        else
        {
            let url = DataUrl::process(data_name)
                .expect(format!("Cannot load {}", data_name).as_str());
            let (body, _fragment) = url.decode_to_vec().unwrap();
            body
        };

        debug_assert!(body.len() > 0);

        raw_buffers.push(body);
    }

    return raw_buffers;
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct BufferView
{
    pub buffer: usize,
    pub byte_offset: usize,
    pub byte_length: usize,
}


#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Accessor
{
    pub buffer_view: usize,
    pub component_type: ComponentType,
    pub byte_offset: usize,
    pub count: usize,
    pub max: Vec<f32>,
    pub min: Vec<f32>,
    pub kind: DataType,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Skin
{
    pub inverse_bind_matrices: usize,
    pub joints: Vec<usize>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub root: Option<usize>,
    pub name: String,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Node
{
    pub name: String,
    #[serde(skip_serializing_if = "<[_]>::is_empty")]
    pub children: Vec<usize>,
    pub rotation: na::UnitQuaternion<f32>,
    pub translation: na::Vector3<f32>,
    pub scale: na::Vector3<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skin: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mesh: Option<usize>,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Target
{
    pub node: usize,
    pub path: ChannelPath,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Channel
{
    pub sampler: usize,
    pub target: Target,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Sampler
{
    pub input: usize,
    pub output: usize,
    pub interpolation: InterpolationMode,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Animation
{
    pub name: String,
    pub channels: Vec<Channel>,
    pub samplers: Vec<Sampler>,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Primitive
{
    pub attributes: Attributes,
    pub indices: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub material: Option<usize>,
}


#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Mesh
{
    pub name: String,
    pub primitives: Vec<Primitive>,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct BaseColorTexture
{
    pub index: usize,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct PbrMetallicRoughness
{
    #[serde(skip_serializing_if = "Option::is_none")]
    pub base_color_texture: Option<BaseColorTexture>,
    pub roughness_factor: f32,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Material
{
    pub name: String,
    pub double_sided: bool,
    pub pbr_metallic_roughness: PbrMetallicRoughness,
}


#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Texture
{
    pub sampler: usize,
    pub source: usize,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Image
{
    pub name: String,
    pub mime_type: String,
    pub buffer_view: usize,
    pub uri: String,
}

#[derive(Clone, serde::Serialize, Debug, GltfReflect)]

pub struct TextureSampler
{
    pub mag_filter: usize,
    pub min_filter: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wrap_s: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wrap_t: Option<usize>,
}


#[derive(Clone, serde::Serialize, Debug, GltfReflect)]
pub struct Scene
{
    name: String,
    nodes: Vec<usize>,
}
