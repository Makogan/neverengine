use std::collections::HashSet;
use std::f32::consts::PI;
use std::fmt::Debug;

use containers::PQueue;
use linear_isomorphic::{InnerSpace, RealField, VectorSpace};

pub use crate::face_handle::FaceHandle;
pub use crate::face_handle::FaceMetrics;
pub use crate::hedge_handle::HEdgeHandle;
use crate::mesh::verify_geometry;
use crate::mesh::verify_topology;
use crate::mesh::GeomId;
use crate::mesh::HasNormals;
use crate::mesh::InternalMeshAccess;
use crate::mesh_deleter::HalfMeshDeleter;
pub use crate::traits::{HalfMeshLike, PrimitiveContainer};
pub use crate::vert_handle::VertHandle;

#[derive(Debug, Clone)]
pub struct RemeshingParameters<S>
where
    S: Debug + Clone + Default + num::Float,
{
    pub minimum_triangle_angle: S,
    pub dihedral_angle_tolerance: S,
    pub iterations: usize,
}

impl<S> Default for RemeshingParameters<S>
where
    S: Debug + Clone + Default + num::Float,
{
    fn default() -> Self {
        Self {
            minimum_triangle_angle: S::from(2.0 * PI / 9.0).unwrap(),
            dihedral_angle_tolerance: S::from(2.0 * PI / 4.0).unwrap(),
            iterations: 10,
        }
    }
}

pub struct RemeshingParametersWithoutCollapse<S>
where
    S: Debug + Clone + Default + num::Float,
{
    pub minimum_triangle_angle: S,
    pub dihedral_angle_tolerance: S,
    pub target_additional_vertices: usize,
}

impl<S> Default for RemeshingParametersWithoutCollapse<S>
where
    S: Debug + Clone + Default + num::Float,
{
    fn default() -> Self {
        Self {
            minimum_triangle_angle: S::from(2.0 * PI / 9.0).unwrap(),
            dihedral_angle_tolerance: S::from(2.0 * PI / 4.0).unwrap(),
            target_additional_vertices: 4000,
        }
    }
}

/// Uses Kobelt's et. al. Simplified remeshing algorithm to generate new
/// triangles and produce a more equilateral triangulation of the input
/// geometry.
pub fn remeshing<S, MeshType, T, L, R>(
    mesh: &mut MeshType,
    parameters: RemeshingParameters<S>,
    adaptive_target_length: L,
    reproject: R,
) where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S> + HasNormals<S> + Debug,
    S: RealField + num::traits::float::FloatCore,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
    L: Fn(usize, usize) -> S,
    R: Fn(&T) -> T,
{
    debug_assert!(verify_topology(mesh).is_none());
    debug_assert!(verify_geometry(mesh));

    let (mut feat_verts, mut feat_edges, mut corners) =
        detect_features(mesh, parameters.dihedral_angle_tolerance);

    let low = S::from(4.0 / 5.0).unwrap();
    let high = S::from(4.0 / 3.0).unwrap();

    for _ in 0..parameters.iterations {
        split_long_edges(
            mesh,
            high,
            &adaptive_target_length,
            &mut feat_verts,
            &mut feat_edges,
            0,
        );
        debug_assert!(verify_topology(mesh).is_none());

        collapse_short_edges(
            mesh,
            low,
            high,
            &adaptive_target_length,
            &mut feat_verts,
            &mut feat_edges,
            &mut corners,
        );
        debug_assert!(verify_topology(mesh).is_none());

        equalize_valences(mesh, parameters.minimum_triangle_angle, &feat_edges);

        tangential_relaxation(mesh, &corners);

        debug_assert!(verify_topology(mesh).is_none());
        debug_assert!(verify_geometry(mesh));
        for v in mesh.iter_verts() {
            let proj = reproject(&v.data());
            v.mutate_data(&proj);
        }
    }
}

pub struct RemeshingContext<S, MeshType, T>
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S> + HasNormals<S> + Debug,
    S: RealField + num::traits::float::FloatCore,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    pub queue: PQueue<GeomId<1>, S>,
    pub mesh: MeshType,
    pub modified_vertices: Vec<GeomId<0>>,
    pub modified_faces: Vec<GeomId<2>>,
}

impl<S, MeshType, T> RemeshingContext<S, MeshType, T>
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S> + HasNormals<S> + Debug,
    S: RealField + num::traits::float::FloatCore,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    pub fn from_mesh(mesh: MeshType) -> Self {
        debug_assert!(verify_topology(&mesh).is_none());
        let pq = PQueue::from_iter(mesh.iter_edges().map(|e| {
            (
                e.id().select_even(),
                (e.source().data() - e.dest().data()).norm(),
            )
        }));

        Self {
            queue: pq,
            mesh,
            modified_vertices: Vec::new(),
            modified_faces: Vec::new(),
        }
    }
}

pub fn remeshing_with_context_without_collapse<S, MeshType, T, L, R>(
    context: &mut RemeshingContext<S, MeshType, T>,
    parameters: RemeshingParametersWithoutCollapse<S>,
    adaptive_target_length: L,
    reproject: R,
) where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S> + HasNormals<S> + Debug,
    S: RealField + num::traits::float::FloatCore,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
    L: Fn(usize, usize) -> S,
    R: Fn(&T) -> T,
{
    debug_assert!(verify_topology(&context.mesh).is_none());
    debug_assert!(verify_geometry(&context.mesh));

    let (mut feat_verts, mut feat_edges, corners) =
        detect_features(&mut context.mesh, parameters.dihedral_angle_tolerance);

    let high = S::from(4.0 / 3.0).unwrap();

    let input_verts = context.mesh.vert_count();
    while context.mesh.vert_count() < input_verts + parameters.target_additional_vertices
    {
        if context.queue.is_empty() {
            let pq = PQueue::from_iter(context.mesh.iter_edges().map(|e| {
                (
                    e.id().select_even(),
                    (e.source().data() - e.dest().data()).norm(),
                )
            }));
            context.queue = pq;
        }

        let (mod_vs, mod_fs) = split_long_edges_with_queue(
            &mut context.mesh,
            high,
            &adaptive_target_length,
            &mut feat_verts,
            &mut feat_edges,
            &mut context.queue,
            parameters.target_additional_vertices,
            true,
        );
        debug_assert!(verify_topology(&context.mesh).is_none());

        context.modified_vertices.extend(mod_vs);
        context.modified_faces.extend(mod_fs);

        equalize_valences(
            &mut context.mesh,
            parameters.minimum_triangle_angle,
            &feat_edges,
        );

        tangential_relaxation(&mut context.mesh, &corners);

        debug_assert!(verify_topology(&context.mesh).is_none());
        debug_assert!(verify_geometry(&context.mesh));
        for v in context.mesh.iter_verts() {
            let proj = reproject(&v.data());
            v.mutate_data(&proj);
        }
    }
}

// +| Internal |+ ==============================================================
fn split_long_edges_with_queue<S, L, MeshType, T>(
    mesh: &mut MeshType,
    high: S,
    adaptive_target_length: &L,
    feature_verts: &mut HashSet<GeomId<0>>,
    feature_edges: &mut HashSet<(GeomId<0>, GeomId<0>)>,
    pq: &mut PQueue<GeomId<1>, S>,
    iter_cap: usize,
    return_modified: bool,
) -> (Vec<GeomId<0>>, Vec<GeomId<2>>)
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S>,
    S: RealField + num::traits::float::FloatCore,
    L: Fn(usize, usize) -> S,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let mut mod_verts = Vec::new();
    let mut mod_faces = Vec::new();

    let mut iter_count = 0;
    while !pq.is_empty() && (iter_count < iter_cap || iter_cap == 0) {
        let (e_id, weight) = pq.pop().unwrap();
        let hedge_handle = mesh.hedge_handle(e_id);
        debug_assert!(!hedge_handle.is_disabled());

        let adaptive_length = adaptive_target_length(
            hedge_handle.source().id().0 as usize,
            hedge_handle.dest().id().0 as usize,
        );
        let target_length = high * adaptive_length;
        debug_assert!(target_length > S::from(f32::EPSILON * 10.0).unwrap());
        if S::from(weight).unwrap() > target_length {
            let hedge_handle = mesh.hedge_handle(e_id);
            // TODO(low): properly handle boundary edges in remeshing.
            if hedge_handle.is_in_boundary_edge() {
                continue;
            }

            let in_feature = feature_edges
                .contains(&(hedge_handle.source().id(), hedge_handle.dest().id()));

            let (vid, _, new_edges) = hedge_handle.split::<S>();
            debug_assert!(verify_topology(mesh).is_none());

            if return_modified {
                mod_verts.push(vid);
                mod_faces
                    .extend(mesh.vert_handle(vid).iter_hedges().map(|h| h.face().id()));
            }

            if in_feature {
                feature_verts.insert(vid);

                let e = mesh.hedge_handle(new_edges[0]);
                feature_edges.insert((e.source().id(), e.dest().id()));
                feature_edges.insert((e.dest().id(), e.source().id()));

                let e = mesh.hedge_handle(new_edges[1]);
                feature_edges.insert((e.source().id(), e.dest().id()));
                feature_edges.insert((e.dest().id(), e.source().id()));
            }

            iter_count += 1;
        }
    }

    (mod_verts, mod_faces)
}

fn split_long_edges<S, L, MeshType, T>(
    mesh: &mut MeshType,
    high: S,
    adaptive_target_length: &L,
    feature_verts: &mut HashSet<GeomId<0>>,
    feature_edges: &mut HashSet<(GeomId<0>, GeomId<0>)>,
    iter_cap: usize,
) where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S>,
    S: RealField + num::traits::float::FloatCore,
    L: Fn(usize, usize) -> S,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    debug_assert!(verify_topology(mesh).is_none());
    let mut pq = PQueue::from_iter(mesh.iter_edges().map(|e| {
        (
            e.id().select_even(),
            (e.source().data() - e.dest().data()).norm(),
        )
    }));

    split_long_edges_with_queue(
        mesh,
        high,
        adaptive_target_length,
        feature_verts,
        feature_edges,
        &mut pq,
        iter_cap,
        false,
    );
}

fn collapse_short_edges<S, MeshType, L, T>(
    mesh: &mut MeshType,
    low: S,
    high: S,
    adaptive_target_length: &L,
    feature_verts: &mut HashSet<GeomId<0>>,
    feature_edges: &mut HashSet<(GeomId<0>, GeomId<0>)>,
    corner_verts: &mut HashSet<GeomId<0>>,
) where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S>,
    S: RealField + num::traits::float::FloatCore,
    L: Fn(usize, usize) -> S,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let is_feature_vert =
        |v: &VertHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>| {
            feature_verts.contains(&v.id())
        };

    let is_feature_edge =
        |e: &HEdgeHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>| {
            feature_edges.contains(&(e.source().id(), e.dest().id()))
        };

    let mut pq = PQueue::from_iter(mesh.iter_edges().map(|e| {
        (
            e.id().select_even(),
            (e.source().data() - e.dest().data()).norm(),
        )
    }));

    debug_assert!(verify_topology(mesh).is_none());
    let dmesh = HalfMeshDeleter::new(mesh);
    while !pq.is_empty() {
        let (e_id, weight) = pq.pop().unwrap();
        let e_handle = dmesh.hedge_handle(e_id);
        if !dmesh.is_edge_enabled(e_id) {
            continue;
        }

        let target_length = adaptive_target_length(
            e_handle.source().id().0 as usize,
            e_handle.dest().id().0 as usize,
        );

        if S::from(weight).unwrap() < low * target_length {
            // If an edge is not a feature and connects to a feature vert, do not
            // collapse.
            if !is_feature_edge(&e_handle)
                && (is_feature_vert(&e_handle.source())
                    || is_feature_vert(&e_handle.dest()))
            {
                continue;
            }
            // Never collapse boundary hedges.
            if e_handle.is_in_boundary_edge() {
                continue;
            }

            // Never collapse edges connecting to the corners.
            if corner_verts.contains(&e_handle.source().id())
                || corner_verts.contains(&e_handle.dest().id())
            {
                continue;
            }

            let a = e_handle.source();
            let b = e_handle.dest();
            debug_assert!(dmesh.is_vert_enabled(a.id()));
            debug_assert!(dmesh.is_vert_enabled(b.id()));
            let b = b.data().clone();

            let mut collapse_ok = true;
            for n in a.iter_verts() {
                if S::from((b.clone() - n.data()).norm()).unwrap() > high * target_length
                {
                    collapse_ok = false;
                }
            }

            if collapse_ok && e_handle.can_collapse() {
                let v_id = e_handle.collapse::<S>();
                dmesh.vert_handle(v_id).mutate_data(&b);
                debug_assert!(verify_topology(dmesh.mesh).is_none());
            }
        }
    }

    let vertex_permutation = dmesh.finish();
    debug_assert!(verify_topology(mesh).is_none());

    // Remap vertices after collapsing deletes some of them.
    *feature_verts = feature_verts
        .iter()
        .filter(|id| vertex_permutation.contains_key(&(id.0 as usize)))
        .map(|&id| GeomId::from(*vertex_permutation.get(&(id.0 as usize)).unwrap()))
        .collect();

    *corner_verts = corner_verts
        .iter()
        .filter(|id| vertex_permutation.contains_key(&(id.0 as usize)))
        .map(|&id| GeomId::from(*vertex_permutation.get(&(id.0 as usize)).unwrap()))
        .collect();

    *feature_edges = feature_edges
        .iter()
        .filter(|(id1, id2)| {
            vertex_permutation.contains_key(&(id1.0 as usize))
                && vertex_permutation.contains_key(&(id2.0 as usize))
        })
        .map(|(id1, id2)| {
            (
                GeomId::from(*vertex_permutation.get(&(id1.0 as usize)).unwrap()),
                GeomId::from(*vertex_permutation.get(&(id2.0 as usize)).unwrap()),
            )
        })
        .collect();
}

fn equalize_valences<MeshType, S, T>(
    mesh: &mut MeshType,
    minimum_angle: S,
    feature_edges: &HashSet<(GeomId<0>, GeomId<0>)>,
) where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S> + HasNormals<S>,
    S: RealField,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let is_feature_edge =
        |e: &HEdgeHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>| {
            feature_edges.contains(&(e.source().id(), e.dest().id()))
        };

    let target_val =
        |v: &VertHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>| {
            if v.is_in_boundary() {
                4
            } else {
                6
            }
        };

    let deviation = |hedge: &HEdgeHandle<
        MeshType::VertData,
        MeshType::EdgeData,
        MeshType::FaceData,
    >| {
        let a = hedge.source();
        let b = hedge.dest();
        let c = hedge.prev().source();
        let d = hedge.pair().prev().source();

        (a.valence() as isize - target_val(&a) as isize).abs()
            + (b.valence() as isize - target_val(&b) as isize).abs()
            + (c.valence() as isize - target_val(&c) as isize).abs()
            + (d.valence() as isize - target_val(&d) as isize).abs()
    };

    debug_assert!(verify_topology(mesh).is_none());
    for e in mesh.iter_edges() {
        // Don't flip if this would either break topology or flip a feature edge.
        if !e.can_flip() || e.is_in_boundary_edge() || is_feature_edge(&e) {
            continue;
        }

        let [o1, o2] = e.opposite_verts();
        let [v1, v2] = [e.source(), e.dest()];

        fn smallest_angle<S: RealField, V: VectorSpace<Scalar = S> + InnerSpace<S>>(
            v1: V,
            v2: V,
            v3: V,
        ) -> S {
            let d1 = (v2.clone() - v1.clone()).normalized();
            let d2 = (v3.clone() - v2.clone()).normalized();
            let d3 = (v1.clone() - v3.clone()).normalized();

            let a1 = (-d1.dot(&d2)).acos();
            let a2 = (-d2.dot(&d3)).acos();
            let a3 = (-d3.dot(&d1)).acos();

            a1.min(a2).min(a3)
        }

        let post_min_angle = smallest_angle(
            o1.data().position(),
            o2.data().position(),
            v1.data().position(),
        )
        .min(smallest_angle(
            o1.data().position(),
            o2.data().position(),
            v2.data().position(),
        ));

        if S::from(post_min_angle).unwrap() < minimum_angle {
            continue;
        }

        let deviation_pre = deviation(&e);
        e.flip();
        let deviation_post = deviation(&e);

        if deviation_pre <= deviation_post {
            e.flip();
            debug_assert!(verify_topology(mesh).is_none());
        }
    }
}

fn tangential_relaxation<S, MeshType>(
    mesh: &mut MeshType,
    corner_verts: &HashSet<GeomId<0>>,
) where
    <MeshType::VertData as PrimitiveContainer>::PrimitiveData:
        Clone + Debug + HasNormals<S>,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
    S: RealField,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let mut normals = Vec::new();
    if !<MeshType::VertData as PrimitiveContainer>::PrimitiveData::has_normals() {
        normals =
            <MeshType::VertData as PrimitiveContainer>::PrimitiveData::approx_normals(
                mesh.vert_count(),
                mesh.iter_faces()
                    .map(|f| f.hedge().iter_loop().map(|e| e.source().data().position())),
            );
    }

    let get_normal = |id: usize| {
        if <MeshType::VertData as PrimitiveContainer>::PrimitiveData::has_normals() {
            mesh.vert_handle(GeomId::from(id)).data().normal()
        } else {
            normals[id].clone()
        }
    };

    let mut barycenters =
        vec![mesh.vert_handle(GeomId(0)).data().position(); mesh.vert_count()];
    let mut valences = vec![0; mesh.vert_count()];
    for vert in mesh.iter_verts() {
        let mut valence = 0;
        for neighbour in vert.iter_verts() {
            barycenters[neighbour.id().0 as usize] += neighbour.data().position();
            valence += 1;
        }
        valences[vert.id().0 as usize] = valence;
    }

    for (i, &valence) in valences.iter().enumerate() {
        barycenters[i] =
            barycenters[i].clone() * (S::from(1.0).unwrap() / S::from(valence).unwrap());
    }

    for vert in mesh.iter_verts() {
        if corner_verts.contains(&vert.id()) {
            continue;
        }
        let i = vert.id().0 as usize;
        let normal = get_normal(i);
        let mut pos = vert.data().position();
        pos = pos.clone()
            + normal.clone() * normal.dot(&(pos.clone() - barycenters[i].clone()));
        vert.data().mutate_position(&pos);
    }
}

fn detect_features<S, MeshType, T>(
    mesh: &mut MeshType,
    dihedral_angle_tolerance: S,
) -> (
    HashSet<GeomId<0>>,              // Feature verts.
    HashSet<(GeomId<0>, GeomId<0>)>, // Feature edges.
    HashSet<GeomId<0>>,              // Corner verts.
)
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: InnerSpace<S>,
    S: RealField,
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    let is_feature_edge =
        |e: &HEdgeHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>| {
            let [o1, o2] = e.opposite_verts();
            let [v1, v2] = [e.source(), e.dest()];

            let n1 = (v1.data() - o1.data())
                .cross(&(v2.data() - o1.data()))
                .normalized();

            let n2 = (v1.data() - o2.data())
                .cross(&(v2.data() - o2.data()))
                .normalized();

            let dihedral_angle = n1.dot(&n2).acos();

            S::from(dihedral_angle).unwrap() < dihedral_angle_tolerance
        };

    let mut feature_verts = HashSet::new();
    let mut feature_edges = HashSet::new();
    for edge in mesh.iter_edges() {
        if is_feature_edge(&edge) {
            feature_verts.insert(edge.source().id());
            feature_verts.insert(edge.dest().id());

            feature_edges.insert((edge.source().id(), edge.dest().id()));
            feature_edges.insert((edge.dest().id(), edge.source().id()));
        }
    }

    // Find corners in the mesh.
    let mut corner_verts = HashSet::new();
    {
        let is_feature_edge =
            |e: &HEdgeHandle<
                MeshType::VertData,
                MeshType::EdgeData,
                MeshType::FaceData,
            >| { feature_edges.contains(&(e.source().id(), e.dest().id())) };

        for v in mesh.iter_verts() {
            let mut feature_edge_count = 0;
            for e in v.iter_hedges() {
                feature_edge_count += is_feature_edge(&e) as usize;
            }

            if feature_edge_count != 2 && feature_edge_count != 0 {
                corner_verts.insert(v.id());
            }
        }
    }

    (feature_verts, feature_edges, corner_verts)
}
// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use chicken_wire::wavefront_loader::ObjData;

    use crate::facets::GeomId;
    use crate::mesh::HalfMesh;
    use crate::remeshing::remeshing;
    use crate::traits::HalfMeshLike;

    #[test]
    fn test_remeshing() {
        let ObjData {
            vertices: verts,
            vertex_face_indices: verts_idx,
            ..
        } = ObjData::from_disk_file("../../Assets/low_poly_bunny.obj");
        let mut mesh = HalfMesh::new(verts, (), (), verts_idx);

        let length = mesh.hedge_handle(GeomId(0)).dir().norm() * 0.2;
        ObjData::export(&mesh, "tmp/pre_remeshed_bunny.obj");
        remeshing(
            &mut mesh,
            super::RemeshingParameters {
                minimum_triangle_angle: (20.0_f32).to_radians(),
                dihedral_angle_tolerance: (135.0_f32).to_radians(),
                iterations: 10,
            },
            |_, _| length,
            |a| *a,
        );

        ObjData::export(&mesh, "tmp/remeshed_bunny.obj");
    }
}
