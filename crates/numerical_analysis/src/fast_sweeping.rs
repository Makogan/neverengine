use algebra::*;
use iterators::raster_line::DDA;
use iterators::MultiIndex;

pub fn fast_sweeping_parametric<const N: usize>(
    corner: &Vec2,
    extent: &Vec2,
    dx: f32,
    f: &dyn Fn(&Vec2) -> f32,
    curve: &dyn Fn(f32) -> Vec2,
    range: (f32, f32),
    resolution: usize,
) -> Vec<f32>
{
    let mut dimensions = [0; N];
    for (i, dim) in dimensions.iter_mut().enumerate()
    {
        *dim = (extent[i] / dx) as i32;
    }

    let grid_size = dimensions.iter().fold(1, |prod, factor| prod * factor);
    let mut grid = vec![f32::MAX; grid_size as usize];

    rasterize_parametric_curve(&mut grid, corner, extent, dx, curve, range, resolution);

    fast_sweeping(corner, extent, dx, f, &mut grid);

    grid
}

pub fn rasterize_parametric_curve(
    grid: &mut [f32],
    corner: &Vec2,
    extent: &Vec2,
    dx: f32,
    curve: &dyn Fn(f32) -> Vec2,
    range: (f32, f32),
    resolution: usize,
)
{
    let stretch = range.1 - range.0;
    let mut points = Vec::new();
    for i in 0..resolution
    {
        let t = i as f32 / (resolution - 1) as f32;
        let t = t * stretch + range.0;
        points.push(curve(t));
    }

    for i in 0..points.len() - 1
    {
        let point = points[i];

        let relative_pos = point - corner;
        let box_corner = relative_pos.map(|x| x - (x % dx)) + corner;
        let index = relative_pos.map(|x| x.div_euclid(dx));

        let resolution: IVec2 = na::try_convert(extent / dx).unwrap();

        let dir = points[i + 1] - points[i];
        let distance = dir.norm();
        let dir = dir / distance;

        let mut line_iter = DDA::new(
            [point.x, point.y],
            [box_corner.x, box_corner.y],
            [dir.x, dir.y],
            [index.x as isize, index.y as isize],
            dx,
        );

        loop
        {
            let (t, [j, k]) = line_iter.next().unwrap();
            if t >= distance
            {
                break;
            }

            grid[(k * resolution.x as isize + j) as usize] =
                distance_to_segment(&(point + dir * t), &point, &points[i + 1]);
        }
    }
}

fn project_point_to_segment(point: &Vec2, origin: &Vec2, end: &Vec2) -> Vec2
{
    let len = (end - origin).norm();
    let dir = (end - origin) / len;
    let v = point - origin;

    let proj_distance = v.dot(&dir).clamp(0.0, len);

    return origin + dir * proj_distance;
}

fn distance_to_segment(point: &Vec2, origin: &Vec2, end: &Vec2) -> f32
{
    let proj = project_point_to_segment(point, origin, end);
    return (point - proj).norm();
}


// Implementation of:
// https://graphics.stanford.edu/courses/cs468-03-fall/Papers/zhao_fastsweep1.pdf
// Also read:
// https://math.berkeley.edu/~sethian/2006/Papers/sethian.vonkarman_1.pdf
pub fn fast_sweeping(
    corner: &Vec2,
    extent: &Vec2,
    dx: f32,
    f: &dyn Fn(&Vec2) -> f32,
    grid: &mut [f32],
)
{
    let mut dimensions = [0; 2];
    for (i, dim) in dimensions.iter_mut().enumerate()
    {
        *dim = (extent[i] / dx) as i32;
    }

    let coords_to_position = |coords: &IVec2| {
        let coords = na::convert::<IVec2, Vec2>(*coords);
        (coords * dx) + corner
    };

    for _ in 0..2
    {
        for [i, j] in MultiIndex::new([-1, -1], [3, 3], [2, 2])
        {
            sweep(
                grid,
                &dimensions,
                dx,
                f,
                &coords_to_position,
                &IVec2::new(i as i32, j as i32),
            );
        }
    }
}

fn check_neighbours(
    grid: &mut [f32],
    grid_dimensions: &[i32],
    dx: f32,
    coords: (usize, usize),
    coords_to_position: &dyn Fn(&IVec2) -> Vec2,
    f: &dyn Fn(&Vec2) -> f32,
)
{
    let grid_index = |i: usize, j: usize| j * grid_dimensions[1] as usize + i;

    // Clamp neighbour access.
    #[rustfmt::skip]
    let x_prev = if coords.0 == 0 { coords.0 } else { coords.0 - 1 };
    #[rustfmt::skip]
    let y_prev = if coords.1 == 0 { coords.1 } else { coords.1 - 1 };
    #[rustfmt::skip]
    let x_next =
        if coords.0 == grid_dimensions[0] as usize - 1 { coords.0 } else { coords.0 + 1 };
    #[rustfmt::skip]
    let y_next =
        if coords.1 == grid_dimensions[1] as usize - 1 { coords.1 } else { coords.1 + 1 };

    let u_x_next = grid[grid_index(x_next, coords.1)];
    let u_x_prev = grid[grid_index(x_prev, coords.1)];
    let u_y_next = grid[grid_index(coords.0, y_next)];
    let u_y_prev = grid[grid_index(coords.0, y_prev)];

    let ux = f32::min(u_x_next, u_x_prev).max(0.0);
    let uy = f32::min(u_y_next, u_y_prev).max(0.0);

    let pos = coords_to_position(&IVec2::new(coords.0 as i32, coords.1 as i32));

    grid[grid_index(coords.0, coords.1)] = grid[grid_index(coords.0, coords.1)].min(
        fast_sweep_quadratic_solve(&mut [ux, uy, f32::MAX], f(&pos) * dx),
    );
}

fn sweep(
    grid: &mut [f32],
    grid_dimensions: &[i32],
    dx: f32,
    f: &dyn Fn(&Vec2) -> f32,
    coords_to_position: &dyn Fn(&IVec2) -> Vec2,
    step_directions: &IVec2,
)
{
    // Initialize the start and end indices of the current sweep for each dimension.
    let mut iteration_bounds = [(0, 0); 2];
    for (dim, &step_dir) in step_directions.iter().enumerate()
    {
        iteration_bounds[dim] = if step_dir > 0
        {
            (0, grid_dimensions[dim] as isize - 1)
        }
        else
        {
            (grid_dimensions[dim] as isize - 1, 0)
        };
    }

    for [i, j] in MultiIndex::new(
        [iteration_bounds[0].0, iteration_bounds[1].0],
        [iteration_bounds[0].1, iteration_bounds[1].1],
        [step_directions[0], step_directions[1]],
    )
    {
        check_neighbours(
            grid,
            grid_dimensions,
            dx,
            (i as usize, j as usize),
            coords_to_position,
            f,
        );
    }
}

// equation 2.6 in Hongkai Zhao's paper:
// https://graphics.stanford.edu/courses/cs468-03-fall/Papers/zhao_fastsweep1.pdf
fn fast_sweep_quadratic_solve(a_coeffs: &mut [f32], fh: f32) -> f32
{
    // Fast sort.
    a_coeffs.sort_unstable_by(|a, b| a.total_cmp(b));
    // Special case if the solution is determined by the first coefficient.
    // Sw Zaho's paper.
    if a_coeffs[0] + fh < a_coeffs[1]
    {
        return a_coeffs[0] + fh;
    }
    // We are solving a sequence of quadratic equations of the form:
    // $ sum (x - a_i)^2 = (fh)^2 $ so our terms are:
    // $ a = p x^2 $
    // $ b = -2(sum a_i) $
    // $ c = (sum a_i^2) - fh $
    // Refer to Zhao's paper for details.
    for p in 2..a_coeffs.len()
    {
        let sum = a_coeffs[..p].iter().sum::<f32>();
        let sum_squares = a_coeffs[..p].iter().fold(0.0, |sum, x| sum + x * x);

        // Coefficients of the quadratic with index p.
        let a = p as f32;
        let b = -2.0 * sum;
        let c = sum_squares - fh * fh;

        let det = b * b - 4.0 * a * c;

        let s1 = -b - det.sqrt();
        let s2 = -b + det.sqrt();
        // Pick largest root.
        let solution = s1.max(s2) / (2.0 * a);

        if p == a_coeffs.len() - 1 || solution <= a_coeffs[p + 1]
        {
            return solution;
        }
    }

    panic!("Something on the SDF quadratic solver is broken.");
}
