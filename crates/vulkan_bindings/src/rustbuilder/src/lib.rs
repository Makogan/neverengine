use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::path::PathBuf;

use log::*;
use spirv_builder::{
    Capability,
    MetadataPrintout,
    ModuleResult::*,
    SpirvBuilder,
    SpirvMetadata,
};

pub fn compile_shader_crate(path: &String) -> Vec<PathBuf>
{
    let compile_result = {
        use std::io::Read;

        assert!(
            std::path::Path::new(path).join("src/lib.rs").exists(),
            "The shader crate at {} is not a valid rust crate, check its files.",
            path
        );

        // use gag::*;
        // let print_gag = Gag::stdout().unwrap();
        // let buf = BufferRedirect::stderr();

        let cached_state = setup_hacked_compiler_toolchain_env_vars();
        let result = SpirvBuilder::new(path, "spirv-unknown-vulkan1.2")
            .print_metadata(MetadataPrintout::None)
            .preserve_bindings(true)
            .spirv_metadata(SpirvMetadata::NameVariables)
            .scalar_block_layout(true)
            .multimodule(true)
            .capability(Capability::SampledImageArrayDynamicIndexing)
            .capability(Capability::RuntimeDescriptorArray)
            .extension("SPV_EXT_descriptor_indexing")
            .extra_arg("target-feature=+RuntimeDescriptorArray,")
            .build();
        restore_hacked_compiler_toolchain_env_vars(cached_state);
        // drop(print_gag);

        match result
        {
            Ok(res) => res,
            Err(err) =>
            {
                // let mut string = String::new();
                // buf.unwrap().read_to_string(&mut string).unwrap();

                // let highlighted = highlight(&string);
                // error!("Shader crate {} failure `{}`\n{}", path, err, highlighted);

                panic!("")
            }
        }
    };

    match &compile_result.module
    {
        SingleModule(path) => return vec![path.clone()],
        MultiModule(multimodule) =>
        {
            return multimodule.iter().map(|(_, path)| path.clone()).collect()
        }
    }
}

struct HackedEnvPriorState
{
    ld_library_path: Option<PathBuf>,
    cargo: Option<PathBuf>,
    rustc: Option<PathBuf>,
    rustup_toolchain: Option<PathBuf>,
}

/// Massive hack needed so that rustgpu runst the correct version of the
/// toolchain.
fn setup_hacked_compiler_toolchain_env_vars() -> HackedEnvPriorState
{
    let toolchain = "nightly-2024-01-08-x86_64-unknown-linux-gnu";

    let builder_path = PathBuf::from(std::env::var("DEM_SPIRV_BUILDER").unwrap());
    let mut builder_dir_path = builder_path;
    builder_dir_path.pop();
    let rustup_home = PathBuf::from(std::env::var("RUSTUP_HOME").unwrap());

    // Example:
    // /home/makogan/.rustup/toolchains/nightly-2024-01-08-x86_64-unknown-linux-gnu/
    // lib
    let lib_path = rustup_home.join(toolchain).join("lib");
    let shared_binaries_path = builder_dir_path;

    let ld_path = format!("{}:{}", lib_path.display(), shared_binaries_path.display());

    // Example:
    // /home/makogan/.rustup/toolchains/nightly-2024-01-08-x86_64-unknown-linux-gnu/
    // bin/rustc
    let rustc_path = rustup_home
        .join("toolchains")
        .join(toolchain)
        .join("bin/rustc");

    // Example:
    // "/home/makogan/.rustup/toolchains/
    // nightly-2024-01-08-x86_64-unknown-linux-gnu/bin/"
    let cargo_path = rustup_home.join("toolchains").join(toolchain).join("bin");

    let rustc_prior = std::env::var("RUSTC");
    let ld_library_path_prior = std::env::var("LD_LIBRARY_PATH");
    let cargo_prior = std::env::var("CARGO");
    let rustup_toolchain_prior = std::env::var("RUSTUP_TOOLCHAIN");

    let cached_state = HackedEnvPriorState {
        rustc: match rustc_prior
        {
            Ok(ref v) => Some(PathBuf::from(v)),
            Err(_) => None,
        },
        ld_library_path: match ld_library_path_prior
        {
            Ok(ref v) => Some(PathBuf::from(v)),
            Err(_) => None,
        },
        cargo: match ld_library_path_prior
        {
            Ok(ref v) => Some(PathBuf::from(v)),
            Err(_) => None,
        },
        rustup_toolchain: match rustup_toolchain_prior
        {
            Ok(ref v) => Some(PathBuf::from(v)),
            Err(_) => None,
        },
    };
    std::env::set_var("RUSTC", rustc_path);
    std::env::set_var("LD_LIBRARY_PATH", ld_path);
    std::env::set_var("CARGO", cargo_path);
    std::env::set_var("RUSTUP_TOOLCHAIN", toolchain);

    cached_state
}

fn restore_hacked_compiler_toolchain_env_vars(state: HackedEnvPriorState)
{
    if state.rustc.is_some()
    {
        std::env::set_var("RUSTC", state.rustc.unwrap());
    }

    if state.ld_library_path.is_some()
    {
        std::env::set_var("LD_LIBRARY_PATH", state.ld_library_path.unwrap());
    }

    if state.cargo.is_some()
    {
        std::env::set_var("CARGO", state.cargo.unwrap());
    }

    if state.rustup_toolchain.is_some()
    {
        std::env::set_var("RUSTUP_TOOLCHAIN", state.rustup_toolchain.unwrap());
    }
}

fn highlight(string: &String) -> String
{
    use synoptic::{Highlighter, TokOpt};
    let mut h = Highlighter::new(4);

    h.keyword("error", r"error\[.*\]");
    h.keyword("indicator_help", r"[^\|]*help:");
    h.keyword("indicator_err", r"[^\|]*error:");
    h.keyword("indicator_warn", r"[^\|]*warning:");
    h.keyword("indicator_raw", r"\^+");
    h.keyword("arrow", r"-->");
    h.keyword("num_bar", r"[0-9]+\s\|");
    h.keyword("bar", r"\s*\|");

    let message = string.split('\n').map(|line| line.to_string()).collect();
    h.run(&message);

    let mut result = "".to_string();
    let mut last_token_color = Fg::White;
    for (line_number, line) in message.iter().enumerate()
    {
        // Line returns tokens for the corresponding line
        for token in h.line(line_number, &line)
        {
            // Tokens can either require highlighting or not require highlighting
            match token
            {
                // This is some text that needs to be highlighted
                TokOpt::Some(text, kind) =>
                {
                    let colour = colour(&kind, &mut last_token_color);
                    result.push_str(&format!(
                        "{}{}{}{}{}",
                        colour,
                        bold(&kind),
                        text,
                        Fg::Reset,
                        lliw::BOLD_RESET,
                    ));
                }
                // This is just normal text with no highlighting
                TokOpt::None(text) => result.push_str(&text),
            }
        }
        // Insert a newline at the end of every line
        result.push_str("\n");
    }

    result
}

use lliw::Fg;
fn colour(name: &str, last_colour: &mut Fg) -> Fg
{
    // This function will take in the function name
    // And it will output the correct foreground colour
    match name
    {
        "error" =>
        {
            *last_colour = Fg::Red;
            *last_colour
        }
        "indicator_help" =>
        {
            *last_colour = Fg::Yellow;
            *last_colour
        }
        "indicator_err" =>
        {
            *last_colour = Fg::Red;
            *last_colour
        }
        "indicator_warn" =>
        {
            *last_colour = Fg::Yellow;
            *last_colour
        }
        "indicator_raw" => *last_colour,
        "arrow" => Fg::Blue,
        "num_bar" => Fg::Blue,
        "bar" => Fg::Blue,
        _ => panic!("unknown token name"),
    }
}

fn bold(name: &str) -> &str
{
    match name
    {
        "error" => lliw::BOLD,
        "indicator_help" => lliw::BOLD,
        "indicator_err" => lliw::BOLD,
        "indicator_warn" => lliw::BOLD,
        "indicator_raw" => lliw::BOLD,
        "arrow" => "",
        "num_bar" => "",
        "bar" => "",
        _ => panic!("unknown token name"),
    }
}
