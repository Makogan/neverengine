#![allow(dead_code)]
use std::f32::consts::PI;
use std::vec::*;

use algebra::Vec3;
use pga::*;

use crate::lyndermeyer::*;

const ROT_ANGLE: f32 = (5.0 / 72.0) * 2.0 * PI;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Alphabet {
    X,
    F,
    Plus,
    Minus,
    Pop,
    Push,
}

pub fn binary_tree_rules(symbol: &Alphabet) -> Vec<Alphabet> {
    let mut next_string = Vec::<Alphabet>::new();
    match &symbol {
        Alphabet::X => {
            next_string.push(Alphabet::F);
            next_string.push(Alphabet::Plus);
            next_string.push(Alphabet::Push);
            next_string.push(Alphabet::Push);
            next_string.push(Alphabet::X);
            next_string.push(Alphabet::Pop);
            next_string.push(Alphabet::Minus);
            next_string.push(Alphabet::X);
            next_string.push(Alphabet::Pop);
            next_string.push(Alphabet::Minus);
            next_string.push(Alphabet::F);
            next_string.push(Alphabet::Push);
            next_string.push(Alphabet::Minus);
            next_string.push(Alphabet::F);
            next_string.push(Alphabet::X);
            next_string.push(Alphabet::Pop);
            next_string.push(Alphabet::Plus);
            next_string.push(Alphabet::X);
        }
        Alphabet::F => {
            next_string.push(Alphabet::F);
            next_string.push(Alphabet::F);
        }
        val => {
            next_string.push(**val);
        }
    }

    next_string
}

pub fn geometry_rules(
    mut turtle: Turtle,
    turtle_stack: &mut Vec<Turtle>,
    symbol: &Alphabet,
) -> (Turtle, TurtleMovement) {
    match *symbol {
        Alphabet::F => {
            turtle.position += turtle.direction;
            (turtle, TurtleMovement::Forward)
        }
        Alphabet::Minus => {
            let rot = Rotor::from_axis_angle(ROT_ANGLE, &Vec3::new(0.0, 0.0, 1.0));
            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle, TurtleMovement::Stop)
        }
        Alphabet::Plus => {
            let rot = Rotor::from_axis_angle(-ROT_ANGLE, &Vec3::new(0.0, 0.0, 1.0));
            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle, TurtleMovement::Stop)
        }
        Alphabet::Pop => {
            turtle = turtle_stack.pop().unwrap();
            (turtle.clone(), TurtleMovement::Teleport(turtle.id))
        }
        Alphabet::Push => {
            turtle_stack.push(turtle.clone());
            (turtle, TurtleMovement::Stop)
        }
        _ => (turtle, TurtleMovement::Stop),
    }
}
