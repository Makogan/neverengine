#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use std::ops::Mul;

mod cpoint;
pub use crate::cpoint::*;
mod multivec3d;
pub use crate::multivec3d::*;
mod motor;
pub use crate::motor::*;
mod rotor;
pub use crate::rotor::*;
mod translator;
pub use crate::translator::*;
mod traits;

pub use crate::traits::*;

const BASIS: &'static [&'static str] = &[
    "1",     // Scalar
    "e0",    // Vectors / Planes
    "e1",    //
    "e2",    //
    "e3",    //
    "e01",   // Bivectors / Lines
    "e02",   //
    "e03",   //
    "e12",   //
    "e31",   //
    "e23",   //
    "e021",  // Trivectors / Points
    "e013",  //
    "e032",  //
    "e123",  //
    "e0123", // Pseudo scalar
];
const BASIS_COUNT: usize = BASIS.len();

pub const e0: MultiVec3D = MultiVec3D::new(1.0, 0);
pub const e1: MultiVec3D = MultiVec3D::new(1.0, 1);
pub const e2: MultiVec3D = MultiVec3D::new(1.0, 2);
pub const e3: MultiVec3D = MultiVec3D::new(1.0, 3);
pub const e123: MultiVec3D = MultiVec3D::new(1.0, 14);
pub const e0123: MultiVec3D = MultiVec3D::new(1.0, 15);
