# Projective Geometric Algebra

The primary reason why math is so useful is because it makes thinking and solving problems easier. Solving linear systems of equations is really useful for logistics, differential equations are great for physics, probability is awesome for finance.

Math is not hard, math is easy, it's just getting used to a new math that can be frustrating. But if you have ever done things that require just a little bit of thinking you know how much the right math makes solving problems easier.

Whith this in mind, I will share what I understand about PGA and why it makes thinking about geometry and graphics easier to deal with.

## Motivation

One of the most useful boosts to geometry problem solving is reasoning about problem in a coordinate free way. Of course all objects are in some coordinate frame when we codbut we really don't need to know which way is down or forward most of the time. Foexample, if we want to compute motion along a line, we don't care if the motiois $a$ units in one direction $b$ in another and $c$ in the last. We just want tmove along the line and we don't really care what the specific coordinates of the line
may be.

So math that is invariant across coordinate systems is awesome. Projecting a point onto a plane for example can be expressed as $v - ((v - p)\cdot \vec n  )\vec n$ where $v$ is the point $\vec n$ the normal to the plane and $p$ a point on the plane. This formula works regardless of the coordinate system we are working in, it works in higher dimensions as well. It's nice because it's easier to think about a few productof 3 symbols than to think about the underlying computation the machine needs to do. We don't care, the computer can deal with that, all we need is to be sure that we are telling the machine to do the right thing.

In this document I will first explore _what_ the gometric product is, and later _why_ it is useful.

## Vectors are not tuples

Very often, whe programming we think of vectors as tuples, in 3D we would hav$(x, y, z)$. But if you spend some time around mathematicians you might have seen the notation $x e_1 + y e_2 + z e_3$, where $e_1, e_2, e_3$ are the unit orthogonal vectors that form the basis. And when you think about what you are doing in classical lin alg, this notation actually makes more sense. A point with coordinates $(x, y, z)$ is actually what you get if you move along the $x$ ($e_1$) axis $x$ units, then move along the $y$ ($e_2$) axis $y$ units, and so on...

So implicitly the cartesian notation is just a shorthand for the idea of adding 3 vectors together, it's convenient but it's hiding information from us, vectors are not tuples, vectors are a sum of basis vectors scaled by some amount. This is key!

First it tells us that we can add stuff that cannot be further simplified (similar to complex numbers like $a + bi$), but it also tells us that tuples are a misleading way to think about geometry.

## There are more than just vectors

The next thing to wrap our head around is the existence of multi vectors. The idea is the following. We know what a scalar is. It's a real number (often), you can add them multiply them, etc. They are in a way a 0 dimensional object they don't reprsent space, or rather they represent the 0 space, what happens when things collapse into a single point.

Next we have vectors, which I will call 1-vectors or monovectors. Often times we think of monovectors as little arrows, but this too is incomplete. A monovector is a quantity embedded in some kind of space that behaves linearly with other elements. This sounds weird so let us think of 3D and basis vectors again. We can add vectors together, as we saw before, even when they are different objects. $e_1+e_2$ is legitimate expression even if we are adding apples to oranges. Their primary usefulnes is actually that they can't me merged further. We will say that monovectors generalize the idea of length. A sum of monovectors is, in a way, an oriented lenth, they are fundementally linear.

Now we move onto unfamiliar territory, just like monovectors are oriented lengths, bivectors or 2-vectors are oriented areas. You sometimes hear people call them orienteparallelograms but this is not right, they are oriented circles, oriented elipses, oriented parallelograms, oriented planar blobs... all at the same time, theare fundamentally what happens if you treat area as it's own thing rather than a property of a specific object. Esentially we are saying that just like distance can be built of 3 basis distances in 3D, areas can be built by combining basis areas, and just like lengths exist independently of the objects that can have those lenghtso areas can exist as their own objects, independently of the objects that can have areas.

And we can keep defining multivectors for any dimension, what are generally knowas k-vectors.

## Deriving the geometric product (in a specific instance)

In 3D we define 3 area elements $E_1, E_2, E_3$ that we will label $e_{12} = e_1e_2, e_{13} = e_1e_3, e_{23} = e_2e_3$. For now, we will just say that the notation $e_ie_j$ is a shorthand for a special kind of product known as the geometric product. All we care about is, if we arallowed to add things that don't simplify, we certainly are allowed tmultiply things that don't simplify. But with addition $e_1 + e_2 + 3e_1 = 4e_1 + e_2$.

So we want the products to also simplify in some cases, mainly when we multiply things together. Lets, start simple what should $e_1e_1$ yield? Well if the product was the dot product then that is just $e_1 \cdot e_1 = 1$. So lets take this as an axiom $e_ie_i = 1$.

If we keep piggy backing off the dot product, we now want to look at $(e_i + e_j)^2$ where the indices are different. Well that better be 2 because it's what the dot product gives, so $1 + e_ie_j + e_je_i + 1 = 2 \iff e_ie_j = -e_ie_j$ so we see that our product must be anti-commutiative.

Now what should $(e_ie_j)(e_ie_j)$ yield? Well can do

$$\begin{align*}
(e_ie_j)(e_ie_j) &= (e_ie_j)(-e_je_i) \\
                 &= -e_i(e_je_j)e_i \\
                 &= -e_ie_i \\
                 &= -1
\end{align*}$$

So now we have almost all definitions we need, we know how to add things and we know how to multiply a few things, but we are missing a few things. First, what happens with more products?

Notice that we only have 3 basis elements to choose from, so the only combination of unique elements we have not explored is $e_1e_2e_3$ and quite quickly we get

$$\begin{align*}
(e_1e_2e_3)^2 &= (e_1e_2e_3)(-e_1e_3e_2) \\
                 &= (e_1e_2e_3)(e_3e_1e_2) \\
                 &=  (e_1e_2)(e_1e_2)\\
                 &= -1
\end{align*}$$

Now if we have a string of $e_i$ with different values for $i$ with more than 3 elements, some of them have to repeat, so we can move them around, flipping the sign of the expression for every swap we do, until two elements have matching $i$ and simplify, just like we did in the prior case. We can do this until we have 3 or less symbols, so we can always simplify down to one of the above cases.

So we know how to multiply simple elements (called blades), elements which are purely products, but as said before, we also have sums, we have composite elements such as $e_1e_2 + e_2e_3$ which we cannot simplify further by construction. Well all we need to do is to distribute the product across. This means that in the most general case our product looks like this:

$$\begin{align*}
&(x_0 + x_1e_1 + x_2e_2 + x_3e_3 + x_4e_1e_2 + x_5e_1e_3 + x_6e_2e_3 + x_7 e_1e_2e_3) \\
\times &(y_0 + y_1e_1 + y_2e_2 + y_3e_3 + y_4e_1e_2 + y_5e_1e_3 + y_6e_2e_3 + y_7 e_1e_2e_3)
\end{align*}$$

We will not expand this out, the point is just noticing that the full product in the most general case is just exausting all possible perumations of binary products between elements in our algebra.

THIS is the goemetric product, the simplification of the above formula is the full geometric product.

## Generalizing into all geometric algebras

In the prior paragraphs I described 3 basis vectors $e_1, e_2, e_3$ and derived product operations from those 3. The main property we assumed, our axiom, was that they square to 1. A geometric algebra is usually expressed as $G_{p, q, r}$. $p$ is the number of basis that square to 1, $q$ those that square to $-1$ and $r$ those that square to 0. For each combination of those numbers we can go through the same process and derive a gometric product, just like we did for the case $p=3$ above.

Note that the full number of elements in the generalized sum with $n = p + q +r$ basis vectors (what we call a general multivector) is going to be $2^n$ split into groups of $n \choose k$ elements. So for example for $n=3$ we have 1 scalar, 3 monovectors, 3 bivectors, one trivector. For $p=3,r=1 \implies n=4$ we would have 1 scalar, 4 basis vectors, 6 bivectors, 4 trivectors and one tetravector. And so one. So Pascal's triangle actually tells us roughly how we expect our generalized multivector to look like.

In all cases the full geometric product is the simplification obtained after we exhaust all $n^2$ combinations of 2 element products in the generalized sum. i.e. when we do the sum of scalar times all elements plus $x_1e_1$ times all elements plus...