#![allow(dead_code)]

use std::ptr::copy_nonoverlapping;

use ash::vk::{self, Handle};

use crate::hardware_interface::*;
use crate::memory::*;
use crate::tools::*;

pub(crate) struct VulkanImage
{
    pub(crate) image: vk::Image,
    pub(crate) image_view: vk::ImageView,

    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) depth: u32,
    pub(crate) channel_num: u8,

    pub(crate) dimension: u8,

    pub(crate) format: vk::Format,
    pub(crate) sampler_adress_mode: vk::SamplerAddressMode,
    pub(crate) filter: vk::Filter,
    pub(crate) layout: vk::ImageLayout,
    pub(crate) aspect: vk::ImageAspectFlags,

    pub(crate) memory: *mut Memory,
    pub(crate) hardware_interface: *mut HardwareInterface,
}

impl VulkanImage
{
    pub(crate) fn empty_2d<'a>(
        hardware_interface: &'a mut HardwareInterface,
        memory: &'a mut Memory,
        format: vk::Format,
        channel_num: u8,
        usage: vk::ImageUsageFlags,
    ) -> VulkanImage
    {
        return Self::from_gpu_only_info_2d(
            hardware_interface,
            memory,
            format,
            vk::Extent2D {
                width: 1,
                height: 1,
            },
            channel_num,
            usage,
        );
    }

    pub(crate) fn new<'a>(
        hardware_interface: &'a mut HardwareInterface,
        memory: &'a mut Memory,
        buffer: Option<*const u8>,
        extent: vk::Extent3D,
        channel_num: u8,
        format: vk::Format,
        tiling_mode: vk::SamplerAddressMode,
    ) -> VulkanImage
    {
        let mut image = Self::load_buffer(
            hardware_interface,
            memory,
            buffer,
            &extent,
            channel_num,
            format,
        );
        image.sampler_adress_mode = tiling_mode;

        if extent.depth > 0
        {
            image.transition_layout(
                vk::ImageLayout::GENERAL,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
            )
        }

        return image;
    }

    fn load_buffer<'a>(
        hardware_interface: &'a mut HardwareInterface,
        memory: &'a mut Memory,
        buffer: Option<*const u8>,
        extent: &vk::Extent3D,
        channel_num: u8,
        image_format: vk::Format,
    ) -> VulkanImage
    {
        debug_assert!(channel_num > 0);
        let format = image_format;
        let width = extent.width;
        let height = extent.height;
        let depth = extent.depth;
        let channel_num = channel_num;
        let dimension = if depth == 0 { 2 } else { 3 };

        // This is the total memory usage of the image. 2D images have a depth of 0,
        // hence the max. The size will vary depending on the format, for
        // example an image with R8B8G8A8 format uses one byte per pixel per
        // channel (e.g. one byte for a red pixel or one byte for an alpha
        // pixel), but an R32 uses 4 bytes for each red pixel.
        let channel_pixel_size = format_to_channel_pixel_size(image_format);
        let image_size: vk::DeviceSize = (width
            * height
            * u32::max(1, depth)
            * channel_num as u32
            * channel_pixel_size) as _;

        let staging_buffer =
            memory.create_buffer(image_size as usize, vk::BufferUsageFlags::TRANSFER_SRC);

        let staging_buffer_allocation = memory
            .buffer_allocations
            .get(&staging_buffer.as_raw())
            .unwrap();

        // Copy data from pointer onto vulkan buffer.
        unsafe {
            let data_dst = staging_buffer_allocation.mapped_ptr().unwrap();

            match buffer
            {
                Some(b) => copy_nonoverlapping(
                    b,
                    data_dst.as_ptr().cast::<u8>(),
                    image_size as usize,
                ),
                None =>
                {
                    for i in 0..image_size as usize
                    {
                        data_dst.as_ptr().cast::<i8>().add(i).write(0)
                    }
                }
            }
        };

        let usage = if format == vk::Format::D32_SFLOAT_S8_UINT
        {
            vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
        }
        else
        {
            vk::ImageUsageFlags::COLOR_ATTACHMENT
                | vk::ImageUsageFlags::SAMPLED
                | vk::ImageUsageFlags::STORAGE
        };

        let aspect = if format == vk::Format::D32_SFLOAT_S8_UINT
        {
            vk::ImageAspectFlags::DEPTH | vk::ImageAspectFlags::STENCIL
        }
        else
        {
            vk::ImageAspectFlags::COLOR
        };

        let (image, image_view) = memory.create_image(
            width,
            height,
            depth,
            format,
            vk::ImageTiling::OPTIMAL,
            usage | vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::TRANSFER_SRC,
            vk::ImageLayout::UNDEFINED,
        );

        let mut image = VulkanImage {
            image,
            image_view,
            width: extent.width,
            height: extent.height,
            depth: extent.depth,
            dimension,
            channel_num,
            format,
            aspect,
            sampler_adress_mode: vk::SamplerAddressMode::REPEAT,
            filter: vk::Filter::LINEAR,
            layout: vk::ImageLayout::UNDEFINED,
            memory,
            hardware_interface,
        };

        image.transition_layout(
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vk::PipelineStageFlags::TRANSFER,
        );

        memory.copy_buffer_to_image(
            &staging_buffer,
            &image.image,
            width,
            height,
            depth,
            aspect,
        );

        if format == vk::Format::D32_SFLOAT_S8_UINT
        {
            image.transition_layout(
                vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                vk::PipelineStageFlags::TOP_OF_PIPE,
            );
        }
        else
        {
            image.transition_layout(
                vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
            );
        }
        memory.destroy_buffer(staging_buffer);

        return image;
    }

    pub(crate) fn from_gpu_only_info_2d<'a>(
        hardware_interface: &'a mut HardwareInterface,
        memory: &'a mut Memory,
        format: vk::Format,
        extent: vk::Extent2D,
        channel_num: u8,
        usage: vk::ImageUsageFlags,
    ) -> VulkanImage
    {
        let attachment_usage = if format == vk::Format::D32_SFLOAT_S8_UINT
        {
            vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
        }
        else
        {
            vk::ImageUsageFlags::COLOR_ATTACHMENT
        };
        let usage = usage
            | attachment_usage
            | vk::ImageUsageFlags::TRANSFER_DST
            | vk::ImageUsageFlags::TRANSFER_SRC;

        let (image, image_view) = memory.create_image(
            extent.width,
            extent.height,
            0,
            format,
            vk::ImageTiling::OPTIMAL,
            usage,
            vk::ImageLayout::UNDEFINED,
        );

        let mut image = VulkanImage {
            image: image,
            image_view: image_view,
            width: extent.width,
            height: extent.height,
            depth: 1,
            dimension: 2,
            channel_num,
            format,
            aspect: if attachment_usage == vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
            {
                vk::ImageAspectFlags::DEPTH | vk::ImageAspectFlags::STENCIL
            }
            else
            {
                vk::ImageAspectFlags::COLOR
            },
            sampler_adress_mode: vk::SamplerAddressMode::REPEAT,
            filter: vk::Filter::LINEAR,
            layout: vk::ImageLayout::UNDEFINED,
            memory,
            hardware_interface,
        };

        if attachment_usage == vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
        {
            image.transition_layout(
                vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                vk::PipelineStageFlags::TOP_OF_PIPE,
            );
        }

        image
    }

    pub(crate) fn transition_layout(
        &mut self,
        new_layout: vk::ImageLayout,
        source_stage: vk::PipelineStageFlags,
    )
    {
        if self.layout == new_layout
        {
            return;
        };

        let barrier = vk::ImageMemoryBarrier {
            old_layout: self.layout,
            new_layout: new_layout,
            dst_access_mask: deduce_access_flags(new_layout),
            src_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            image: self.image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: self.aspect,
                layer_count: 1,
                base_array_layer: 0,
                level_count: 1,
                base_mip_level: 0,
                ..Default::default()
            },

            ..Default::default()
        };

        let destination_stage = deduce_destination_stage(new_layout);

        let command_buffer = self.hardware_interface().begin_single_time_command();

        unsafe {
            self.hardware_interface().device.cmd_pipeline_barrier(
                command_buffer,
                source_stage,
                destination_stage,
                vk::DependencyFlags::default(),
                &[],
                &[],
                &[barrier],
            );
        }

        self.hardware_interface()
            .end_single_time_command(command_buffer);

        self.layout = new_layout;
    }

    fn hardware_interface(&self) -> &HardwareInterface
    {
        unsafe { &*self.hardware_interface }
    }
}

impl Drop for VulkanImage
{
    fn drop(&mut self)
    {
        unsafe {
            (*self.hardware_interface)
                .device
                .destroy_image_view(self.image_view, None);
            (*self.memory).destroy_image(self.image);
        }
    }
}

// +| Internal |+ ==============================================================
fn deduce_access_flags(layout: vk::ImageLayout) -> vk::AccessFlags
{
    match layout
    {
        vk::ImageLayout::TRANSFER_SRC_OPTIMAL => return vk::AccessFlags::TRANSFER_READ,
        vk::ImageLayout::TRANSFER_DST_OPTIMAL => return vk::AccessFlags::TRANSFER_WRITE,
        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL =>
        {
            return vk::AccessFlags::SHADER_WRITE;
        }
        vk::ImageLayout::GENERAL =>
        {
            return vk::AccessFlags::SHADER_WRITE | vk::AccessFlags::SHADER_READ;
        }
        vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL =>
        {
            return vk::AccessFlags::COLOR_ATTACHMENT_WRITE;
        }
        vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL =>
        {
            return vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ
                | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;
        }

        _ => panic!("Unrecognized layout {:?}.", layout),
    }
}

fn deduce_destination_stage(layout: vk::ImageLayout) -> vk::PipelineStageFlags
{
    match layout
    {
        vk::ImageLayout::TRANSFER_SRC_OPTIMAL | vk::ImageLayout::TRANSFER_DST_OPTIMAL =>
        {
            return vk::PipelineStageFlags::TRANSFER;
        }
        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL =>
        {
            return vk::PipelineStageFlags::FRAGMENT_SHADER;
        }
        vk::ImageLayout::GENERAL =>
        {
            return vk::PipelineStageFlags::COMPUTE_SHADER;
        }
        vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL =>
        {
            return vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT;
        }
        vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL =>
        {
            return vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS;
        }
        _ => panic!("Unreconginzed layout."),
    }
}
