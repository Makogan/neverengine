use chicken_wire::skeleton_to_graph;
use clap::*;

/// Simple program to extract the skeleton of a gltf file
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// Path to an input gltf file containing the edges of the skeleton.
    #[arg(short, long)]
    input_path: String,

    /// Path in which the mesh file will be written to.
    #[arg(short, long)]
    output_path: String,

    // Name for the files
    #[arg(short, long, default_value_t = ("o".to_string()))]
    name_file: String,
}

fn main()
{
    let args = Args::parse();

    let (meshes, skins, skeletons, _animations, _associations) =
        chicken_wire::load_skinned_data_from_path(args.input_path.as_str());

    for (i, skeleton) in skeletons.iter().enumerate()
    {
        let (ps, es) = skeleton_to_graph(&skeleton);
        panic!();
        // ObjData::export(
        //     &ps,
        //     &es,
        //     &vec![],
        //     format!("{}/{}_{}.obj", args.output_path, args.name_file,
        // i).as_str(), );
    }
}
