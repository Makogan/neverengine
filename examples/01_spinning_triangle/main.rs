use algebra::*;
use dem_core::{gpu_api::*, rendering::*};
use dem_peripherals::*;

use vulkan_bindings::RenderContext;

#[repr(C)]
struct Vertex {
    position: Vec2,
    color: Vec3,
}
fn main() {
    let mut io_context = IoContext::new("Spinning Triangle", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let spinning_shader = render_context.add_shader(vec![
        "examples/01_spinning_triangle/spinning_triangle_shader".to_string(),
    ]);

    let triangle = [
        Vertex {
            position: Vec2::new(0.0, 0.5),
            color: Vec3::new(0.0, 1.0, 0.0),
        },
        Vertex {
            position: Vec2::new(-0.5, -0.5),
            color: Vec3::new(1.0, 0.0, 0.0),
        },
        Vertex {
            position: Vec2::new(0.5, -0.5),
            color: Vec3::new(0.0, 0.0, 1.0),
        },
    ];

    let gpu_mesh = GraphicsInput::new()
        .add_attribute_buffer_from_slice(&triangle, &mut render_context);

    let mut time = 0.00;
    while io_context.poll_io(&mut None) {
        render_context.start_frame(&io_context);
        time += 0.02;
        let mat = Mat2::new(
            f32::cos(time),
            -f32::sin(time),
            f32::sin(time),
            f32::cos(time),
        );

        let render_request = RenderRequest::new(spinning_shader)
            .vertex_input(gpu_mesh.clone())
            .add_ubo(0, mat)
            .build();

        render_context.draw(&render_request);
    }

    render_context.free_input(gpu_mesh);
}
