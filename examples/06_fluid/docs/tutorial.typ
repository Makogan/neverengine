
#import "dependencies/algorithm.typ" : *

#let matx = $bold(X)$
#let worx = $bold(chi)$
#let bf = $bold(F)$
#let jacob(x, y) = $frac(diff #x, diff #y)$
#let jacobmat(x) = $jacob(#x, matx)$

#set math.equation(numbering: "(1)")

#align(center, text(17pt)[
  *MLS-MPM for fluids*
])

#grid(
  columns: (1fr),
  align(center)[
    Camilo Talero \
    University of British Columbia
  ],
)

#set heading(numbering: "I.")

#outline()

= Introduction

This document will attempt to explain the paper MLS-MPM @hu2018moving from a practical implementation point of view. I will do my best to convey as much of the mathematical formalism from the literature but the main goal is to explain how the paper works from an implementation point of view.

== Eulerian and Lagrangian views of fluid

There are two ways to look at fluids in terms of mathematics, traditionally referred to as the Eulerian view and the Lagrangian view.

The Eulerian view sees the space in which the fluid is, and studies how material properties change over time on that space. Commonly this means we have a cartesian grid and each cell has properties (e.g. temperature, pressure...) that we are interested in. As the fluid moves and evolves, these properties will change on a particular cell and we study how that happens. The Lagrangian view instead sees the fluid as a collection of particles whose intrinsic properties change over time. As shown in @fluid_views.

#figure(
    table(
        columns: 2,
        stroke: none,
        image("eulerian.svg", width: 60%),
        image("lagrangian.svg", width: 60%),
        "Eulerian", "Lagrangian"
    ),
  caption: [In the Eulerian view we focus on how the properties of a fixed parcel of space change over time as the fluid flows through it. In the Lagrangian view we focus on how a specific block of the fluid (a "particle") evolves over time as it moves and deforms.],
) <fluid_views>

Each view is useful for different things. MPM is a hybrid method, meaning we will combine both strategies, leveraging advantages each has, to solve for different aspects of the simulation. In general, Eulerian refers to a 2D or 3D grid, and Lagrangian refers to discrete particles for the purposes of implementation.

These are our data structures/representation, the first is just a 2D/3D array like any other, i.e. something we can index like $M_(i j)$ or $M[i][j]$ in code. So the particle and grid would look something like @DataStructures.

#figure(
table(
columns : (1fr, 1fr),
inset: 1pt,
align: left,
stroke: none,
    block(
    fill: luma(230),
    inset: 8pt,
    radius: 4pt,

    ```rs
    struct Particle
    {
        mass: f32,
        position: Vec2,
        velocity: Vec2,
        // Other properties.
    }
    ```),

    block(
    fill: luma(230),
    inset: 8pt,
    radius: 4pt,

    ```rs
    struct GridCell
    {
        mass: f32,
        velocity: Vec2,
        // Other properties.
    }

    struct Grid
    {
        // Flattened array (2D or 3D),
        cells : Vec<GridCell>
    }
    ```)
),
caption: "Code representation of our data")<DataStructures>

== The Navier-Stoke equations

Now we know how we are going to represent our fluid, next we need to understand what laws/forces govern our simulation. This is given by the Navier-Stoke equations.

$ underbrace(rho, "density")(underbrace(frac(diff arrow(v), diff t) + arrow(v) dot.op nabla arrow(v), "material derivative")) = -nabla underbrace(p, "pressure")+ overbrace(nu, "viscosity") overbrace(nabla dot.op underbrace(nabla arrow(v), "stress"), "laplacian") + underbrace(arrow(f), "body forces") $<NavierStoke>

Now it is useful to think about the underlying computations each term will be subjected to, so I will annotate the types of each term. Note we assume that density and viscosity will be constant for all particles belonging to the same material/fluid.

$ underbrace(rho, "scalar")(underbrace(frac(diff arrow(v), diff t),"vector") + overbrace(arrow(v),  "vector") dot.op underbrace(nabla arrow(v), "Jacobian matrix")) = -underbrace(nabla p, "gradient vector")+ underbrace(nu, "scalar") overbrace(nabla dot.op underbrace(nabla arrow(v), "stress matrix"), "vector") + underbrace(arrow(f), "vector") $

For the purposes of simulation we also add a constraint which is not technically true at microscopic scales but true enough for the scale at which we simulate fluids. The incompressibility condition, also known as conservation of mass that is:

$ nabla dot.op arrow(v) = arrow(0) $

This condition basicallly tells us that particles cannot "squish" together, a particle can only move by first pushing all particles in front of it aside to make space for itself. In reality fluids are compressible, but it requires a tremendous amount of force to compress them even a little bit, so at macroscopic scales we can ignore the effects of compression, they will be negligible.

== Geometry of the fluid

Because we are studying the evolution of a fluid over time we must be able to extract all the variables from the prior section. i.e. we  need to define valid differential operators, here's were differential geometry comes to the rescue.

We will say that at time $t=0$ our fluid has some "shape". This we will consider the "starting" or "material" configuration of the fluid. As time passes the fluid/body will deform, inducing stress forces on itself depending on the relative velocities around each point. Points that are moving at the same speed as their neighbours expereince no internal forces, but a point that is moving faster than it's neighbours is having to push ofther material around itself, this in turn produces a force pushing the point such that the velocity of that point matches that of its surroundings.

#figure(
    image("manifolds.svg", width: 60%),
    caption: "As time passes a fluid/body deforms under external forces, this will produce internal forces that also affect the future evolution of the fluid/body."
)<blobChange>

Let's call $Omega_0$ the set of points of our fluid at time $0$, and define $matx$ to be what some resources call material coordinates (the coordinates for $Omega_t$). Call $Omega_t$ the coordinates at time $t$, and  $worx$ the world coordinates. We can define a map $Phi(X, t): Omega_0 arrow Omega_t := Phi(matx,t) = worx$ a map from the coordinates of starting configuration of the fluid to the current one.

Handwavily we can say that $Phi$ maps our starting fluid to our fluid at time $t$\*. Let us fix $t$ as a constant.

Since $Phi_t$ is a function that maps vectors to vectors, it has a well defined Jacobian matrix $bf$:

$ bf := frac(diff Phi_t, diff matx)(matx) $

Let us write things explicitly in 2D:

$ Phi_t(x, y) = (U(x, y), V(x, y)) $

Where $U,V : RR^2 arrow RR$ are the world coordinates. So:

#set align((center))
$jacobmat(Phi_t) = mat(
  delim: "[",
  jacob(U(x, y), x), jacob(U(x, y), y);
  jacob(V(x, y), x), jacob(V(x, y), y);
) =
mat(
  delim: "[",
  nabla U;
  nabla V;
)
=
mat(
  delim: "[",
  jacob(dot.op, x), jacob(dot.op, y);
  jacob(dot.op, x), jacob(dot.op, y);
) dot.op vec(U(x, y), V(x, y))
$
#set align((left))

From here it is easy to see that for any dimension the Jacobian is a matrix made by the tangent vectors to the coordinate curves of our target manifold (in the setting we have defined).

#figure(
    table(
    columns: 3,
    stroke: none,
    image("stretching_before.svg", width: 50%),
    image("stretching_compressed.svg", width: 50%),
    image("stretching_stretched.svg", width: 50%),
    "Original", "Compression", "Stretching"
    ),
    caption: [Effects of a 2D linear transformation $J_Phi$ on a set of basis vectors. Compression would correspond to a determinant of $det(J_Phi)<1$, stretching to one of $det(J_Phi)>1$.]
)<jacobian_stretching>

#block(
  fill: luma(230),
  inset: 8pt,
  radius: 4pt,
  "*Note: This is a little bit of a lie. It maps the coordinates of one to the coordinates of the other, but we will ignore the mathematical formalism."
)

Note that in the source manifold, the fluid/body at time $t=0$, the coordinate curves are just the Euclidean axes $x, y$. Thus the Jacobian we calculated is indeed a linear map from the tangent space of the material coordinates into the tangent space of the world coordinates. If our map $Phi_t$ is a rigid transformation, then the Jacobian will be an orthogonal matrix and thus have a determinant of 1. This matches our intution that if we just translate and rotate jelly without deforming it it should not experience any internal deformation forces.

This kind of analysis on the Jacobian matrix allows us to determine the local deformation intrinsic to a map. For example we can view the velocity of our fluid at some time $t$ to also be a map $Phi$ like before. And we can see how much deformation it induces in its output by looking at the determinant. In a fluid moving at a constant velocity for example, the Jacobian of the velocity field within the fluid is the 0 matrix, thus it experiences no stress forces anywhere, which matches our expectation.

#figure(
    image("manifold.svg", width: 60%),
    caption: [The different mapping relationships between points in the fluid and its tangent space at the  beginning of the simulation and after some time. $Phi$ maps points to points, its differential, i.e. its Jacobian, maps the tangent space in the source to that of the target.]
)<manifold_pushforward>

== Governing equations

For the purposes of our simlation there are two main equations we want our fluid to  satisfy at all times:

$ R(matx, t)J(matx, t) = R(X, 0) $

Where $R$ is the Lagrangian mass density (i.e. conservation of mass). Basically, the combined effect of the current density and the deformation must cancel out to the starting density of the fluid. In layman terms, if the fluid/body is being stretched then the density must be low, and conversely if it is being compressed then the density must increase, both as a funciton of the original density.

The other is the conservation of momentum:
$ R(matx, 0) (diff V)/(diff t) = nabla^matx dot.op P + R(matx, 0)g $

The left is the mass density times velocity, so it is momentum per unit of volume. The right is composed of the divergence of the stress and the impulse per volume ($g$ is the external forces acting on the body, mostly gravity).

= MLS-MPM

== Algorithm Preliminaries <AlgorithmPreliminaries>

The MLS-MPM algorithm itself is straightforward at a high level. The idea is to use both particles (Lagrangian) and a grid (Eulerian) to run the simulation. The particles will be used to advect (update) the simulation, while the grid will be used to enforce the physical constraints (conservation of mass, conservation of momentum).

In particular, each particle will influence a number of cells around it based on an interpolation function. In the case of a cubic spline, the neighbourhood would be the 25 adjacent cells surrounding the particle. In the case of a quadratic spline (the best choice if we intend for the simulation to be fast instead of accurate) only 9 neighbouring cells are needed, like in figure @particle_cells.

#figure(
    image("fluid_grid.svg", width: 20%),
    caption: [Cells surrounding a particle.]
)<particle_cells>

We will compute the axis aligned distances (i.e. the component wise differences between the coordinates) of the particle to the centers of its surrounding cells. When scattering we will write the current values of the particle to the cell, weighted by some interpolation function (quadratic spline in our case). More formally, let $d_i(p, c) = c_i - p_i$ where $c_i, p_i$ are the $i^(t h)$ components of the cell center and particle position respectively. Let $l(d)$ be an interpolation function that returns a weight based on the above distances. Then for a cell, particle pair, their relative weight becomes:

$ w_(p, c) = product^n_(i=1)l(d_i(p, c)) $

Noting that this should only be evaluated on the neighbourhood where the interpolation function is supported (the 9 cells in the case of a quadratic spline in the 2D case). $n$ is the dimension of the problem, so 2 for a planar simulation.

So for example, if $m_p$ is the mass of a particle, if we are to scatter this quantity then for each cell we would add $w_(p, c) dot.op m_p$ to the cell with center $c$. If we are to gather the mass from the grid to the current particle we would compute:

$ m_p = sum_(i=-r)^r sum_(j=-r)^r w(p,c_(i j))dot.op m_c_(i j) $

Where $r$ is the radius of the interpolation kernel (1 in the case of the quadratic spline, leading to 9 visited cells), $m_c_(i j)$ is the mass of cell $c_(i j)$ and $m_p$ is the gathered mass of the particle.

A similar gahtering step will be performed for other quantities such as the velocity.

== Constitutive Equations

Depending on what we are trying to simulate, the specific quantities and the calculations that must be executed change. The rules needed to simulate an elastic solid are slightly different than those needed to simulate a fluid. We need then different mathematical models depending on what we are trying to simulate. The specific set of mathematical rules will be called a constitutive equation.

For example for an elastic body made out of MPM particles we would use a NeoHookean constitutive equation and a fluid would use an equation of state based on pressure (as potential examples).

=== Fluid Constitutive model

Fluids only experience stress forces under velocity differences, if the fluid moves rigidly then it experiences no internal stress (as there is no strain). From @NavierStoke, we get that the relevant forces acting on a particle at a given time are given by the gradient of the pressure $nabla p$ and the Laplacian of the velocity field $Delta arrow(v)$. Said differently, we have two internal forces acting on a particle, forces induced by pressure differences and forces induced by viscous stress due to differences in the velocity surrounding that particle.

==== Pressure

Let us first focus on pressure. Each particle in our simulation will be imbued with mass $m_p$, analogously each cell in our grid has a corresponding mass $m_c$. The mass of a cell at some simulation step is a weighted sum of the particles surrounding it. In our case we use the spline weights from @AlgorithmPreliminaries. I.e. at a given simulation step we have for a given cell:

$ m_c = sum_(i=1)^n w(p_i, c) m_(p_i) $

Where $w$ is the exact same weight function we used before defined over the same kernel. Most particles will have a weight of $0$, thus we can ignore them during computation as an optimization.

With the mass we can then compute the density, as each cell has a fixed volume $V$ equal to $h^d$ where $h$ is the side length of a voxel and $d$ is the dimension (we use "volume" loosely to refer to the dimensional volume element, so in 2D we mean area and in 4D we mean the 4D volume element). And so we have our cell density:

$ rho_t = m_c/(V^d) $

As our fluid simulation deforms our starting particles, the densities of cells will change, leading to pressure differences. We will use the following pressure formulation for the grid centers:

$ p = k(rho_t/rho_0)^gamma - 1 $ 

Where $k$ is a stiffness coefficient governing how strong the pressure forces will be, $gamma$ is a user parameter to modulate the behaviour of the fluid (i.e. an artistic parameter rather than a physical parameter) and $rho_0$ is the pressure of the fluid at rest (or at the start of the simulation). We are working with the assumption that the fluid is shallow, thus the effect of weight/gravity on the pressure is negligible when compared to density differences.

The stress excerted by the pressure field at a given particle will then be:

$ S = -p I + nu (nabla arrow(v) + nabla arrow(v)^T) $

As specified by equation 17 in @hu2018moving.

==== Momentum

Just like with mass, we can transfer momentum between the grid and the particles using the same spline weights. Since particles can have angular momentum we must be able to express it in some form. If we pick the cell center as the cener of our reference frame from which we compute momentum. According to @Jiang_2017 we can create an "inertia-like" matrix $D_i$ as follows:

$ D_i = w_(i j)(x_c - x_i)(x_c - x_i)^T $

Where $(x_c - x_i)$ is the vector difference of the cell center and the particle's position. @Jiang_2017 claims that for quadratic splines this simplifies to $D_i = 1/4 Delta x^2 I$. I will have to trust the authors here.

With that defined we can then express a matrix to pass momentum from the grid to the particles (again from @Jiang_2017):

$ B_p = sum_(i=1)^n w(p_i, c) arrow(v)_i (x_c - p_i)^T $

So in total our contribution from the particles to a grid cell's momentum is:

$ m_c v_c = sum_(p) w(p_i, c)m_i(arrow(v_i) + 1/4 B (x_c - p_i)) $

And the contribution from the grid cells to a particle is similar but inverting their roles in the formulation:

$ m_i v_i = sum_(c) w(p, c_i)m_i(arrow(v_i) + 1/4 B (x_i - p)) $

We can thus use the collocation grid to update momentum and then transfer the result to the particles through the use of equation 16 in @hu2018moving.

=== Neo Hookean Model (Solids)

Following @siggraph, if w want to model an elastic solid, then at a point the energy density function is given by:

$ Psi(F) = mu / 2(t r a c e(F^T F) − d)− mu log(J) + lambda / 2 log^2(J) $

Where $d$ is the dimension, $F$ is the deformation gradient (Jacobian), $J$ its determinant and $mu, lambda$ are physcal modeling parameters governing the sitffness of the solid.

In these case we get the following stress tensor:

$ mu(F − F^(-T) ) + lambda log(J)F^(-T) $

From here we can compute the update in the particles to the grid cells transfer as in the fluid case.
We just need to replace the stress matrix in the Navier Stokes with the tensor we just computed.



= Algorithm

Combining all the above information we get the following algorithm:

#algo(
  title: "MLS-MPM Simulation",       // title and parameters are optional
  // parameters: ("",)
)[
    Reset grid cells to default (0 out velocity, mass, etc..)#i\

    \/\/ Cell initialization.\
    for each particle#i\
      calculate 9 interpolation weights for the 9 surrounding cells\

      for each surrounding cell#i\
        calculate the cell distance weighted momentum matrix\
        calculate the interpolation weighted mass contribution\
        scatter new weighted momentum and mass to the cell#d\
    #d\
    \/\/ Particles to grid.\
    for each particle#i\
      calculate 9 interpolation weights for the 9 surrounding cells\
      use constitutive model to compute parameters (stress, density, strain, ...)\
      update particle momentum\

      for each surrounding cell#i\
        scatter new modified and weighted momentum to the cell#d\
    #d\

    \/\/ Grid update.\
    for each cell#i\
      convert momentum to velocity\
      apply body forces (e.g. gravity)\
    #d

    \/\/ Grid to particles.\
    for each particle#i\
      calculate 9 interpolation weights for the 9 surrounding cells\

      for each surrounding cell#i\
        gather weighted velocity from the cell#d\

    Advect particles\
    #d\

    \/\/ Dependent on constitutive model, not always needed.\
    for each particle#i\
      Compute new deformation gradient.
]

= Acknowledgments

This document and the accompanying simulation were achieved through a combination of
multiple resources. I have done my best to try to convey the intuition and mathematical
tools required to implement the MLS-MPM algorithm by combining these resources. In
particular, the folks on the Physics Programming discord provided lots of helpful advice
to help me understand the theory behind the MPM method.

I used the @mpm_guide by Niall Lavigne as a kickstart point to understand the
general idea. Then I read #cite("siggraph", "Jiang_2017", "hu2018moving") to better understand
the theory behind MPM methods in general, the APIC matrix and MLS-MPM specifically.
Finally, I relied on #cite("robkau", "yuanming-hu_2019") to cross reference my implementation
to make sure I wasn't missing a key component.

#bibliography("works.bib")
