
#version 460
#extension GL_EXT_scalar_block_layout : enable

#ifdef never
[[ne::depth_test(false)]]
[[ne::cull_mode(none)]]
#endif

layout(location = 0) in vec2 a_pos;
layout(location = 1) in vec2 a_tc;
layout(location = 2) in vec4 a_srgba; // {format : R8G8B8A8Unorm}

layout(location = 0) out vec4 v_rgba;
layout(location = 1) out vec2 v_tc;

layout(binding = 0, scalar) uniform Uniform
{
    vec2 u_screen_size;
};

void main() {
    gl_Position = vec4(
                      2.0 * a_pos.x / /*u_screen_size.x*/800.0 - 1.0,
                      2.0 * a_pos.y / /*u_screen_size.y*/800.0 - 1.0,
                      0.0,
                      1.0);

    v_rgba = a_srgba;//linear_from_srgba(a_srgba);
    v_tc = a_tc;
}