use std::collections::{BTreeSet, VecDeque};

use algebra::*;
use containers::PQueue;
use hedge::{
    face_handle::FaceHandle,
    facets::ABSENT,
    hedge_handle::HEdgeHandle,
    mesh::{verify_geometry, verify_topology, GeomId, HalfMesh},
    mesh_deleter::HalfMeshDeleter,
    FaceMetrics,
    HalfMeshLike,
};
use linear_isomorphic::*;

use crate::{osculating_circle, segment_segment_intersection};
use crate::{planar_bounding_primitives::*, point_segment_distance};

// Theory:
// http://www.karlchenofhell.org/cppswp/lischinski.pdf

/// Implementation of unconstrained Delaunay triangulation following an
/// incremental approach.
/// TODO(medium): This should also be able to handle f64.
pub fn delaunay_triangulation_as_mesh(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
) -> HalfMesh<Vec<Vec3>, (), ()>
{
    debug_assert!(point_count >= 3);

    // === Algorithm ===
    let bounding_triangle = bounding_triangle(points, point_count);

    let triangle = vec![
        bounding_triangle.p1,
        bounding_triangle.p2,
        bounding_triangle.p3,
    ];

    let mut point_data: Vec<_> = (0..point_count).map(points).collect();
    point_data.extend(triangle);

    let mut mesh = HalfMesh::<Vec<Vec3>, (), ()>::new(
        point_data,
        (),
        (),
        vec![vec![
            point_count as u64,
            (point_count + 1) as u64,
            (point_count + 2) as u64,
        ]],
    );

    // Ignore the 3 triangle vertices, use symbolic logic to treat them as always
    // outside any circumcircle.
    let symbolic_delaunay_test = |e: &HEdgeHandle<_, _, _>| {
        let opposite = e.pair().prev().source().id().0 as usize;
        (opposite < point_count) && delaunay_test(e)
    };
    let mut point_set: Vec<_> =
        (0..point_count).rev().map(|i| GeomId(i as u64)).collect();

    while let Some(point_id) = point_set.pop()
    {
        let vert = mesh.vert_handle(point_id);

        let point_type = find_containing_triangle(&vert.data(), &mesh)
            .expect("Point unexpectedly not found in triangulation.");

        match point_type
        {
            TriangulationPointType::InFace(face) =>
            {
                // Split face and assign the selected point to the topological center of
                // the face.
                face.split_triangle_face_and_fit::<f32>(point_id);
            }
            TriangulationPointType::InEdge(edge) =>
            {
                // Split one of the 2 faces, introducing 3 new edges. Then flip the
                // edge. This produces a cross pattern.
                edge.face().split_triangle_face_and_fit::<f32>(point_id);
                edge.flip();
            }
        }

        // Test all edges in the triangle and flip if necessary.
        let mut candidate_edges: Vec<_> = vert
            .iter_hedges()
            .filter(|e| !e.next().is_in_boundary_edge())
            .map(|e| e.next().id())
            .collect();

        if candidate_edges.is_empty()
        {
            continue;
        }

        while !candidate_edges.is_empty()
        {
            let e = mesh.hedge_handle(candidate_edges.pop().unwrap());

            if !e.is_in_boundary_edge() && symbolic_delaunay_test(&e) && e.can_flip()
            {
                e.flip();
                debug_assert!(verify_topology(&mesh).is_none());
                debug_assert!(verify_geometry(&mesh));

                candidate_edges.push(e.prev().id());
                candidate_edges.push(e.pair().next().id());
            }
        }
    }

    debug_assert!(verify_topology(&mesh).is_none());
    let mut deleter = HalfMeshDeleter::new(&mut mesh);

    deleter.delete_vert(GeomId(point_count as u64 + 2));
    deleter.delete_vert(GeomId(point_count as u64 + 1));
    deleter.delete_vert(GeomId(point_count as u64 + 0));
    deleter.finish();

    mesh
}

/// Compute a delaunay triangulation for a set of points. The set is abstracted
/// away through an accessing function to make it simple to triangulate a subset
/// of a larger set (e.g the points in a single face of the mesh).
/// It returns an array of indices into the input points where every 3 indices
/// are a triangle.
pub fn delaunay_triangulation(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
) -> Vec<usize>
{
    let mesh = delaunay_triangulation_as_mesh(points, point_count);

    mesh.get_topology()
        .iter()
        .flatten()
        .map(|id| *id as usize)
        .collect()
}

/// Constrained DT. First produces a DT and then enforces the constrained edge
/// through edge flipping.
/// Implementation of A "FAST ALGORITHM FOR GENERATING CONSTRAINED DELAUNAY
/// TRIANGULATIONS" by S. W. SLOAN
/// Link
/// https://rb.gy/8xpfc
pub fn constrained_delaunay_triangulation_as_mesh(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
    segments: &dyn Fn(usize) -> [usize; 2],
    segment_count: usize,
) -> HalfMesh<Vec<Vec3>, (), ()>
{
    let mesh = delaunay_triangulation_as_mesh(points, point_count);
    debug_assert!(verify_topology(&mesh).is_none());
    debug_assert!(verify_geometry(&mesh));

    let is_in_triangulation = |p1_id, p2_id| {
        let p1_id = GeomId(p1_id as u64);
        let p2_id = GeomId(p2_id as u64);

        let v1 = mesh.vert_handle(p1_id);
        v1.is_connected_to(p2_id)
    };

    // --- 1. Loop over each constrained edge ---
    for [p1_id, p2_id] in (0..segment_count).map(segments)
    {
        // --- 2. Find intersecting edges ---
        // Constrained edge is already in the triangulation.
        if is_in_triangulation(p1_id, p2_id)
        {
            continue;
        };

        let (p1, p2) = (points(p1_id), points(p2_id));

        // --- 2. Find intersecting edges  ---
        // Find all edges that cross the current constraint edge.
        let mut intersecting_edge_list = VecDeque::from(find_intersecting_edges(
            GeomId(p1_id as u64),
            GeomId(p2_id as u64),
            &mesh,
        ));

        // --- 3. Remove intersecting edges  ---
        // Flip intersecting edges until none intersect.
        let mut new_edge_list = VecDeque::with_capacity(mesh.hedge_count());

        while !intersecting_edge_list.is_empty()
        {
            // 3.1) Remove edge from the list.
            let edge = intersecting_edge_list.pop_front().unwrap();

            debug_assert!(verify_topology(&mesh).is_none());
            debug_assert!(verify_geometry(&mesh));
            let e = mesh.hedge_handle(edge);

            // You cannot flip boundary edges, so if we get this, ignore it.
            if e.is_in_boundary_edge()
            {
                continue;
            }

            // 3.2) Depending on convexity, flip it and check if it still intersects.
            let [q1, q2, q3, q4] = butterfly_points(&e);
            let is_convex = quad_is_convex(&q1, &q2, &q3, &q4);

            // Quad is not convex.
            if !is_convex
            {
                intersecting_edge_list.push_back(e.id());
                continue;
            }

            debug_assert!(quad_is_convex(&q1, &q2, &q3, &q4));
            e.flip();

            debug_assert!(verify_topology(&mesh).is_none());
            let [q1, q2, q3, q4] = butterfly_points(&e);
            debug_assert!(quad_is_convex(&q1, &q2, &q3, &q4));

            // TODO(low): `segment_segment_intersection` should be modified to detect
            // overlapping segments. This will not be needed once that is done.
            // We have just a constrained edge so skip.
            if match_endpoints(&e, GeomId(p1_id as u64), GeomId(p2_id as u64))
            {
                continue;
            }

            // Check new diagonal, if it intersects, back into the list it goes, else
            // add it to the list of new edges.
            let (intersect, _) = segment_segment_intersection(&p1, &p2, &q1, &q3);
            if intersect
                && !share_endpoint(&e, GeomId(p1_id as u64), GeomId(p2_id as u64))
            {
                intersecting_edge_list.push_back(e.id());
            }
            else
            {
                new_edge_list.push_back(e.id());
            }
        }

        let is_same_as_constraint_edge = |e: &HEdgeHandle<Vec<Vec3>, (), ()>| {
            (e.source().id().0 as usize == p1_id && e.dest().id().0 as usize == p2_id)
                || (e.source().id().0 as usize == p2_id
                    && e.dest().id().0 as usize == p1_id)
        };

        // --- 4. Restore Delaunay Constraint ---
        loop
        {
            // Among the new edges, see which can be flipped to be restored to
            // conform with the Delaunay criterion.
            let mut swapped = false;
            for edge in &new_edge_list
            {
                let e = mesh.hedge_handle(*edge);
                if is_same_as_constraint_edge(&e)
                {
                    continue;
                }

                // NEVER!!! flip a concave edge.
                let [q1, q2, q3, q4] = butterfly_points(&e);
                let is_convex = quad_is_convex(&q1, &q2, &q3, &q4);
                if !delaunay_test(&e) && is_convex
                {
                    e.flip();
                    swapped = true;
                }
            }

            // Nothing was swapped so we are done.
            if !swapped
            {
                break;
            }
        }
    }

    mesh
}

/// Recursively flips triangles around a determined vertex until all pass the
/// delaunay test (or are constrained).
pub fn lawsons_algorithm<F>(
    source: GeomId<0>,
    mesh: &HalfMesh<Vec<Vec3>, (), ()>,
    is_constraint_edge: F,
) where
    F: Fn(&HEdgeHandle<Vec<Vec3>, (), ()>) -> bool,
{
    let mut queue = VecDeque::from_iter(
        mesh.vert_handle(source)
            .iter_hedges()
            .map(|e| e.face().id()),
    );

    let find_opposite = |face: &FaceHandle<Vec<Vec3>, (), ()>| {
        face.hedge()
            .iter_loop()
            .find(|e| e.source().id() != source && e.dest().id() != source)
            .unwrap()
    };
    while !queue.is_empty()
    {
        let face_id = queue.pop_front().unwrap();
        if face_id == ABSENT()
        {
            continue;
        }
        let edge_handle = find_opposite(&mesh.face_handle(face_id));
        if delaunay_test(&edge_handle)
            && edge_handle.can_flip()
            && !is_constraint_edge(&edge_handle)
        {
            edge_handle.flip();
            queue.push_back(edge_handle.face().id());
            queue.push_back(edge_handle.pair().face().id());
        }
    }
}

/// Compute a constrained Delaunay triangulation of a set of points embedded in
/// an arbitrary plane in R^3.
pub fn constrained_delaunay_triangulation(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
    segments: &dyn Fn(usize) -> [usize; 2],
    segment_count: usize,
) -> Vec<usize>
{
    let mesh = constrained_delaunay_triangulation_as_mesh(
        points,
        point_count,
        segments,
        segment_count,
    );

    mesh.get_topology()
        .iter()
        .flatten()
        .map(|id| *id as usize)
        .collect()
}

/// Compute a constrained Delaunay Triangulation. Some points are assumed to be
/// seeds for face deletion. All faces transitively connected to those points
/// will be deleted until a constriant edge is hit.
/// i.e. make sure that `hole_points` are always fully inside the interior
/// of a closed polygon. That polygon will be a hole.
pub fn constrained_delaunay_triangulation_hollow_as_mesh(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
    mut segments: &dyn Fn(usize) -> [usize; 2],
    segment_count: usize,
    hole_points: &dyn Fn(usize) -> usize,
    hole_point_count: usize,
) -> HalfMesh<Vec<Vec3>, (), ()>
{
    let mut mesh = constrained_delaunay_triangulation_as_mesh(
        points,
        point_count,
        segments,
        segment_count,
    );

    debug_assert!(verify_topology(&mesh).is_none());
    debug_assert!(verify_geometry(&mesh));

    let segment_set = BTreeSet::from_iter((0..segment_count).map(&mut segments));

    let mut deleter = HalfMeshDeleter::new(&mut mesh);

    let is_in_polygon = |e: &HEdgeHandle<Vec<Vec3>, (), ()>| {
        let v1_id = usize::from(e.source().id());
        let v2_id = usize::from(e.dest().id());
        segment_set.contains(&[v1_id, v2_id]) || segment_set.contains(&[v2_id, v1_id])
    };

    // For each point marked for deletion, recursively delete faces connected
    // to that point until a polygon boundary is hit.
    for hole_id in 0..hole_point_count
    {
        let id = hole_points(hole_id);

        let vert = deleter.vert_handle(GeomId(id as u64));
        let mut stack = vec![vert.hedge().unwrap().face().id()];

        while !stack.is_empty()
        {
            let c_face_id = stack.pop().unwrap();
            let c_face = deleter.face_handle(c_face_id);
            if c_face.is_disabled()
            {
                continue;
            }

            for edge in c_face.hedge().iter_loop().filter(|e| !is_in_polygon(&e))
            {
                if edge.is_in_boundary_edge()
                {
                    continue;
                }
                stack.push(edge.pair().face().id());
            }

            deleter.delete_face(c_face.id());
        }
    }

    deleter.finish();

    debug_assert!(verify_topology(&mesh).is_none());

    mesh
}

pub fn constrained_delaunay_triangulation_hollow(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
    segments: &dyn Fn(usize) -> [usize; 2],
    segment_count: usize,
    hole_points: &dyn Fn(usize) -> usize,
    hole_point_count: usize,
) -> Vec<usize>
{
    let mesh = constrained_delaunay_triangulation_hollow_as_mesh(
        points,
        point_count,
        segments,
        segment_count,
        hole_points,
        hole_point_count,
    );

    mesh.get_topology()
        .iter()
        .flatten()
        .map(|id| *id as usize)
        .collect()
}

// Based on:
// https://sites.google.com/site/samsoninfinite/multivariable-calculus/constrained-delaunay-triangulation
// https://en.wikipedia.org/wiki/Delaunay_refinement
// TODO(low): improve output through:
// Off-centers: A new type of Steiner points for computing size-optimal
// quality-guaranteed Delaunay triangulations
pub fn conforming_delaunay_triangulation_as_mesh(
    points: &dyn Fn(usize) -> Vec3,
    point_count: usize,
    segments: &dyn Fn(usize) -> [usize; 2],
    segment_count: usize,
    angle_constraint: f32,
    area_constraint: f32,
) -> HalfMesh<Vec<Vec3>, (), ()>
{
    // Compute a triangulation.
    let mesh = constrained_delaunay_triangulation_as_mesh(
        points,
        point_count,
        segments,
        segment_count,
    );

    // Sort all *bad* faces from worst to best in apriority queue.
    // === 1. Score all triangles and filter the bad ones ===
    let mut bad_face_queue = PQueue::new();
    for triangle in mesh
        .iter_faces()
        .filter(|t| triangle_is_bad(t, angle_constraint, area_constraint))
    {
        let weight = triangle_weight(&triangle);
        bad_face_queue.push(triangle.id(), weight);
    }

    // Find all constrained and boundary segments.
    let mut segment_set: BTreeSet<GeomId<1>> = (0..segment_count)
        .map(|i| {
            let [v1, v2] = segments(i);
            mesh.vert_handle(GeomId::from(v1))
                .shared_hedge(GeomId::from(v2))
                .unwrap()
        })
        .map(|e| e.id().select_even())
        .collect();
    segment_set.extend(
        mesh.iter_edges()
            .filter(|e| e.is_in_boundary_edge())
            .map(|e| e.id()),
    );

    // === Algorithm ===
    while !bad_face_queue.is_empty()
    {
        // === 2. Pop current worst triangle ===
        let (fid, _w) = bad_face_queue.pop().unwrap();
        let points = mesh.face_handle(fid).vertices_tri();
        let circumcenter = osculating_circle(&points[0], &points[1], &points[2]);

        // === 3. Check if the circumcircle encroaches an edge ===
        let mut segment_encroached = false;
        let mut edge = mesh.hedge_handle(GeomId(0));
        for segment in &segment_set
        {
            edge = mesh.hedge_handle(*segment);
            if encroaches(&edge, &circumcenter)
            {
                segment_encroached = true;
                break;
            }
        }

        // === 4. If it encroaches the edge, split the edge ===
        if segment_encroached
        {
            let new_edges;
            let vid;
            // Split the edge (check is needed because boundary edges must use special
            // logic).
            if !edge.is_in_boundary_edge()
            {
                (vid, _, new_edges) = edge.split::<f32>();
            }
            else
            {
                (vid, _, new_edges) = edge
                    .get_non_boundary_hedge()
                    .unwrap()
                    .split_boundary::<f32>();
            }

            // Restore Delaunay criterion.
            lawsons_algorithm(vid, &mesh, |e| e.is_in_boundary_edge());
            // We added new faces and changed existing ones, update the queue.
            for face in mesh
                .vert_handle(vid)
                .iter_hedges()
                .filter(|e| !e.is_boundary_hedge())
                .map(|e| e.face())
            {
                if !triangle_is_bad(&face, angle_constraint, area_constraint)
                {
                    continue;
                }
                let weight = triangle_weight(&face);
                bad_face_queue.push(face.id(), weight);
            }
            segment_set.extend(new_edges);
        }
        // === 5. If it does not encroach the edge, split the face ===
        else
        {
            let point_type = find_containing_triangle(&circumcenter, &mesh);
            if point_type.is_none()
            {
                continue;
            }

            let face = match point_type.unwrap()
            {
                TriangulationPointType::InEdge(_) => panic!(),
                TriangulationPointType::InFace(face) => face,
            };
            let vid = face.split_triangle_face::<f32>();
            // Set face to circumcircle:
            let v = mesh.vert_handle(vid);
            v.mutate_data(&circumcenter);

            // Restore Delaunay criterion.
            lawsons_algorithm(vid, &mesh, |e| e.is_in_boundary_edge());

            // We added new faces and changed existing ones, update the queue.
            for face in mesh.vert_handle(vid).iter_hedges().map(|e| e.face())
            {
                if !triangle_is_bad(&face, angle_constraint, area_constraint)
                {
                    continue;
                }
                let weight = triangle_weight(&face);
                bad_face_queue.push(face.id(), weight);
            }
        }
    }

    mesh
}

// === Internal ===
fn encroaches(segment: &HEdgeHandle<Vec<Vec3>, (), ()>, c: &Vec3) -> bool
{
    let dir = segment.dir() / 2.0;
    let radius_squared = dir.norm_squared();
    let mid_point = segment.source().data() + dir;

    (c - mid_point).norm_squared() < radius_squared
}

fn triangle_is_bad(
    triangle: &FaceHandle<Vec<Vec3>, (), ()>,
    angle_constraint: f32,
    area_constraint: f32,
) -> bool
{
    let points = triangle.vertices_tri();
    let angle = smallest_triangle_angle(&points[0], &points[1], &points[2]);
    angle < angle_constraint || triangle.area() > area_constraint
}

fn triangle_weight(triangle: &FaceHandle<Vec<Vec3>, (), ()>) -> f32
{
    let points = triangle.vertices_tri();
    let circumcenter = osculating_circle(&points[0], &points[1], &points[2]);
    let radius = (points[0] - circumcenter).norm();
    let shortest_edge = triangle
        .hedge()
        .iter_loop()
        .map(|e| e.dir().norm())
        .min_by(|a, b| a.total_cmp(b))
        .unwrap();

    radius / shortest_edge
}

fn smallest_triangle_angle(v1: &Vec3, v2: &Vec3, v3: &Vec3) -> f32
{
    let d1 = (v2 - v1).normalized();
    let d2 = (v3 - v2).normalized();
    let d3 = (v1 - v3).normalized();

    let a1 = (-d1.dot(&d2)).acos();
    let a2 = (-d2.dot(&d3)).acos();
    let a3 = (-d3.dot(&d1)).acos();

    a1.min(a2).min(a3)
}

fn quad_is_convex(p1: &Vec3, p2: &Vec3, p3: &Vec3, p4: &Vec3) -> bool
{
    let (intersect, _) = segment_segment_intersection(p1, p3, p2, p4);

    intersect
}

fn share_endpoint(
    test_edge: &HEdgeHandle<Vec<Vec3>, (), ()>,
    p1_id: GeomId<0>,
    p2_id: GeomId<0>,
) -> bool
{
    test_edge.dest().id() == p1_id
        || test_edge.dest().id() == p2_id
        || test_edge.source().id() == p1_id
        || test_edge.source().id() == p2_id
}

fn match_endpoints(
    test_edge: &HEdgeHandle<Vec<Vec3>, (), ()>,
    p1_id: GeomId<0>,
    p2_id: GeomId<0>,
) -> bool
{
    (test_edge.dest().id() == p1_id || test_edge.dest().id() == p2_id)
        && (test_edge.source().id() == p1_id || test_edge.source().id() == p2_id)
}

// Based on figure 9 from:
// https://rb.gy/8xpfc
fn find_intersecting_edges(
    p1_id: GeomId<0>,
    p2_id: GeomId<0>,
    mesh: &HalfMesh<Vec<Vec3>, (), ()>,
) -> Vec<GeomId<1>>
{
    let v1 = mesh.vert_handle(p1_id);
    let v2 = mesh.vert_handle(p2_id);

    let p1 = v1.data();
    let p2 = v2.data();

    let mut candidate_edges = Vec::new();
    for edge in v1.iter_hedges()
    {
        let test_edge = edge.next();
        candidate_edges.push(test_edge.id());
    }

    let mut res = Vec::new();
    while let Some(test_id) = candidate_edges.pop()
    {
        let test_edge = mesh.hedge_handle(test_id);

        // If either end point of the testing edge is in the constrained edge then
        // it cannot cross the constraint, but it technically intersects it.
        if share_endpoint(&test_edge, p1_id, p2_id)
        {
            continue;
        }

        let o1 = test_edge.source().data();
        let o2 = test_edge.dest().data();

        let (intersect, _) = segment_segment_intersection(&p1, &p2, &o1, &o2);
        if intersect
        {
            res.push(test_id);
            candidate_edges.push(test_edge.pair().next().id());
            candidate_edges.push(test_edge.pair().prev().id());
        }
    }

    res
}

fn in_circle(p1: &Vec3, p2: &Vec3, p3: &Vec3, p: &Vec3) -> bool
{
    let center = osculating_circle(p1, p2, p3);
    let radius2 = (center - p1).norm_squared();

    (p - center).norm_squared() < radius2
}

/// Test whether the vertex opposite to this half edge's pair is inside the
/// circumcircle described by the face.
fn delaunay_test(e: &HEdgeHandle<Vec<Vec3>, (), ()>) -> bool
{
    let t1: Vec3 = e.source().data();
    let t2: Vec3 = e.next().source().data();
    let t3: Vec3 = e.prev().source().data();

    // This would indicate the triangle is degenerate.
    if (t2 - t1).normalized().cross(&(t3 - t1).normalized()).norm() < 1e-5
    {
        return true;
    }

    let o = e.pair().prev().source().data();

    in_circle(&t1, &t2, &t3, &o)
}

enum TriangulationPointType
{
    InFace(FaceHandle<Vec<Vec3>, (), ()>),
    InEdge(HEdgeHandle<Vec<Vec3>, (), ()>),
}
// Potential improvement.
// Page 11 of:
// http://www.karlchenofhell.org/cppswp/lischinski.pdf
fn find_containing_triangle(
    point: &Vec3,
    mesh: &HalfMesh<Vec<Vec3>, (), ()>,
) -> Option<TriangulationPointType>
{
    for face in mesh.iter_faces()
    {
        let mut inside = true;
        for edge in face.hedge().iter_loop()
        {
            inside = inside && !right_of(point, &edge);
        }

        if inside
        {
            return Some(TriangulationPointType::InFace(face));
        }
        else
        {
            for edge in face.hedge().iter_loop()
            {
                let distance = point_segment_distance(
                    &edge.source().data(),
                    &edge.dest().data(),
                    point,
                );

                if distance <= f32::EPSILON * 100.0
                {
                    return Some(TriangulationPointType::InEdge(edge));
                }
            }
        }
    }

    None
}

fn edge_to_points(handle: &HEdgeHandle<Vec<Vec3>, (), ()>) -> (Vec3, Vec3, Vec3)
{
    (
        handle.source().data(),
        handle.dest().data(),
        handle.prev().source().data(),
    )
}

fn right_of(point: &Vec3, hedge: &HEdgeHandle<Vec<Vec3>, (), ()>) -> bool
{
    let (p1, p2, p3) = edge_to_points(hedge);
    !is_inside_half_space(point, &p1, &p2, &p3)
}

fn is_inside_half_space(x: &Vec3, p1: &Vec3, p2: &Vec3, p3: &Vec3) -> bool
{
    let orthogonal = orthogonal_vec_from_basis(&(p2 - p1), &(p3 - p1));

    (x - p1).dot(&orthogonal) >= 0.0
}

fn butterfly_points(e: &HEdgeHandle<Vec<Vec3>, (), ()>) -> [Vec3; 4]
{
    let q1 = e.source().data();
    let q2 = e.pair().prev().source().data();
    let q3 = e.next().source().data();
    let q4 = e.prev().source().data();

    [q1, q2, q3, q4]
}

#[cfg(test)]
mod tests
{
    use std::{collections::HashSet, f32::consts::PI};

    use algebra::*;
    use chicken_wire::wavefront_loader::ObjData;
    use hedge::{mesh::HalfMesh, HalfMeshLike};
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    use super::quad_is_convex;
    use crate::{
        conforming_delaunay_triangulation_as_mesh,
        constrained_delaunay_triangulation,
        constrained_delaunay_triangulation_hollow_as_mesh,
        osculating_circle,
        triangulation::delaunay_triangulation,
    };

    #[test]
    fn test_quad_is_convex()
    {
        let p1 = Vec3::new(0.54795533, 2.474151, -0.10707931);
        let p2 = Vec3::new(0.5513274, 2.475076, -0.09567261);
        let p3 = Vec3::new(0.53652054, 2.4783394, -0.095137835);
        let p4 = Vec3::new(0.5312085, 2.4766655, -0.11460401);

        let result = quad_is_convex(&p1, &p2, &p3, &p4);
        assert!(result);
    }

    #[test]
    fn test_triangulation()
    {
        let mut points = Vec::with_capacity(30);
        let seed: <ChaCha8Rng as SeedableRng>::Seed = Default::default();
        let mut rng = ChaCha8Rng::from_seed(seed);

        for _ in 0..points.capacity()
        {
            let x = rng.gen_range(-1.0..1.0);
            let y = rng.gen_range(-1.0..1.0);
            points.push(Vec3::new(x, y, 0.0));
        }

        let indices: Vec<_> = delaunay_triangulation(&|id| points[id], points.len())
            .chunks(3)
            .map(|tri| tri.iter().map(|&i| i as u64).collect())
            .collect();

        let mesh =
            HalfMesh::<Vec<Vec3>, (), ()>::new(points.clone(), (), (), indices.clone());

        for face in mesh.iter_faces()
        {
            let p1_id = face.hedge().source().id();
            let p2_id = face.hedge().dest().id();
            let p3_id = face.hedge().prev().source().id();

            let p1 = face.hedge().source().data();
            let p2 = face.hedge().dest().data();
            let p3 = face.hedge().prev().source().data();

            let center = osculating_circle(&p1, &p2, &p3);
            let radius = (center - p1).norm();

            let mut set = HashSet::new();
            set.insert(p1_id);
            set.insert(p2_id);
            set.insert(p3_id);

            for vert in mesh.iter_verts()
            {
                if !set.contains(&vert.id())
                {
                    assert!(
                        (vert.data() - center).norm() >= radius,
                        "vertex: {:?}; distance {}; face {:?}",
                        vert.id(),
                        (vert.data() - center).norm() - radius,
                        face.id()
                    );
                }
            }
        }
    }

    #[test]
    fn test_constrained_triangulation()
    {
        let ran_count = 100;
        let mut points = Vec::with_capacity(ran_count);
        let seed: <ChaCha8Rng as SeedableRng>::Seed = Default::default();
        let mut rng = ChaCha8Rng::from_seed(seed);

        let point_in_set = |point: &Vec3, set: &Vec<Vec3>| {
            for p in set
            {
                if (*p - *point).norm() <= f32::EPSILON * 1000.0
                {
                    return true;
                }
            }

            false
        };

        for _ in 0..points.capacity()
        {
            let x = rng.gen_range(-1.0..1.0);
            let y = rng.gen_range(-1.0..1.0);
            let mut p = Vec3::new(x, y, 0.0);

            while point_in_set(&p, &points)
            {
                let x = rng.gen_range(-1.0..1.0);
                let y = rng.gen_range(-1.0..1.0);
                p = Vec3::new(x, y, 0.0);
            }

            points.push(p);
        }

        let size = 20;
        let n = points.len() + size;
        let mut segments = Vec::new();
        // Make a circle.
        for i in 0..size
        {
            let t = (i as f32 / (size as f32 - 1.0)) * 2.0 * PI;
            let start = points.len();
            let mut end = points.len() + 1;

            let mut p = Vec3::new(t.cos() * 0.5, t.sin() * 0.5, 0.0);
            if point_in_set(&p, &points)
            {
                let x = t - 2.0 / (size - 1) as f32;
                p = Vec3::new(x.cos() * 0.5, x.sin() * 0.5, 0.0);
            }
            points.push(p);

            if end >= n
            {
                end = ran_count;
            }
            segments.push([start, end]);
        }

        let indices = constrained_delaunay_triangulation(
            &|id| points[id],
            points.len(),
            &|id| segments[id],
            segments.len(),
        );

        ObjData::export(&(&points, &indices), "tmp/constrained_triangulation.obj");
    }

    // Off-centers: A new type of Steiner points for computing size-optimal
    // quality-guaranteed Delaunay triangulations
    #[test]
    fn test_conforming_triangulation()
    {
        let ran_count = 4;
        let mut points = vec![
            Vec3::new(-1., -1., 0.),
            Vec3::new(1., -1., 0.),
            Vec3::new(1., 1., 0.),
            Vec3::new(-1., 1., 0.),
        ];
        let point_in_set = |point: &Vec3, set: &Vec<Vec3>| {
            for p in set
            {
                if (*p - *point).norm() <= f32::EPSILON * 1000.0
                {
                    return true;
                }
            }

            false
        };

        let size = 20;
        let n = points.len() + size;
        let mut segments = Vec::new();
        // Make a circle.
        for i in 0..size
        {
            let t = (i as f32 / (size as f32 - 1.0)) * 2.0 * PI;
            let start = points.len();
            let mut end = points.len() + 1;

            let mut p = Vec3::new(t.cos() * 0.5, t.sin() * 0.5, 0.0);
            if point_in_set(&p, &points)
            {
                let x = t - 2.0 / (size - 1) as f32;
                p = Vec3::new(x.cos() * 0.5, x.sin() * 0.5, 0.0);
            }
            points.push(p);

            if end >= n
            {
                end = ran_count;
            }
            segments.push([start, end]);
        }

        let indices = conforming_delaunay_triangulation_as_mesh(
            &|id| points[id],
            points.len(),
            &|id| segments[id],
            segments.len(),
            f32::to_radians(24.9),
            f32::MAX,
        );

        ObjData::export(&indices, "tmp/conforming_delaunay_mesh.obj");
    }

    #[test]
    fn test_constrained_triangulation_holes()
    {
        let ran_count = 100;
        let mut points = Vec::with_capacity(ran_count);
        let seed: <ChaCha8Rng as SeedableRng>::Seed = Default::default();
        let mut rng = ChaCha8Rng::from_seed(seed);

        let point_in_set = |point: &Vec3, set: &Vec<Vec3>| {
            for p in set
            {
                if (*p - *point).norm() <= f32::EPSILON * 1000.0
                {
                    return true;
                }
            }

            false
        };

        for _ in 0..points.capacity()
        {
            let x = rng.gen_range(-1.0..1.0);
            let y = rng.gen_range(-1.0..1.0);
            let mut p = Vec3::new(x, y, 0.0);

            while point_in_set(&p, &points)
            {
                let x = rng.gen_range(-1.0..1.0);
                let y = rng.gen_range(-1.0..1.0);
                p = Vec3::new(x, y, 0.0);
            }

            points.push(p);
        }

        let size = 20;
        let n = points.len() + size;
        let mut segments = Vec::new();
        // Make a circle.
        for i in 0..size
        {
            let t = (i as f32 / (size as f32 - 1.0)) * 2.0 * PI;
            let start = points.len();
            let mut end = points.len() + 1;

            let mut p = Vec3::new(t.cos() * 0.5, t.sin() * 0.5, 0.0);
            if point_in_set(&p, &points)
            {
                let x = t - 2.0 / (size - 1) as f32;
                p = Vec3::new(x.cos() * 0.5, x.sin() * 0.5, 0.0);
            }
            points.push(p);

            if end >= n
            {
                end = ran_count;
            }
            segments.push([start, end]);
        }

        points.push(Vec3::zeros());
        let mesh = constrained_delaunay_triangulation_hollow_as_mesh(
            &|id| points[id],
            points.len(),
            &|id| segments[id],
            segments.len(),
            &|id| points.len() + id - 1,
            1,
        );

        ObjData::export(&mesh, "tmp/constrained_triangulation_holes.obj");
    }
}

// More resources:
// https://arxiv.org/pdf/1707.05949.pdf
// https://www.iue.tuwien.ac.at/phd/fleischmann/node54.html
// http://www.cs.cmu.edu/~bhudson/manuscripts/hudson07what.pdf
