use std::cell::RefCell;
use std::ops::{Add, Mul};
use std::rc::Rc;
use std::time::Instant;

use algebra::{Mat3, Mat4, Vec3};
use camera::*;
use chicken_wire::wavefront_loader::ObjData;
use dem_core::{io::*, rendering::*};
use dem_gui::*;
use dem_peripherals::*;
use hedge::*;
use hedge::{mesh::GeomId, mesh::HalfMesh};
use vulkan_bindings::RenderContext;

type DVec = algebra::na::DVector<f32>;

#[derive(Debug, Clone)]
struct HyperVertex {
    position: algebra::na::DVector<f32>,
}

impl Add<HyperVertex> for HyperVertex {
    type Output = HyperVertex;

    fn add(self, rhs: HyperVertex) -> Self::Output {
        HyperVertex {
            position: self.position + rhs.position,
        }
    }
}

impl Mul<f32> for HyperVertex {
    type Output = HyperVertex;

    fn mul(self, rhs: f32) -> Self::Output {
        HyperVertex {
            position: self.position * rhs,
        }
    }
}

fn serialize_hypervertex(verts: &Vec<HyperVertex>) -> Vec<Vec3> {
    let mut vec_positions = Vec::<Vec3>::new();
    for datum in verts {
        vec_positions.push(Vec3::new(
            datum.position[0],
            datum.position[1],
            datum.position[2],
        ));
    }

    vec_positions
}

fn flatten<T>(nested: Vec<Vec<T>>) -> Vec<T> {
    nested.into_iter().flatten().collect()
}

fn update_covariance(vec: &mut DVec, mat: &Mat3) {
    assert!(vec.len() == 9);
    let mut i = 0;
    let mut j = 0;
    for k in 0..6 {
        let x = i;
        let y = j;

        vec[3 + k] = mat[(x, y)];

        i += 1;
        if i > j {
            i = 0;
            j += 1
        }
    }
}

fn load_data_from_disk(path: &String) -> HalfMesh<Vec<HyperVertex>, (), ()> {
    let ObjData {
        vertices: verts,
        vertex_face_indices: verts_idx,
        ..
    } = ObjData::from_disk_file(path.as_str());

    let mut vert_data = Vec::<HyperVertex>::new();
    for vert in verts {
        vert_data.push(HyperVertex {
            position: DVec::from_row_slice(vert.as_slice()),
        });

        // row 1
        let mut vec = vert_data.last_mut().unwrap().position.push(1.0);

        // row 2
        vec = vec.push(0.0);
        vec = vec.push(1.0);

        // row 3
        vec = vec.push(0.0);
        vec = vec.push(0.0);
        vec = vec.push(1.0);

        vert_data.last_mut().unwrap().position = vec;
    }

    HalfMesh::new(vert_data, (), (), verts_idx)
}

fn dual_to_components(position: &HyperVertex) -> (Vec3, Mat3) {
    let dual_position = Vec3::new(
        position.position[0],
        position.position[1],
        position.position[2],
    );

    let mut sigma_inv = Mat3::zeros();
    let mut i = 0;
    let mut j = 0;
    for k in 0..6 {
        let x = i;
        let y = j;

        sigma_inv[(x, y)] = position.position[3 + k];
        sigma_inv[(y, x)] = sigma_inv[(x, y)];

        i += 1;
        if i > j {
            i = 0;
            j += 1
        }
    }

    (dual_position, sigma_inv)
}

fn gaussian_subdivision(mesh: &mut HalfMesh<Vec<HyperVertex>, (), ()>) {
    for vert in mesh.iter_verts() {
        let (pos, sigma) = dual_to_components(&vert.data());

        let new_pos = sigma * pos;
        vert.data().position[0] = new_pos[0];
        vert.data().position[1] = new_pos[1];
        vert.data().position[2] = new_pos[2];
    }

    loop_subdivision(mesh);

    for vert in mesh.iter_verts() {
        let (pos, sigma) = dual_to_components(&vert.data());

        let trans = match sigma.try_inverse() {
            None => Mat3::identity(),
            Some(x) => x,
        };
        let new_pos = trans * pos;

        let mut new_data = vert.data();
        new_data.position[0] = new_pos[0];
        new_data.position[1] = new_pos[1];
        new_data.position[2] = new_pos[2];
        vert.mutate_data(&new_data);
    }
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct MVP {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn main() {
    let mut mesh = load_data_from_disk(&"./Assets/cube.obj".to_string());
    let o_mesh = mesh.clone();

    let mut io_context = IoContext::new("Gaussian Subdivision", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let phong_shader = render_context.add_shader(vec![
        "examples/03_gaussian_subdivision/phong_shader/".to_string(),
    ]);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let mut gui = Some(DebugGui::init(
        &mut render_context,
        io_context.window().get_window_size(),
    ));

    let topology = flatten(mesh.get_topology());
    let gpu_mesh = GraphicsInput::new()
        .add_attribute_buffer_from_slice(
            &serialize_hypervertex(&mesh.verts_mut()),
            &mut render_context,
        )
        .set_index_buffer_from_slice(&topology, &mut render_context);

    let mut render_request = RenderRequest::new(phong_shader)
        .vertex_input(gpu_mesh.clone())
        .add_ubo(0, Mat4::default())
        .build();

    let mut update_count = 0;
    let mut selected_vertex = 0;

    let mut now = Instant::now();
    while io_context.poll_io(&mut gui) {
        render_context.start_frame(&io_context);
        let mvp = MVP {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };
        let elapsed_time = now.elapsed().as_millis() as f32;
        now = Instant::now();

        render_request.set_ubo(0, mvp);
        render_context.draw(&render_request);

        let (_pos, mut mat) =
            dual_to_components(&mesh.vert_handle(GeomId(selected_vertex)).data());
        let pre_mat = mat;
        let mut update = false;

        gui.as_mut().unwrap().draw(&mut render_context, &mut |ctx| {
            dem_gui::Window::new("Program info:").show(ctx, |ui| {
                ui.label(format!(
                    "{:.0}ms ({:.0} FPS)",
                    elapsed_time,
                    1000.0 / elapsed_time
                ));

                ui.label(format!("Vertex Count {}", mesh.vert_count()));

                ui.horizontal(|ui| {
                    if ui.button("-").clicked() {
                        update_count -= 1;
                        update = true;
                    }
                    ui.label(format!("Sudivision level {}", update_count));
                    if ui.button("+").clicked() {
                        update_count += 1;
                        update = true;
                    }
                });

                ui.horizontal(|ui| {
                    if ui.button("-").clicked() {
                        selected_vertex -= 1;
                        update = true;
                    }
                    ui.label(format!("Selected Vertex {}", selected_vertex));
                    if ui.button("+").clicked() {
                        selected_vertex += 1;
                        update = true;
                    }
                });

                for j in 0..3 {
                    ui.horizontal(|ui| {
                        for i in 0..3 {
                            if ui
                                .add(
                                    dem_gui::egui::DragValue::new(&mut mat[(j, i)])
                                        .speed(0.01),
                                )
                                .changed()
                            {
                                let mut data =
                                    o_mesh.vert_handle(GeomId(selected_vertex)).data();

                                update_covariance(&mut data.position, &mat);

                                o_mesh
                                    .vert_handle(GeomId(selected_vertex))
                                    .mutate_data(&data);
                            }
                        }
                    });
                }
            });

            camera.borrow_mut().should_update = !ctx.wants_pointer_input();
        });

        if pre_mat != mat || update {
            mesh = o_mesh.clone();
            for _ in 0..update_count {
                gaussian_subdivision(&mut mesh);
            }

            let topology = flatten(mesh.get_topology());
            let gpu_mesh = GraphicsInput::new()
                .add_attribute_buffer_from_slice(
                    &serialize_hypervertex(&mesh.verts_mut()),
                    &mut render_context,
                )
                .set_index_buffer_from_slice(&topology, &mut render_context);

            render_context.free_input(render_request.vertex_input);
            render_request = RenderRequest::new(phong_shader)
                .vertex_input(gpu_mesh.clone())
                .add_ubo(0, Mat4::default())
                .build();
        }
    }

    render_context.free_input(render_request.vertex_input);
}
