use std::cell::Cell;

use algebra::*;

use crate::differential_geometry::tangent;

// https://medium.com/intuition/lockdown-geometry-rotation-minimizing-frames-ff373d2f355b
/// Representation of a rotation minimizing frame for a given parametric
/// function.
#[derive(Debug, Clone)]
pub struct RotationMinimizedFrame
{
    last_sampled_index: Cell<usize>,
    parameters: Vec<f32>,
    // TODO(low): Change this to an array instead of tuple.
    frames: Vec<(Vec3, Vec3, Vec3)>,
}

impl RotationMinimizedFrame
{
    /// Construct a RMF from pre-sampled parameters (assumed to be in the domain
    /// of $f$).
    pub fn new_from_parameter_list<F>(parameters: &Vec<f32>, f: F) -> Self
    where
        F: Fn(f32) -> Vec3,
    {
        let first_tangent = tangent(parameters[0], &f).normalize();
        let first_up = orthogonal_vector(&first_tangent).normalize();
        let first_side = first_tangent.cross(&first_up).normalize();

        let current_frame = (first_tangent, first_up, first_side);

        let mut frames = Vec::with_capacity(parameters.len());
        frames.push(current_frame);

        let mut current_up = current_frame.1;
        let mut current_tangent = tangent(parameters[0], &f).normalize();
        for i in 0..parameters.len() - 1
        {
            let current_point = f(parameters[i]);
            let next_point = f(parameters[i + 1]);
            let next_tangent = tangent(parameters[i + 1], &f).normalize();

            let v1 = next_point - current_point;
            let c1 = v1.dot(&v1);
            let ri_l = current_up - (2.0 / c1) * (v1.dot(&current_up)) * v1;
            let ti_l = current_tangent - (2.0 / c1) * (v1.dot(&current_tangent)) * v1;
            let v2 = next_tangent - ti_l;
            let c2 = v2.dot(&v2);
            let next_up = ri_l - (2.0 / c2) * (v2.dot(&ri_l)) * v2;
            let next_side = next_tangent.cross(&next_up);

            frames.push((next_tangent, next_up, next_side));
            current_up = next_up;
            current_tangent = next_tangent;
        }

        debug_assert!(frames.len() == parameters.len());

        Self {
            parameters: parameters.clone(),
            frames,
            last_sampled_index: 0.into(),
        }
    }

    /// Construct a RMF from parameters sampled uniformly along the specified
    /// range.
    pub fn new_from_range_resolution<F>(range: [f32; 2], resolution: usize, f: F) -> Self
    where
        F: Fn(f32) -> Vec3,
    {
        debug_assert!(range[1] > range[0]);
        let mut parameters = Vec::with_capacity(resolution);

        for i in 0..resolution
        {
            let t = i as f32 / (resolution - 1) as f32;
            let t = t * (range[1] - range[0]) + range[0];
            parameters.push(t);
        }

        Self::new_from_parameter_list(&parameters, f)
    }

    /// Return the frame triplet (Tangent, Normal, Binormal) at parameter $x$.
    /// Linear interpolation will be used to create frames for parameters
    /// not contained in the parameter vector used to construct the
    /// function.
    ///
    /// Returns the vectors that form the local frame, the first one is the
    /// tangent the other two span the orthogonal complement of the curve
    /// tangent.
    pub fn sample(&self, x: f32) -> [Vec3; 3]
    {
        debug_assert!(x >= self.parameters[0] && x <= *self.parameters.last().unwrap());

        let mut index = self.last_sampled_index.get();
        while !(x >= self.parameters[index] && x <= self.parameters[index + 1])
        {
            index = (index + 1) % self.parameters.len();
        }
        self.last_sampled_index.set(index);
        debug_assert!(index < self.parameters.len() - 1);

        let t = (x - self.parameters[index])
            / (self.parameters[index + 1] - self.parameters[index]);

        // TODO(low): Use slerp instead of lerp.
        let frame1 = self.frames[index];
        let frame2 = self.frames[index + 1];

        let tangent = slerp(&frame1.0, &frame2.0, t);
        let proxy_up = slerp(&frame1.1, &frame2.1, t);
        let side = tangent.cross(&proxy_up).normalize();
        let up = tangent.cross(&side);

        [tangent, up, side]
    }
}
