use std::fmt;
use std::ops::{Index, IndexMut};

use crate::*;

#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct Motor
{
    pub coeffs: [f32; 8],
}

impl Index<usize> for Motor
{
    type Output = f32;

    fn index<'a>(&'a self, index: usize) -> &'a Self::Output { &self.coeffs[index] }
}

impl IndexMut<usize> for Motor
{
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut Self::Output
    {
        &mut self.coeffs[index]
    }
}

impl Motor
{
    pub fn zero() -> Self { Self { coeffs: [0.0; 8] } }

    pub fn s(&self) -> f32 { self[0] }

    pub fn e01(&self) -> f32 { self[1] }

    pub fn e02(&self) -> f32 { self[2] }

    pub fn e03(&self) -> f32 { self[3] }

    pub fn e12(&self) -> f32 { self[4] }

    pub fn e31(&self) -> f32 { self[5] }

    pub fn e23(&self) -> f32 { self[6] }

    pub fn e0123(&self) -> f32 { self[7] }

    pub fn s_mut(&mut self) -> &mut f32 { &mut self[0] }

    pub fn e01_mut(&mut self) -> &mut f32 { &mut self[1] }

    pub fn e02_mut(&mut self) -> &mut f32 { &mut self[2] }

    pub fn e03_mut(&mut self) -> &mut f32 { &mut self[3] }

    pub fn e12_mut(&mut self) -> &mut f32 { &mut self[4] }

    pub fn e31_mut(&mut self) -> &mut f32 { &mut self[5] }

    pub fn e23_mut(&mut self) -> &mut f32 { &mut self[6] }

    pub fn e0123_mut(&mut self) -> &mut f32 { &mut self[7] }

    pub fn from_multi_vec(other: &MultiVec3D) -> Self
    {
        let mut res = Self::default();
        *res.s_mut() = other.s();
        *res.e01_mut() = other.e01();
        *res.e02_mut() = other.e02();
        *res.e03_mut() = other.e03();
        *res.e12_mut() = other.e12();
        *res.e31_mut() = other.e31();
        *res.e23_mut() = other.e23();
        *res.e0123_mut() = other.e0123();
        res
    }

    pub fn identity() -> Self
    {
        let mut res = Self::default();
        *res.s_mut() = 1.0;
        *res.e01_mut() = 0.0;
        *res.e02_mut() = 0.0;
        *res.e03_mut() = 0.0;
        *res.e12_mut() = 0.0;
        *res.e31_mut() = 0.0;
        *res.e23_mut() = 0.0;
        *res.e0123_mut() = 0.0;
        res
    }

    pub fn to_multi_vec(&self) -> MultiVec3D
    {
        let mut res = MultiVec3D::default();
        *res.s_mut() = self.s();
        *res.e01_mut() = self.e01();
        *res.e02_mut() = self.e02();
        *res.e03_mut() = self.e03();
        *res.e12_mut() = self.e12();
        *res.e31_mut() = self.e31();
        *res.e23_mut() = self.e23();
        *res.e0123_mut() = self.e0123();
        res
    }

    pub fn sandwich<T>(&self, b: &T) -> T
    where
        T: CPointLike + Default,
    {
        let a = self;
        let rr0 = a.e12() * b.e021() + a.e31() * b.e013() + a.e23() * b.e032()
            - a.e0123() * 1.0;
        let rr1 = -a.e23() * 1.0;
        let rr2 = -a.e31() * 1.0;
        let rr3 = -a.e12() * 1.0;
        let rr4 =
            a.s() * b.e021() - a.e03() * 1.0 + a.e31() * b.e032() - a.e23() * b.e013();
        let rr5 =
            a.s() * b.e013() - a.e02() * 1.0 - a.e12() * b.e032() + a.e23() * b.e021();
        let rr6 =
            a.s() * b.e032() - a.e01() * 1.0 + a.e12() * b.e013() - a.e31() * b.e021();
        let rr7 = a.s() * 1.0;

        let mut ret = T::default();
        *ret.e032_mut() = rr0 * a.e23() + rr1 * a.e0123() - rr2 * a.e03() + rr3 * a.e02()
            - rr4 * a.e31()
            + rr5 * a.e12()
            + rr6 * a.s()
            - rr7 * a.e01();
        *ret.e013_mut() = rr0 * a.e31() + rr1 * a.e03() + rr2 * a.e0123() - rr3 * a.e01()
            + rr4 * a.e23()
            + rr5 * a.s()
            - rr6 * a.e12()
            - rr7 * a.e02();
        *ret.e021_mut() =
            rr0 * a.e12() - rr1 * a.e02() + rr2 * a.e01() + rr3 * a.e0123() + rr4 * a.s()
                - rr5 * a.e23()
                + rr6 * a.e31()
                - rr7 * a.e03();
        let div = -rr1 * a.e23() - rr2 * a.e31() - rr3 * a.e12() + rr7 * a.s();

        *ret.e032_mut() = ret.e032() / div;
        *ret.e013_mut() = ret.e013() / div;
        *ret.e021_mut() = ret.e021() / div;

        ret
    }
}

impl Mul for Motor
{
    type Output = Motor;

    fn mul(self, b: Self) -> Self::Output
    {
        let mut ret = Self::default();
        let a = self;

        *ret.s_mut() =
            b.s() * a.s() - b.e12() * a.e12() - b.e31() * a.e31() - b.e23() * a.e23();
        *ret.e01_mut() = b.e01() * a.s() + b.s() * a.e01() - b.e12() * a.e02()
            + b.e31() * a.e03()
            + b.e02() * a.e12()
            - b.e03() * a.e31()
            - b.e0123() * a.e23()
            - b.e23() * a.e0123();
        *ret.e02_mut() = b.e02() * a.s() + b.e12() * a.e01() + b.s() * a.e02()
            - b.e23() * a.e03()
            - b.e01() * a.e12()
            - b.e0123() * a.e31()
            + b.e03() * a.e23()
            - b.e31() * a.e0123();
        *ret.e03_mut() =
            b.e03() * a.s() - b.e31() * a.e01() + b.e23() * a.e02() + b.s() * a.e03()
                - b.e0123() * a.e12()
                + b.e01() * a.e31()
                - b.e02() * a.e23()
                - b.e12() * a.e0123();
        *ret.e12_mut() =
            b.e12() * a.s() + b.s() * a.e12() + b.e23() * a.e31() - b.e31() * a.e23();
        *ret.e31_mut() =
            b.e31() * a.s() - b.e23() * a.e12() + b.s() * a.e31() + b.e12() * a.e23();
        *ret.e23_mut() =
            b.e23() * a.s() + b.e31() * a.e12() - b.e12() * a.e31() + b.s() * a.e23();
        *ret.e0123_mut() = b.e0123() * a.s()
            + b.e23() * a.e01()
            + b.e31() * a.e02()
            + b.e12() * a.e03()
            + b.e03() * a.e12()
            + b.e02() * a.e31()
            + b.e01() * a.e23()
            + b.s() * a.e0123();

        ret
    }
}

const BASIS: &'static [&'static str] = &[
    "1",     // Scalar
    "e01",   // Bivectors / Lines
    "e02",   //
    "e03",   //
    "e12",   //
    "e31",   //
    "e23",   //
    "e0123", // Pseudo scalar
];

impl fmt::Display for Motor
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        let mut n = 0;
        let ret = self
            .coeffs
            .iter()
            .enumerate()
            .filter_map(|(i, &coeff)| {
                if coeff.abs() != 0.000000
                {
                    n = 1;
                    Some(format!(
                        "{}{}",
                        format!("{:.*}", 16, coeff)
                            .trim_end_matches('0')
                            .trim_end_matches('.'),
                        if i > 0 { BASIS[i] } else { "" }
                    ))
                }
                else
                {
                    None
                }
            })
            .collect::<Vec<String>>()
            .join(" + ");
        if n == 0
        {
            write!(f, "0")
        }
        else
        {
            write!(f, "{}", ret)
        }
    }
}
