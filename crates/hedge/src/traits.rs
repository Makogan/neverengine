use std::{fmt::Debug, ops::Add, ops::Mul};

use algebra::{DVec3, Vec2, Vec3};
use chicken_wire::wavefront_loader::ObjData;
use itertools::Itertools;
use linear_isomorphic::*;
use trait_gen::trait_gen;

use crate::face_handle::*;
use crate::facets::*;
use crate::hedge_handle::*;
use crate::mesh::HalfMesh;
use crate::vert_handle::*;

pub trait HalfMeshLike {
    type VertData: PrimitiveContainer;
    type EdgeData: PrimitiveContainer;
    type FaceData: PrimitiveContainer;

    type VertIterator: Iterator<
        Item = VertHandle<Self::VertData, Self::EdgeData, Self::FaceData>,
    >;
    type EdgeIterator: Iterator<
        Item = HEdgeHandle<Self::VertData, Self::EdgeData, Self::FaceData>,
    >;
    type FaceIterator: Iterator<
        Item = FaceHandle<Self::VertData, Self::EdgeData, Self::FaceData>,
    >;

    fn last_vert_id(&self) -> GeomId<0>;

    fn iter_verts(&self) -> Self::VertIterator;
    fn iter_edges(&self) -> Self::EdgeIterator;
    fn iter_faces(&self) -> Self::FaceIterator;

    fn vert_handle(
        &self,
        id: GeomId<0>,
    ) -> VertHandle<Self::VertData, Self::EdgeData, Self::FaceData>;
    fn hedge_handle(
        &self,
        id: GeomId<1>,
    ) -> HEdgeHandle<Self::VertData, Self::EdgeData, Self::FaceData>;
    fn face_handle(
        &self,
        id: GeomId<2>,
    ) -> FaceHandle<Self::VertData, Self::EdgeData, Self::FaceData>;

    fn vert_count(&self) -> usize;
    fn hedge_count(&self) -> usize;
    fn edge_count(&self) -> usize;
    fn face_count(&self) -> usize;

    /// Returns the new vertex id and the two ids of the new edges crossing the
    /// old edge.
    /// When implementing this trait:
    /// Algorithms will assume that 6 new half edges are added such that the
    /// returned handles corresponds to the transversal hedges then the
    /// longitudinal ones. i.e. if $e$ is the number of hedges before a
    /// split, then $e + 6$ is the number of hedges after the split. The
    /// first array is the first 2 new transversal edges. The second array
    /// is the longitudinal ones, such that the first is the new one.
    fn split_edge<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 2], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float;

    /// Returns the new verex id and the two ids of the new edges crossing the
    /// old edge.
    /// When implementing this trait:
    /// Algorithms will assume that 4 new half edges are added such that the
    /// returned handles corresponds to the transversal hedge followed by
    /// the longitudinal ones. i.e. if $e$ is the number of hedges before a
    /// split, then $e + 4$ is the number of hedges after the split. The
    /// first array is one of the first 2 new hedges and it is in the
    /// transversal edge. The next array is the two longitudinal
    /// hedges where the first is newly added one.
    fn split_edge_boundary<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 1], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float;

    fn flip_edge(&mut self, id: GeomId<1>);

    fn is_in_boundary_edge(&self, id: GeomId<1>) -> bool;

    // TODO(high): Implement full functionality that also copies the edges and
    // faces.
    fn to_mesh(
        &self,
    ) -> HalfMesh<Vec<<Self::VertData as PrimitiveContainer>::PrimitiveData>, (), ()>
    {
        let mut topology = Vec::new();
        for face in self.iter_faces() {
            let mut face_topology = Vec::new();
            for edge in self.hedge_handle(face.hedge().id()).iter_loop() {
                face_topology.push(edge.source().id.0 as u64);
            }
            topology.push(face_topology);
        }

        let vertices: Vec<_> = self.iter_verts().map(|v| v.data()).collect();

        HalfMesh::new(vertices, (), (), topology)
    }
}

/// Interface used to initialize the half edge. Types can be () if the data is
/// not needed.
pub trait MeshData {
    /// Data that will be stored at each vertex. e.g. position and normal
    /// vecors.
    type VertData;
    /// Data that will be stored at each edge. e.g. sharpness weights for
    /// subdivision.
    type EdgeData;
    /// Data that will be stored at each face. e.g. uv list or normal list.
    type FaceData;

    /// Returns the data for a given vertex.
    fn get_vert_data(&self, vert_id: u64) -> Self::VertData;
    /// `hedge_id` is a *local* identifier. For example in a triangle mesh,
    /// `hedge_id` ranges from 0 to 3. Care should be taken to maintain
    /// symmetry if necessary. Each half edge will have an independent
    /// `EdgeData` value.
    ///
    /// Returns the data for a given hedge within a face.
    fn get_hedge_data(&self, face_id: u64, hedge_id: u64) -> Self::EdgeData;
    /// Returns the data for a face.
    fn get_face_data(&self, face_id: u64) -> Self::FaceData;
    /// List of faces, each inner array is a list of indices making up a face.
    ///
    /// For example for a mesh with triangles and quads, some arrays would be of
    /// length 3, some of length 4. Each array corresponding to a specific
    /// face.
    fn get_topology(&self) -> Vec<Vec<u64>>;
    /// Returns the total number of vertices in this data.
    fn vert_count(&self) -> usize;
}

/// Default implementation for the case where the mesh data is represented
/// as a list of vertices and an (implicit) list of polygonal faces.
#[trait_gen(T -> u8, u16, u32, u64, u128, usize)]
impl<V> MeshData for (Vec<V>, Vec<T>)
where
    V: Clone + Debug,
{
    type EdgeData = ();
    type FaceData = ();
    type VertData = V;

    fn get_vert_data(&self, vert_id: u64) -> Self::VertData {
        self.0[vert_id as usize].clone()
    }

    fn get_hedge_data(&self, _face_id: u64, _hedge_id: u64) -> Self::EdgeData {
        ()
    }

    fn get_face_data(&self, _face_id: u64) -> Self::FaceData {
        ()
    }

    fn get_topology(&self) -> Vec<Vec<u64>> {
        let mut toplogy = Vec::new();

        for i in (0..self.1.len()).step_by(3) {
            let face = vec![
                self.1[i + 0] as u64,
                self.1[i + 1] as u64,
                self.1[i + 2] as u64,
            ];

            toplogy.push(face);
        }

        toplogy
    }

    fn vert_count(&self) -> usize {
        self.0.len()
    }
}

/// Default implementation for the case where the mesh data is represented
/// as a list of vertices and an list of faces.
#[trait_gen(T -> u8, u16, u32, u64, u128, usize)]
impl<V> MeshData for (Vec<V>, Vec<Vec<T>>)
where
    V: Clone + Debug,
{
    type EdgeData = ();
    type FaceData = ();
    type VertData = V;

    fn get_vert_data(&self, vert_id: u64) -> Self::VertData {
        self.0[vert_id as usize].clone()
    }

    fn get_hedge_data(&self, _face_id: u64, _hedge_id: u64) -> Self::EdgeData {
        ()
    }

    fn get_face_data(&self, _face_id: u64) -> Self::FaceData {
        ()
    }

    fn get_topology(&self) -> Vec<Vec<u64>> {
        self.1
            .iter()
            .map(|inner: &Vec<_>| {
                inner
                    .into_iter()
                    .map(|y: &T| *y as u64)
                    .collect::<Vec<u64>>()
            })
            .collect::<Vec<Vec<u64>>>()
    }

    fn vert_count(&self) -> usize {
        self.0.len()
    }
}

/// Default implementation for initializing a mesh from vertex, hedge, face and
/// topology data.
impl<V, E, F> MeshData for (Vec<V>, Vec<Vec<E>>, Vec<F>, Vec<Vec<usize>>)
where
    V: Clone + Debug,
    E: Clone + Debug,
    F: Clone + Debug,
{
    type EdgeData = E;
    type FaceData = F;
    type VertData = V;

    fn get_vert_data(&self, vert_id: u64) -> Self::VertData {
        self.0[vert_id as usize].clone()
    }

    fn get_hedge_data(&self, face_id: u64, hedge_id: u64) -> Self::EdgeData {
        self.1[face_id as usize][hedge_id as usize].clone()
    }

    fn get_face_data(&self, face_id: u64) -> Self::FaceData {
        self.2[face_id as usize].clone()
    }

    fn get_topology(&self) -> Vec<Vec<u64>> {
        let mut toplogy = Vec::new();

        for face in &self.3 {
            let new_face = face.iter().map(|&i| i as u64).collect();

            toplogy.push(new_face);
        }

        toplogy
    }

    fn vert_count(&self) -> usize {
        self.0.len()
    }
}

/// Default implementation for the case where the mesh data is represented
/// as a list of vertices, a list of normals and two lists of
/// faces.
#[trait_gen(T -> u8, u16, u32, u64, u128, usize)]
impl<V, N> MeshData for ((Vec<V>, Vec<N>), [Vec<Vec<T>>; 2])
where
    V: Clone + Debug,
    N: Clone + Debug,
{
    type EdgeData = ();
    type FaceData = Vec<N>;
    type VertData = V;

    fn get_vert_data(&self, vert_id: u64) -> Self::VertData {
        self.0 .0[vert_id as usize].clone()
    }

    fn get_hedge_data(&self, _face_id: u64, _hedge_id: u64) -> Self::EdgeData {
        ()
    }

    fn get_face_data(&self, face_id: u64) -> Self::FaceData {
        // Get the normals.
        self.1[1][face_id as usize]
            .iter()
            .map(|i| self.0 .1[*i as usize].clone())
            .collect_vec()
    }

    fn get_topology(&self) -> Vec<Vec<u64>> {
        self.1[0]
            .iter()
            .map(|inner: &Vec<_>| {
                inner
                    .into_iter()
                    .map(|y: &T| *y as u64)
                    .collect::<Vec<u64>>()
            })
            .collect::<Vec<Vec<u64>>>()
    }

    fn vert_count(&self) -> usize {
        self.0 .0.len()
    }
}

impl MeshData for ObjData {
    type EdgeData = ();
    type FaceData = (Vec<Vec2>, Vec<Vec3>);
    type VertData = Vec3;

    fn vert_count(&self) -> usize {
        self.vertices.len()
    }

    fn get_vert_data(&self, vert_id: u64) -> Self::VertData {
        self.vertices[vert_id as usize]
    }

    fn get_face_data(&self, face_id: u64) -> Self::FaceData {
        debug_assert!(
            self.uv_face_indices.len() == 0
                || self.normal_face_indices.len() == 0
                || self.uv_face_indices.len() == self.normal_face_indices.len()
        );

        (
            if self.uv_face_indices.len() > 0 {
                self.uv_face_indices[face_id as usize]
                    .iter()
                    .map(|&i| self.uvs[i as usize])
                    .collect_vec()
            } else {
                Vec::new()
            },
            if self.normal_face_indices.len() > 0 {
                self.normal_face_indices[face_id as usize]
                    .iter()
                    .map(|&i| self.normals[i as usize])
                    .collect_vec()
            } else {
                Vec::new()
            },
        )
    }

    fn get_hedge_data(&self, _face_id: u64, _hedge_id: u64) -> Self::EdgeData {
        ()
    }

    fn get_topology(&self) -> Vec<Vec<u64>> {
        self.vertex_face_indices
            .iter()
            .map(|l| l.iter().map(|&i| i as u64).collect_vec())
            .collect_vec()
    }
}

pub trait InternalMeshAccess<V, E, F> {
    unsafe fn inner_mesh(&self) -> *mut HalfMesh<V, E, F>
    where
        V: PrimitiveContainer,
        E: PrimitiveContainer,
        F: PrimitiveContainer;
}

pub trait HasNormals<S>
where
    S: RealField,
{
    type Vector: InnerSpace<S>;
    fn has_normals() -> bool;
    fn normal(&self) -> Self::Vector;
    fn position(&self) -> Self::Vector;
    fn mutate_position(&mut self, data: &Self::Vector);

    /// Approximates a vertex normals given an iterator over the faces of the
    /// model.
    fn approx_normals<F, V>(vert_count: usize, face_iter: F) -> Vec<Self::Vector>
    where
        F: Iterator<Item = V>,
        V: Iterator<Item = Self::Vector>,
    {
        let mut res = vec![Self::Vector::default(); vert_count];
        let mut weights = vec![S::from(0.).unwrap(); vert_count];
        for face in face_iter {
            let verts: Vec<_> = face.collect();

            let mut id = 0;
            for (&ref prev, &ref current, &ref next) in
                verts.iter().circular_tuple_windows()
            {
                let d1 = (prev.clone() - current.clone()).normalized();
                let d2 = (next.clone() - current.clone()).normalized();
                let n = d1.cross(&d2);

                let weight = (d1.dot(&d2)).acos();

                res[id] = res[id].clone() + n;
                weights[id] += weight;
                id += 1;
            }
        }

        for (i, w) in weights.iter().enumerate() {
            res[i] = res[i].clone() * (S::from(1.0).unwrap() / *w);
        }

        res
    }
}

impl HasNormals<f32> for Vec3 {
    type Vector = Vec3;

    fn has_normals() -> bool {
        false
    }

    fn normal(&self) -> Self::Vector {
        Vec3::default()
    }

    fn position(&self) -> Self::Vector {
        *self
    }

    fn mutate_position(&mut self, data: &Self::Vector) {
        *self = *data;
    }
}

impl HasNormals<f64> for DVec3 {
    type Vector = DVec3;

    fn has_normals() -> bool {
        false
    }

    fn normal(&self) -> Self::Vector {
        DVec3::default()
    }

    fn position(&self) -> Self::Vector {
        *self
    }

    fn mutate_position(&mut self, data: &Self::Vector) {
        *self = *data;
    }
}

pub trait PrimitiveContainer: Clone + Debug {
    type PrimitiveData: Clone + Debug + Default;

    fn get(&self, index: u64) -> &Self::PrimitiveData;
    fn get_mut(&mut self, index: u64) -> &mut Self::PrimitiveData;
    fn set(&mut self, index: u64, data: Self::PrimitiveData);

    fn push(&mut self, data: Self::PrimitiveData);
    fn remove(&mut self, index: u64) -> Self::PrimitiveData;
    fn swap_remove(&mut self, index: u64) -> Self::PrimitiveData;

    fn retain<F: FnMut(&Self::PrimitiveData) -> bool>(&mut self, f: F);

    fn len(&self) -> usize;
}

impl<T: Clone + Debug + Default> PrimitiveContainer for Vec<T> {
    type PrimitiveData = T;

    fn get(&self, index: u64) -> &Self::PrimitiveData {
        &self[index as usize]
    }

    fn get_mut(&mut self, index: u64) -> &mut Self::PrimitiveData {
        &mut self[index as usize]
    }

    fn set(&mut self, index: u64, data: Self::PrimitiveData) {
        self[index as usize] = data
    }

    fn push(&mut self, data: Self::PrimitiveData) {
        self.push(data)
    }

    fn remove(&mut self, index: u64) -> Self::PrimitiveData {
        self.remove(index as usize)
    }

    fn swap_remove(&mut self, index: u64) -> Self::PrimitiveData {
        self.swap_remove(index as usize)
    }

    fn retain<F: FnMut(&Self::PrimitiveData) -> bool>(&mut self, f: F) {
        self.retain(f)
    }

    fn len(&self) -> usize {
        self.len()
    }
}

impl PrimitiveContainer for () {
    type PrimitiveData = ();

    fn get(&self, _index: u64) -> &Self::PrimitiveData {
        self
    }

    fn get_mut(&mut self, _index: u64) -> &mut Self::PrimitiveData {
        self
    }

    fn set(&mut self, _index: u64, _data: Self::PrimitiveData) {}

    fn push(&mut self, _he1data: Self::PrimitiveData) {}

    fn remove(&mut self, _index: u64) -> Self::PrimitiveData {}

    fn swap_remove(&mut self, _index: u64) -> Self::PrimitiveData {
        ()
    }

    fn retain<F: FnMut(&Self::PrimitiveData) -> bool>(&mut self, _f: F) {}

    fn len(&self) -> usize {
        0
    }
}
