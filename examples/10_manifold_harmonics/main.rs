use std::cell::RefCell;
use std::rc::Rc;

use algebra::eigen_decomposition::eigen_decompose_sparse_symmetric_matrix;
use algebra::Mat4;
use camera::*;
use chicken_wire::wavefront_loader::ObjData;
use dem_core::{io::*, rendering::*};
use dem_gui::*;
use dem_peripherals::*;
use geometry::{global_mesh_laplacian, MeshWeights};
use hedge::{
    mesh::{GeomId, HalfMesh},
    HalfMeshLike,
};
use vulkan_bindings::RenderContext;

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct Mvp {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

// https://graphics.stanford.edu/courses/cs233-21-spring/ReferencedPapers/understand_geometry_01631196.pdf
fn main() {
    let mut io_context = IoContext::new("Shape Blending", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let phong_shader = render_context.add_shader(vec![
        "examples/10_manifold_harmonics/scalar_shader/".to_string(),
    ]);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let mut gui = Some(DebugGui::init(
        &mut render_context,
        io_context.window().get_window_size(),
    ));

    let ObjData {
        vertices: verts,
        vertex_face_indices: verts_idx,
        ..
    } = ObjData::from_disk_file("./Assets/stanford_dragon.obj");

    let mesh = HalfMesh::new(verts.clone(), (), (), verts_idx.clone());
    let (cot_matrix, mut mass, _index_permutation, _unkown_count) = global_mesh_laplacian(
        mesh.iter_verts().map(|v| v.data()),
        |id| {
            mesh.vert_handle(GeomId::from(id))
                .iter_verts()
                .map(|v| (v.data(), v.id().0 as usize))
        },
        |_| false,
        MeshWeights::Cotan,
    );

    for v in mass.values_mut() {
        *v = 1.0 / ((1.0 / *v).sqrt());
    }
    let mat = mass.clone() * cot_matrix * mass.clone();
    // Not sure why this hack is needed.
    // let symmetric_mat = -0.5 * (mat.clone() +
    // mat.transpose().to_col_major().unwrap());
    let symmetric_mat = -mat;

    use std::time::Instant;
    let start = Instant::now();
    let eigenval_count = 200;
    let (evals, eigen_vectors) =
        eigen_decompose_sparse_symmetric_matrix(&symmetric_mat, eigenval_count);
    let duration = start.elapsed();
    println!("Time computing MHB is: {:?}", duration);

    let fun1 = eigen_vectors.col(0);
    let fun1_vals: Vec<_> = fun1.iter().map(|&v| (v as f32 * 100.0)).collect();

    let gpu_mesh = GraphicsInput::new()
        .add_attribute_buffer_from_slice(&verts, &mut render_context)
        .add_attribute_buffer_from_slice(&fun1_vals, &mut render_context)
        .set_index_buffer_from_slice(
            &verts_idx
                .iter()
                .flatten()
                .map(|&i| i as u32)
                .collect::<Vec<_>>(),
            &mut render_context,
        );

    let mut render_request = RenderRequest::new(phong_shader)
        .vertex_input(gpu_mesh.clone())
        .add_ubo(0, Mat4::default())
        .build();

    let mut now = Instant::now();
    let mut selected_eigenvector = 0;
    let mut update = true;
    while io_context.poll_io(&mut gui) {
        render_context.start_frame(&io_context);
        let mvp = Mvp {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };
        let elapsed_time = now.elapsed().as_millis() as f32;
        now = Instant::now();

        render_request.set_ubo(0, mvp);
        render_context.draw(&render_request);

        gui.as_mut().unwrap().draw(&mut render_context, &mut |ctx| {
            dem_gui::Window::new("Program info:").show(ctx, |ui| {
                ui.label(format!(
                    "{:.0}ms ({:.0} FPS)",
                    elapsed_time,
                    1000.0 / elapsed_time
                ));

                ui.horizontal(|ui| {
                    if ui.button("-").clicked() {
                        selected_eigenvector =
                            (selected_eigenvector + evals.len() - 1) % evals.len();
                        update = true;
                    }
                    ui.label(format!("Selected Eigen Level {}", selected_eigenvector));
                    if ui.button("+").clicked() {
                        selected_eigenvector = (selected_eigenvector + 1) % evals.len();
                        update = true;
                    }
                });
            });
        });

        if update {
            let buffer = render_request.vertex_input.get_attribute_buffer_handle(1);
            let fun1 = eigen_vectors.col(selected_eigenvector);
            let fun1 = mass.clone() * fun1;

            let fun1_vals: Vec<_> = fun1.iter().map(|&v| (v as f32 * 10.0)).collect();
            render_context
                .update_buffer_from_slice(&GpuMemoryHandle::Buffer(buffer), &fun1_vals);
            update = false;
        }
    }

    render_context.free_input(render_request.vertex_input);
}
