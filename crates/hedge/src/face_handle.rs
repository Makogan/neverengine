use core::ops::{Add, Mul, Neg};
use std::fmt::Debug;
use std::fmt::Display;

use linear_isomorphic::*;

use crate::facets::*;
use crate::hedge_handle::*;
use crate::mesh::*;
use crate::VertHandle;

/// As a best practice don't store this handle, prefer using its id as  returned
/// by the `id()` method.
#[derive(Clone, Copy, Debug)]
pub struct FaceHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) id: GeomId<2>,
}

impl<V, E, F> FaceHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn data(&self) -> F::PrimitiveData
    {
        unsafe { (*self.mesh).faces.borrow().get(self.id.0).clone() }
    }

    pub fn id(&self) -> GeomId<2> { self.id }

    pub fn disable(&self)
    {
        unsafe {
            (*(*self.mesh).faces_meta.borrow_mut())[self.id.0 as usize].enabled = false;
        }
    }

    pub fn is_disabled(&self) -> bool
    {
        unsafe {
            (*self.mesh).faces_meta.borrow_mut()[self.id.0 as usize].enabled == false
        }
    }

    pub fn hedge(&self) -> HEdgeHandle<V, E, F>
    {
        unsafe {
            let face_hedge_id =
                (*self.mesh).faces_meta.borrow()[self.id.0 as usize].hedge_id;
            debug_assert!(face_hedge_id != ABSENT());
            HEdgeHandle::<V, E, F> {
                id: (*self.mesh).hedges_meta.borrow()[face_hedge_id.0 as usize].id,
                mesh: self.mesh,
            }
        }
    }

    /// Split a face into 3 faces sharing the barycenter as a vertex. Assumes
    /// the face is triangular.
    /// Returns the id of the new vertex.
    pub fn split_triangle_face<S>(&self) -> GeomId<0>
    where
        V::PrimitiveData:
            Add<Output = V::PrimitiveData> + Mul<S, Output = V::PrimitiveData>,
        S: num::Float,
    {
        let e1 = &self.hedge();
        let e2 = e1.next();
        let e3 = e2.next();

        let e1_id = e1.id();
        let e2_id = e2.id();
        let e3_id = e3.id();

        let v1 = e1.source();
        let v2 = e2.source();
        let v3 = e3.source();

        let v1_id = v1.id();
        let v2_id = v2.id();
        let v3_id = v3.id();

        let vdata = (v1.data() + v2.data() + v3.data()) * S::from(1.0 / 3.0).unwrap();

        let vn_id = unsafe { (*self.mesh).add_vert(vdata) };

        let edata = e1.data();

        let (e5_id, e8_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v1_id, edata.clone(), edata.clone()) };

        let (e7_id, e4_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v2_id, edata.clone(), edata.clone()) };

        let (e9_id, e6_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v3_id, edata.clone(), edata.clone()) };

        let fdata1 = self.data();

        let f1_id = self.id();
        let f2_id = unsafe { (*self.mesh).add_face_through_edge(e1_id, fdata1.clone()) };
        let f3_id = unsafe { (*self.mesh).add_face_through_edge(e2_id, fdata1.clone()) };

        unsafe {
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e3_id,
                e8_id,
                e9_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e1_id,
                e4_id,
                e5_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f3_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e2_id,
                e6_id,
                e7_id,
            );
        }

        vn_id
    }

    /// Split a face into 3 faces sharing the identified vertex. Care should be
    /// taken that the specified vertex is valid, this function does not
    /// check for topological invariants.
    pub fn split_triangle_face_and_fit<S>(&self, vn_id: GeomId<0>)
    where
        <V as PrimitiveContainer>::PrimitiveData: Add<Output = <V as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <V as PrimitiveContainer>::PrimitiveData>,
        S: num::Float,
    {
        let e1 = &self.hedge();
        let e2 = e1.next();
        let e3 = e2.next();

        let e1_id = e1.id();
        let e2_id = e2.id();
        let e3_id = e3.id();

        let v1 = e1.source();
        let v2 = e2.source();
        let v3 = e3.source();

        let v1_id = v1.id();
        let v2_id = v2.id();
        let v3_id = v3.id();

        let edata = e1.data();

        let (e5_id, e8_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v1_id, edata.clone(), edata.clone()) };

        let (e7_id, e4_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v2_id, edata.clone(), edata.clone()) };

        let (e9_id, e6_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v3_id, edata.clone(), edata.clone()) };

        let fdata1 = self.data();

        let f1_id = self.id();
        let f2_id = unsafe { (*self.mesh).add_face_through_edge(e1_id, fdata1.clone()) };
        let f3_id = unsafe { (*self.mesh).add_face_through_edge(e2_id, fdata1.clone()) };

        unsafe {
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e3_id,
                e8_id,
                e9_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e1_id,
                e4_id,
                e5_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f3_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e2_id,
                e6_id,
                e7_id,
            );
        }
    }

    pub fn vertices(&self) -> Vec<V::PrimitiveData>
    {
        self.hedge()
            .iter_loop()
            .map(|e| e.source().data())
            .collect()
    }

    pub fn vertex_handles(&self) -> impl Iterator<Item = VertHandle<V, E, F>>
    {
        self.hedge().iter_loop().map(|e| e.source())
    }

    pub fn vertices_tri(&self) -> [V::PrimitiveData; 3]
    {
        debug_assert!(self.hedge().iter_loop().count() == 3);
        [
            self.hedge().source().data(),
            self.hedge().next().source().data(),
            self.hedge().prev().source().data(),
        ]
    }

    pub fn naive_normal<S>(&self) -> V::PrimitiveData
    where
        V::PrimitiveData: InnerSpace<S> + Neg<Output = V::PrimitiveData>,
        S: Display + Debug + RealField,
    {
        let e1: V::PrimitiveData = self.hedge().dir();
        let e2: V::PrimitiveData = self.hedge().prev().dir();

        return e2.cross(&e1);
    }
}

#[derive(Clone, Copy)]
pub struct FullFaceIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) current_id: GeomId<2>,
    pub(crate) end_id: GeomId<2>,
}

impl<V, E, F> Iterator for FullFaceIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = FaceHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let val = if self.current_id.0 < self.end_id.0
        {
            Some(FaceHandle {
                mesh: self.mesh,
                id: self.current_id,
            })
        }
        else
        {
            None
        };

        self.current_id.0 += 1;

        val
    }
}

impl<V, E, F> FullFaceIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn skip_to(&mut self, f_id: GeomId<2>) -> FullFaceIter<V, E, F>
    {
        self.current_id = f_id;
        (*self).clone()
    }
}

// +| Traits |+ ======================================================
pub trait FaceMetrics<V, N, S>
where
    V: Clone + Debug,
    S: RealField,
{
    fn area(&self) -> S;
    fn normal(&self) -> N;
}

impl<V, E, F, S> FaceMetrics<V, V::PrimitiveData, S> for FaceHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
    V::PrimitiveData: InnerSpace<S> + Neg<Output = V::PrimitiveData>,
    S: RealField,
{
    fn area(&self) -> S
    {
        let e1: V::PrimitiveData = self.hedge().dir();
        let e2: V::PrimitiveData = self.hedge().prev().dir();

        return e1.cross(&e2).norm() / S::from(2.0).unwrap();
    }

    fn normal(&self) -> V::PrimitiveData
    {
        let e1: V::PrimitiveData = self.hedge().dir();
        let e2: V::PrimitiveData = self.hedge().prev().dir();

        return e1.cross(&e2);
    }
}
