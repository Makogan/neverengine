use algebra::{na::DMatrix, *};
use graphs::{depth_first_search::DFSIterator, permute};
use pga::MultiVec3D;

use crate::{convex_hull, indexed_planar_convex_hull};
const EPSILON_DEBUG: f64 = 1e-6;
const EPSILON: f64 = 1e-7;
// B-meshes.
// https://onlinelibrary.wiley.com/doi/full/10.1111/j.1467-8659.2010.01805.x

/// Given a polytope defined as the intersection of half spaces, compute a mesh
/// representation of the polytope. This effectively performs a depth first
/// search over the graph described by the polytope.
/// This is guaranteed to return a list of faces matching the order of the input
/// planes.
pub fn vertex_enumeration_from_half_spaces(
    planes: &DMatrix<f64>,
) -> (Vec<DVec3>, Vec<Vec<usize>>)
{
    debug_assert!(planes.nrows() > 3);
    debug_assert!(planes.ncols() == 4);

    let mut faces = vec![vec![]; planes.nrows()];
    let points: Vec<_> = HalfspaceVertexEnumerator::new(planes)
        .enumerate()
        .map(|(vid, (position, planes))| {
            for fid in planes
            {
                faces[fid].push(vid)
            }

            position
        })
        .collect();

    let centroid = points.iter().sum::<DVec3>() / points.len() as f64;
    for face in &mut faces
    {
        debug_assert!(face.len() >= 3);
        // With an orientation picked, ensure the order of the face's indices
        // actually makes a face.
        let fetch = |i: usize| points[face[i]];
        let indices = indexed_planar_convex_hull(&fetch, face.len());

        permute(face, indices.clone());

        // Reverse orientation of the face if it points towards the centroid.
        let normal = |f: &Vec<usize>| {
            let p1 = points[f[0]];
            let p2 = points[f[1]];
            let p3 = points[f[2]];

            (p2 - p1).cross(&(p3 - p1))
        };

        let n = normal(face);
        let p1 = points[face[0]];
        if (p1 - centroid).dot(&n) <= 0.0
        {
            face.reverse()
        }
    }

    (points, faces)
}

/// Iterator for enumerating all vertices of a convex polytope defined as the
/// intersection of half spaces. It returns a list of the indices of *all*
/// planes that intersect at a given vertex. (Currently only works on 3D
/// planes).
/// WARNING!: This is very sensitive to numerical imprecision.
pub struct HalfspaceVertexEnumerator<'a>
{
    dfs_iter: DFSIterator<
        PolyVertexData,
        Box<
            dyn Fn(&PolyVertexData, usize, &PolyVertexData) -> PolytopeEdgeIterator<'a>
                + 'a,
        >,
        PolytopeEdgeIterator<'a>,
    >,
}

impl<'a> HalfspaceVertexEnumerator<'a>
{
    pub fn new(planes: &'a DMatrix<f64>) -> Self
    {
        // Get SDF of a point relative to a plane.
        let sdf_test = |point: &DVec3, plane: usize| {
            planes
                .row(plane)
                .fixed_columns::<3>(0)
                .dot(&point.transpose())
                - planes[(plane, 3)]
        };

        // All planes are assumed to correspond to a face, thus plane 0 is one of the
        // planes intersecting at some vertex in the polytope.
        let selected_normal = planes.row(0).fixed_columns::<3>(0).transpose();
        // The plane most closely coplanar to plane 0 must be adjacent to it, thus
        // togheter they form an edge in the polytope.
        let mut best_test = -2.0;
        let mut other_plane = 0;
        for r in 1..planes.nrows()
        {
            let normal = planes.row(r).fixed_columns::<3>(0).transpose();
            debug_assert!(
                (normal.norm() - 1.0).abs() <= EPSILON_DEBUG,
                "{}",
                (normal.norm() - 1.0).abs()
            );

            let test = selected_normal.dot(&normal);
            if test > best_test
            {
                other_plane = r;
                best_test = test;
            }
        }

        let p1 = 0;
        let p2 = other_plane;
        let p3 = (0..planes.nrows())
            .find(|&i| {
                i != p1
                    && i != p2
                    && get_point(&PolytopeVertex(vec![p1, p2, i]), planes).is_some()
            })
            .unwrap();

        // Linearly searching along the edge of the polytope we just found. A vertex
        // in the polytope will satisfy that it is at most 0. Thus any plane that
        // produces a positive signed distance for the current vertex is a better
        // candidate. At the end we are guaranteed a vertex in the polytope.
        let mut current_vertex = PolytopeVertex(vec![p1, p2, p3]);
        let mut point = get_point(&current_vertex, planes).unwrap();
        for r in 0..planes.nrows()
        {
            if r == p1 || r == p2 || r == p3
            {
                continue;
            }

            let test = sdf_test(&point, r);
            if test >= EPSILON
            {
                let mut next_vertex = current_vertex.clone();
                next_vertex.0[2] = r;

                match get_point(&next_vertex, planes)
                {
                    None =>
                    {}
                    Some(p) =>
                    {
                        point = p;
                        current_vertex = next_vertex;
                    }
                }
            }
        }

        let current_vertex = find_all_planes_at_point(&current_vertex, planes);
        let current_vertex = sort_degenerate_planes(&current_vertex, planes);

        let edges: Box<
            dyn Fn(&PolyVertexData, usize, &PolyVertexData) -> PolytopeEdgeIterator<'a>,
        > = Box::new(|vertex: &PolyVertexData, _, _| {
            PolytopeEdgeIterator::new(vertex.clone(), planes)
        });

        let dfs_iter = DFSIterator::new(PolyVertexData(point, current_vertex), edges);

        Self { dfs_iter }
    }
}

impl<'a> Iterator for HalfspaceVertexEnumerator<'a>
{
    type Item = (DVec3, Vec<usize>);

    fn next(&mut self) -> Option<Self::Item>
    {
        match self.dfs_iter.next()
        {
            Some((PolyVertexData(pos, plane_ids), _graph_depth, _ancestor)) =>
            {
                Some((pos, plane_ids.0))
            }
            None => None,
        }
    }
}

// +| Internal |+ ==============================================================

/// A point is defined by the intersection of d hyper planes. A line is given by
/// d-1 of them. Given the split of the d-1 h-planes for the line and the last
/// one that defines the point, search among all planes for the one that yields
/// the closest one along the line.
/// i.e. This will look for all intersections of planes with the line and find a
/// point that:
/// * Is on the polytope.
/// * Is on the negative side of the half plane identified by complement.
///
/// Returns both the point and the plane that generates it when intersecting the
/// line.
fn line_search(
    complement: usize,
    line: &[usize],
    ignore: &[usize],
    planes: &DMatrix<f64>,
) -> (DVec3, usize)
{
    let plane1 = MultiVec3D::from_implicit_plane(
        planes[(line[0], 0)] as f32,
        planes[(line[0], 1)] as f32,
        planes[(line[0], 2)] as f32,
        planes[(line[0], 3)] as f32,
    );
    let plane2 = MultiVec3D::from_implicit_plane(
        planes[(line[1], 0)] as f32,
        planes[(line[1], 1)] as f32,
        planes[(line[1], 2)] as f32,
        planes[(line[1], 3)] as f32,
    );
    // Find the direction of the line at the intersection of both planes.
    let intersection = plane1 ^ plane2;
    let d: DVec3 = na::convert(intersection.get_line_dir().normalize());
    let test = d.dot(&planes.row(complement).fixed_columns::<3>(0).transpose());
    let dir = -1.0 * test.signum() * d;

    let mut selected_index = complement;
    let (d1, _) = get_plane_params(line[0], planes);
    let (d2, _) = get_plane_params(line[1], planes);
    let (d3, _) = get_plane_params(complement, planes);

    // Plane normals should be unit length.
    debug_assert!((d1.norm_squared() - 1.0).abs() < EPSILON_DEBUG);
    debug_assert!((d2.norm_squared() - 1.0).abs() < EPSILON_DEBUG);
    debug_assert!((d3.norm_squared() - 1.0).abs() < EPSILON_DEBUG);

    let mut point =
        get_point(&PolytopeVertex(vec![line[0], line[1], complement]), planes).unwrap();
    let mut best_mu = f64::MAX;
    for i in 0..planes.nrows()
    {
        if ignore.contains(&i)
        {
            continue;
        }
        let (d, t) = get_plane_params(i, planes);

        let mu = (t - d.dot(&point)) / d.dot(&dir);
        if mu > 0.0 && mu < best_mu
        {
            best_mu = mu;
            selected_index = i;
        }
    }

    point += dir * best_mu;
    (point, selected_index)
}

use std::hash::{Hash, Hasher};
#[derive(Clone, Debug)]
struct PolytopeVertex(Vec<usize>);

impl Hash for PolytopeVertex
{
    fn hash<H: Hasher>(&self, state: &mut H)
    {
        let mut seed = 1;
        for i in &self.0
        {
            seed *= *i;
        }
        seed.hash(state);
    }
}

impl PartialEq for PolytopeVertex
{
    fn eq(&self, other: &Self) -> bool
    {
        let mut c1 = self.clone();
        let mut c2 = other.clone();

        if c1.0.len() != c2.0.len()
        {
            return false;
        }

        c1.0.sort();
        c2.0.sort();

        for i in 0..c1.0.len()
        {
            if c1.0[i] != c2.0[i]
            {
                return false;
            }
        }
        true
    }
}
impl Eq for PolytopeVertex {}

#[derive(Debug, Clone)]
struct PolyVertexData(DVec3, PolytopeVertex);

impl PartialEq for PolyVertexData
{
    fn eq(&self, other: &Self) -> bool { self.1 == other.1 }
}
impl Eq for PolyVertexData {}

impl PartialOrd for PolyVertexData
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering>
    {
        self.1 .0.partial_cmp(&other.1 .0)
    }
}

impl Ord for PolyVertexData
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering { self.1 .0.cmp(&other.1 .0) }
}

impl Hash for PolyVertexData
{
    fn hash<H: Hasher>(&self, state: &mut H) { self.1.hash(state) }
}

struct PolytopeEdgeIterator<'a>
{
    index: usize,
    vertex: PolyVertexData,
    planes: &'a DMatrix<f64>,
}

impl<'a> PolytopeEdgeIterator<'a>
{
    pub fn new(vertex: PolyVertexData, planes: &'a DMatrix<f64>) -> Self
    {
        Self {
            index: 0,
            vertex,
            planes,
        }
    }
}

impl<'a> Iterator for PolytopeEdgeIterator<'a>
{
    type Item = PolyVertexData;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.index >= self.vertex.1 .0.len()
        {
            return None;
        }

        let i = self.index;
        self.index += 1;

        // Find the line at the intersection of the planes.
        let line = vec![
            self.vertex.1 .0[i],
            self.vertex.1 .0[(i + 1) % self.vertex.1 .0.len()],
        ];
        let opposite = self.vertex.1 .0[(i + 2) % self.vertex.1 .0.len()];

        let (pos, i) = line_search(opposite, &line, &self.vertex.1 .0, self.planes);

        let vertex_planes = find_all_planes_at_point(
            &PolytopeVertex(vec![line[0], line[1], i]),
            self.planes,
        );

        debug_assert!(vertex_planes.0.len() >= 3);

        let sorted_vertex_planes = sort_degenerate_planes(&vertex_planes, self.planes);

        debug_assert!(sorted_vertex_planes.0.len() == vertex_planes.0.len());
        Some(PolyVertexData(pos, sorted_vertex_planes))
    }
}

/// Find all planes that intersect at the specified point.
fn find_all_planes_at_point(
    vertex: &PolytopeVertex,
    planes: &DMatrix<f64>,
) -> PolytopeVertex
{
    debug_assert!(vertex.0.len() >= 3);
    let point = get_point(vertex, planes).unwrap();
    let mut result = Vec::from_iter(vertex.0.iter().copied());

    for i in 0..planes.nrows()
    {
        if vertex.0.contains(&i)
        {
            continue;
        }

        let new_vertex = PolytopeVertex(vec![vertex.0[0], vertex.0[1], i]);
        let candidate = get_point(&new_vertex, planes);

        if candidate.is_none()
        {
            continue;
        }

        let candidate = candidate.unwrap();
        if (candidate - point).norm_squared() <= 1e-11
        {
            result.push(i);
        }
    }
    PolytopeVertex(result)
}

/// Sort all planes that intersect at the same point such that they have a
/// consistent orientation.
fn sort_degenerate_planes(
    indices: &PolytopeVertex,
    planes: &DMatrix<f64>,
) -> PolytopeVertex
{
    let mut indices = indices.clone();
    indices.0.sort();

    let (d1, _t1) = get_plane_params(indices.0[0], planes);
    let (d2, _t2) = get_plane_params(indices.0[1], planes);
    let (d3, _t3) = get_plane_params(indices.0[2], planes);

    // Small hack to find the correct orientation, improve if possible.
    let test_dir = (d1 + d2 + d3).normalize();
    let mut result = vec![indices.0[0], indices.0[1], indices.0[2]];

    for &pivot in indices.0.iter().skip(3)
    {
        let (pn, _t) = get_plane_params(pivot, planes);
        let mut insert_index = 0;
        for i in 0..result.len()
        {
            let ni = (i + 1) % result.len();
            insert_index = ni;

            let (d1, t1) = get_plane_params(result[i], planes);
            let (d2, t2) = get_plane_params(result[ni], planes);

            let plane1 = MultiVec3D::from_implicit_plane(
                d1.x as f32,
                d1.y as f32,
                d1.z as f32,
                t1 as f32,
            );
            let plane2 = MultiVec3D::from_implicit_plane(
                d2.x as f32,
                d2.y as f32,
                d2.z as f32,
                t2 as f32,
            );

            let intersection = plane1 ^ plane2;
            let line_dir: DVec3 = na::convert(intersection.get_line_dir().normalize());

            let orientation = test_dir.dot(&line_dir);
            let dir = -orientation.signum() * line_dir;

            let test = dir.dot(&pn);
            // Test the orientation, if it is positive then the current candidate cuts
            // through the polytope.
            if test >= 0.0
            {
                break;
            }
        }
        result.insert(insert_index, pivot);
    }

    PolytopeVertex(result)
}

pub(crate) fn get_plane_params(i: usize, planes: &DMatrix<f64>) -> (DVec3, f64)
{
    let r = planes.row(i);

    (r.fixed_columns::<3>(0).transpose(), r[(0, 3)])
}

// Get a point from 3 planes intersecting.
fn get_point(vertex: &PolytopeVertex, planes: &DMatrix<f64>) -> Option<DVec3>
{
    let (d1, t1) = get_plane_params(vertex.0[0], planes);
    let (d2, t2) = get_plane_params(vertex.0[1], planes);
    let (d3, t3) = get_plane_params(vertex.0[2], planes);

    // Plane normals should be unit length.
    debug_assert!((d1.norm_squared() - 1.0).abs() < EPSILON_DEBUG);
    debug_assert!((d2.norm_squared() - 1.0).abs() < EPSILON_DEBUG);
    debug_assert!((d3.norm_squared() - 1.0).abs() < EPSILON_DEBUG);

    let constraints = DVec3::new(t1, t2, t3);
    let coeffs = DMat3::from_columns(&[d1, d2, d3]).transpose();

    coeffs.lu().solve(&constraints)
}

use itertools::Itertools;
/// For testing purposes only.
pub(crate) fn _vertex_enumeration_from_half_spaces_brute_force(
    planes: &DMatrix<f64>,
) -> (Vec<DVec3>, Vec<Vec<usize>>)
{
    let mut vertices = Vec::with_capacity(planes.nrows());
    for (p1, p2, p3) in (0..planes.nrows()).tuple_combinations()
    {
        let (d1, t1) = get_plane_params(p1, planes);
        let (d2, t2) = get_plane_params(p2, planes);
        let (d3, t3) = get_plane_params(p3, planes);

        let constraints = DVec3::new(t1, t2, t3);
        let coeffs = DMat3::from_columns(&[d1, d2, d3]).transpose();
        let vertex = coeffs.lu().solve(&constraints);
        if vertex.is_none()
        {
            continue;
        }

        let vertex = vertex.unwrap();

        let is_inside = _is_inside_polytope(&vertex, planes);
        if is_inside
        {
            vertices.push(vertex);
        }
    }

    let fvertices = vertices
        .iter()
        .map(|v| Vec3::new(v[0] as f32, v[1] as f32, v[2] as f32))
        .collect_vec();

    let indices = convex_hull(&fvertices);

    (vertices, indices)
}

fn _is_inside_polytope(point: &DVec3, planes: &DMatrix<f64>) -> bool
{
    for r in 0..planes.nrows()
    {
        let (plane, t) = get_plane_params(r, planes);

        assert!((plane.norm() - 1.0).abs() < EPSILON_DEBUG);
        // The SDF for a plane defined with a unit direction and a distance is:
        // $P dot n < t$
        if point.dot(&plane) > t * 1.000
        {
            return false;
        }
    }

    true
}

fn _polytope_sdf(point: &DVec3, planes: &DMatrix<f64>) -> f64
{
    let mut val = f64::MIN;
    for r in 0..planes.nrows()
    {
        let (plane, t) = get_plane_params(r, planes);

        assert!((plane.norm() - 1.0).abs() < f64::EPSILON * 100.0);
        // The SDF for a plane defined with a unit direction and a distance is:
        // $P dot n - t$
        val = val.max(point.dot(&plane) - t);
    }

    val
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use algebra::*;
    use chicken_wire::wavefront_loader::ObjData;
    use na::DMatrix;

    use crate::vertex_enumeration_from_half_spaces;

    #[test]
    fn test_vertex_enumeration_from_half_spaces_degenerate()
    {
        #[rustfmt::skip]
        let planes = na::DMatrix::from_vec(
            4,
            5,
            vec![
                0., -0.8479983, 0.52999896, 0.12160137,
                0., -0.8479983, -0.52999896, 0.12160137,
                0., -1., 0., 0.,
                1., 0., 0., 0.8,
                -0.34776774, 0.9375807, -0., 1.5500646,
            ],
        )
        .transpose();

        let (vs_res, is_res) = vertex_enumeration_from_half_spaces(&planes);
        ObjData::export(&(&vs_res, &is_res), "tmp/degenerate_test_normal.obj");
    }

    #[test]
    fn test_vertex_enumeration_from_half_spaces_edge_case()
    {
        #[rustfmt::skip]
        let mat = DMatrix::from_vec(
            4,
            11,
            vec![
                -0.2840365469455719,   -0.9563649892807007,   -0.0684785395860672,      -4.3069167137146,
                -0.4269607663154602,   -0.9034146666526794,  0.039323657751083374,    -4.109272480010986,
                0.4125642478466034,    0.9070128798484802,  -0.08437052369117737,    4.1034626960754395,
                -0.28315502405166626,    0.8755069375038147,    0.3915492296218872,    3.8051040172576904,
                -0.958615243434906,   0.28470486402511597,                     0.,     0.425659716129303,
                -0.019496172666549683,  -0.06564456969499588,    0.9976526498794556,    0.5745928287506104,
                0.958615243434906,  -0.28470486402511597,                    -0.,  -0.39629310369491577,
                0.019496172666549683,   0.06564456969499588,   -0.9976526498794556,   -0.5452262163162231,
                0.28315502405166626,   -0.8755069375038147,   -0.3915492296218872,   -3.7757372856140137,
                0.9590741395950317,   0.25848281383514404,   0.11560016870498657,    1.9914697408676147,
                -0.9590741395950317,  -0.25848281383514404,  -0.11560016870498657,    -1.962103009223938]
        ).transpose();

        let center = DVec3::new(0.822608, 4.213279, 0.854532);
        let mut dbg = Vec::new();
        for i in 0..mat.nrows()
        {
            let dir = mat.row(i) * 0.1;
            for i in 0..100
            {
                let t = i as f64 / 99.;
                let pos = (1. - t) * dir.clone();
                dbg.push(
                    DVec3::new(convert(pos[0]), convert(pos[1]), convert(pos[2]))
                        + center,
                );
            }
        }

        let (verts, indices) = vertex_enumeration_from_half_spaces(&mat);

        use hedge;
        hedge::mesh::HalfMesh::new(verts, (), (), indices);
    }

    #[test]
    fn test_vertex_enumeration_from_half_spaces()
    {
        // let mut rng = ChaCha8Rng::seed_from_u64(0xdeadbeef);
        // let mut planes = DMatrix::from_vec(0, 4, vec![]);
        // for _ in 0..30
        // {
        //     let x: f64 = rng.gen_range(-1.0..1.0);
        //     let y: f64 = rng.gen_range(-1.0..1.0);
        //     let z: f64 = rng.gen_range(-1.0..1.0);

        //     let norm = (x * x + y * y + z * z).sqrt();

        //     let n = planes.nrows();
        //     planes = planes.insert_row(n, 0.0);
        //     planes.set_row(
        //         n,
        //         &DVec4::new(x / norm, y / norm, z / norm, 1.0).transpose(),
        //     );
        // }

        // let mut points = Vec::with_capacity(planes.len());
        // for pid in 0..planes.nrows()
        // {
        //     let plane = planes.row(pid);
        //     points.push(plane.fixed_columns::<3>(0) * plane[3]);
        // }

        // let (vs_res, is_res) = vertex_enumeration_from_half_spaces(&planes);

        // ObjData::export(&(&vs_res, &is_res), "tmp/vertex_enumeration.obj");
        // let cached = ObjData::from_disk_file(
        //     "test_data/vertex_enumeration/vertex_enumeration.obj",
        // );

        // for p in &vs_res
        // {
        //     let mut exists = false;
        //     for &v in &cached.vertices
        //     {
        //         exists = (v - p.cast()).norm() <= 1e-7 || exists;
        //     }
        //     assert!(exists);
        // }

        let icosahedron = vec![
            DVec3::new(0.934172, 0.356822, 0.000000),
            DVec3::new(0.934172, -0.356822, 0.000000),
            DVec3::new(-0.934172, 0.356822, 0.000000),
            DVec3::new(-0.934172, -0.356822, 0.000000),
            DVec3::new(0.000000, 0.934172, 0.356822),
            DVec3::new(0.000000, 0.934172, -0.356822),
            DVec3::new(0.356822, 0.000000, -0.934172),
            DVec3::new(-0.356822, 0.000000, -0.934172),
            DVec3::new(0.000000, -0.934172, -0.356822),
            DVec3::new(0.000000, -0.934172, 0.356822),
            DVec3::new(0.356822, 0.000000, 0.934172),
            DVec3::new(-0.356822, 0.000000, 0.934172),
            DVec3::new(0.577350, 0.577350, -0.577350),
            DVec3::new(0.577350, 0.577350, 0.577350),
            DVec3::new(-0.577350, 0.577350, -0.577350),
            DVec3::new(-0.577350, 0.577350, 0.577350),
            DVec3::new(0.577350, -0.577350, -0.577350),
            DVec3::new(0.577350, -0.577350, 0.577350),
            DVec3::new(-0.577350, -0.577350, -0.577350),
            DVec3::new(-0.577350, -0.577350, 0.577350),
        ];

        let mut planes = DMatrix::from_vec(0, 4, vec![]);

        for normal in icosahedron
        {
            let x: f64 = normal.x;
            let y: f64 = normal.y;
            let z: f64 = normal.z;

            let norm = (x * x + y * y + z * z).sqrt();

            let n = planes.nrows();
            planes = planes.insert_row(n, 0.0);
            planes.set_row(
                n,
                &DVec4::new(x / norm, y / norm, z / norm, 1.0).transpose(),
            );
        }

        let (vs_res, is_res) = vertex_enumeration_from_half_spaces(&planes);
        ObjData::export(&(&vs_res, &is_res), "tmp/convex_polytope_result_ico.obj");
    }
}
