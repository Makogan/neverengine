pub fn find_leaves(skeleton: &Vec<usize>) -> Vec<usize>
{
    let mut leaf_flag = vec![true; skeleton.len()];
    for parent_node in skeleton
    {
        leaf_flag[*parent_node] = false;
    }

    let mut leaves = Vec::new();
    for i in 0..leaf_flag.len()
    {
        if leaf_flag[i]
        {
            leaves.push(i);
        }
    }

    leaves
}

pub fn triangulation_reverse_winding_order(indices: &mut Vec<usize>)
{
    debug_assert!(indices.len() % 3 == 0);
    for i in (0..indices.len()).step_by(3)
    {
        indices.swap(i, i + 2);
    }
}

/// Adds the vertices of `vs2` onto `vs1` and then recomputes the indices in
/// `top2` to add them to `top1`. The result is the union of all polygons
/// defined by both meshes before concatenation.
pub fn concatenate_meshes<V>(
    mut vs1: Vec<V>,
    mut top1: Vec<Vec<usize>>,
    vs2: Vec<V>,
    mut top2: Vec<Vec<usize>>,
) -> (Vec<V>, Vec<Vec<usize>>)
{
    let n = vs1.len();
    vs1.extend(vs2);

    for face in &mut top2
    {
        for i in face
        {
            *i += n;
        }
    }

    top1.extend(top2);

    (vs1, top1)
}

/// Remap the topology of a triangle mesh into an equivalent one that can be
/// extended into polygons.
pub fn triangle_mesh_to_poly_mesh(topology: Vec<usize>) -> Vec<Vec<usize>>
{
    debug_assert!(topology.len() % 3 == 0);

    let mut result = Vec::new();
    for i in (0..topology.len()).step_by(3)
    {
        result.push(vec![topology[i + 0], topology[i + 1], topology[i + 2]]);
    }

    result
}

// Consider modifications based on:
// http://algorithmicbotany.org/papers/macmurchy.th2004.html
