use std::rc::Rc;
use std::{cell::RefCell, mem::size_of};

use algebra::*;
use camera::{CameraProperties, FlyCamera};
use chicken_wire::texture::Texture;
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use voctree::SimpleNodeData;
use vulkan_bindings::RenderContext;

#[inline]
pub fn bytes_of<T>(t: &T) -> &[u8] {
    unsafe { std::slice::from_raw_parts(t as *const T as *const u8, size_of::<T>()) }
}

#[allow(dead_code)]
#[repr(C)]
struct MVP {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

#[allow(dead_code)]
#[derive(Default, Copy, Clone)]
#[repr(C, align(16))]
struct VoxelVolumeMeta {
    box_corner: Vec4,
    box_extents: Vec4,
    voxel_size: f32,
}

#[allow(dead_code)]
#[derive(Default, Copy, Clone)]
#[repr(C, align(16))]
struct WindowData {
    dimensions: Vec4,
    view_matrix: Mat4,
}

const VOXEL_RESOLUTION: u32 = 128;

fn main() {
    let mut io_context = IoContext::new("Voxel Tracing", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let voxel_shader = render_context.add_shader(vec![
        "examples/09_voxel_tracing/voxelization_shader/".to_string(),
    ]);

    let tracing_shader = render_context.add_shader(vec![
        "examples/09_voxel_tracing/voxel_tracing_shader/".to_string(),
    ]);

    let (width, height) = io_context.window().get_window_size();
    let mut camera = FlyCamera::new(width, height, 0.0);
    camera.base_camera.position.y += 200.0;
    camera.base_camera.position.z -= 8000.;
    camera.base_camera.position.x += 0.;
    camera.theta = -270.;
    camera.speed = 10.;

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                FlyCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();

        io_context
            .window()
            .add_key_callback(Box::new(move |key_states| {
                if key_states[KbKey::Space.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        -1.0,
                        0.0,
                    );
                }
                if key_states[KbKey::LeftControl.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        1.0,
                        0.0,
                    );
                }
                if key_states[KbKey::W.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        0.0,
                        1.0,
                    );
                }
                if key_states[KbKey::S.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        0.0,
                        0.0,
                        -1.0,
                    );
                }
                if key_states[KbKey::A.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        1.0,
                        0.0,
                        0.0,
                    );
                }
                if key_states[KbKey::D.to_index()] == KeyActionState::Held {
                    FlyCamera::update_camera_position(
                        &mut camera.borrow_mut(),
                        -1.0,
                        0.0,
                        0.0,
                    );
                }
            }));
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                FlyCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let meshes =
        chicken_wire::load_static_meshes_from_path("./Assets/Sponza/glTF/Sponza.gltf");
    let textures = Texture::from_path("./Assets/Sponza/glTF/Sponza.gltf");

    let mut gpu_meshes = Vec::new();
    let mut mins = Vec3::repeat(f32::MAX);
    let mut maxs = Vec3::repeat(f32::MIN);

    for mesh in &meshes {
        let mut positions = Vec::new();
        let mut normals = Vec::new();
        let mut uvs = Vec::new();
        for face_ids in mesh.indices.chunks(3) {
            debug_assert!(face_ids.len() == 3);

            let p1 = mesh.positions[face_ids[0] as usize];
            let p2 = mesh.positions[face_ids[1] as usize];
            let p3 = mesh.positions[face_ids[2] as usize];

            mins = mins.inf(&p1);
            maxs = maxs.sup(&p1);

            mins = mins.inf(&p2);
            maxs = maxs.sup(&p2);

            mins = mins.inf(&p3);
            maxs = maxs.sup(&p3);

            positions.push(p1);
            positions.push(p2);
            positions.push(p3);

            let normal = (p2 - p1).cross(&(p3 - p1)).normalize();
            normals.push(normal);
            normals.push(normal);
            normals.push(normal);

            let uv1 = mesh.uvs[face_ids[0] as usize];
            let uv2 = mesh.uvs[face_ids[1] as usize];
            let uv3 = mesh.uvs[face_ids[2] as usize];

            uvs.push(uv1);
            uvs.push(uv2);
            uvs.push(uv3);
        }

        let input = GraphicsInput::new()
            .add_attribute_buffer_from_slice(&positions, &mut render_context)
            .add_attribute_buffer_from_slice(&normals, &mut render_context)
            .add_attribute_buffer_from_slice(&uvs, &mut render_context)
            .set_count(positions.len());

        gpu_meshes.push(input)
    }

    let aabb_extent = maxs - mins;
    let largest_dim = aabb_extent.max();

    let mut texture_handles = Vec::new();
    for texture in textures {
        let raw_image_data = RawImageData {
            width: texture.width as u32,
            height: texture.height as u32,
            depth: 0,
            format: texture.format,
            data: Some(texture.data.as_ptr() as *const u8),
        };
        let image_handle = render_context.allocate_image(&raw_image_data);

        texture_handles.push(image_handle);
    }

    let raw_image_data = RawImageData {
        width: VOXEL_RESOLUTION,
        height: VOXEL_RESOLUTION,
        depth: VOXEL_RESOLUTION,
        format: ImageFormat::RGBA8,
        data: None,
    };
    let voxel_volume_handle = render_context.allocate_image(&raw_image_data);

    let counter = [0_u32];
    let counter_buf = render_context.allocate_buffer(&counter);

    let maximum_nodes = VOXEL_RESOLUTION * VOXEL_RESOLUTION;
    let node_list =
        vec![SimpleNodeData::default(); (10. * maximum_nodes as f64) as usize];
    let voxel_buf = render_context.allocate_buffer(&node_list);

    let node_list = vec![Vec4::default(); node_list.len()];
    let corner_buf = render_context.allocate_buffer(&node_list);

    // We have to multiply by 2 to normalize to [-1, 1] instead of [0, 1].
    let trans = Mat4::new_translation(&-(mins * 2.0));
    let scale = Mat4::new_scaling(2. / largest_dim);

    let mut first = true;
    for mesh in &gpu_meshes {
        let voxel_request = RenderRequest::new(voxel_shader)
            .vertex_input(mesh.clone())
            .add_image(0, 0, voxel_volume_handle)
            .add_storage_buffer(4, 0, counter_buf)
            .add_storage_buffer(5, 0, voxel_buf)
            .add_storage_buffer(6, 0, corner_buf)
            .add_images_from_iterator(7, texture_handles.iter().copied())
            .add_ubo(8, (scale * trans).transpose())
            .render_target(RenderTargets::None(VOXEL_RESOLUTION, VOXEL_RESOLUTION))
            .should_clear(first)
            .build();

        first = false;

        render_context.draw(&voxel_request);
    }

    let read = render_context.read_memory(&counter_buf);
    let (_head, body, _tail) = unsafe { read.align_to::<u32>() };
    let value = &body[0];

    let mut read = render_context.read_memory(&voxel_buf);
    let nodes: Vec<SimpleNodeData> = unsafe {
        std::slice::from_raw_parts(read.as_mut_ptr() as *mut _, *value as usize).to_vec()
    };

    let mut read = render_context.read_memory(&corner_buf);
    let corners: Vec<Vec4> = unsafe {
        std::slice::from_raw_parts(read.as_mut_ptr() as *mut _, *value as usize).to_vec()
    };

    let corners: Vec<_> = corners.iter().map(|v| v.xyz()).collect();

    use voctree::CpuVoctree;
    let svo = CpuVoctree::<SimpleNodeData>::from_points(
        &corners,
        mins,
        largest_dim,
        7,
        *value,
        nodes,
    );

    render_context.update_buffer(
        &counter_buf,
        (&(svo.nodes().len() as u32)) as *const _ as *const u8,
        size_of::<u32>(),
    );

    let svo_buf = render_context.allocate_buffer(&svo.nodes());

    while io_context.poll_io(&mut None) {
        render_context.start_frame(&io_context);

        #[allow(unused)]
        let mvp = MVP {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };

        let tracing_request = RenderRequest::new(tracing_shader)
            .vertex_modifiers(GraphicsInputModifiers {
                element_count: 3,
                ..Default::default()
            })
            .add_image(0, 0, voxel_volume_handle)
            .add_storage_buffer(4, 0, counter_buf)
            .add_storage_buffer(5, 0, svo_buf)
            .add_ubo(
                1,
                WindowData {
                    dimensions: Vec4::new(800., 800., 0., 0.),
                    view_matrix: camera.borrow().view_matrix(),
                },
            )
            .add_ubo(2, svo.meta_data())
            .add_ubo(
                3,
                VoxelVolumeMeta {
                    box_corner: Vec4::new(mins.x, mins.y, mins.z, 1.0),
                    box_extents: Vec4::new(largest_dim, largest_dim, largest_dim, 1.0),
                    voxel_size: largest_dim / VOXEL_RESOLUTION as f32,
                },
            )
            .build();

        render_context.draw(&tracing_request);
    }
}
