use std::collections::BTreeMap;
use std::collections::HashMap;
use std::ops::{Add, Mul};

use crate::{
    face_handle::FaceHandle,
    facets::ABSENT,
    hedge_handle::HEdgeHandle,
    mesh::*,
    vert_handle::VertHandle,
};

/// Temporary wrapper to delete facets from a mesh.
/// The elements in the mesh (e.g. vertices) will be preserved even after
/// deleted while this object is alive. Upon calling `finish`, the mesh will be
/// reconstructed and all surviving elements will be in contiguous order again.
pub struct HalfMeshDeleter<'a, MeshType>
where
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    finished: bool,
    pub mesh: &'a mut MeshType,
}

impl<'a, MeshType> Drop for HalfMeshDeleter<'a, MeshType>
where
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    fn drop(&mut self)
    {
        assert!(
            self.finished,
            "`self.finish()` *must* be called before the object runs out of scope."
        );
    }
}

impl<'a, MeshType> HalfMeshDeleter<'a, MeshType>
where
    MeshType: HalfMeshLike
        + InternalMeshAccess<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>,
{
    pub fn new(mesh: &'a mut MeshType) -> Self
    {
        Self {
            finished: false,
            mesh,
        }
    }

    pub fn vert_handle(
        &self,
        id: GeomId<0>,
    ) -> VertHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>
    {
        self.mesh.vert_handle(id)
    }

    pub fn hedge_handle(
        &self,
        id: GeomId<1>,
    ) -> HEdgeHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>
    {
        self.mesh.hedge_handle(id)
    }

    pub fn face_handle(
        &self,
        id: GeomId<2>,
    ) -> FaceHandle<MeshType::VertData, MeshType::EdgeData, MeshType::FaceData>
    {
        self.mesh.face_handle(id)
    }

    pub fn is_vert_enabled(&self, id: GeomId<0>) -> bool
    {
        let mesh = unsafe { &*self.mesh.inner_mesh() };
        mesh.verts_meta.borrow()[id.0 as usize].enabled
    }

    pub fn is_edge_enabled(&self, id: GeomId<1>) -> bool
    {
        let mesh = unsafe { &*self.mesh.inner_mesh() };
        mesh.hedges_meta.borrow()[id.0 as usize].enabled
    }

    pub fn is_face_enabled(&self, id: GeomId<2>) -> bool
    {
        let mesh = unsafe { &*self.mesh.inner_mesh() };
        mesh.faces_meta.borrow()[id.0 as usize].enabled
    }

    pub fn delete_vert(&mut self, delete_id: GeomId<0>)
    {
        let mesh = unsafe { &mut *self.mesh.inner_mesh() };
        mesh.disable_vertex(delete_id);
        mesh.verts_meta.borrow_mut()[delete_id.0 as usize].enabled = false;
    }

    pub fn delete_edge(&mut self, delete_id: GeomId<1>)
    {
        let mesh = unsafe { &mut *self.mesh.inner_mesh() };
        mesh.disable_edge(delete_id);
    }

    pub fn delete_face(&mut self, delete_id: GeomId<2>)
    {
        let mesh = unsafe { &mut *self.mesh.inner_mesh() };
        mesh.disable_face(delete_id);
    }

    pub fn collapse<S, T>(&mut self, id: GeomId<1>) -> GeomId<0>
    where
        MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
        T: Add<Output = T> + Mul<S, Output = T>,
        S: num::Float,
    {
        self.hedge_handle(id).collapse()
    }

    /// Finish deleting and compact the mesh data into continuous ranges.
    pub fn finish(mut self) -> BTreeMap<usize, usize>
    {
        let mesh = unsafe { &mut *self.mesh.inner_mesh() };

        // Compute the permutation map of where new vertices will end up.
        let mut count = 0;
        let mut vertex_data = Vec::with_capacity(self.mesh.vert_count());
        let mut vertex_permutation = BTreeMap::new();
        for vert in mesh
            .verts_meta
            .borrow_mut()
            .iter_mut()
            .filter(|v| v.enabled)
        {
            if !mesh.hedges_meta.borrow()[vert.hedge_id.0 as usize].enabled
            {
                vert.enabled = false;
                continue;
            }
            vertex_data.push(mesh.verts.borrow().get(vert.id.0).clone());
            vertex_permutation.insert(vert.id.0 as usize, count);
            count += 1;
        }

        let mut count = 0;
        let mut hedge_permutation = HashMap::<usize, usize>::new();
        for hedge in mesh.hedges_meta.borrow().iter().filter(|e| e.enabled)
        {
            hedge_permutation.insert(hedge.id.0 as usize, count);
            count += 1;
        }

        let mut count = 0;
        let mut face_permutation = HashMap::<usize, usize>::new();
        for face in mesh.faces_meta.borrow().iter().filter(|f| f.enabled)
        {
            face_permutation.insert(face.id.0 as usize, count);
            count += 1;
        }

        mesh.verts_meta
            .borrow_mut()
            .iter_mut()
            .filter(|vert| vert.enabled)
            .for_each(|vert| {
                vert.hedge_id = GeomId::from(
                    *hedge_permutation.get(&(vert.hedge_id.0 as usize)).unwrap(),
                );

                vert.id =
                    GeomId::from(*vertex_permutation.get(&(vert.id.0 as usize)).unwrap())
            });

        mesh.hedges_meta
            .borrow_mut()
            .iter_mut()
            .filter(|hedge| hedge.enabled)
            .for_each(|hedge| {
                hedge.vert_id = GeomId::from(
                    *vertex_permutation.get(&(hedge.vert_id.0 as usize)).unwrap(),
                );

                hedge.next_id = GeomId::from(
                    *hedge_permutation.get(&(hedge.next_id.0 as usize)).unwrap(),
                );

                hedge.prev_id = GeomId::from(
                    *hedge_permutation.get(&(hedge.prev_id.0 as usize)).expect(
                        format!("Hedge id {} does not exist.", hedge.prev_id.0).as_str(),
                    ),
                );

                hedge.pair_id = GeomId::from(
                    *hedge_permutation.get(&(hedge.pair_id.0 as usize)).unwrap(),
                );

                if hedge.face_id != ABSENT()
                {
                    hedge.face_id = GeomId::from(
                        *face_permutation.get(&(hedge.face_id.0 as usize)).unwrap(),
                    );
                }

                hedge.id =
                    GeomId::from(*hedge_permutation.get(&(hedge.id.0 as usize)).unwrap());
            });

        mesh.faces_meta
            .borrow_mut()
            .iter_mut()
            .filter(|face| face.enabled)
            .for_each(|face| {
                face.hedge_id = GeomId::from(
                    *hedge_permutation.get(&(face.hedge_id.0 as usize)).unwrap(),
                );

                face.id =
                    GeomId::from(*face_permutation.get(&(face.id.0 as usize)).unwrap());
            });

        let mut counter = 0;
        mesh.verts.borrow_mut().retain(|_| {
            let enabled = mesh.verts_meta.borrow()[counter].enabled;
            counter += 1;
            enabled
        });

        let mut counter = 0;
        mesh.edges.borrow_mut().retain(|_| {
            let enabled = mesh.hedges_meta.borrow()[counter].enabled;
            counter += 1;
            enabled
        });

        let mut counter = 0;
        mesh.faces.borrow_mut().retain(|_| {
            let enabled = mesh.faces_meta.borrow()[counter].enabled;
            counter += 1;
            enabled
        });

        mesh.verts_meta.borrow_mut().retain(|vert| vert.enabled);
        mesh.hedges_meta.borrow_mut().retain(|hedge| hedge.enabled);
        mesh.faces_meta.borrow_mut().retain(|faces| faces.enabled);

        debug_assert!(verify_topology(mesh).is_none());

        debug_assert!(verify_topology(mesh).is_none());

        self.finished = true;

        vertex_permutation
    }
}
