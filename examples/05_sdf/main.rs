use algebra::*;
use dem_core::{gpu_api::*, rendering::*};
use dem_peripherals::*;

use numerical_analysis::fast_sweeping::fast_sweeping_parametric;
use vulkan_bindings::RenderContext;

fn main() {
    let mut io_context = IoContext::new("SDF", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let sdf_shader =
        render_context.add_shader(vec!["examples/05_sdf/sdf_shader/".to_string()]);

    use std::f32::consts::PI;
    let grid = fast_sweeping_parametric::<2>(
        &Vec2::new(-1.0, -1.0),
        &Vec2::new(2.0, 2.0),
        0.02,
        &|_| 1.0,
        &|t: f32| Vec2::new(0.5 * (t.cos() - 1.0 / (1.0 + 4.0 * t * t)), 0.5 * t.sin()),
        (-PI, PI),
        100,
    );

    // From the above.
    let dimensions = (100, 100);
    let raw_image_data = RawImageData {
        width: dimensions.0 as u32,
        height: dimensions.1 as u32,
        depth: 0,
        format: ImageFormat::R32,
        data: Some(grid.as_ptr() as *const u8),
    };
    let image_handle = render_context.allocate_image(&raw_image_data);

    let render_request = RenderRequest::new(sdf_shader)
        .vertex_modifiers(GraphicsInputModifiers {
            element_count: 6,
            ..Default::default()
        })
        .add_image(1, 0, image_handle)
        .build();

    while io_context.poll_io(&mut None) {
        render_context.start_frame(&io_context);
        render_context.draw(&render_request);
    }
}
