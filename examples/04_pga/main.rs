use std::rc::Rc;
use std::sync::{Arc, RwLock};
use std::{cell::RefCell, mem::size_of};

use algebra::*;
use camera::{ArcballCamera, CameraProperties};
use chicken_wire::{texture::Texture, AnimatedDataAssociation};
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use pga::*;
use vulkan_bindings::RenderContext;

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct MVP {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn main() {
    let mut io_context = IoContext::new("PGA", (800, 800), true);
    let mut render_context =
        RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let line_shader =
        render_context.add_shader(vec!["examples/04_pga/line_shader/".to_string()]);

    let pga_skinning_shader =
        render_context.add_shader(vec!["examples/04_pga/skin_shader/".to_string()]);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    let animation_index = Arc::new(RwLock::new(0usize));
    {
        let anim_index = animation_index.clone();
        io_context
            .window()
            .add_key_callback(Box::new(move |key_states| {
                if key_states[KbKey::Space.to_index()] == KeyActionState::Press {
                    let mut index = anim_index.write().unwrap();
                    *index += 1;
                }
            }));
    }

    let (meshes, skins, skeletons, animations, associations) =
        chicken_wire::load_skinned_data_from_path("./Assets/default_textured.gltf");
    let textures = Texture::from_path("./Assets/default_textured.gltf");

    let mut gpu_meshes = Vec::new();
    for i in 0..meshes.len() {
        let gpu_mesh = GraphicsInput::new()
            .add_attribute_buffer_from_slice(&meshes[i].positions, &mut render_context)
            .add_attribute_buffer_from_slice(&meshes[i].normals, &mut render_context)
            .add_attribute_buffer_from_slice(&meshes[i].uvs, &mut render_context)
            .add_attribute_buffer_from_slice(&skins[i].weights, &mut render_context)
            .add_attribute_buffer_from_slice(&skins[i].joints, &mut render_context)
            .set_index_buffer_from_slice(&meshes[i].indices, &mut render_context);

        gpu_meshes.push(gpu_mesh);
    }

    let mut gpu_textures = Vec::new();
    for texture in textures {
        let raw_image_data = RawImageData {
            width: texture.width as u32,
            height: texture.height as u32,
            depth: 0,
            format: texture.format,
            data: Some(texture.data.as_ptr() as *const u8),
        };
        let image_handle = render_context.allocate_image(&raw_image_data);
        gpu_textures.push(image_handle);
    }

    let mut ibms = Vec::new();
    for skeleton in &skeletons {
        let mut points = Vec::new();
        let mut ibms_l = Vec::new();
        for mv in &skeleton.inverse_poses {
            let tmp = Motor::from_multi_vec(&mv.reverse());
            let p = tmp.sandwich(&Vec3::new(0.0, 0.0, 0.0));

            points.push(p);
            ibms_l.push(Motor::from_multi_vec(mv));
        }

        let ibm_ssbo = render_context.allocate_buffer(&ibms_l);
        ibms.push(ibm_ssbo);
    }

    let loc_anims = animations[0].sample(0.0, &skeletons[0], 0);
    let anim_ssbo = render_context.allocate_buffer(&loc_anims);

    use chicken_wire::skeleton_to_graph;
    let (skeleton_points, skeleton) = skeleton_to_graph(&skeletons[0]);
    let skeleton: Vec<u32> = skeleton
        .iter()
        .flat_map(|&[i, j]| vec![i as u32, j as u32])
        .collect();

    let mut time = 0.0;
    while io_context.poll_io(&mut None) {
        render_context.start_frame(&io_context);

        time += 0.0005;

        let mvp = MVP {
            model: Mat4::identity(),
            view: camera.borrow().view_matrix(),
            proj: camera.borrow().proj_matrix(),
        };

        let mut first = true;
        for &AnimatedDataAssociation {
            mesh_id,
            animation_set_id,
            skin_id: _,
            skeleton_id,
        } in &associations
        {
            let index: usize = *animation_index.read().unwrap();
            let loc_anims =
                animations[animation_set_id].sample(time, &skeletons[skeleton_id], index);

            render_context.update_buffer(
                &anim_ssbo,
                loc_anims.as_ptr() as _,
                loc_anims.len() * size_of::<Motor>(),
            );

            let render_request = RenderRequest::new(pga_skinning_shader)
                .vertex_input(gpu_meshes[mesh_id].clone())
                .add_storage_buffer(3, 0, ibms[skeleton_id])
                .add_storage_buffer(4, 0, anim_ssbo)
                .should_clear(first)
                .add_images_from_iterator(1, gpu_textures.iter().copied())
                .add_ubo(0, mvp)
                .build();

            render_context.draw(&render_request);

            first = false;
        }

        let gpu_mesh = GraphicsInput::new()
            .add_attribute_buffer_from_slice(&skeleton_points, &mut render_context)
            .set_index_buffer_from_slice(&skeleton, &mut render_context);

        let render_request = RenderRequest::new(line_shader)
            .vertex_input(gpu_mesh.clone())
            .add_storage_buffer(3, 0, ibms[0])
            .add_storage_buffer(4, 0, anim_ssbo)
            .should_clear(false)
            .add_ubo(0, mvp)
            .build();

        render_context.draw(&render_request);
        render_context.free_input(gpu_mesh);
    }
}
