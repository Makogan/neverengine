use convert_case::{Case, Casing};
use proc_macro::TokenStream;
use quote::quote;
use syn::__private::TokenStream2;
use syn::{parse_macro_input, Data, DataStruct, DeriveInput, Field, Fields};

fn to_plural(word: &String) -> String
{
    match word.as_str()
    {
        "Mesh" | "mesh" =>
        {
            return "es".to_string();
        }
        _ =>
        {
            return "s".to_string();
        }
    }
}

fn field_from_json(field: &Field) -> TokenStream2
{
    let field_name = field.clone().ident.unwrap();

    let name_as_string = format!("{}", field_name);

    let mut camel_name = name_as_string.to_string().to_case(Case::Camel);
    if camel_name == "kind"
    {
        camel_name = "type".to_string();
    }

    let field_ty = field.clone().ty;
    quote! {
        #field_name: <#field_ty>::from_json(
            &#camel_name.to_string(),
            &json[&#camel_name.to_string()]),
    }
}

#[proc_macro_derive(GltfReflect)]
pub fn reflect(input: TokenStream) -> TokenStream
{
    let input = parse_macro_input!(input as DeriveInput);

    let st_ident = input.ident;
    let st_name = st_ident.to_string();
    let st_vis = input.vis;
    let st_fields = match input.data
    {
        Data::Struct(DataStruct {
            fields: Fields::Named(fields),
            ..
        }) => fields.named,
        _ => panic!("This derive macro only works on structs with named fields."),
    };
    let plural_suffix = to_plural(&st_ident.to_string().to_case(Case::Snake));
    let plural_ident = syn::Ident::new(
        format!(
            "{}{}",
            st_ident.to_string().to_case(Case::Snake),
            plural_suffix
        )
        .as_str(),
        st_ident.span(),
    );
    let mut plural_name = plural_ident.to_string();
    if plural_name == "texture_samplers"
    {
        plural_name = "samplers".to_string();
    }
    let parse_ident =
        syn::Ident::new(format!("parse_{}", plural_ident).as_str(), st_ident.span());

    let json_to_fields = st_fields.clone().into_iter().map(|f| field_from_json(&f));

    let plural_name = plural_name.to_case(Case::Camel);
    let output = quote! {
        impl #st_ident
        {
            #st_vis fn #parse_ident(json : &serde_json::Value) -> Vec<#st_ident>
            {
                let mut token_name = #st_name.to_string();

                let mut result = Vec::new();

                for object in json[&#plural_name].as_array().unwrap()
                {
                    result.push(#st_ident::from_json(&token_name, object));
                }

                result
            }
        }

        impl FromJson<#st_ident> for #st_ident
        {
            fn from_json(name : &String, json : &serde_json::Value)->#st_ident
            {
                let new = #st_ident{
                    #(#json_to_fields)*
                };

                return new;
            }
        }
    };

    output.into()
}
