use std::{fmt::Debug, ops::Mul};

use linear_isomorphic::*;

use crate::{find_planar_orthonormal_basis, traits::cross};

pub fn point_segment_distance<Vec, S>(start: &Vec, end: &Vec, point: &Vec) -> S
where
    Vec: InnerSpace<S>,
    S: RealField,
{
    let dir = end.clone() - start.clone();
    let t = (point.clone() - start.clone()).dot(&dir) / dir.norm_squared();

    let t = S::clamp(&t, S::from(0.0).unwrap(), S::from(1.0).unwrap());

    let closest = start.clone() + dir * t;

    (closest - point.clone()).norm()
}

pub fn project_point_onto_segment<Vec, S>(start: &Vec, end: &Vec, point: &Vec) -> Vec
where
    Vec: InnerSpace<S>,
    S: RealField,
{
    let dir = end.clone() - start.clone();

    let denom = dir.norm_squared();
    if denom <= S::epsilon() * S::from(100.).unwrap() {
        return start.clone();
    }

    let t = (point.clone() - start.clone()).dot(&dir) / denom;

    let t = t.clamp(S::from(0.0).unwrap(), S::from(1.0).unwrap());

    let closest = start.clone() + dir * t;

    closest
}

pub fn point_line_distance<Vec, S>(start: &Vec, dir: &Vec, point: &Vec) -> S
where
    Vec: InnerSpace<S>,
    S: RealField,
{
    (point.clone() - project_onto_line(start, dir, point)).norm()
}

pub fn line_line_intersection<Vec>(
    origin1: &Vec,
    ray1: &Vec,
    origin2: &Vec,
    ray2: &Vec,
) -> f32
where
    Vec: InnerSpace<f32> + Debug,
{
    let epsilon = 1e-3;
    if ((origin1.clone() - origin2.clone()).dot(&(origin1.clone() - origin2.clone()))
        - 1.0)
        .abs()
        < epsilon
    {
        return 0.0;
    };

    let n1 = cross(&(origin2.clone() - origin1.clone()), ray2);
    let n2 = cross(ray1, ray2);

    // Use this to test whether the vectors point in the same or opposite
    // directions.
    let n = n2.normalized();
    // If n2 is the 0 vector or if the cross products are not colinear, no solution
    // exists.
    if !n[0].is_finite() || (n1.dot(&n).abs() - n1.norm()).abs() > epsilon {
        return f32::INFINITY;
    }

    n1.dot(&n) / n2.dot(&n)
}

pub fn segment_segment_distance<Vec>(p1: &Vec, p2: &Vec, p3: &Vec, p4: &Vec) -> f32
where
    Vec: InnerSpace<f32> + Debug,
{
    let (p, q) = segment_segment_shortest_points(p1, p2, p3, p4);

    (p - q).norm()
}
pub fn segment_segment_shortest_points<Vec>(
    p1: &Vec,
    p2: &Vec,
    p3: &Vec,
    p4: &Vec,
) -> (Vec, Vec)
where
    Vec: InnerSpace<f32> + Debug,
{
    let q1 = p1;
    let q2 = p3;
    let v1 = p2.clone() - p1.clone();
    let v2 = p4.clone() - p3.clone();
    let dq = q2.clone() - q1.clone();

    let v22 = v2.dot(&v2);
    let v11 = v1.dot(&v1);
    let v21 = v2.dot(&v1);
    let v21_1 = dq.dot(&v1);
    let v21_2 = dq.dot(&v2);
    let denom = v21 * v21 - v22 * v11;

    let mut s;
    let mut t;
    if denom.abs() < f32::EPSILON * 100.0 {
        s = 0.;
        t = (v11 * s - v21_1) / v21;
    } else {
        s = (v21_2 * v21 - v22 * v21_1) / denom;
        t = (-v21_1 * v21 + v11 * v21_2) / denom;
    }

    s = s.min(1.).max(0.);
    t = t.min(1.).max(0.);

    let p_a = q1.clone() + v1 * s;
    let p_b = q2.clone() + v2 * t;

    return (p_a, p_b);
}
/// Find the center of the osculating circle of 3 unaligned points. returns the
/// center of the osculating circle.
///
/// (The radius can be trivially computed from the center and one of the
/// points).
///
/// #Example:
///
/// ```
/// // (Find osculating circle of basis vectors)
/// use algebra::Vec3;
/// use geometry::osculating_circle;
/// let p1 = Vec3::new(1.0, 0.0, 0.0);
/// let p2 = Vec3::new(0.0, 1.0, 0.0);
/// let p3 = Vec3::new(0.0, 0.0, 1.0);
///
/// let center = osculating_circle(&p1, &p2, &p3);
/// // EXPECT_NEAR(0.3333333, center.x(), 0.00001);
/// // EXPECT_NEAR(0.3333333, center.y(), 0.00001);
/// // EXPECT_NEAR(0.3333333, center.z(), 0.00001);
/// ```
pub fn osculating_circle<Vec>(p1: &Vec, p2: &Vec, p3: &Vec) -> Vec
where
    Vec: InnerSpace<f32> + Debug,
{
    let d1 = p2.clone() - p1.clone();
    let d2 = p3.clone() - p1.clone();

    let m1 = (p1.clone() + p2.clone()) * (1.0 / 2.0);
    let m2 = (p1.clone() + p3.clone()) * (1.0 / 2.0);
    let normal = cross(&d1, &d2).normalized();

    let o1 = cross(&normal, &d1);
    let o2 = cross(&normal, &d2);

    let t = line_line_intersection(&m1, &o1, &m2, &o2);
    debug_assert!(t.is_finite());

    m1 + o1 * t
}

/// Computes the intersection of a line and a segment.
pub fn line_segment_intersection<Vec>(
    l0: &Vec,
    l1: &Vec,
    s0: &Vec,
    s1: &Vec,
) -> (bool, Vec)
where
    Vec: InnerSpace<f32>,
    Vec: Default,
    Vec: Debug,
{
    segment_segment_intersection_tolerance(
        l0,
        l1,
        s0,
        s1,
        -f32::MAX,
        -(s0.clone() - s1.clone()).norm() * 0.01,
    )
}

// Page 304 of geometry gems "Intersection of two lines in 3 space":
// https://theswissbay.ch/pdf/Gentoomen%20Library/Game%20Development/Programming/Graphics%20Gems%201.pdf
// Also:
// https://stackoverflow.com/questions/34602761/intersecting-3d-line-segments

/// Computes the intersection of 2 segments.
pub fn segment_segment_intersection<Vec>(
    p0: &Vec,
    p1: &Vec,
    q0: &Vec,
    q1: &Vec,
) -> (bool, Vec)
where
    Vec: InnerSpace<f32>,
    Vec: Default,
    Vec: Debug,
{
    segment_segment_intersection_tolerance(p0, p1, q0, q1, 0.0, 0.0)
}

/// Like `segment_segment_intersection` but it allows to specify a tolerance
/// value for the end points. i.e. if the intersection occurs near either
/// endpoint, such as in the vertex joining 2 sides of a triangle, it won't be
/// considered an intersection. Use positive epsilons to make the test stricter
/// and negative to make it more permissive.
pub fn segment_segment_intersection_tolerance<Vec>(
    p0: &Vec,
    p1: &Vec,
    q0: &Vec,
    q1: &Vec,
    epsilon1: f32,
    epsilon2: f32,
) -> (bool, Vec)
where
    Vec: InnerSpace<f32>,
    Vec: Default,
    Vec: Debug,
{
    let dp = p1.clone() - p0.clone();
    let dq = q1.clone() - q0.clone();
    let pq = q0.clone() - p0.clone();

    let a = dp.dot(&dp);
    let b = dp.dot(&dq);
    let c = dq.dot(&dq);
    let d = dp.dot(&pq);
    let e = dq.dot(&pq);

    let dd = -(a * c - b * b);
    let cos_angle = dp.normalized().dot(&dq.normalized());
    // Lines are parallel, so we need to take care of that.
    if dd.abs() < f32::EPSILON * 10.0 && cos_angle > 0.99 {
        // If this is true, lines do not overlap.
        if (d - dp.norm() * pq.norm()) >= f32::EPSILON * 100.0 {
            return (false, p0.clone() * f32::INFINITY);
        }

        let distance = |a: &Vec| (a.clone() - p0.clone()).dot(&dp);

        let dp0 = distance(p0);
        let dp1 = distance(p1);
        let dq0 = distance(q0);
        let dq1 = distance(q1);

        if dp0 >= dq0 + epsilon2 && dp0 <= dq1 - epsilon2 {
            return (true, p0.clone());
        }

        if dp1 >= dq0 + epsilon2 && dp1 <= dq1 - epsilon2 {
            return (true, p0.clone());
        }

        return (false, p0.clone());
    }

    let t = (b * e - c * d) / dd;
    let s = (a * e - b * d) / dd;

    let pi = p0.clone() + dp.clone() * t;
    let qi = q0.clone() + dq.clone() * s;

    // If this fails then the points are skew. Use the norm of one direction as the
    // epsilon.
    let skewness_test = (pi.clone() - qi.clone()).norm() <= dp.norm() * 0.01;

    let validity_test = (0.0 + epsilon1..=1.0 - epsilon1).contains(&t)
        && (0.0 + epsilon2..=1.0 - epsilon2).contains(&s);

    (validity_test && skewness_test, pi)
}

pub fn project_onto_plane<Vec>(
    plane_point: &Vec,
    plane_normalized_normal: &Vec,
    input_point: &Vec,
) -> Vec
where
    Vec: InnerSpace<f32>,
    Vec: Default,
    Vec: Debug,
{
    let d1 = input_point.clone() - plane_point.clone();
    let proj = plane_normalized_normal.clone() * d1.dot(&plane_normalized_normal);

    input_point.clone() - proj.clone()
}

pub fn project_onto_line<Vec, S>(source: &Vec, dir: &Vec, input_point: &Vec) -> Vec
where
    Vec: InnerSpace<S>,
    Vec: Default,
    S: RealField,
{
    dir.normalized().clone()
        * (input_point.clone() - source.clone()).dot(&dir.normalized())
        + source.clone()
}

// https://stackoverflow.com/a/38245767/6202327
pub fn interior_polygon_point<Vec>(polygon: &[Vec]) -> Vec
where
    Vec: InnerSpace<f32>,
    Vec: Default,
    Vec: Debug,
    f32: Mul<Vec, Output = Vec>,
{
    let (center, _x_axis, y_axis) =
        find_planar_orthonormal_basis(&|i| polygon[i].clone(), polygon.len());

    let start = (polygon[0].clone() + polygon[1].clone()) * 0.5 - y_axis.clone() * 2.0;
    let mut intersections = vec![];
    for i in 0..polygon.len() {
        let p1 = polygon[i].clone();
        let p2 = polygon[(i + 1) % polygon.len()].clone();

        let (test, intersection) = line_segment_intersection(
            &start,
            &(start.clone() + y_axis.normalized()),
            &p1,
            &p2,
        );
        if test {
            intersections.push((test, intersection));
        }
    }

    intersections.sort_by(|a, b| {
        let x_1 = (a.1.clone() - center.clone()).dot(&y_axis);
        let x_2 = (b.1.clone() - center.clone()).dot(&y_axis);

        x_1.partial_cmp(&x_2).unwrap()
    });

    (intersections[0].1.clone() + intersections[1].1.clone()) * 0.5
}

pub fn project_onto_open_poly_line<Vec, S>(p: &Vec, polyline: &[Vec]) -> (Vec, S, usize)
where
    Vec: InnerSpace<S>,
    S: RealField + std::iter::Sum,
{
    let arclength: S = polyline
        .windows(2)
        .map(|vs| (vs[0].clone() - vs[1].clone()).norm())
        .sum();

    let mut closest_segment = 0;
    let mut closest_projection = polyline[0].clone();
    let mut cum_length = S::from(0.).unwrap();
    let mut best_length = S::from(0.).unwrap();
    let mut best_distance = S::from(f32::MAX).unwrap();
    for i in 0..polyline.len() - 1 {
        let v1 = &polyline[i];
        let v2 = &polyline[i + 1];

        let proj = project_point_onto_segment(v1, v2, p);

        let d = (p.clone() - proj.clone()).norm();
        if d < best_distance {
            best_distance = d;
            closest_segment = i;
            closest_projection = proj.clone();
            best_length = cum_length + (v1.clone() - proj.clone()).norm();
        }

        cum_length += (v1.clone() - v2.clone()).norm();
    }

    (closest_projection, best_length / arclength, closest_segment)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::segment_segment_intersection;
    use algebra::*;
    use core::f32::consts::PI;

    #[test]
    fn test_project_onto_open_poly_line() {
        let curve: Vec<_> = (0..50)
            .map(|i| {
                let t = i as f32 / 49.;
                let t = t * PI;

                Vec2::new(t.cos(), t.sin())
            })
            .collect();

        let point = Vec2::new(0., 10.);
        let (proj, param, interval) = project_onto_open_poly_line(&point, &curve);

        assert!((proj - Vec2::new(0., 1.0)).norm() < 0.001,);
        assert!((param - 0.5).abs() < 0.001);
        assert!(interval == 24);
    }

    #[test]
    fn test_segment_segment_intersection_3d() {
        let p1 = Vec3::new(-1.0, 0.0, 0.0);
        let p2 = Vec3::new(1.0, 0.0, 0.0);
        let p3 = Vec3::new(0.0, -1.0, 0.0);
        let p4 = Vec3::new(0.0, 1.0, 0.0);

        let (test, point) = segment_segment_intersection(&p1, &p2, &p3, &p4);

        assert!(test);
        assert!(point.norm() <= f32::EPSILON * 10.0);

        let p1 = Vec3::new(-1.0, 0.0, 0.0);
        let p2 = Vec3::new(1.0, 0.0, 0.0);
        let p3 = Vec3::new(0.5, -1.0, 0.0);
        let p4 = Vec3::new(0.5, 1.0, 0.0);

        let (test, point) = segment_segment_intersection(&p1, &p2, &p3, &p4);

        assert!(test);
        assert!((point - Vec3::new(0.5, 0.0, 0.0)).norm() <= f32::EPSILON * 10.0);

        let p1 = Vec3::new(-1.0, 0.5, 0.0);
        let p2 = Vec3::new(1.0, 0.5, 0.0);
        let p3 = Vec3::new(0.5, -1.0, 0.0);
        let p4 = Vec3::new(0.5, 1.0, 0.0);

        let (test, point) = segment_segment_intersection(&p1, &p2, &p3, &p4);

        assert!(test);
        assert!((point - Vec3::new(0.5, 0.5, 0.0)).norm() <= f32::EPSILON * 10.0);

        let p1 = Vec3::new(-1.0, -1.0, 0.0);
        let p2 = Vec3::new(1.0, 1.0, 0.0);
        let p3 = Vec3::new(1.0, -1.0, 0.0);
        let p4 = Vec3::new(-1.0, 1.0, 0.0);

        let (test, point) = segment_segment_intersection(&p1, &p2, &p3, &p4);

        assert!(test);
        assert!((point - Vec3::new(0.0, 0.0, 0.0)).norm() <= f32::EPSILON * 10.0);
    }
}
