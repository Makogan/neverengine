use std::vec;

use algebra::*;

// TODO(low): Clean these imports.
pub mod b_spline;

pub use b_spline::*;

pub mod traits;

pub use traits::*;

pub mod parametric_surfaces;

pub use parametric_surfaces::*;

pub mod differential_geometry;

pub use differential_geometry::*;

pub mod rotation_minimizing_frame;

pub use rotation_minimizing_frame::*;

pub mod triangulation;

pub use triangulation::*;

pub mod convex_hull;

pub use convex_hull::*;

pub mod vertex_enumeration;

pub use vertex_enumeration::*;

pub mod planar_bounding_primitives;

pub use planar_bounding_primitives::*;

pub mod euclidean;

pub use euclidean::*;

pub mod utils;

pub use utils::*;

pub mod convex_polytope;

pub use convex_polytope::*;

pub mod skeleton_meshing;

pub use skeleton_meshing::*;

pub mod geometry_cleaning;

pub use geometry_cleaning::*;

pub mod interpolation;

pub use interpolation::*;

pub mod sdf;

pub use sdf::*;

/// Compute the *per vertex* normals of a *triangle* mesh.
pub fn compute_normals(points: &Vec<Vec3>, faces: &Vec<usize>) -> Vec<Vec3> {
    assert!(faces.len() % 3 == 0);
    let mut normals = vec![Vec3::zeros(); points.len()];

    for i in 0..faces.len() / 3 {
        let p1 = points[faces[i * 3 + 0]];
        let p2 = points[faces[i * 3 + 1]];
        let p3 = points[faces[i * 3 + 2]];

        let n = (p2 - p1).cross(&(p3 - p1));

        normals[faces[i * 3 + 0]] += n;
        normals[faces[i * 3 + 1]] += n;
        normals[faces[i * 3 + 2]] += n;
    }

    for normal in &mut normals {
        *normal = normal.normalize();
    }

    normals
}
