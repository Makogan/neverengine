use std::ops::{Add, Div, Mul, Sub};

use algebra::{na::Const, *};

/// Compute a first order accurate numeric gradient from a function.
///
/// `x` is the N dimensional point at which the gradient is evaluated.
/// `f` : R^N-> R is the function whose gradient is being computed.
/// `epsilon` is the coordinate wise spacing, usually named `h` in most sources.
pub fn gradient<T, const N: usize>(
    x: &na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>,
    f: &dyn Fn(&na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>) -> T,
    epsilon: T,
) -> na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>
where
    T: Scalar
        + Add<Output = T>
        + Sub<Output = T>
        + Div<Output = T>
        + Mul<Output = T>
        + Copy,
    na::ArrayStorage<T, N, 1>: Default,
{
    let mut grad =
        na::Matrix::<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>::default();
    for i in 0..N
    {
        let mut nx = x.clone();
        nx[i] = nx[i] + epsilon;
        let mut px = x.clone();
        px[i] = px[i] - epsilon;
        grad[i] = (f(&nx) - f(&px)) / (epsilon + epsilon);
    }

    grad
}

/// Compute a first order accurate numeric divergence from a function.
///
/// `x` is the N dimensional point at which the divergence is evaluated.
/// `f` : R^N-> R is the function whose divergence is being computed.
/// `epsilon` is the coordinate wise spacing, usually named `h` in most sources.
pub fn divergence<T, const N: usize>(
    x: &na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>,
    f: &dyn Fn(
        &na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>,
    ) -> na::Matrix<T, Const<N>, Const<1>, na::ArrayStorage<T, N, 1>>,
    epsilon: T,
) -> T
where
    T: Scalar
        + Add<Output = T>
        + Sub<Output = T>
        + Div<Output = T>
        + Mul<Output = T>
        + Copy
        + Default,
    na::ArrayStorage<T, N, 1>: Default,
{
    let mut div = T::default();
    for i in 0..N
    {
        let mut nx = x.clone();
        nx[i] = nx[i] + epsilon;
        let mut px = x.clone();
        px[i] = px[i] - epsilon;
        div = div + (f(&nx)[i] - f(&px)[i]) / (epsilon + epsilon);
    }

    div
}
