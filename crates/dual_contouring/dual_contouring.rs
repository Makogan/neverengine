#![crate_name = "dual_contouring"]

use std::collections::HashMap;

use nalgebra as na;

pub type Vec2 = na::Vector2<f32>;
pub type Vec3 = na::Vector3<f32>;
pub type Vec4 = na::Vector4<f32>;

fn optimize_cell_center(
    point: &Vec3,
    sdf: &dyn Fn(&Vec3) -> f32,
    side_length: &Vec3,
) -> (bool, Vec3) {
    let cell_center = point + 0.5 * side_length;

    let surface_penalty = 10.0;

    let loss_function = |v: &Vec3| -> f32 {
        let eccentric_penalty = (cell_center - v).dot(&(cell_center - v));
        return sdf(v) * sdf(v) * surface_penalty + eccentric_penalty;
    };

    let gradient = |v: &Vec3| -> Vec3 {
        let epsilon = 1e-3;
        return Vec3::new(
            loss_function(&(v + Vec3::new(epsilon, 0.0, 0.0)))
                - loss_function(&(v + Vec3::new(-epsilon, 0.0, 0.0))),
            loss_function(&(v + Vec3::new(0.0, epsilon, 0.0)))
                - loss_function(&(v + Vec3::new(0.0, -epsilon, 0.0))),
            loss_function(&(v + Vec3::new(0.0, 0.0, epsilon)))
                - loss_function(&(v + Vec3::new(0.0, 0.0, -epsilon))),
        ) / epsilon;
    };

    // Cubes have 8 vertices.
    let mut signed_distances: [f32; 8] = [0.0; 8];

    for i in 0..8 {
        let point = Vec3::new(
            (i % 8) as f32 / 4.0,
            (i % 4) as f32 / 2.0,
            (i % 2) as f32 / 1.0,
        )
        .component_mul(&side_length);
        signed_distances[i] = sdf(&point);
    }

    // Test all corners of a cell, if any 2 differ in sign then this cell crosses
    // the boundary.
    let mut cell_crosses_boundary = false;
    let sign = sdf(point);
    for i in 0..2 {
        for j in 0..2 {
            for k in 0..2 {
                let position = point
                    + Vec3::new(i as f32, j as f32, k as f32).component_mul(&side_length);
                let test = sign * sdf(&position);
                cell_crosses_boundary = cell_crosses_boundary || (test < 0.0);
            }
        }
    }

    let mut p = cell_center;
    if cell_crosses_boundary {
        for _ in 0..10 {
            p = p - gradient(&p) * 0.02;
        }
    }

    return (cell_crosses_boundary, p);
}

pub fn dual_contouring(
    sdf: &dyn Fn(&Vec3) -> f32,
    resolution: (usize, usize, usize),
    corner: &Vec3,
    span: &Vec3,
) -> (Vec<Vec3>, Vec<usize>) {
    let mut vertices = Vec::<Vec3>::new();
    let mut connectivity = Vec::<usize>::new();
    let mut vertex_index_map = HashMap::<(usize, usize, usize), usize>::new();

    let side_length = Vec3::new(
        span.x / (resolution.0 - 1) as f32,
        span.y / (resolution.1 - 1) as f32,
        span.z / (resolution.2 - 1) as f32,
    );

    for i in 0..resolution.0 {
        let x = (i as f32 * span.x / (resolution.0 - 1) as f32) + corner.x;
        for j in 0..resolution.1 {
            let y = (j as f32 * span.y / (resolution.1 - 1) as f32) + corner.y;
            for k in 0..resolution.2 {
                let z = (k as f32 * span.z / (resolution.2 - 1) as f32) + corner.z;

                let (found, v) =
                    optimize_cell_center(&Vec3::new(x, y, z), sdf, &side_length);
                if found {
                    vertex_index_map.insert((i, j, k), vertices.len());
                    vertices.push(v);
                }
            }
        }
    }

    // The range of each iteration skips the first index to prevent sampling outside
    // the bounds of the array. However, this still spans all cells so the final
    // result is still the full boundary even if the boundary is contained in
    // the cells not explicitely tested.
    for (key, _) in vertex_index_map.iter() {
        let (i, j, k) = *key;
        let x = (i as f32 * span.x / (resolution.0 - 1) as f32) + corner.x;
        let y = (j as f32 * span.y / (resolution.1 - 1) as f32) + corner.y;
        let z = (k as f32 * span.z / (resolution.2 - 1) as f32) + corner.z;

        for axis in 0..3 {
            let mut offset = Vec3::zeros();
            offset[axis] = 1.0;
            offset.component_mul_assign(&side_length);

            let global_coord = Vec3::new(x, y, z);

            let sign0 = sdf(&global_coord);
            let sign1 = sdf(&(global_coord + offset));

            if sign0 * sign1 <= 0.0 {
                let (_f, s, u) = axis_index_to_basis_indices(axis);
                let coords0 = [i, j, k];
                let mut coords1 = coords0;
                coords1[u] -= 1;
                let mut coords2 = coords0;
                coords2[u] -= 1;
                coords2[s] -= 1;
                let mut coords3 = coords0;
                coords3[s] -= 1;

                // First triangle.
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords0[0], coords0[1], coords0[2]))
                        .unwrap(),
                );
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords1[0], coords1[1], coords1[2]))
                        .unwrap(),
                );
                println!("{:?}", coords1);
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords2[0], coords2[1], coords2[2]))
                        .unwrap(),
                );

                if sign0 <= 0.0 {
                    let l = connectivity.len();
                    connectivity.swap(l - 1, l - 2);
                }

                // Second triangle.
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords0[0], coords0[1], coords0[2]))
                        .unwrap(),
                );
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords2[0], coords2[1], coords2[2]))
                        .unwrap(),
                );
                connectivity.push(
                    *vertex_index_map
                        .get(&(coords3[0], coords3[1], coords3[2]))
                        .unwrap(),
                );

                if sign0 <= 0.0 {
                    let l = connectivity.len();
                    connectivity.swap(l - 1, l - 2);
                }
            }
        }
    }

    return (vertices, connectivity);
}

/// Given an axis in 3D, returns the indices of the 3 basis axis in the order
/// such that the first index represents forward (the input) the next the side
/// and the final the up. i.e. it computes the even permutation of basis indices
/// that creates the rotation that aligns the basis with the selected axis.
///
/// # Arguments
///
/// * index an index from 0-2 selecting the forward axis.
///
/// # Examples
///
/// ```
/// use dual_contouring::axis_index_to_basis_indices;
/// let x_index = 0;
/// let y_index = 1;
/// let z_index = 2;
/// let (forward, side, up) = axis_index_to_basis_indices(x_index);
/// assert!(forward == x_index);
/// assert!(side == y_index);
/// assert!(up == z_index);
///
/// let (forward, side, up) = axis_index_to_basis_indices(z_index);
/// assert!(forward == z_index);
/// assert!(side == x_index);
/// assert!(up == y_index);
/// ```
pub fn axis_index_to_basis_indices(index: usize) -> (usize, usize, usize) {
    match index {
        0 => (0, 1, 2),
        1 => (1, 2, 0),
        2 => (2, 0, 1),
        _ => panic!(),
    }
}
