use chicken_wire::wavefront_loader::*;
use clap::Parser;
use geometry::geometry_cleaning::*;
#[derive(Parser, Debug)]
#[command(author = "Makogan", version, about = "Cleans a topological skeleton
stored in an .obj", long_about = None)]
struct Args
{
    #[arg(short, long)]
    input: String,

    #[arg(short, long)]
    output: String,
}

fn main()
{
    let args = Args::parse();

    let ObjData {
        mut vertices,
        lines,
        ..
    } = ObjData::from_disk_file(&args.input);

    let mut lines = lines
        .iter()
        .map(|[i, j]| [*i as usize, *j as usize])
        .collect();
    merge_overlapping_vertices_and_edges(&mut vertices, &mut lines, 0.01);

    ObjData::export(&(&vertices, &lines), args.output.as_str());
}
