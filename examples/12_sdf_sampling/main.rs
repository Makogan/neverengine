use std::cell::RefCell;
use std::rc::Rc;

use algebra::*;
use camera::ArcballCamera;
use chicken_wire::wavefront_loader::ObjData;
use core::f32::consts::PI;
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use geometry::project_onto_plane;
use numerical_analysis::differential_operators;
use rstar::Point;
use rstar::RTree;
use spade::{DelaunayTriangulation, Triangulation};
use std::collections::{BTreeMap, BTreeSet, VecDeque};
use vulkan_bindings::RenderContext;

#[derive(Clone, PartialEq, Debug, Copy)]
struct TreePoint(Vec3, usize);

impl Point for TreePoint {
    type Scalar = f32;
    const DIMENSIONS: usize = 3;

    fn generate(mut generator: impl FnMut(usize) -> Self::Scalar) -> Self {
        TreePoint(
            Vec3::new(generator(0), generator(1), generator(2)),
            usize::MAX,
        )
    }

    fn nth(&self, index: usize) -> Self::Scalar {
        self.0[index]
    }

    fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
        &mut self.0[index]
    }
}

fn seek_surface<F>(mut point: Vec3, sdf: &F, reach: f32) -> Vec3
where
    F: Fn(&Vec3) -> f32,
{
    loop {
        let d = sdf(&point);
        let mut g = differential_operators::gradient(&point, &sdf, 0.00001 * reach);
        if d.abs() < 0.001 {
            break;
        }
        if g.norm() == 0.0 {
            g = Vec3::new(0., -1.0, 0.);
        }
        point -= g * (d + 0.0001 * reach);
    }

    point
}

// fn round_box(p: &Vec3, b: &Vec3, r: f32) {
//     let q = p.abs() - b + r;
//     return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0) - r;
// }

fn sample_sdf<F>(sdf: F, reach: f32) -> RTree<TreePoint>
where
    F: Fn(&Vec3) -> f32,
{
    let mut point = Vec3::default();
    let point = seek_surface(point, &sdf, reach);

    let mut seeds = VecDeque::new();
    seeds.push_back(point);

    let mut tree = RTree::new();
    tree.insert(TreePoint(*seeds.front().unwrap(), tree.size()));

    while let Some(point) = seeds.pop_front() {
        let g = differential_operators::gradient(&point, &sdf, 0.00001).normalize();
        let u = orthogonal_vector(&g).normalize();
        let v = g.cross(&u).normalize();

        let sample_count = 6;

        for i in 0..sample_count {
            let t = (i as f32 / sample_count as f32) * 2. * PI;
            let candidate = (t.cos() * u + t.sin() * v) * reach * 1.2 + point;
            let candidate = seek_surface(candidate, &sdf, reach);
            let mut candidate = TreePoint(candidate, usize::MAX);

            if let Some(nearest) = tree.nearest_neighbor(&candidate) {
                if (nearest.0 - candidate.0).norm() < reach {
                    continue;
                }
            };

            seeds.push_back(candidate.0);
            candidate.1 = tree.size();
            tree.insert(candidate.clone());
        }
    }

    tree
}

fn topologize_sdf<F>(sdf: F, tree: &RTree<TreePoint>, reach: f32) -> Vec<[usize; 3]>
where
    F: Fn(&Vec3) -> f32,
{
    let mut points: Vec<_> = tree.into_iter().collect();
    points.sort_by(|a, b| a.1.cmp(&b.1));
    let points: Vec<_> = points.into_iter().map(|p| p.0).collect();

    let mut faces = Vec::new();
    let mut seen_faces = BTreeSet::new();
    let mut seen_edges = BTreeSet::new();
    for point in tree.iter() {
        let mut n_iter = tree.nearest_neighbor_iter(point);
        let _ = n_iter.next().unwrap(); // skip the active point.

        let g = differential_operators::gradient(&point.0, &sdf, 0.00001).normalize();
        let x = orthogonal_vector(&g).normalize();
        let y = g.cross(&x).normalize();

        let get_2d_coords = |p: &Vec3| spade::Point2::new(x.dot(&p), y.dot(&p));

        let mut local_points = vec![point];
        let mut triangulation = DelaunayTriangulation::<spade::Point2<_>>::new();
        let _ = triangulation.insert(get_2d_coords(&point.0));
        for neighbour in n_iter {
            if (neighbour.0 - point.0).norm() >= reach * 2.5 {
                break;
            }

            let q = neighbour.0;
            let proj = project_onto_plane(&point.0, &g, &q);
            let d = (proj - point.0).normalize();

            let a = (q - point.0).norm() / 2.0;
            let theta = d.dot(&(q - point.0).normalize());
            let l = a / theta;

            let res = 2. * l * d + point.0;

            let _ = triangulation.insert(get_2d_coords(&res));
            local_points.push(neighbour);
        }

        let mut face_map = BTreeMap::new();
        let mut triangulation_faces = Vec::new();
        for (i, face) in triangulation.inner_faces().enumerate() {
            triangulation_faces.push([
                face.vertices()[0].index(),
                face.vertices()[1].index(),
                face.vertices()[2].index(),
            ]);
            if !face.vertices().iter().any(|v| v.index() == 0) {
                continue;
            }

            let index = face.vertices().iter().position(|v| v.index() == 0).unwrap();

            let next = face.vertices()[(index + 1) % 3];
            let prev = face.vertices()[(index + 2) % 3];
            let list = face_map.entry(next.index()).or_insert(Vec::new());
            list.push(i);
            let list = face_map.entry(prev.index()).or_insert(Vec::new());
            list.push(i);
        }

        let mut tris = Vec::<usize>::new();
        for face in triangulation_faces {
            if !face.iter().any(|v| *v == 0) {
                continue;
            }

            tris.push(face[0]);
            tris.push(face[1]);
            tris.push(face[2]);

            let i0 = face[0];
            let i1 = face[1];
            let i2 = face[2];

            let ids = [local_points[i0].1, local_points[i1].1, local_points[i2].1];
            let mut key = ids.clone();
            key.sort();

            if seen_faces.contains(&key) {
                continue;
            }

            let e1 = [ids[0], ids[1]];
            let e2 = [ids[1], ids[2]];
            let e3 = [ids[2], ids[0]];

            let edges = [e1, e2, e3];

            let mut skip = false;
            for edge in edges {
                if seen_edges.contains(&edge) {
                    skip = true;
                    break;
                }
            }

            let edge_length = |edge: &[usize; 2], points: &[Vec3]| {
                (points[edge[0]] - points[edge[1]]).norm()
            };

            if skip {
                continue;
            }

            for edge in edges {
                seen_edges.insert(edge);
            }
            seen_faces.insert(key);
            faces.push(ids);
        }
    }

    faces
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct Mvp {
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn init<'io>() -> (IoContext<'io>, RenderContext, Rc<RefCell<ArcballCamera>>) {
    let mut io_context = IoContext::new("SDF Sampling", (800, 800), true);
    let render_context = RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    (io_context, render_context, camera)
}

fn main() {
    let (mut io_context, mut render_context, camera) = init();

    let plant_shader = render_context
        .add_shader(vec!["examples/12_sdf_sampling/plant_shader/".to_string()]);

    let sphere = |p: &Vec3| {
        return p.norm() - 1.0;
    };

    let radius = 0.1;
    let points = sample_sdf(sphere, radius);
    let faces = topologize_sdf(sphere, &points, radius);

    let mut points: Vec<_> = points.into_iter().collect();
    points.sort_by(|a, b| a.1.cmp(&b.1));
    let points: Vec<_> = points.into_iter().map(|p| p.0).collect();

    let faces: Vec<_> = faces.into_iter().map(|v| v.to_vec()).collect();

    ObjData::export(&(points, faces), "sdf_points.obj");
    // println!("{:?}", verts.len());
    // let mut normals = compute_normals(&verts, &connect);

    // while io_context.poll_io(&mut None) {
    //     render_context.start_frame(&io_context);

    //     let connect: Vec<_> = connect.iter().map(|&i| i as u32).collect();
    //     let gpu_mesh = GraphicsInput::new()
    //         .add_attribute_buffer_from_slice(&verts, &mut render_context)
    //         .add_attribute_buffer_from_slice(&normals, &mut render_context)
    //         .set_index_buffer_from_slice(&connect, &mut render_context);

    //     let mvp = Mvp {
    //         model: Mat4::identity(),
    //         view: camera.borrow().view_matrix(),
    //         proj: camera.borrow().proj_matrix(),
    //     };

    //     let render_request = RenderRequest::new(plant_shader)
    //         .vertex_input(gpu_mesh.clone())
    //         .add_ubo(0, mvp)
    //         .build();

    //     render_context.draw(&render_request);
    //     render_context.free_input(gpu_mesh);
    // }
}
