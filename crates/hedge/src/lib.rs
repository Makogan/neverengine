use core::ops::Add;
use core::ops::Mul;
use std::fmt::Debug;

pub use traits::{HalfMeshLike, PrimitiveContainer};
pub mod face_handle;
pub use face_handle::FaceHandle;
pub use face_handle::FaceMetrics;
pub mod hedge_handle;
pub use hedge_handle::HEdgeHandle;
pub use vert_handle::VertHandle;

pub mod facets;
pub mod geometry_traits;
pub mod mesh;
pub mod mesh_deleter;
pub mod quadric_simplification;
pub mod remeshing;
pub mod sub_mesh;
pub mod topology;
pub mod traits;
pub mod vert_handle;
pub mod wavefront;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum TagColor
{
    BLACK,
    BLUE,
}

/// Subdivides all edges in a mesh such that each old face now has a new
/// triangle inside of it. The new vertices are guaranteed to be added into the
/// list after the existing ones.
// Implementation notes: this function heavily relies on assumptions about how
// half edges are allocated by the split functions. The indexing only works
// because of it.
pub fn linear_subdivision<S, MeshType, T>(mesh: &mut MeshType)
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: Add<Output = T> + Mul<S, Output = T> + Clone + Debug,
    S: num::Float,
    MeshType: HalfMeshLike,
{
    let old_edge_count = mesh.hedge_count();
    let cutoff_vert_index = mesh.last_vert_id().0 as usize;
    // For each existing *edge* (not half edge) we will add 6 new half edges.
    let mut edge_flags = vec![TagColor::BLACK; old_edge_count + (old_edge_count / 2) * 6];
    let mut edge_offset = old_edge_count;
    for edge_handle in mesh.iter_edges()
    {
        if mesh.is_in_boundary_edge(edge_handle.id())
        {
            let (_mid, [_trans], _long) = mesh.split_edge_boundary(edge_handle.id());

            // A boundary split should add 4 new half edges such that the
            // first two make the transversal edge.
            edge_flags[edge_offset + 0] = TagColor::BLUE;
            edge_flags[edge_offset + 1] = TagColor::BLUE;

            edge_offset += 4;
        }
        else
        {
            let (_mid, [_he1, _he2], _long) = mesh.split_edge(edge_handle.id());

            edge_flags[edge_offset + 0] = TagColor::BLUE;
            edge_flags[edge_offset + 1] = TagColor::BLUE;

            edge_flags[edge_offset + 2] = TagColor::BLUE;
            edge_flags[edge_offset + 3] = TagColor::BLUE;

            edge_offset += 6;
        };
    }

    let new_edges_start = old_edge_count / 2;
    let offset = old_edge_count;

    for (i, hedge_handle) in mesh.iter_edges().skip(new_edges_start).enumerate()
    {
        if mesh.is_in_boundary_edge(hedge_handle.id())
        {
            continue;
        }

        let v1_is_old = hedge_handle.source().id.0 < cutoff_vert_index as u64;
        let v2_is_old = hedge_handle.dest().id.0 < cutoff_vert_index as u64;

        if v1_is_old != v2_is_old && edge_flags[offset + i * 2] == TagColor::BLUE
        {
            hedge_handle.flip();
        };
    }
}

pub fn loop_subdivision<S, MeshType, T>(mesh: &mut MeshType)
where
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: Add<Output = T> + Mul<S, Output = T> + Clone + Debug,
    S: num::Float,
    MeshType: HalfMeshLike,
{
    // TODO(medium): Optimize this function to use less memory, ideally $O(1)$
    let mut vertex_new = Vec::<_>::with_capacity(mesh.vert_count() + mesh.edge_count());

    for vert in mesh.iter_verts()
    {
        let v_data = vert.data().clone();
        let mut average = v_data.clone() * S::from(0.0).unwrap();
        let mut valence = 0;
        for neighbour in vert.iter_verts()
        {
            average = average + neighbour.data();
            valence += 1;
        }

        let n = S::from(valence).unwrap();
        let u = if valence == 3
        {
            S::from(3.0 / 16.0).unwrap()
        }
        else
        {
            S::from(3.0).unwrap() / (S::from(8.0).unwrap() * n)
        };

        average = v_data * (S::from(1.0).unwrap() - u * n) + average * u;
        vertex_new.push(average);
    }

    for hedge_handle in mesh.iter_edges()
    {
        let v1 = hedge_handle.source().data();
        let v2 = hedge_handle.pair().prev().source().data();
        let v3 = hedge_handle.dest().data();
        let v4 = hedge_handle.prev().source().data();

        let average = (v1 + v3) * S::from(3.0 / 8.0).unwrap()
            + (v2 + v4) * S::from(1.0 / 8.0).unwrap();

        vertex_new.push(average);
    }

    linear_subdivision(mesh);

    for vert in mesh.iter_verts()
    {
        vert.mutate_data(&vertex_new[vert.id().0 as usize]);
    }
}

/// Average the vertices with its immediate neighbours. Vertices evaluated as
/// true by the mask will be moved, all others will remain in place. This is a
/// cheap smoothing approach, laplacian smoothing should be better in many
/// cases.
///
/// # Arguments
///
/// `mesh` the mesh to modify.
///
/// `mask` set of vertices to hold in place.
pub fn average_vertices<MeshType, S, M, T>(mesh: &mut MeshType, mask: M)
where
    MeshType: HalfMeshLike,
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: Add<Output = T> + Mul<S, Output = T> + Clone + Debug,
    S: num::FromPrimitive,
    M: Fn(&usize) -> bool,
{
    let mut new_positions = Vec::with_capacity(mesh.vert_count());
    for vert in mesh.iter_verts()
    {
        let mut average = vert.data() * S::from_f64(0.0).unwrap();
        let mut count = 0.0;
        for n in mesh.vert_handle(vert.id()).iter_verts()
        {
            average = average + n.data();
            count += 1.0;
        }
        average = average * S::from_f64(1.0 / count).unwrap();

        new_positions.push(average);
    }

    for (index, vert) in mesh.iter_verts().enumerate()
    {
        // Skip mutating data of fixed vertices.
        if !mask(&(vert.id().0 as usize))
        {
            continue;
        }

        vert.mutate_data(&new_positions[index]);
    }
}

/// Similar to `average_vertices` but computes an average with the existing
/// position. The parameter `weight` refers to a number in $[0, 1]$ that
/// determines how much of the average position is taken on each invocation.
/// e.g. `weight = 0` means no change occurs, `weight = 0.5` is the halfway
/// point and `weight = 1` is equivalent to `average_vertices`.
pub fn smooth_vertices<MeshType, S, M, T>(mesh: &mut MeshType, keep_fixed: M, weight: S)
where
    MeshType: HalfMeshLike,
    MeshType::VertData: PrimitiveContainer<PrimitiveData = T>,
    T: Add<Output = T> + Mul<S, Output = T> + Clone + Debug,
    S: num::FromPrimitive + Clone + std::ops::Sub<Output = S>,
    M: Fn(&usize) -> bool,
{
    let mut new_positions = Vec::with_capacity(mesh.vert_count());
    for vert in mesh.iter_verts()
    {
        let mut average = vert.data() * S::from_f64(0.0).unwrap();
        let mut count = 0.0;
        for n in mesh.vert_handle(vert.id()).iter_verts()
        {
            average = average + n.data();
            count += 1.0;
        }
        average = average * S::from_f64(1.0 / count).unwrap();

        new_positions.push(
            vert.data() * (S::from_f64(1.0).unwrap() - weight.clone())
                + average * weight.clone(),
        );
    }

    for (index, vert) in mesh.iter_verts().enumerate()
    {
        if keep_fixed(&(vert.id().0 as usize))
        {
            continue;
        }

        vert.mutate_data(&new_positions[index]);
    }
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests
{
    use std::ops::{Add, Mul};

    use algebra::{Vec2, Vec3};

    use crate::geometry_traits::FaceDataGetters;
    use crate::geometry_traits::VertDataGetters;
    use crate::loop_subdivision;
    use crate::mesh::verify_topology;
    use crate::mesh::HalfMesh;
    use crate::traits::MeshData;

    #[derive(Clone, Copy, Debug)]
    struct VertexData
    {
        position: Vec3,
    }

    impl Mul<f32> for VertexData
    {
        type Output = Self;

        fn mul(self, rhs: f32) -> Self::Output
        {
            let new_vec = self.position * rhs;

            return VertexData { position: new_vec };
        }
    }

    impl Add<VertexData> for VertexData
    {
        type Output = VertexData;

        fn add(self, rhs: VertexData) -> Self::Output
        {
            let new_vec = self.position + rhs.position;
            return VertexData { position: new_vec };
        }
    }

    impl VertDataGetters for VertexData
    {
        type Scalar = f32;
        type V2 = Vec2;
        type V3 = Vec3;

        fn position(&self) -> Self::V3 { self.position }

        fn normal(&self) -> Self::V3 { Vec3::zeros() }

        fn uv(&self) -> Self::V2 { Vec2::zeros() }
    }

    #[derive(Clone, Debug)]
    struct ObjFaceData
    {
        normals: Vec<Vec3>,
        uvs: Vec<Vec2>,
    }

    impl FaceDataGetters for ObjFaceData
    {
        type Normal = Vec3;
        type Uv = Vec2;

        fn normal_count(&self) -> usize { self.normals.len() }

        fn normal(&self, id: usize) -> Self::Normal { self.normals[id] }

        fn uv_count(&self) -> usize { self.uvs.len() }

        fn uv(&self, id: usize) -> Self::Uv { self.uvs[id] }
    }

    #[derive(Clone, Debug)]
    struct EdgeData {}

    impl MeshData
        for (
            Vec<VertexData>,
            Vec<Vec3>,
            Vec<Vec2>,
            Vec<Vec<u64>>,
            Vec<Vec<u64>>,
            Vec<Vec<u64>>,
        )
    {
        type EdgeData = EdgeData;
        type FaceData = ObjFaceData;
        type VertData = VertexData;

        fn get_vert_data(&self, vert_id: u64) -> Self::VertData
        {
            self.0[vert_id as usize]
        }

        fn get_hedge_data(&self, _face_id: u64, _edge_id: u64) -> Self::EdgeData
        {
            EdgeData {}
        }

        fn get_face_data(&self, face_id: u64) -> Self::FaceData
        {
            let dummy = Vec::new();

            let face_normals = if !self.4.is_empty()
            {
                &self.4[face_id as usize]
            }
            else
            {
                &dummy
            };
            let mut normals = Vec::<Vec3>::with_capacity(face_normals.len());

            for normal_id in face_normals
            {
                normals.push(self.1[*normal_id as usize]);
            }

            let face_uvs = if !self.5.is_empty()
            {
                &self.5[face_id as usize]
            }
            else
            {
                &dummy
            };
            let mut uvs = Vec::<Vec2>::with_capacity(face_uvs.len());

            for uv_id in face_uvs
            {
                uvs.push(self.2[*uv_id as usize]);
            }

            return ObjFaceData { normals, uvs };
        }

        fn get_topology(&self) -> Vec<Vec<u64>> { return self.3.clone(); }

        fn vert_count(&self) -> usize { return self.0.len(); }
    }

    use chicken_wire::wavefront_loader::ObjData;
    #[test]
    fn test_linear_subdivision()
    {
        let ObjData {
            vertices: verts,
            normals,
            vertex_face_indices: verts_idx,
            normal_face_indices: normals_idx,
            ..
        } = ObjData::from_disk_file("../../Assets/bunny.obj");

        let mut split_data = Vec::<VertexData>::new();
        for vert in verts
        {
            split_data.push(VertexData { position: vert });
        }

        let faces = normals_idx
            .iter()
            .map(|face_ids| {
                face_ids
                    .iter()
                    .map(|id| normals[*id as usize])
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        let mut mesh = HalfMesh::new(split_data, (), faces, verts_idx);

        ObjData::export(&mesh, "old.obj");
        loop_subdivision(&mut mesh);
        verify_topology(&mesh);
        ObjData::export(&mesh, "test.obj");
    }
}
