#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

/*
#ifdef never
[[ne::topology(point list)]]
[[ne::polygon_mode(point)]]
#endif
 */
extern crate bytemuck;
extern crate spirv_std;

pub use spirv_std::glam::{Mat2, Vec2, Vec3, Vec4};
use spirv_std::{arch::IndexUnchecked, glam::*, image::*, num_traits::Float, spirv};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

#[spirv(fragment)]
pub fn main_fs(vel: Vec2, output: &mut Vec4)
{
    let len = vel.length();
    let white = Vec4::splat(1.);
    let c = Vec4::new(0., 1., 0., 1.);
    let color = c.lerp(white, len * 5.0);

    *output = color;
    output.w = 1.;
}

#[spirv(vertex)]
pub fn main_vs(
    in_pos_bind0: Vec2,
    in_vel_bind0: Vec2,
    mass_bind0: f32,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] transform: &Mat2,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    #[spirv(point_size)] point_size: &mut f32,
    out_vel: &mut Vec2,
)
{
    let normalized_pos = ((in_pos_bind0.xy() / 100.0) - Vec2::new(0.5, 0.5)) * 2.;
    *screen_pos = (*transform * normalized_pos)
        .extend(0. + mass_bind0 * 0.0000001)
        .extend(1.);

    *point_size = 10.;
    *out_vel = in_vel_bind0;
}
