use chicken_wire::wavefront_loader::WaveFrontCompatible;

use crate::geometry_traits::*;
use crate::mesh::*;

impl<'a, V, E, F> WaveFrontCompatible<'a> for HalfMesh<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
    V::PrimitiveData: VertDataGetters,
    F::PrimitiveData: FaceDataGetters<
        Normal = <<V as PrimitiveContainer>::PrimitiveData as VertDataGetters>::V3,
        Uv = <<V as PrimitiveContainer>::PrimitiveData as VertDataGetters>::V2,
    >,
{
    type Scalar = <<V as PrimitiveContainer>::PrimitiveData as VertDataGetters>::Scalar;

    fn pos_iterator(&'a self) -> impl Iterator<Item = [Self::Scalar; 3]>
    {
        self.iter_verts().map(|v| {
            let pos = v.data().position();
            [pos[0], pos[1], pos[2]]
        })
    }

    fn norm_iterator(&'a self) -> impl Iterator<Item = [Self::Scalar; 3]>
    {
        self.iter_faces()
            .map(|face| {
                let normal_count = face.data().normal_count();
                (0..normal_count).map(move |i| {
                    let normal = face.data().normal(i);
                    [normal[0], normal[1], normal[2]]
                })
            })
            .flatten()
    }

    fn uv_iterator(&'a self) -> impl Iterator<Item = [Self::Scalar; 2]>
    {
        self.iter_faces()
            .map(|face| {
                let uv_count = face.data().uv_count();
                (0..uv_count).map(move |i| {
                    let uv = face.data().uv(i);
                    [uv[0], uv[1]]
                })
            })
            .flatten()
    }

    fn pos_index_iterator(&'a self) -> impl Iterator<Item = impl Iterator<Item = usize>>
    {
        self.iter_faces()
            .map(|f| f.vertex_handles().map(|v| v.id().0 as usize))
    }

    fn norm_index_iterator(&'a self)
        -> impl Iterator<Item = impl Iterator<Item = usize>>
    {
        self.iter_faces().map(|face| {
            let data = face.data();
            let norm_count = data.normal_count();
            face.hedge()
                .iter_loop()
                .map(|n| n.source().id().0 as usize)
                .take(norm_count)
        })
    }

    fn uv_index_iterator(&'a self) -> impl Iterator<Item = impl Iterator<Item = usize>>
    {
        self.iter_faces().map(|face| {
            let data = face.data();
            let uv_count = data.uv_count();
            face.hedge()
                .iter_loop()
                .map(|n| n.source().id().0 as usize)
                .take(uv_count)
        })
    }

    fn segment_iterator(&'a self) -> impl Iterator<Item = [usize; 2]>
    {
        std::iter::empty()
    }
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests
{
    use chicken_wire::wavefront_loader::ObjData;
    use tempfile::*;

    use crate::{
        mesh::{verify_topology, GeomId, HalfMesh},
        topology::find_boundaries,
        traits::HalfMeshLike,
    };

    #[test]
    fn test_obj_loading()
    {
        use chicken_wire::wavefront_loader::ObjData;
        let ObjData {
            vertices,
            normals,
            uvs,
            vertex_face_indices,
            normal_face_indices,
            uv_face_indices,
            ..
        } = ObjData::from_disk_file("../../Assets/bunny.obj");

        assert_eq!(vertex_face_indices.len(), normal_face_indices.len());
        assert_eq!(0, uv_face_indices.len());

        assert_eq!(vertices.len(), 2503);
        assert_eq!(normals.len(), 4966);
        assert_eq!(uvs.len(), 0);

        assert_eq!(vertex_face_indices[0].len(), 3);
        assert_eq!(normal_face_indices[0].len(), 3);
    }

    #[test]
    fn test_half_mesh_building()
    {
        let ObjData {
            vertices: vs,
            vertex_face_indices: vids,
            ..
        } = ObjData::from_disk_file("../../Assets/bunny.obj");

        let mesh = HalfMesh::new(vs, (), (), vids);

        let dir = tempdir().unwrap();
        let file_path = dir.path().join("test.obj");

        let tmp_path = file_path.as_os_str().to_str().unwrap();

        ObjData::export(&mesh, tmp_path);
        ObjData::export(&mesh, "./test.obj");

        let ObjData {
            vertices: vs,
            vertex_face_indices: vids,
            ..
        } = ObjData::from_disk_file(tmp_path);
        let other_mesh = HalfMesh::new(vs, (), (), vids);

        assert_eq!(other_mesh.vert_count(), mesh.vert_count());

        for vert in mesh.verts_meta.borrow().iter()
        {
            assert_eq!(
                other_mesh.verts.borrow()[vert.id.0 as usize],
                mesh.verts.borrow()[vert.id.0 as usize],
            );
        }

        // This fails because the current logic does not take into account
        // duplicate normals.

        // for face in mesh.faces.borrow().iter()
        // {
        //     let f1 = &mesh.faces.borrow()[face.id.0 as usize].data;
        //     let f2 = &other_mesh.faces.borrow()[face.id.0 as usize].data;

        //     for i in 0..f1.len()
        //     {
        //         assert_eq!(f1[i], f2.1[i]);
        //     }
        // }
    }

    #[test]
    fn test_edge_removal()
    {
        let ObjData {
            vertices: vs,
            vertex_face_indices: fs,
            ..
        } = ObjData::from_disk_file("../../Assets/bunny.obj");
        let mut mesh = HalfMesh::new(vs, (), (), fs);

        verify_topology(&mesh);
        let hedge_count_before = mesh.hedge_count();
        let face_count_before = mesh.face_count();
        let vert_count_before = mesh.vert_count();
        let boundaries_before = find_boundaries(&mesh).len();
        mesh.remove_edge(GeomId(0));

        assert!(mesh.hedge_count() == hedge_count_before - 2);
        assert!(
            mesh.face_count() == face_count_before - 2,
            "{} {}",
            mesh.face_count(),
            face_count_before
        );
        assert!(mesh.vert_count() == vert_count_before);
        let boundary_count = find_boundaries(&mesh).len();
        assert!(
            boundary_count == boundaries_before + 1,
            "{} {}",
            boundary_count,
            boundaries_before + 1
        );

        mesh.remove_edge(GeomId(64));
        mesh.remove_edge(GeomId(72));
        mesh.remove_edge(GeomId(100));
        mesh.remove_edge(GeomId(900));

        mesh.remove_edge(GeomId(0));
        mesh.remove_edge(GeomId(0));
        mesh.remove_edge(GeomId(0));
        mesh.remove_edge(GeomId(0));
        verify_topology(&mesh);

        let heid = find_boundaries(&mesh)[0][0];

        mesh.remove_edge(heid);
        verify_topology(&mesh);

        let vert_count_before = mesh.vert_count();
        mesh.remove_vertex(GeomId(0));
        assert!(
            mesh.vert_count() == vert_count_before - 1,
            "{} {}",
            mesh.vert_count(),
            vert_count_before
        );

        let vert_count_before = mesh.vert_count();
        mesh.remove_vertex(GeomId(0));
        mesh.remove_vertex(GeomId(0));
        mesh.remove_vertex(GeomId(0));
        assert!(mesh.vert_count() == vert_count_before - 3);

        ObjData::export(&mesh, "tmp/remove_test.obj");
    }
}
