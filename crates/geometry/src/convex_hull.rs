use std::{collections::HashSet, ops::Mul};

use algebra::{cross, na::Vector2, na::Vector3, Vec3};
use linear_isomorphic::*;

use crate::find_planar_orthonormal_basis;

pub fn planar_convex_hull(points: &Vec<Vec3>) -> Vec<usize>
{
    indexed_planar_convex_hull(&|i| points[i], points.len())
}

pub fn indexed_planar_convex_hull<S>(
    points: &dyn Fn(usize) -> Vector3<S>,
    point_count: usize,
) -> Vec<usize>
where
    S: RealField,
    S: Mul<Vector3<S>, Output = Vector3<S>>,
{
    debug_assert!(point_count >= 3);
    if point_count == 3
    {
        return vec![0, 1, 2];
    }

    // Find a direction that is numerically good to orthonormalize.

    // Compute a direction orthogonal to the canonical `x` direction `dir`.
    let (center, dir, normal) = find_planar_orthonormal_basis(points, point_count);
    debug_assert!(normal.dot(&dir).abs() < S::from(1e-4).unwrap());

    let axis_distance = |x: &Vector3<S>| dir.dot(&(x - center));

    // Find the "leftmost" point, it is guaranteed to be in the hull.
    let mut leftmost = 0;
    let mut smallest_distance = S::max_value();
    for i in 0..point_count
    {
        let d = axis_distance(&points(i));
        if d < smallest_distance
        {
            smallest_distance = d;
            leftmost = i;
        }
    }

    // Function to compute the determinant formed by 3 points.
    let det = |x: usize, y: usize, z: usize| {
        let express = |v: Vector3<S>| {
            Vector2::<S>::new((v - center).dot(&dir), (v - center).dot(&normal))
        };

        let p1 = express(points(x));
        let p2 = express(points(y));
        let p3 = express(points(z));

        (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x)
    };

    let mut ch = vec![leftmost];
    let mut current_hull_point = leftmost;
    let _next_hull_point = (current_hull_point + 1) % point_count;
    let _index = 0;

    loop
    {
        let mut next_hull_point = (current_hull_point + 1) % point_count;

        for i in 0..point_count
        {
            if i == current_hull_point
            {
                continue;
            }

            let distance_sq = |v| (points(v) - points(current_hull_point)).norm_squared();

            let test = det(current_hull_point, i, next_hull_point);
            let epsilon = S::min_positive_value() * S::from(1000.).unwrap();
            if test >= epsilon
                || (test.abs() <= epsilon
                    && distance_sq(i) > distance_sq(next_hull_point))
            {
                next_hull_point = i;
            }
        }
        current_hull_point = next_hull_point;

        if current_hull_point == leftmost
        {
            break;
        }

        ch.push(current_hull_point);
    }

    debug_assert!(ch.len() >= 3);
    ch
}

/// Like `convex_hull_triangulated` but accepts a first triangle parameter to
/// start creating the CH.
pub fn convex_hull_with_hint_triangulated(
    points: &Vec<Vec3>,
    ch_triangle: (usize, usize, usize),
) -> Vec<usize>
{
    let get_normal = |i, j, k| -> Vec3 {
        cross(&(points[j] - points[i]), &(points[k] - points[i])).normalize()
    };

    // The centroid is used to assert winding order.
    let mut centroid = Vec3::zeros();
    for point in points
    {
        centroid += point;
    }
    centroid /= points.len() as f32;

    // Add the first triangle to the set.
    let mut face_ids = vec![ch_triangle.0, ch_triangle.1, ch_triangle.2];

    // Test and fix winding order if necessary.
    let normal = get_normal(face_ids[0], face_ids[1], face_ids[2]);
    let test = normal.dot(&(points[face_ids[0]] - centroid));
    if test < 0.0
    {
        face_ids.swap(0, 2);
    }

    // Find all points that are coplanar with the starting face. We need  to create
    // a triangulation of the convex polyface that contains them.
    let current_normal = get_normal(face_ids[0], face_ids[1], face_ids[2]);
    let mut coplanar_points = Vec::new();
    for point_id in 0..points.len()
    {
        if point_id == face_ids[0] || point_id == face_ids[1]
        {
            continue;
        }
        let normal = get_normal(face_ids[0], face_ids[1], point_id);
        let dot = normal.dot(&current_normal);

        if f32::abs(dot.abs() - 1.0) <= f32::EPSILON * 2.0
        {
            coplanar_points.push(point_id);
            continue;
        }
    }

    // Let us construct a convex planar polygonal triangulation that contains at
    // least 2 points from the starting triangle and has the same normal. This
    // will initialize our state into validity and we can start adding new faces
    // from this point on.
    let p1 = face_ids[0];
    let p2 = face_ids[1];
    face_ids.clear();

    let mut edge_queue = Vec::new();
    let mut seen_edges = HashSet::new();
    triangulate_coplanar_points(
        points,
        p1,
        p2,
        &centroid,
        &current_normal,
        &mut coplanar_points,
        &mut seen_edges,
        &mut face_ids,
        &mut edge_queue,
    );

    // Start adding faces from here on out.
    while let Some((p1_id, p2_id, current_normal)) = edge_queue.pop()
    {
        if seen_edges.contains(&(p1_id, p2_id))
        {
            continue;
        }

        let mut best_point = 0;
        let mut best_dot = -1000.1;
        let mut best_normal = Vec3::zeros();
        // Find the point that produces the most coplanar triangle with the other
        // triangle in the current edge.
        let mut coplanar_points = Vec::new();
        for point_id in 0..points.len()
        {
            if point_id == p1_id || point_id == p2_id
            {
                continue;
            }

            let normal = get_normal(p1_id, p2_id, point_id);
            let dot = normal.dot(&current_normal);

            if f32::abs(dot - best_dot) <= f32::EPSILON * 10.0
            {
                // Due to coplanarity our current best point might generate already
                // processed edges, but we may still need to make a triangle for
                // this pair of points. So keep changing the selected best point.
                if seen_edges.contains(&(best_point, p1_id))
                    || seen_edges.contains(&(p2_id, best_point))
                {
                    best_point = point_id;
                }
                coplanar_points.push(point_id);

                continue;
            }
            if dot > best_dot
            {
                best_point = point_id;
                best_dot = dot;
                best_normal = normal;

                coplanar_points.clear();
                coplanar_points.push(point_id);
            }
        }

        // At this stage we must triangulate the point set and add all new edges to
        // the seen set.
        triangulate_coplanar_points(
            points,
            p1_id,
            p2_id,
            &centroid,
            &best_normal,
            &mut coplanar_points,
            &mut seen_edges,
            &mut face_ids,
            &mut edge_queue,
        );
    }

    face_ids
}

/// Computes the convex hull for a point cloud. The result is a triangulation of
/// the convex hull using the original point set. If you want a triangulation
/// using only the points that are part of the convex hull use
/// `compress_sparse_mesh` to remove non hull points and remap the indices.
///
/// WARNING! this will create degenerate geometry for sets of 4 or more colinear
/// points who all are in the hull. Output must be sanitized.
pub fn convex_hull_triangulated(points: &Vec<Vec3>) -> Vec<usize>
{
    convex_hull_with_hint_triangulated(points, find_ch_triangle(points))
}

pub fn convex_hull_with_hint(
    points: &Vec<Vec3>,
    ch_triangle: (usize, usize, usize),
) -> Vec<Vec<usize>>
{
    let epsilon = f32::EPSILON * 100.0;
    let get_normal = |i, j, k| -> Vec3 {
        cross(&(points[j] - points[i]), &(points[k] - points[i])).normalize()
    };

    // The centroid is used to assert winding order.
    let mut centroid = Vec3::zeros();
    for point in points
    {
        centroid += point;
    }
    centroid /= points.len() as f32;

    // Add the first triangle to the set.
    let mut face_ids = vec![ch_triangle.0, ch_triangle.1, ch_triangle.2];

    // Test and fix winding order if necessary.
    let normal = get_normal(face_ids[0], face_ids[1], face_ids[2]);
    let test = normal.dot(&(points[face_ids[0]] - centroid));
    if test < 0.0
    {
        face_ids.swap(0, 2);
    }

    // Find all points that are coplanar with the starting face. We need  to create
    // a triangulation of the convex polyface that contains them.
    let current_normal = get_normal(face_ids[0], face_ids[1], face_ids[2]);
    let mut coplanar_points = Vec::new();
    for point_id in 0..points.len()
    {
        if point_id == face_ids[0] || point_id == face_ids[1]
        {
            continue;
        }
        let normal = get_normal(face_ids[0], face_ids[1], point_id);
        let dot = normal.dot(&current_normal);

        if f32::abs(dot.abs() - 1.0) <= epsilon
        {
            coplanar_points.push(point_id);
            continue;
        }
    }

    // Let us construct a convex planar polygonal face that contains at
    // least 2 points from the starting triangle and has the same normal. This
    // will initialize our state into validity and we can start adding new faces
    // from this point on.
    let p1 = face_ids[0];
    let p2 = face_ids[1];
    face_ids.clear();

    let mut face_ids = Vec::new();
    let mut edge_queue = Vec::new();
    let mut seen_edges = HashSet::new();
    topologize_coplanar_points(
        points,
        p1,
        p2,
        &centroid,
        &current_normal,
        &mut coplanar_points,
        &mut seen_edges,
        &mut face_ids,
        &mut edge_queue,
    );

    // Start adding faces from here on out.
    while let Some((p1_id, p2_id, current_normal)) = edge_queue.pop()
    {
        if seen_edges.contains(&(p1_id, p2_id))
        {
            continue;
        }

        let mut best_point = 0;
        let mut best_dot = f32::MIN;
        let mut best_normal = Vec3::zeros();
        // Find the point that produces the most coplanar triangle with the other
        // triangle in the current edge.
        let mut coplanar_points = Vec::new();
        for point_id in 0..points.len()
        {
            if point_id == p1_id || point_id == p2_id
            {
                continue;
            }

            let normal = get_normal(p1_id, p2_id, point_id);
            let dot = normal.dot(&current_normal);

            if f32::abs(dot - best_dot) <= epsilon
            {
                // Due to coplanarity our current best point might generate already
                // processed edges, but we may still need to make a triangle for
                // this pair of points. So keep changing the selected best point.
                if seen_edges.contains(&(best_point, p1_id))
                    || seen_edges.contains(&(p2_id, best_point))
                {
                    best_point = point_id;
                }
                coplanar_points.push(point_id);

                continue;
            }
            if dot >= best_dot
            {
                best_point = point_id;
                best_dot = dot;
                best_normal = normal;

                coplanar_points.clear();
                coplanar_points.push(point_id);
            }
        }

        topologize_coplanar_points(
            points,
            p1_id,
            p2_id,
            &centroid,
            &best_normal,
            &mut coplanar_points,
            &mut seen_edges,
            &mut face_ids,
            &mut edge_queue,
        );
    }

    face_ids
}

pub fn convex_hull(points: &Vec<Vec3>) -> Vec<Vec<usize>>
{
    convex_hull_with_hint(points, find_ch_triangle(points))
}

// +| Internal |+ ==============================================================
fn find_ch_triangle(points: &Vec<Vec3>) -> (usize, usize, usize)
{
    // The lowest point is part of the 3D convex hull.
    let lowest_id = points
        .iter()
        .enumerate()
        .min_by(|(_, p1), (_, p2)| p1.z.partial_cmp(&p2.z).unwrap())
        .map(|(id, _)| id)
        .unwrap();

    // The lowest point is guaranteed to be connected to one of the points in the 2D
    // convex hull.
    let mut best_angle = 1.0;
    let mut best_angle_id = 0;
    for (id, p2) in points.iter().enumerate()
    {
        if id == lowest_id
        {
            continue;
        }

        let mut e1 = p2 - points[lowest_id];
        e1.y = 0.0;
        e1 = e1.normalize();

        let dir = Vec3::new(0.0, 0.0, 1.0);
        let dot = e1.dot(&dir);
        if dot < best_angle
        {
            best_angle = dot;
            best_angle_id = id;
        }
    }

    // Pick a candidate to initialize the next part of the algorithm. Should be
    // distinct from the points we already found.
    let mut candidate = 0;
    while candidate == lowest_id || candidate == best_angle_id
    {
        candidate += 1;
    }

    let get_normal = |i, j, k| -> Vec3 {
        cross(&(points[j] - points[i]), &(points[k] - points[i])).normalize()
    };
    // Find the third point by finding a point such that all other points are in one
    // side of the triangle it forms with the existing points.
    for i in 0..points.len()
    {
        if i == lowest_id || i == best_angle_id
        {
            continue;
        }

        let normal = get_normal(lowest_id, best_angle_id, candidate);
        if normal.dot(&(points[i] - points[lowest_id])) < 0.0
        {
            candidate = i;
        }
    }

    (lowest_id, best_angle_id, candidate)
}

fn topologize_coplanar_points(
    points: &Vec<Vec3>,
    p1_id: usize,
    p2_id: usize,
    centroid: &Vec3,
    best_normal: &Vec3,
    coplanar_points: &mut Vec<usize>,
    seen_edges: &mut HashSet<(usize, usize)>,
    face_ids: &mut Vec<Vec<usize>>,
    edge_queue: &mut Vec<(usize, usize, Vec3)>,
)
{
    let get_normal = |i, j, k| -> Vec3 {
        cross(&(points[j] - points[i]), &(points[k] - points[i])).normalize()
    };

    coplanar_points.push(p1_id);
    coplanar_points.push(p2_id);

    seen_edges.insert((p1_id, p2_id));

    let mut hull = indexed_planar_convex_hull(
        &|i| points[coplanar_points[i]],
        coplanar_points.len(),
    );

    // The CH algorithm picks an arbitrary winding order. We must check
    // if the returned order is appropriate for our current data or reverse
    // the order. Checking the normal of the first 3 points and the centroid is a
    // heuristic. If a better solution is found, change it.
    let should_swap = (points[coplanar_points[0]] - centroid).dot(&get_normal(
        coplanar_points[hull[0]],
        coplanar_points[hull[1]],
        coplanar_points[hull[2]],
    )) < 0.0;
    if should_swap
    {
        hull.reverse();
    }

    let mut face = Vec::new();
    for i in 0..hull.len()
    {
        let n = hull.len();
        let p1 = coplanar_points[hull[i]];
        let p2 = coplanar_points[hull[(i + 1) % n]];

        face.push(coplanar_points[hull[i]]);
        seen_edges.insert((p1, p2));
    }

    // Add all edges we have introduced that we have not seen to the queue.
    // Order here matters, this should happen after the above loop.
    for i in 0..hull.len()
    {
        let p1 = coplanar_points[hull[i]];
        let p2 = coplanar_points[hull[(i + 1) % hull.len()]];

        if !seen_edges.contains(&(p2, p1))
        {
            edge_queue.push((p2, p1, *best_normal));
        }
    }

    face_ids.push(face);
}

fn triangulate_coplanar_points(
    points: &[Vec3],
    p1_id: usize,
    p2_id: usize,
    centroid: &Vec3,
    best_normal: &Vec3,
    coplanar_points: &mut Vec<usize>,
    seen_edges: &mut HashSet<(usize, usize)>,
    face_ids: &mut Vec<usize>,
    edge_queue: &mut Vec<(usize, usize, Vec3)>,
)
{
    let get_normal = |i, j, k| -> Vec3 {
        cross(&(points[j] - points[i]), &(points[k] - points[i])).normalize()
    };

    coplanar_points.push(p1_id);
    coplanar_points.push(p2_id);

    seen_edges.insert((p1_id, p2_id));

    let mut hull = indexed_planar_convex_hull(
        &|i| points[coplanar_points[i]],
        coplanar_points.len(),
    );

    // The CH algorithm picks an arbitrary winding order. We must check
    // if the returned order is appropriate for our current data or reverse
    // the order. Checking the normal of the first 3 points and the centroid is a
    // heuristic. If a better solution is found, change it.
    let should_swap = (points[coplanar_points[0]] - centroid).dot(&get_normal(
        coplanar_points[hull[0]],
        coplanar_points[hull[1]],
        coplanar_points[hull[2]],
    )) < 0.0;
    if should_swap
    {
        hull.reverse();
    }

    // TODO(low): replace this with a proper triangulation algorithm.
    // Triangulate the face. Thankfully we know for a fact our set is convex
    // so we can use a naive triangulation.
    for i in 1..hull.len() - 1
    {
        let p1 = coplanar_points[hull[0]];
        let p2 = coplanar_points[hull[i + 0]];
        let p3 = coplanar_points[hull[i + 1]];

        face_ids.push(p1);
        face_ids.push(p2);
        face_ids.push(p3);

        seen_edges.insert((p1, p2));
        seen_edges.insert((p2, p3));
        seen_edges.insert((p3, p1));
    }

    // Add all edges we have introduced that we have not seen to the queue.
    // Order here matters, this should happen after the above loop.
    for i in 0..hull.len()
    {
        let p1 = coplanar_points[hull[i]];
        let p2 = coplanar_points[hull[(i + 1) % hull.len()]];

        if !seen_edges.contains(&(p2, p1))
        {
            edge_queue.push((p2, p1, *best_normal));
        }
    }
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use std::f32::consts::PI;
    use std::fs;

    use algebra::*;
    use chicken_wire::wavefront_loader::ObjData;
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    use crate::convex_hull::{convex_hull, convex_hull_triangulated, planar_convex_hull};
    use crate::utils::compress_sparse_triangle_mesh;

    #[test]
    fn test_convex_hull_triangulated()
    {
        let mut points = Vec::new();
        let seed: <ChaCha8Rng as SeedableRng>::Seed = Default::default();
        let mut rng = ChaCha8Rng::from_seed(seed);

        for _ in 0..1000
        {
            let x = rng.gen_range(0.0..1.0);
            let y = rng.gen_range(0.0..1.0);
            let z = rng.gen_range(0.0..1.0);
            points.push(Vec3::new(x, y, z));
        }

        let indices = convex_hull_triangulated(&points);
        let (points, indices, _) = compress_sparse_triangle_mesh(&points, &indices);
        fs::create_dir_all("./tmp").unwrap();
        ObjData::export(&(&points, &indices), "tmp/hull_triangulated_test.obj");
    }

    #[test]
    fn test_convex_hull()
    {
        let mut points = Vec::new();
        let p_count = 8;
        for i in 0..p_count
        {
            let t = (i as f32 / (p_count - 1) as f32) * 2.0 * PI;

            let x = t.cos();
            let y = t.sin();

            points.push(Vec3::new(x, y, 0.0));
            points.push(Vec3::new(x, y, 1.0));
        }

        let indices = convex_hull(&points);
        fs::create_dir_all("./tmp").unwrap();
        ObjData::export(&(&points, &indices), "tmp/hull_test.obj");
    }

    #[test]
    fn test_planar_convex_hull()
    {
        let mut points = Vec::new();

        let resolution = 100;
        for i in 0..resolution
        {
            let x = i as f32 / (resolution) as f32;
            let x = x * 2.0 * PI - PI;
            points.push(Vec3::new(
                f32::cos(x) - 0.6 / (1.0 + 9.0 * x * x),
                f32::sin(x),
                0.0,
            ));
        }
        let indices = planar_convex_hull(&points);
        let mut line_indices = Vec::new();
        for i in 0..indices.len() - 1
        {
            let pair = [indices[i], indices[i + 1]];
            line_indices.push(pair);
        }
        fs::create_dir_all("./tmp").unwrap();
        ObjData::export(&points, "tmp/planar_points.obj");
        ObjData::export(&(&points, &line_indices), "tmp/planar_hull.obj");
    }
}
