#[cfg(not(feature = "rustgpu"))]
mod not_rustgpu
{
    pub use std::fmt::Debug;

    pub use algebra::*;
}

#[cfg(not(feature = "rustgpu"))]
use not_rustgpu::*;

#[cfg(not(feature = "rustgpu"))]
pub trait FeatureDependentTraits: Debug + Default + Clone {}
#[cfg(not(feature = "rustgpu"))]
impl<T: Debug + Default + Clone> FeatureDependentTraits for T {}

#[cfg(feature = "rustgpu")]
pub trait FeatureDependentTraits {}
#[cfg(feature = "rustgpu")]
impl<T> FeatureDependentTraits for T {}

use crate::*;

const EMPTY_NODE: u32 = u32::MAX;

pub trait VoctreeLike<T: FeatureDependentTraits>
{
    fn node_at_mut(&mut self, index: usize) -> &mut Node<T>;

    fn node_at(&self, index: usize) -> &Node<T>;

    fn corner_position(&self) -> Vec3;

    fn box_width(&self) -> f32;

    fn tree_depth(&self) -> u32;

    fn add_node(&mut self) -> u32;

    fn current_nodes(&self) -> u32;

    fn maximum_nodes(&self) -> u32;
}

/// Result of a walk query in the SVO.
pub struct WalkResult
{
    /// Whether a node was hit in the query.
    pub was_hit: bool,
    /// Id of the first node hit.
    pub hit_node: u32,
    /// The min corner of the hit node.
    pub hit_corner: Vec3,
    pub iter_count: u32,
}

pub trait VoctreeBehaviour<N, V>
where
    N: Clone + FeatureDependentTraits,
{
    unsafe fn add_node_recursively(&mut self, point: Vec3, data: N);

    unsafe fn walk_tree<F>(
        &self,
        query: F,
        permutation_mask: u32,
        max_level: u32,
    ) -> WalkResult
    where
        F: Fn(BoxQuery) -> bool;
}

impl<N, V: VoctreeLike<N>> VoctreeBehaviour<N, V> for V
where
    N: Clone + FeatureDependentTraits,
{
    unsafe fn add_node_recursively(&mut self, point: Vec3, data: N)
    {
        let mut current_node_id = 0;
        let mut corner = self.corner_position();
        let mut size = self.box_width();

        let mut level = 0;
        loop
        {
            level += 1;
            if level >= self.tree_depth()
            {
                break;
            }

            let query = BoxQuery {
                box_position: corner,
                box_dimension: size,
            };
            let is_inside = is_inside_box(&point, &query);
            if !is_inside
            {
                continue;
            }

            let child_index = position_to_child_index(
                &point,
                BoxQuery {
                    box_position: corner,
                    box_dimension: size,
                },
            );

            size = size / 2.;
            let child_offset = index_to_vec(child_index);
            corner = corner + child_offset * size;

            let child_id = self
                .node_at_mut(current_node_id)
                .child(child_index as usize);

            // The node has already been set.
            if child_id != EMPTY_NODE
            {
                self.node_at_mut(current_node_id)
                    .set_child(child_index as usize, child_id);

                current_node_id = child_id as usize;
                continue;
            }

            // We are exceeding the total number of nodes, skip.
            if current_node_id >= self.maximum_nodes() as _
            {
                return;
            }

            let prior_node_id = current_node_id;
            current_node_id = self.add_node() as usize;

            self.node_at_mut(prior_node_id)
                .set_child(child_index as usize, current_node_id as u32);

            self.node_at_mut(current_node_id).data = data.clone();
        }
    }

    unsafe fn walk_tree<F>(
        &self,
        // A function like object which tests whether a given child is to be
        // expanded.
        query: F,
        // A permutation to change the order in which the children are visited.
        permutation_mask: u32,
        max_level: u32,
    ) -> WalkResult
    where
        F: Fn(BoxQuery) -> bool,
    {
        let mut stack =
            [StackValue::new(0, 0, &self.corner_position()); TREE_WALK_STACK_SIZE];
        let mut stack_count = 1;

        let mut iter_count = 0;
        while stack_count > 0
        {
            iter_count += 1;
            stack_count -= 1;
            let StackValue {
                node_id: current_node_id,
                level,
                corner,
            } = *index_stack(&mut stack, stack_count);

            let size = self.box_width() / 2_f32.powf(level as f32);
            let is_valid = query(BoxQuery {
                box_position: corner,
                box_dimension: size,
            });

            if !is_valid
            {
                continue;
            }

            if level == max_level - 1
            {
                return WalkResult {
                    was_hit: true,
                    hit_node: current_node_id,
                    hit_corner: corner,
                    iter_count: iter_count - 1,
                };
            }

            let mut index = 0;
            loop
            {
                index += 1;
                if index > 8
                {
                    break;
                }

                let permuted_index = permute_index((index - 1) as u32, permutation_mask);
                let child_id = self
                    .node_at(current_node_id as usize)
                    .child(permuted_index as usize);
                if child_id == u32::MAX
                {
                    continue;
                }

                let logical_offset = index_to_vec(permuted_index);

                // Push to stack.
                *index_stack(&mut stack, stack_count) = StackValue {
                    node_id: child_id,
                    level: level + 1,
                    corner: corner + (logical_offset * size / 2.),
                };
                stack_count += 1;
            }
        }

        WalkResult {
            was_hit: false,
            hit_node: u32::MAX,
            hit_corner: Vec3::default(),
            iter_count: iter_count - 1,
        }
    }
}

fn index_stack(
    stack: &mut [StackValue; TREE_WALK_STACK_SIZE],
    index: usize,
) -> &mut StackValue
{
    #[cfg(not(feature = "rustgpu"))]
    {
        &mut stack[index]
    }

    #[cfg(feature = "rustgpu")]
    {
        unsafe { stack.index_unchecked_mut(index) }
    }
}

/// Compute the bit mask permutation of a direction. Useful for efficient
/// ray-tracing as it determines what the order in which children of an SVO
/// should be in a few bit flips.
#[inline]
pub fn direction_permutation(dir: &Vec3) -> u32
{
    let mut res = 0;
    res |= ((dir.x > 0.) as u32) << 0;
    res |= ((dir.y > 0.) as u32) << 1;
    res |= ((dir.z > 0.) as u32) << 2;

    res
}
