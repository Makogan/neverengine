#![allow(dead_code)]

use std::ffi::{c_void, CStr, CString};

use ash::vk::{self, *};
use dem_core::{gpu_api::*, rendering::*};
use log::error;
use paste::paste;
use termcolor::WriteColor;

use crate::tools::vk_to_string;

// +| Configuration |+ =========================================================

const VALIDATION_LAYERS: &'static [&str] = &["VK_LAYER_KHRONOS_validation"];

const INSTANCE_EXTENSIONS: &'static [&str] = &[
    "VK_EXT_debug_utils",
    "VK_KHR_get_physical_device_properties2",
];

const DEVICE_EXTENSIONS: &'static [&str] = &[
    "VK_KHR_buffer_device_address",
    "VK_KHR_dynamic_rendering",
    "VK_KHR_pipeline_library",
    "VK_KHR_swapchain",
    "VK_KHR_vulkan_memory_model",
    // "VK_EXT_conservative_rasterization",
    "VK_EXT_extended_dynamic_state",
    "VK_EXT_graphics_pipeline_library",
];

const VALIDATION_MESSAGE_WHITELIST: &'static [i32] = &[
    4, // Swapchain out of date validation error.
];

// +| Public |+ ================================================================
pub(crate) struct HardwareInterface
{
    pub(crate) entry: ash::Entry,
    pub(crate) instance: ash::Instance,
    pub(crate) surface: vk::SurfaceKHR,
    pub(crate) physical_device: vk::PhysicalDevice,
    pub(crate) device: ash::Device,
    pub(crate) command_pool: vk::CommandPool,
    pub(crate) command_buffer: vk::CommandBuffer,

    pub(crate) debug_utils_messenger: DebugUtilsMessengerEXT,

    pub(crate) queue_family: u32,
    pub(crate) graphics_queue: vk::Queue,
    pub(crate) compute_queue: vk::Queue,
}

impl HardwareInterface
{
    pub(crate) fn get_surface_format(&self) -> SurfaceFormatKHR
    {
        let formats = unsafe {
            let formats = ash::khr::surface::Instance::new(&self.entry, &self.instance)
                .get_physical_device_surface_formats(self.physical_device, self.surface)
                .expect("Could not load surface formats.");
            formats
        };

        for format in &formats
        {
            if format.format == vk::Format::B8G8R8A8_UNORM
                && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
            {
                return *format;
            }
        }

        debug_assert!(formats.len() > 0);
        return formats[0];
    }

    pub(crate) fn start_cmd_update(&mut self) -> vk::CommandBuffer
    {
        unsafe {
            let begin_info = vk::CommandBufferBeginInfo::default();

            self.device
                .begin_command_buffer(self.command_buffer, &begin_info)
                .expect("Faield to begin render update.");
        };
        return self.command_buffer;
    }

    pub(crate) fn end_cmd_update(&self, cmd_buffer: vk::CommandBuffer)
    {
        unsafe {
            self.device
                .end_command_buffer(cmd_buffer)
                .expect("Could not end command bbuffer.");
        }
    }

    pub(crate) fn begin_single_time_command(&self) -> vk::CommandBuffer
    {
        let create_info = vk::CommandBufferAllocateInfo {
            command_pool: self.command_pool,
            level: vk::CommandBufferLevel::PRIMARY,
            command_buffer_count: 1,
            ..Default::default()
        };
        let transient_cmd_buffer = unsafe {
            self.device
                .allocate_command_buffers(&create_info)
                .expect("Could not allocate single time command buffer.")[0]
        };

        let begin_info = vk::CommandBufferBeginInfo {
            flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            ..Default::default()
        };
        unsafe {
            self.device
                .begin_command_buffer(transient_cmd_buffer, &begin_info)
                .expect("Could not begin single time commands.");
        }

        return transient_cmd_buffer;
    }

    pub(crate) fn end_single_time_command(&self, command_buffer: vk::CommandBuffer)
    {
        unsafe {
            self.device
                .end_command_buffer(command_buffer)
                .expect("Could not end single time command buffer.");
        }

        let create_info = vk::FenceCreateInfo {
            ..Default::default()
        };
        let fence = unsafe {
            self.device
                .create_fence(&create_info, None)
                .expect("Could not create single time command fence.")
        };

        let submit_info = [vk::SubmitInfo {
            command_buffer_count: 1,
            p_command_buffers: &command_buffer,
            ..Default::default()
        }];

        unsafe {
            self.device
                .queue_submit(self.compute_queue, &submit_info, fence)
                .expect("Could not submit single time command to compute queue.");

            self.device
                .wait_for_fences(&[fence], true, u64::MAX)
                .expect("Failed to wait for single time command fence.");

            self.device
                .queue_wait_idle(self.compute_queue)
                .expect("Idle wait for compute queue failed.");

            self.device
                .free_command_buffers(self.command_pool, &[command_buffer]);
            self.device.destroy_fence(fence, None);
        }
    }

    pub(crate) fn create_semaphores(&self, semaphore_num: u32) -> Vec<vk::Semaphore>
    {
        let mut semaphores = Vec::<vk::Semaphore>::with_capacity(semaphore_num as usize);
        let semaphore_info = vk::SemaphoreCreateInfo::default();
        for _ in 0..semaphore_num
        {
            let semaphore = unsafe {
                self.device
                    .create_semaphore(&semaphore_info, None)
                    .expect("Could not creat semaphore")
            };
            semaphores.push(semaphore);
        }

        return semaphores;
    }

    pub(crate) fn create_fences(&self, fence_num: u32) -> Vec<vk::Fence>
    {
        let mut fences = Vec::<vk::Fence>::with_capacity(fence_num as usize);
        let fence_info = vk::FenceCreateInfo {
            flags: vk::FenceCreateFlags::SIGNALED,
            ..Default::default()
        };
        for _ in 0..fence_num
        {
            let fence = unsafe {
                self.device
                    .create_fence(&fence_info, None)
                    .expect("Could not create fence.")
            };
            fences.push(fence);
        }

        return fences;
    }

    pub(crate) fn new<T>(monitor: &T) -> HardwareInterface
    where
        T: AbstractRenderWindow,
    {
        let entry = unsafe { ash::Entry::load().unwrap() };

        let instance = create_instance(&entry, monitor);

        let surface = create_surface(&instance, monitor);
        let physical_device = pick_physical_device(
            &entry,
            &instance,
            &surface,
            &DEVICE_EXTENSIONS.to_vec(),
        );

        let (device, queue_family_index) = create_logical_device(
            &instance,
            &physical_device,
            &DEVICE_EXTENSIONS.to_vec(),
        );

        let device_name = unsafe {
            instance
                .get_physical_device_properties(physical_device)
                .device_name
        };

        let debug_utils_messenger = create_debug_messenger(&entry, &instance);

        let physical_device_name = vk_to_string(&device_name);
        physical_device.set_name(&device, &instance, &physical_device_name);

        let logical_device_name =
            format!("{} {}", "Logical device from", physical_device_name);

        device
            .handle()
            .set_name(&device, &instance, &logical_device_name);

        let queue_properties = unsafe {
            instance.get_physical_device_queue_family_properties(physical_device)
                [queue_family_index as usize]
        };

        let compute_queue_index: u32 = if queue_properties.queue_count >= 2
        {
            1
        }
        else
        {
            0
        };
        let graphics_queue = unsafe { device.get_device_queue(queue_family_index, 0) };
        let compute_queue =
            unsafe { device.get_device_queue(queue_family_index, compute_queue_index) };

        let command_pool = create_command_pool(&device, queue_family_index);
        let command_buffer = create_command_buffer(&device, &command_pool);

        return HardwareInterface {
            entry,
            instance,
            surface,
            physical_device,
            device,
            command_pool,

            debug_utils_messenger,

            queue_family: queue_family_index,
            graphics_queue,
            compute_queue,
            command_buffer,
        };
    }

    pub(crate) fn draw(&self, inputs: &GraphicsInput, modifiers: &GraphicsInputModifiers)
    {
        let index_buffer = vk::Buffer::from_raw(inputs.index_buffer.to_raw());
        let instance_buffer = vk::Buffer::from_raw(modifiers.instance_buffer.to_raw());

        let vert_buffer_count = inputs.buffers.len();
        let offsets: Vec<vk::DeviceSize> = vec![0; vert_buffer_count];

        let instance_buffer_binding_point = vert_buffer_count;
        if modifiers.instance_count > 0
        {
            unsafe {
                self.device.cmd_bind_vertex_buffers(
                    self.command_buffer,
                    instance_buffer_binding_point as u32,
                    &[instance_buffer],
                    &offsets,
                );
            }
        }

        let instance_count = 1;
        let vertex_buffer_first_binding_point = 0;
        unsafe {
            // If there are no input buffers we might be doing an input less render
            // for example a raytracing screen quad.
            if !inputs.buffers.is_empty()
            {
                self.device.cmd_bind_vertex_buffers(
                    self.command_buffer,
                    vertex_buffer_first_binding_point as u32,
                    std::slice::from_raw_parts(
                        inputs.buffers.as_ptr() as *const vk::Buffer,
                        inputs.buffers.len(),
                    ),
                    &offsets,
                );
            }

            let first_instance = 0;
            // If the index buffer is 0 it was not allocated, so we don't use the index
            // buffer.
            if index_buffer.as_raw() != 0
            {
                self.device.cmd_bind_index_buffer(
                    self.command_buffer,
                    index_buffer,
                    modifiers.index_offset as u64,
                    vk::IndexType::UINT32,
                );

                self.device.cmd_draw_indexed(
                    self.command_buffer,
                    modifiers.element_count as u32,
                    instance_count as u32,
                    0,
                    modifiers.vertex_offset as i32,
                    first_instance as u32,
                );
            }
            else
            {
                self.device.cmd_draw(
                    self.command_buffer,
                    modifiers.element_count as u32,
                    instance_count as u32,
                    modifiers.vertex_offset as u32,
                    first_instance as u32,
                );
            }
        }; // unsafe
    }

    pub(crate) fn transition_vk_image(
        &self,
        image: &vk::Image,
        old_layout: vk::ImageLayout,
        new_layout: vk::ImageLayout,
        access_mask: vk::AccessFlags,
        source_stage: vk::PipelineStageFlags,
        destination_stage: vk::PipelineStageFlags,
    )
    {
        let barrier = vk::ImageMemoryBarrier {
            old_layout,
            new_layout,
            dst_access_mask: access_mask,
            src_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            image: *image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            },
            ..Default::default()
        };

        let cmd = self.begin_single_time_command();
        unsafe {
            self.device.cmd_pipeline_barrier(
                cmd,
                source_stage,
                destination_stage,
                vk::DependencyFlags::default(),
                &[],
                &[],
                &[barrier],
            );
        }
        self.end_single_time_command(cmd);
    }
}

impl Drop for HardwareInterface
{
    fn drop(&mut self)
    {
        unsafe {
            self.device.destroy_command_pool(self.command_pool, None);
            self.device.destroy_device(None);
            let surface_instance =
                ash::khr::surface::Instance::new(&self.entry, &self.instance);
            surface_instance.destroy_surface(self.surface, None);
            let debug_instance =
                ash::ext::debug_utils::Instance::new(&self.entry, &self.instance);
            debug_instance
                .destroy_debug_utils_messenger(self.debug_utils_messenger, None);
        }
    }
}

// +| Internal |+ ==============================================================
pub fn create_instance<T>(vk_entry: &ash::Entry, monitor: &T) -> ash::Instance
where
    T: AbstractRenderWindow,
{
    let instance_extensions = request_instance_extensions(vk_entry, monitor);

    let layers_are_available = check_available_layers(vk_entry);

    let enabled = vk::ValidationFeatureEnableEXT::BEST_PRACTICES;
    let mut features = vk::ValidationFeaturesEXT::default();
    features.enabled_validation_feature_count = 0;
    features.p_enabled_validation_features = &enabled;

    let app_info = vk::ApplicationInfo {
        s_type: vk::StructureType::APPLICATION_INFO,
        p_next: ::std::ptr::null(),
        p_application_name: env!("CARGO_PKG_NAME").as_ptr() as *const i8,
        application_version: vk::make_api_version(
            0,
            env!("CARGO_PKG_VERSION_MAJOR")
                .parse()
                .expect("Could not find Major version."),
            env!("CARGO_PKG_VERSION_MINOR")
                .parse()
                .expect("Could not find Minor verison."),
            env!("CARGO_PKG_VERSION_PATCH")
                .parse()
                .expect("Could not find Patch version."),
        ),
        p_engine_name: env!("CARGO_PKG_NAME").as_ptr() as *const i8,
        engine_version: vk::make_api_version(
            0,
            env!("CARGO_PKG_VERSION_MAJOR")
                .parse()
                .expect("Could not find Major version."),
            env!("CARGO_PKG_VERSION_MINOR")
                .parse()
                .expect("Could not find Minor verison."),
            env!("CARGO_PKG_VERSION_PATCH")
                .parse()
                .expect("Could not find Patch version."),
        ),
        api_version: vk::make_api_version(0, 1, 3, 0),
        ..Default::default()
    };

    let layers: Vec<CString> = VALIDATION_LAYERS
        .iter()
        .map(|layer_name| CString::new(layer_name.to_string()).unwrap())
        .collect();

    let mut raw_layers: Vec<_> = layers
        .iter()
        .map(|layer_name| layer_name.as_ptr())
        .collect();
    if !layers_are_available
    {
        raw_layers.clear();
    }

    let extensions: Vec<CString> = instance_extensions
        .iter()
        .map(|extension_name| CString::new(extension_name.to_string()).unwrap())
        .collect();

    let raw_extensions: Vec<_> = extensions
        .iter()
        .map(|extension| extension.as_ptr())
        .collect();

    let mut create_info = vk::InstanceCreateInfo::default();
    create_info.p_next = (&features) as *const _ as *const c_void;
    create_info.p_application_info = &app_info;
    create_info.enabled_layer_count = raw_layers.len() as u32;
    create_info.pp_enabled_layer_names = raw_layers.as_ptr() as _;
    create_info.enabled_extension_count = raw_extensions.len() as u32;
    create_info.pp_enabled_extension_names = raw_extensions.as_ptr() as _;

    let instance = unsafe {
        vk_entry.create_instance(&create_info, None).expect(
            format!(
                "Failed to create instance from layers {:?}.",
                VALIDATION_LAYERS
            )
            .as_str(),
        )
    };

    return instance;
}

fn create_surface<T>(instance: &ash::Instance, monitor: &T) -> vk::SurfaceKHR
where
    T: AbstractRenderWindow,
{
    let instance_handle: u64 = vk::Handle::as_raw(instance.handle());

    let surface_handle = monitor.create_surface(instance_handle);

    return vk::SurfaceKHR::from_raw(surface_handle);
}

fn pick_physical_device(
    entry: &ash::Entry,
    instance: &ash::Instance,
    surface: &vk::SurfaceKHR,
    requested_device_extensions: &Vec<&str>,
) -> vk::PhysicalDevice
{
    let available_devices = unsafe {
        instance
            .enumerate_physical_devices()
            .expect("Could not enumerate physical devices.")
    };
    debug_assert!(
        available_devices.len() > 0,
        "No available physical devices."
    );

    let mut suitable_devices = find_all_suitable_devices(
        entry,
        instance,
        surface,
        &available_devices,
        requested_device_extensions,
    );

    suitable_devices.sort_by(|d1, d2| {
        let t1 = unsafe { instance.get_physical_device_properties(**d1).device_type };
        let t2 = unsafe { instance.get_physical_device_properties(**d2).device_type };

        let mut v1 = t1.as_raw();
        let mut v2 = t2.as_raw();

        // Permute enumerator values such that discrete GPUs become the maximum.
        // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPhysicalDeviceType.html
        v1 = (v1 + 2) % 5;
        v2 = (v2 + 2) % 5;

        return v1.cmp(&v2);
    });

    return **suitable_devices
        .last()
        .expect("Could not find a single device that supports all extensions.");
}

fn create_logical_device(
    instance: &ash::Instance,
    physical_device: &vk::PhysicalDevice,
    requested_device_extensions: &Vec<&str>,
) -> (ash::Device, u32)
{
    let mut dynamic_rendering_features =
        ash::vk::PhysicalDeviceDynamicRenderingFeatures::default();

    let mut buffer_device_address_features =
        ash::vk::PhysicalDeviceBufferDeviceAddressFeatures::default();
    let mut indexing_features = vk::PhysicalDeviceDescriptorIndexingFeatures::default();
    indexing_features.shader_sampled_image_array_non_uniform_indexing = true as u32;
    indexing_features.runtime_descriptor_array = true as u32;
    indexing_features.descriptor_binding_variable_descriptor_count = true as u32;
    indexing_features.descriptor_binding_partially_bound = true as u32;

    let mut vulkan_memory_model = vk::PhysicalDeviceVulkanMemoryModelFeatures::default();

    let mut graphics_pipeline_library =
        vk::PhysicalDeviceGraphicsPipelineLibraryFeaturesEXT::default();

    let mut features2 = vk::PhysicalDeviceFeatures2::default()
        .push_next(&mut buffer_device_address_features)
        .push_next(&mut dynamic_rendering_features)
        .push_next(&mut indexing_features)
        .push_next(&mut vulkan_memory_model)
        .push_next(&mut graphics_pipeline_library);

    unsafe { instance.get_physical_device_features2(*physical_device, &mut features2) };

    let extensions: Vec<CString> = requested_device_extensions
        .iter()
        .map(|extension_name| CString::new(extension_name.to_string()).unwrap())
        .collect();

    let raw_extensions: Vec<_> = extensions
        .iter()
        .map(|extension| extension.as_ptr())
        .collect();

    let queue_family_index = find_queue_family_index_that_supports_graphics_and_compute(
        instance,
        &physical_device,
    );
    debug_assert!(queue_family_index >= 0, "Queue family not found.");
    let queue_family_index = queue_family_index as u32;

    let queue_properties = unsafe {
        instance.get_physical_device_queue_family_properties(*physical_device)
            [queue_family_index as usize]
    };

    // Ideally we want two queues, but we will settle for one if not possible.
    let queue_count = std::cmp::min(2, queue_properties.queue_count);
    let queue_priorities = [1.0, 0.0];
    let queue_create_info = vk::DeviceQueueCreateInfo {
        queue_family_index,
        queue_count,
        p_queue_priorities: queue_priorities.as_ptr(),
        ..Default::default()
    };

    // The enabledLayerCount and ppEnabledLayerNames parameters are deprecated,
    // they should always be 0 and nullptr.
    // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkDeviceCreateInfo.html
    let create_info = vk::DeviceCreateInfo {
        p_next: &mut features2 as *const _ as *const c_void,
        queue_create_info_count: 1,
        p_queue_create_infos: &queue_create_info,
        enabled_extension_count: raw_extensions.len() as u32,
        pp_enabled_extension_names: raw_extensions.as_ptr(),
        ..Default::default()
    };

    return unsafe {
        let device = instance
            .create_device(*physical_device, &create_info, None)
            .expect("Failed to create logical device.");

        (device, queue_family_index)
    };
}

fn create_command_pool(device: &ash::Device, queue_family_index: u32) -> vk::CommandPool
{
    let pool_info = vk::CommandPoolCreateInfo {
        flags: CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        queue_family_index,
        ..Default::default()
    };
    unsafe {
        let pool = device
            .create_command_pool(&pool_info, None)
            .expect("Failed to create command pool.");
        return pool;
    };
}

fn create_command_buffer(
    device: &ash::Device,
    command_pool: &vk::CommandPool,
) -> vk::CommandBuffer
{
    let create_info = vk::CommandBufferAllocateInfo {
        command_buffer_count: 1,
        command_pool: *command_pool,
        level: vk::CommandBufferLevel::PRIMARY,
        ..Default::default()
    };

    unsafe {
        let cmd_buffer = device
            .allocate_command_buffers(&create_info)
            .expect("Coult not allocate command buffer.")[0];

        return cmd_buffer;
    };
}

fn find_all_suitable_devices<'a>(
    entry: &ash::Entry,
    instance: &ash::Instance,
    surface: &vk::SurfaceKHR,
    available_devices: &'a Vec<vk::PhysicalDevice>,
    requested_device_extensions: &Vec<&str>,
) -> Vec<&'a vk::PhysicalDevice>
{
    let mut suitable_devices = Vec::<&vk::PhysicalDevice>::new();
    for physical_device in available_devices
    {
        let queue_family_index: i32 =
            find_queue_family_index_that_supports_graphics_and_compute(
                instance,
                &physical_device,
            );

        let surface_is_supported = unsafe {
            let surface_instance = ash::khr::surface::Instance::new(&entry, &instance);
            surface_instance
                .get_physical_device_surface_support(
                    *physical_device,
                    queue_family_index as u32,
                    *surface,
                )
                .expect("Failed to query surface support.")
        };

        let extensions_are_supported = device_extensions_are_supported(
            instance,
            &physical_device,
            requested_device_extensions,
        );

        if queue_family_index < 0
        {
            continue;
        }
        if !surface_is_supported
        {
            continue;
        }
        if !extensions_are_supported
        {
            continue;
        }

        suitable_devices.push(physical_device);
    }

    return suitable_devices;
}

fn device_extensions_are_supported(
    instance: &ash::Instance,
    physical_device: &vk::PhysicalDevice,
    requested_extensions: &Vec<&str>,
) -> bool
{
    let device_properties = unsafe {
        instance
            .enumerate_device_extension_properties(*physical_device)
            .expect("Failed to enumerate device extension properties.")
    };

    for required_extension in requested_extensions
    {
        let mut extension_found = false;
        for device_properties in &device_properties
        {
            let name =
                unsafe { CStr::from_ptr(device_properties.extension_name.as_ptr()) };

            if *(*required_extension) == *name.to_owned().to_str().unwrap()
            {
                extension_found = true;
                break;
            }
        }

        if !extension_found
        {
            error!("not found {:?}", required_extension);
            return false;
        }
    }

    return true;
}

fn find_queue_family_index_that_supports_graphics_and_compute(
    instance: &ash::Instance,
    physical_device: &vk::PhysicalDevice,
) -> i32
{
    let queue_fam_properties =
        unsafe { instance.get_physical_device_queue_family_properties(*physical_device) };

    for (queue_family_index, q_property) in queue_fam_properties.iter().enumerate()
    {
        let mask = q_property.queue_flags;
        if mask.contains(vk::QueueFlags::GRAPHICS)
            && mask.contains(vk::QueueFlags::COMPUTE)
        {
            return queue_family_index as i32;
        }
    }
    return -1;
}

fn check_available_layers(vk_entry: &ash::Entry) -> bool
{
    let result = unsafe { vk_entry.enumerate_instance_layer_properties() };
    if result.is_err()
    {
        error!("Failed to enumerate Instance Layer properties.");
        return false;
    }

    let supported_layers = result.unwrap();
    for required_layer in VALIDATION_LAYERS
    {
        let mut layer_found = false;
        for supported_layer in &supported_layers
        {
            let name = unsafe { CStr::from_ptr(supported_layer.layer_name.as_ptr()) };
            if *required_layer == name.to_owned().to_str().unwrap()
            {
                layer_found = true;
            }
        }

        if !layer_found
        {
            error!("Could not find layer {}.", required_layer);
            return false;
        }
    }

    return true;
}

fn request_instance_extensions<T>(vk_entry: &ash::Entry, monitor: &T) -> Vec<String>
where
    T: AbstractRenderWindow,
{
    let mut required_extension_strings = Vec::<String>::new();
    for extension in INSTANCE_EXTENSIONS
    {
        required_extension_strings.push(extension.to_string());
    }
    for extension in monitor.get_required_vulkan_extensions()
    {
        required_extension_strings.push(extension.to_string());
    }

    let supported_extensions = unsafe {
        vk_entry
            .enumerate_instance_extension_properties(None)
            .expect("Could not enuemrate Instance Extension properties.")
    };

    debug_assert!(supported_extensions.len() > 0);

    for extension in &required_extension_strings
    {
        let mut found = false;
        for extension_property in &supported_extensions
        {
            let name =
                unsafe { CStr::from_ptr(extension_property.extension_name.as_ptr()) };
            if name.to_owned().to_str().unwrap() == extension
            {
                found = true;
                break;
            }
        }

        debug_assert!(found, "Could not find required extension {}.", extension);
    }

    return required_extension_strings;
}

fn create_debug_messenger(
    entry: &ash::Entry,
    instance: &ash::Instance,
) -> DebugUtilsMessengerEXT
{
    let mut create_info = vk::DebugUtilsMessengerCreateInfoEXT::default();
    create_info.message_severity = vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
        | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
        | vk::DebugUtilsMessageSeverityFlagsEXT::INFO;
    create_info.message_type = vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
        | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION;
    create_info.pfn_user_callback = Some(vulkan_debug_utils_callback);

    unsafe {
        let debug_instance = ash::ext::debug_utils::Instance::new(&entry, &instance);
        let messenger = debug_instance
            .create_debug_utils_messenger(&create_info, None)
            .expect("Could not create debug utils messsenger");
        return messenger;
    }
}

unsafe extern "system" fn vulkan_debug_utils_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> vk::Bool32
{
    let severity = match message_severity
    {
        vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => "[Verbose]",
        vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => "[Warning]",
        vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => "[Error]",
        vk::DebugUtilsMessageSeverityFlagsEXT::INFO => "[Info]",
        _ => "[Unknown]",
    };
    let types = match message_type
    {
        vk::DebugUtilsMessageTypeFlagsEXT::GENERAL => "[General]",
        vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE => "[Performance]",
        vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION => "[Validation]",
        _ => "[Unknown]",
    };

    let mut stdout = termcolor::StandardStream::stderr(termcolor::ColorChoice::Always);

    let message = CStr::from_ptr((*p_callback_data).p_message);
    let msg_id = (*p_callback_data).message_id_number;

    if VALIDATION_MESSAGE_WHITELIST.contains(&msg_id)
    {
        return vk::FALSE;
    }

    match message_severity
    {
        vk::DebugUtilsMessageSeverityFlagsEXT::INFO
        | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
        | vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE =>
        {
            stdout
                .set_color(
                    termcolor::ColorSpec::new().set_fg(Some(termcolor::Color::Yellow)),
                )
                .unwrap();

            if cfg!(debug_assertions)
            {
                error!(
                    "\n[Debug]{}{}\nmsg id:{:?}\n{:?}",
                    severity, types, msg_id, message
                );
            }
        }
        vk::DebugUtilsMessageSeverityFlagsEXT::ERROR =>
        {
            stdout
                .set_color(
                    termcolor::ColorSpec::new().set_fg(Some(termcolor::Color::Red)),
                )
                .unwrap();

            debug_assert!(
                false,
                "\n[Debug]{}{}\nmsg id:{:?}\n{:?}",
                severity, types, msg_id, message
            );
        }
        _ => todo!(),
    }

    stdout
        .set_color(termcolor::ColorSpec::new().set_fg(Some(termcolor::Color::White)))
        .unwrap();

    return vk::FALSE;
}

pub(crate) trait SetVulkanName<T>
{
    fn set_name(&self, device: &ash::Device, instance: &ash::Instance, name: &String);
}

macro_rules! ImplVulkanDebug {
    ($kind : ty) => {
        paste! {
        impl SetVulkanName<$kind> for $kind
        {
            fn set_name(
                &self, device : &ash::Device, instance: &ash::Instance, name : &String)
            {
                let raw_string = CString::new(name.to_owned()).unwrap();
                let name_info =
                    vk::DebugUtilsObjectNameInfoEXT
                    {
                        object_type : vk::ObjectType::[<$kind : snake : upper >],
                        object_handle : self.as_raw(),
                        p_object_name : raw_string.as_ptr(),
                        ..Default::default()
                    };
                unsafe {
                    let debug_device = ash::ext::debug_utils::Device::new(instance, device);
                    debug_device.set_debug_utils_object_name(&name_info)
                        .expect("Could not set name of vulkan struct.");
                };
            }
        }
        }
    };
}

ImplVulkanDebug!(Device);
ImplVulkanDebug!(PhysicalDevice);
ImplVulkanDebug!(Queue);
ImplVulkanDebug!(ShaderModule);
