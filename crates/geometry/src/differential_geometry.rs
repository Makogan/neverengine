use std::{
    fmt::Debug,
    ops::{AddAssign, Mul},
};

use crate::sparse_vector;
use dem_log::{dem_debug_assert, error};
use faer::{prelude::SpSolver, sparse::*};
use linear_isomorphic::*;

/// Compute a derivative for a differentiable type (e.g. a `Vec3` parametric
/// curve) at some parameter $x$ assumed to be in its domain. Basically just
/// $\frac{f(x+ h) - f(x-h)}{2 h}$. i.e. the central difference with 2
/// points.
pub fn numerical_derivative<T, F>(x: f32, f: &F) -> T
where
    T: ArithmeticType<f32>,
    f32: Mul<T, Output = T>,
    F: Fn(f32) -> T,
{
    // Consider computing epsilons as:
    //https://scicomp.stackexchange.com/questions/42489/computing-numerical-derivatives/42491#42491

    let mul = <f32 as Mul<f32>>::mul;
    let h = mul(1e-3, f32::max(f32::abs(x), 1.0));
    // Convoluted but rustc fails to understand 2.0 * h.
    let denom = h + h;
    let num = f(x + h) - f(x - h);

    // Convoluted but rustc fails to understand num / denom.
    num * (1.0 / denom)
}

/// Compute the tangent vector for some vector function (scalar functions are
/// vector functions) at parameter x. Result is *not* normalized.
pub fn tangent<T, F>(x: f32, f: &F) -> T
where
    T: ArithmeticType<f32> + std::fmt::Debug,
    f32: Mul<T, Output = T>,
    F: Fn(f32) -> T,
{
    numerical_derivative(x, f)
}

/// Computes the Laplace operator for a graph.
/// https://en.wikipedia.org/wiki/Discrete_Laplace_operator#Graph_Laplacians
pub fn graph_laplacian<'a, Vert, Edges, Weights, I, Func, R>(
    vert: &'a Vert,
    edges: Edges,
    weights: Weights,
    f: Func,
) -> R
where
    Vert: Clone,
    Edges: Fn(&'a Vert) -> I,
    I: Iterator<Item = Vert>,
    Weights: Fn(&Vert, &'a Vert, usize) -> f32,
    Func: Fn(&Vert) -> R,
    R: VectorSpace<Scalar = f32> + Default + AddAssign,
{
    let mut result = R::default();
    for (i, n) in edges(vert).enumerate() {
        result += (f(&n) - f(vert)) * weights(&n, vert, i);
    }

    result
}

pub fn plain_graph_laplacian<'a, Vert, Edges, I, Func, R>(
    vert: &'a Vert,
    edges: Edges,
    f: Func,
) -> R
where
    Vert: Clone,
    Edges: Fn(&'a Vert) -> I,
    I: Iterator<Item = Vert>,
    Func: Fn(&Vert) -> R,
    R: VectorSpace<Scalar = f32> + Default + AddAssign,
{
    graph_laplacian(vert, edges, |_, _, _| 1.0, f)
}

/// Computes the cotan weights for the triangle mesh laplace operator.
pub fn laplacian_cotan_weights<V>(v_i: &V, v_j: &V, o1: &V, o2: &V) -> f32
where
    V: InnerSpace<f32> + Debug,
{
    let cos_alpha = (v_i.clone() - o1.clone())
        .normalized()
        .dot(&(v_j.clone() - o1.clone()).normalized());
    let cos_beta = (v_i.clone() - o2.clone())
        .normalized()
        .dot(&(v_j.clone() - o2.clone()).normalized());

    let cot_alpha = cos_alpha / (1.0 - cos_alpha * cos_alpha).sqrt();
    let cot_beta = cos_beta / (1.0 - cos_beta * cos_beta).sqrt();

    let res = 0.5 * (cot_alpha + cot_beta);

    if res.is_nan() || res.is_infinite() {
        return 0.0;
    }

    debug_assert!(!res.is_nan());
    debug_assert!(!res.is_infinite());

    res
}

fn vertex_ring<'a, Vert, Edges, I>(vert: &'a Vert, edges: &Edges) -> Vec<Vert>
where
    Vert: Clone + VectorSpace<Scalar = f32>,
    Edges: Fn(&'a Vert) -> I,
    I: Iterator<Item = Vert>,
{
    edges(vert).collect()
}

#[inline]
fn opposites<V: VectorSpace<Scalar = f32>>(i: usize, ring: &[V]) -> [V; 2] {
    let n = ring.len();
    let p = (i + n - 1) % n;
    let n = (i + 1) % n;

    [ring[p].clone(), ring[n].clone()]
}

/// Computes a triangle mesh laplacian entry.
pub fn triangle_mesh_laplacian<'a, Vert, Edges, I, Func, R>(
    vert: &'a Vert,
    edges: Edges,
    f: Func,
) -> R
where
    Vert: InnerSpace<f32> + Debug,
    Edges: Fn(&'a Vert) -> I,
    I: Iterator<Item = Vert>,
    Func: Fn(&Vert) -> R,
    R: VectorSpace<Scalar = f32> + Default + AddAssign,
{
    let ring: Vec<Vert> = vertex_ring(vert, &edges);
    let mut area = 0.0;
    for (n, neighbour) in ring.iter().enumerate() {
        let [o1, o2] = opposites(n, &ring);
        area += laplacian_cotan_weights(vert, neighbour, &o1, &o2);
    }

    graph_laplacian(
        vert,
        edges,
        |vert, neighbour, i| {
            let [o1, o2] = opposites(i, &ring);
            laplacian_cotan_weights(vert, &neighbour, &o1, &o2)
        },
        f,
    ) * (1.0 / area)
}

pub enum MeshWeights {
    Cotan,
    Graph,
    Length,
}

/// Computes the Laplacian matrix of an entire mesh.
/// The third return value corresponds to a permutation of the vertex indices.
/// In order to solve the system, boundary/fixed values must be put on high
/// indices and unkown vertices must be put on low indices. The permutation
/// tracks for a given index, to which row of the matrix it was assigned to.
pub fn global_mesh_laplacian<'a, Vert, VertIter, Edges, I, K>(
    verts: VertIter,
    edges: Edges,
    fixed_vert: K,
    weight_kind: MeshWeights,
) -> (
    SparseColMat<usize, f64>,
    SparseColMat<usize, f64>,
    Vec<usize>,
    usize,
)
where
    Vert: Clone + InnerSpace<f32> + Debug + 'a + Copy,
    VertIter: Iterator<Item = Vert> + ExactSizeIterator,
    Edges: Fn(usize) -> I + Clone,
    I: Iterator<Item = (Vert, usize)>,
    K: Fn(usize) -> bool,
{
    let mut unknown_count = 0;
    let mut known_count = 0;

    let vert_count = verts.len();
    let mut index_permutation = vec![usize::MAX; vert_count];
    // Compute a permutation of the vertex indices, with unkown at the beginning and
    // known at the end.
    for v in 0..verts.len() {
        // If known, put it at the end of the indices.
        if fixed_vert(v) {
            known_count += 1;
            index_permutation[v] = vert_count - known_count;
        }
        // If unkown put it at the beginning.
        else {
            index_permutation[v] = unknown_count;
            unknown_count += 1;
        }
    }

    // TODO(low): Consider iterating over the faces instead of the edges.
    let mut faer_laplacian_triplets = Vec::new();
    let mut faer_mass_triplets = Vec::new();
    for (original_vid, vert) in verts.enumerate() {
        let edgs = edges.clone();
        let ring = vertex_ring(&vert, &|_| edgs(original_vid).map(|(d, _)| d));

        let new_vid = index_permutation[original_vid];

        let mut area = 0.0;
        for (c, (neighbour, nid)) in edges(original_vid).enumerate() {
            // Select the weight type to use based on input.
            let wij = match weight_kind {
                MeshWeights::Cotan => {
                    let [o1, o2] = opposites(c, &ring);
                    let wij = laplacian_cotan_weights(&vert, &neighbour, &o1, &o2);
                    area += (vert.clone() - neighbour).cross(&(vert - o1)).norm() / 2.0;
                    wij
                }
                MeshWeights::Graph => {
                    area += 1.0;
                    1.0
                }
                MeshWeights::Length => {
                    let wij = (vert - neighbour).norm();
                    area += wij;
                    wij
                }
            };

            let new_nid = index_permutation[nid];
            debug_assert!(!wij.is_nan());
            debug_assert!(!wij.is_infinite());
            debug_assert!(new_vid != new_nid);

            faer_laplacian_triplets.push((new_vid, new_nid, wij as f64));
            faer_laplacian_triplets.push((new_vid, new_vid, -wij as f64));
        }

        dem_debug_assert!(!area.is_nan(), "");
        dem_debug_assert!(!area.is_infinite(), "");

        dem_debug_assert!(
            area > f32::EPSILON * 10.0,
            "Vertex {} has an area of {} but the allowed minimum is {}.",
            original_vid,
            area,
            f32::EPSILON * 10.0
        );

        faer_mass_triplets.push((new_vid, new_vid, 1.0 / (area as f64 / 3.0)));
    }

    let faer_laplacian = SparseColMat::try_new_from_triplets(
        vert_count,
        vert_count,
        &faer_laplacian_triplets,
    )
    .unwrap();

    let faer_mass =
        SparseColMat::try_new_from_triplets(vert_count, vert_count, &faer_mass_triplets)
            .unwrap();

    (faer_laplacian, faer_mass, index_permutation, unknown_count)
}

// https://en.wikipedia.org/wiki/Conjugate_gradient_method
// pub fn conjugate_gradient(
//     a: &CsrMatrix<f64>,
//     threshold: f64,
//     solution: &mut DVector<f64>,
// ) -> usize
// {
//     let mut r = &*solution - a * &*solution;
//     let mut p = r.clone();
//     let mut iter_count = 0;

//     loop
//     {
//         let alpha = r.dot(&r) / p.dot(&(a * &p));

//         *solution = &*solution + alpha * &p;
//         let r_next = &r - alpha * a * &p;

//         let delta = r_next.norm_squared();
//         debug_assert!(!delta.is_nan());

//         if delta <= threshold
//         {
//             break;
//         }

//         let beta = r_next.dot(&r_next) / r.dot(&r);
//         p = &r_next + beta * &p;
//         r = r_next;

//         iter_count += 1;
//     }

//     iter_count
// }

pub fn symmetrize(a: SparseColMat<usize, f64>) -> SparseColMat<usize, f64> {
    let mut triplets = Vec::new();
    for (r, c, v) in sparse_vector::to_triplets_iter(&a) {
        // Small variations in values makes this solver mistakenly believe the matrix is
        // not symmetric, so use this little hack to make it behave.
        if c < r {
            triplets.push((r, c, v));
            triplets.push((c, r, v));
        } else if c == r {
            triplets.push((r, c, v));
        }
    }

    SparseColMat::try_new_from_triplets(a.nrows(), a.ncols(), &triplets).unwrap()
}

pub fn mesh_laplacian_smoothing<'a, Vert, VertIter, VertMut, Edges, I, K>(
    verts: VertIter,
    mut mutate_vert: VertMut,
    edges: Edges,
    fixed_vert: K,
    order: usize,
    weight_kind: MeshWeights,
) where
    Vert: Clone + InnerSpace<f32> + Debug + 'a + std::marker::Copy,
    VertIter: Iterator<Item = Vert> + ExactSizeIterator + Clone,
    VertMut: FnMut(&Vert, usize),
    Edges: Fn(usize) -> I + Clone,
    I: Iterator<Item = (Vert, usize)>,
    K: Fn(usize) -> bool + Clone,
{
    let (cot_matrix, mass, index_permutation, unknown_count) =
        global_mesh_laplacian(verts.clone(), edges, fixed_vert.clone(), weight_kind);

    let m = &mass * &cot_matrix;
    let mut l = -cot_matrix;
    for _ in 0..order - 1 {
        l = &l * &m;
    }

    let mut solutions = faer::Mat::<f64>::zeros(verts.len() - unknown_count, 3);
    for (vid, vert) in verts.clone().enumerate() {
        if fixed_vert(vid) {
            let i = index_permutation[vid];
            // Set the x, y, z values of the fixed vertices.
            *solutions.get_mut(i - unknown_count, 0) = vert[0] as f64;
            *solutions.get_mut(i - unknown_count, 1) = vert[1] as f64;
            *solutions.get_mut(i - unknown_count, 2) = vert[2] as f64;
        }
    }

    // Copy the matrix subslice.
    let mut unknown_triplets = Vec::new();
    let mut known_triplets = Vec::new();
    for (r, c, v) in sparse_vector::to_triplets_iter(&l) {
        if r < unknown_count && c < unknown_count {
            unknown_triplets.push((r, c, v));
        } else if r < unknown_count {
            known_triplets.push((r, c - unknown_count, v));
        }
    }

    let a = -SparseColMat::try_new_from_triplets(
        unknown_count,
        unknown_count,
        &unknown_triplets,
    )
    .unwrap();

    let r = SparseColMat::try_new_from_triplets(
        unknown_count,
        verts.len() - unknown_count,
        &known_triplets,
    )
    .unwrap();

    // Move all known coefficients to the right hand side.
    solutions = r * solutions;

    // let system = cholesky_factor(&a);
    let system = a.sp_cholesky(faer::Side::Lower).unwrap();

    let solx = system.solve(solutions.col_mut(0));
    let soly = system.solve(solutions.col_mut(1));
    let solz = system.solve(solutions.col_mut(2));

    for (i, mut vert) in verts.clone().enumerate() {
        let index = index_permutation[i];
        if index < unknown_count {
            let x = solx[index];
            let y = soly[index];
            let z = solz[index];

            vert[0] = x as f32;
            vert[1] = y as f32;
            vert[2] = z as f32;

            mutate_vert(&vert, i);
        }
    }
}

pub fn sample_discrete_curve<V, S>(t: S, curve: &[V]) -> V
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    debug_assert!(t.is_finite());
    t.clamp(S::from(0.).unwrap(), S::from(1.).unwrap());

    let t = t.clamp(S::from(0.).unwrap(), S::from(1.).unwrap());

    let arclength = (0..curve.len() - 1)
        .map(|i| (curve[i + 1].clone() - curve[i].clone()).norm())
        .fold(S::from(0.).unwrap(), |acc, x| acc + x);

    let target = t * arclength;

    let mut current = S::from(0.).unwrap();
    let mut i = 0;
    loop {
        let l = (curve[i].clone() - curve[i + 1].clone()).norm();

        if current + l >= target || i >= curve.len() - 1 {
            break;
        }
        current += l;
        i += 1;
    }

    let v = target - current;
    let extent = (curve[i].clone() - curve[i + 1].clone()).norm();

    let t = v / extent;
    curve[i].clone() * (S::from(1.).unwrap() - t) + curve[i + 1].clone() * t
}

pub fn sample_discrete_bitangents<V, S>(t: S, curve: &[V], bitangents: &[V]) -> V
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    debug_assert!(t.is_finite());
    t.clamp(S::from(0.).unwrap(), S::from(1.).unwrap());

    let t = t.clamp(S::from(0.).unwrap(), S::from(1.).unwrap());

    let arclength = (0..curve.len() - 1)
        .map(|i| (curve[i + 1].clone() - curve[i].clone()).norm())
        .fold(S::from(0.).unwrap(), |acc, x| acc + x);

    let target = t * arclength;

    let mut current = S::from(0.).unwrap();
    let mut i = 0;
    loop {
        let l = (curve[i].clone() - curve[i + 1].clone()).norm();

        if current + l >= target || i >= curve.len() - 1 {
            break;
        }
        current += l;
        i += 1;
    }

    let v = target - current;
    let extent = (curve[i].clone() - curve[i + 1].clone()).norm();

    let t = v / extent;
    bitangents[i].clone() * (S::from(1.).unwrap() - t) + bitangents[i + 1].clone() * t
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_numerical_derivative() {
        let epsilon = 1e-4;

        let f = |x: f32| x * x;

        let val = numerical_derivative(0.0, &f);
        assert!(f32::abs(val - 0.0) < epsilon, "{}", val);
        let val = numerical_derivative(1.0, &f);
        assert!(f32::abs(val - 2.0) < epsilon, "{}", val);
        let val = numerical_derivative(-1.0, &f);
        assert!(f32::abs(val + 2.0) < epsilon, "{}", val);

        let f = |x: f32| f32::cos(x);

        let val = numerical_derivative(0.0, &f);
        assert!(f32::abs(val + f32::sin(0.0)) < epsilon, "{}", val);
        let val = numerical_derivative(1.0, &f);
        assert!(f32::abs(val + f32::sin(1.0)) < epsilon, "{}", val);
        let val = numerical_derivative(-1.0, &f);
        assert!(f32::abs(val + f32::sin(-1.0)) < epsilon, "{}", val);
    }
}
