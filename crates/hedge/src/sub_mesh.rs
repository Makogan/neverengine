use core::ops::{Add, Mul};
use std::collections::HashSet;

use crate::traits::InternalMeshAccess;
use crate::{
    face_handle::FaceHandle,
    mesh::{GeomId, HalfMesh},
    traits::PrimitiveContainer,
    vert_handle::VertHandle,
    HEdgeHandle,
    HalfMeshLike,
};

pub struct SubMesh<'a, V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    mesh: &'a HalfMesh<V, E, F>,

    verts: Vec<GeomId<0>>,
    hedges: Vec<GeomId<1>>,
    faces: Vec<GeomId<2>>,

    vert_set: HashSet<GeomId<0>>,
}

impl<'a, V, E, F> SubMesh<'a, V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn from_fn<T>(mesh: &'a HalfMesh<V, E, F>, vert_is_in_mesh: T) -> Self
    where
        T: Fn(&GeomId<0>) -> bool,
    {
        let mut verts = Vec::new();
        for v in mesh.iter_verts()
        {
            if vert_is_in_mesh(&v.id())
            {
                verts.push(v.id());
            }
        }

        let vert_set = HashSet::from_iter(verts.iter().map(|id| *id));

        let mut hedges = Vec::new();
        for e in mesh.iter_edges()
        {
            if vert_is_in_mesh(&e.source().id()) && vert_is_in_mesh(&e.dest().id())
            {
                hedges.push(e.id());
                hedges.push(e.pair().id());
            }
        }

        let mut faces = Vec::new();
        for f in mesh.iter_faces()
        {
            let mut all_verts_are_in = true;
            for v in f.hedge().iter_loop().map(|e| e.source())
            {
                all_verts_are_in = all_verts_are_in && vert_is_in_mesh(&v.id());
            }

            if all_verts_are_in
            {
                faces.push(f.id());
            }
        }

        Self {
            mesh,
            verts,
            hedges,
            faces,
            vert_set,
        }
    }
}

impl<'a, V, E, F> HalfMeshLike for SubMesh<'a, V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type EdgeData = E;
    type EdgeIterator = SubEdgeIter<'a, V, E, F>;
    type FaceData = F;
    type FaceIterator = SubFaceIter<'a, V, E, F>;
    type VertData = V;
    type VertIterator = SubVertIter<'a, V, E, F>;

    fn last_vert_id(&self) -> GeomId<0> { *self.verts.last().unwrap() }

    fn iter_verts(&self) -> SubVertIter<'a, V, E, F>
    {
        SubVertIter {
            mesh: self as *const SubMesh<V, E, F> as *mut _,
            current_id: 0,
            end_id: self.verts.len(),
        }
    }

    fn iter_edges(&self) -> SubEdgeIter<'a, V, E, F>
    {
        SubEdgeIter {
            mesh: self as *const SubMesh<V, E, F> as *mut _,
            current_id: 0,
            end_id: self.hedges.len(),
        }
    }

    fn iter_faces(&self) -> SubFaceIter<'a, V, E, F>
    {
        SubFaceIter {
            mesh: self as *const SubMesh<V, E, F> as *mut _,
            current_id: 0,
            end_id: self.faces.len(),
        }
    }

    fn vert_count(&self) -> usize { self.verts.len() }

    fn edge_count(&self) -> usize { self.hedges.len() / 2 }

    fn hedge_count(&self) -> usize { self.hedges.len() }

    fn face_count(&self) -> usize { self.faces.len() }

    fn face_handle(&self, id: GeomId<2>) -> FaceHandle<V, E, F>
    {
        (*self.mesh).face_handle(id)
    }

    fn hedge_handle(&self, id: GeomId<1>) -> HEdgeHandle<V, E, F>
    {
        (*self.mesh).hedge_handle(id)
    }

    fn vert_handle(&self, id: GeomId<0>) -> VertHandle<V, E, F>
    {
        (*self.mesh).vert_handle(id)
    }

    fn flip_edge(&mut self, id: GeomId<1>) { (*self.mesh).hedge_handle(id).flip() }

    fn split_edge<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 2], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float,
    {
        let last_id = (*self.mesh).hedge_count();
        let res = (*self.mesh).hedge_handle(id).split();
        self.verts.push(res.0);
        self.vert_set.insert(res.0);
        self.hedges.push(GeomId::from(last_id + 0));
        self.hedges.push(GeomId::from(last_id + 1));
        self.hedges.push(GeomId::from(last_id + 2));
        self.hedges.push(GeomId::from(last_id + 3));
        self.hedges.push(GeomId::from(last_id + 4));
        self.hedges.push(GeomId::from(last_id + 5));

        res
    }

    fn split_edge_boundary<S>(
        &mut self,
        id: GeomId<1>,
    ) -> (GeomId<0>, [GeomId<1>; 1], [GeomId<1>; 2])
    where
        <Self::VertData as PrimitiveContainer>::PrimitiveData: Add<Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>
            + Mul<S, Output = <Self::VertData as PrimitiveContainer>::PrimitiveData>,
        S: num::Float,
    {
        let handle = (*self.mesh).hedge_handle(id);
        let res = if handle.is_in_boundary_edge()
        {
            let res = handle.split_boundary();
            self.verts.push(res.0);
            self.vert_set.insert(res.0);

            let e1 = res.1[0];
            let e2 = res.2[0];
            let p1 = res.1[0].pair();
            let p2 = res.2[0].pair();
            self.hedges.push(e1);
            self.hedges.push(p1);
            self.hedges.push(e2);
            self.hedges.push(p2);

            res
        }
        else
        {
            let res = (*self.mesh).hedge_handle(id).split();
            self.verts.push(res.0);
            self.vert_set.insert(res.0);

            let hedge = (*self.mesh).hedge_handle(res.1[0]);
            // Check which of the 2 transversal hedges is the right one.
            let id = if self.vert_set.contains(&hedge.source().id())
                && self.vert_set.contains(&hedge.dest().id())
            {
                res.1[0]
            }
            else
            {
                res.1[1]
            };

            let e1 = id;
            let e2 = res.2[0];
            let p1 = res.1[1].pair();
            let p2 = res.2[0].pair();
            self.hedges.push(e1);
            self.hedges.push(p1);
            self.hedges.push(e2);
            self.hedges.push(p2);

            (res.0, [GeomId::from(id)], res.2)
        };

        res
    }

    fn is_in_boundary_edge(&self, id: GeomId<1>) -> bool
    {
        let handle = (*self.mesh).hedge_handle(id);
        let is_true_boundary = handle.is_in_boundary_edge();

        let opposite1 = handle.prev().source().id();
        let opposite2 = handle.pair().prev().source().id();

        let is_conceptual_boundary =
            !(self.vert_set.contains(&opposite1) && self.vert_set.contains(&opposite2));

        is_true_boundary || is_conceptual_boundary
    }
}

#[derive(Clone)]
pub struct SubVertIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    pub(crate) mesh: *mut SubMesh<'a, V, E, F>,
    pub(crate) current_id: usize,
    pub(crate) end_id: usize,
}

impl<'a, V, E, F> Iterator for SubVertIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    type Item = VertHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let val = if self.current_id < self.end_id
        {
            let geom_id = unsafe { (*self.mesh).verts[self.current_id] };
            unsafe { Some((*self.mesh).mesh.vert_handle(geom_id)) }
        }
        else
        {
            None
        };

        self.current_id += 1;

        val
    }
}

#[derive(Clone)]
pub struct SubEdgeIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    pub(crate) mesh: *mut SubMesh<'a, V, E, F>,
    pub(crate) current_id: usize,
    pub(crate) end_id: usize,
}

impl<'a, V, E, F> Iterator for SubEdgeIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    type Item = HEdgeHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let val = if self.current_id < self.end_id
        {
            let geom_id = unsafe { (*self.mesh).hedges[self.current_id] };
            unsafe { Some((*self.mesh).mesh.hedge_handle(geom_id)) }
        }
        else
        {
            None
        };

        self.current_id += 2;

        val
    }
}

#[derive(Clone)]
pub struct SubFaceIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    pub(crate) mesh: *mut SubMesh<'a, V, E, F>,
    pub(crate) current_id: usize,
    pub(crate) end_id: usize,
}

impl<'a, V, E, F> Iterator for SubFaceIter<'a, V, E, F>
where
    V: PrimitiveContainer + 'a,
    E: PrimitiveContainer + 'a,
    F: PrimitiveContainer + 'a,
{
    type Item = FaceHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let val = if self.current_id < self.end_id
        {
            let geom_id = unsafe { (*self.mesh).faces[self.current_id] };
            unsafe { Some((*self.mesh).mesh.face_handle(geom_id)) }
        }
        else
        {
            None
        };

        self.current_id += 1;

        val
    }
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests
{
    use chicken_wire::wavefront_loader::ObjData;

    use super::SubMesh;
    use crate::{
        linear_subdivision,
        mesh::{verify_topology, HalfMesh},
        HalfMeshLike,
    };

    #[test]
    fn test_sub_mesh()
    {
        let ObjData {
            vertices: vs,
            vertex_face_indices: vi,
            ..
        } = ObjData::from_disk_file("../../Assets/cube.obj");
        let mesh = HalfMesh::new(vs, (), (), vi);
        {
            let mut sub_mesh = SubMesh::from_fn(&mesh, |id| id.0 < 4);
            linear_subdivision::<f32, _, _>(&mut sub_mesh);
        }

        ObjData::export(&mesh, "tmp/submesh_subdivided_cube.obj");
        assert!(verify_topology(&mesh).is_none());

        let ObjData {
            vertices: vs,
            vertex_face_indices: vi,
            ..
        } = ObjData::from_disk_file("../../Assets/bunny.obj");
        let mesh = HalfMesh::new(vs, (), (), vi);
        {
            let n = mesh.vert_count() as u64;
            let mut sub_mesh = SubMesh::from_fn(&mesh, |id| id.0 < n / 2);
            linear_subdivision::<f32, _, _>(&mut sub_mesh);
        }
        assert!(verify_topology(&mesh).is_none());

        ObjData::export(&mesh, "tmp/submesh_subdivided_bunny.obj");
    }
}

// +| Internal |+ ==============================================================
impl<'a, V, E, F> InternalMeshAccess<V, E, F> for SubMesh<'a, V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    unsafe fn inner_mesh(&self) -> *mut HalfMesh<V, E, F>
    where
        V: PrimitiveContainer,
        E: PrimitiveContainer,
        F: PrimitiveContainer,
    {
        self.mesh as *const _ as *mut HalfMesh<V, E, F>
    }
}
