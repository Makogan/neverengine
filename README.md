# Demiurge

Demiurge is a toolbox developed to ease development of graphics based programs.
It aims to provide utilities to quickly boot strap complex worflows while providing
mechanisms to modify any of the tools. Algorithms are implemented as generically
as possible so as to maximize flexibility.

To see the documentation and tutorials on how to use the tools in the project please see
the [documentation](https://dryad1.gitlab.io/documentation/).

Some examples of algorithms and systems that are included:

* An abstraction layer over Vulkan and HLSL to facilitate making new shaders and complex rendering techniques.

* A Half Edge data structure that is generic over its own data (i.e. does not assume any specific representation for
  vertices, edges or faces). It supports all elementary operations, that is:
    * Edge Flipping
    * Edge Splitting
    * Face Splitting
    * Edge Collpasing

  Other expected utilities such as iterating over edges, faces, handles that provide ergonomic iteration over adjacent
  neighbours or to loop over a face are also included.

* Complex common algorithms such as Laplacian smoothing, Delaunay and Constrained Delaunay triangulations, Mesh
  Subdivision, Dual Contouring, DDA line iteration, Vertex enumeration and many more...

* Physics simulation algorithms such as MLS-MPM for fluids and soft bodies.

* An implementation of geometric algebra for representing space transformations.

* A gltf loader with support for skinned animation.

Currently the easiest way to see what the tool is capable of is to download the project through:

```url
git clone https://gitlab.com/dryad1/demiurge.git demiurge
```

And then run the following commands:

```sh
cd demiurge/ 
cargo run --example 01_spinning_triangle --release 
```

To build all examples run:

```sh
cargo make deploy
```

For the full list of examples see
the [repository](https://gitlab.com/Makogan/neverengine/-/tree/master/examples?ref_type=heads).

Disclaimer: The tool is under active development and things will break often. No guarantees are made for API stability. 
