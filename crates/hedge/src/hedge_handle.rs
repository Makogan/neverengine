use core::ops::{Add, Mul};
use std::collections::HashSet;
use std::fmt::Debug;

use linear_isomorphic::*;

use crate::face_handle::*;
use crate::facets::*;
use crate::mesh::*;
use crate::vert_handle::*;

/*
The handle variables in the following functions are named base on this diagram, where
`e0` denotes the starting edge handle.
```
                                           v4
                                          /||\
                                         / || -\
                                       /-  ||   \
                                      /    ||    \
                                     /     ||     -\
                                   /-   e11||e8     \
                                  /        ||        -\
                                 /         ||          \
                               /-          ||           -\
                              /            ||             \
                             /    f2       ||      f3      \
                           /-             v1                -\
                          /e9            /-||-\               \
                         /             /-  ||  -\           e7 -\
                       /-            /-    ||    -\              \
                      /            /-      ||      -\             \
                     /      e10  /-        ||        --\           -\
                    /          /-          ||           -\           \
                  /-         /-            ||             -\          -\
                 /         /-              ||               -\  e6      \
                /        /-                ||                 -\         \
              /-       /- e1             e0||e3                 --\       -\
             /       /-                    ||                  e5  -\       \
            /      /-           f0         ||         f1             -\      -\
          /-     /-                        ||                          -\      \
         /     /-                          ||                            --\    -\
        /    /-                            ||                               -\    \
      /-   /-                              ||                                 -\   \
     /   /-                                ||                                   -\  -\
    -  /-                e2                ||        e4                           -\  -
   v2 -------------------------------------||-----------------------------------------v3
                                          v0
```
 */

/// As a best practice don't store this handle, prefer using its id as returned
/// by the `id()` method.
#[derive(Clone, Copy, Debug)]
pub struct HEdgeHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) id: GeomId<1>,
}

impl<V, E, F> HEdgeHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn new(mesh: &mut HalfMesh<V, E, F>, id: GeomId<1>) -> HEdgeHandle<V, E, F> {
        HEdgeHandle::<V, E, F> { id: id, mesh: mesh }
    }

    pub fn id(&self) -> GeomId<1> {
        self.id
    }

    pub fn disable(&self) {
        unsafe {
            let pair_id =
                (*self.mesh).hedges_meta.borrow_mut()[self.id.0 as usize].pair_id;
            (*self.mesh).hedges_meta.borrow_mut()[self.id.0 as usize].enabled = false;
            (*self.mesh).hedges_meta.borrow_mut()[pair_id.0 as usize].enabled = false;
        }
    }

    pub fn is_disabled(&self) -> bool {
        unsafe {
            (*self.mesh).hedges_meta.borrow_mut()[self.id.0 as usize].enabled == false
        }
    }

    pub fn iterate_butterfly(&self) -> impl Iterator<Item = HEdgeHandle<V, E, F>> {
        self.iter_loop().chain(self.pair().iter_loop())
    }

    // https://stackoverflow.com/questions/27049163/mesh-simplification-edge-collapse-conditions
    /// Returns the id of the surviving vertex. i.e. the one that remains valid
    /// afer the collapse.
    pub fn collapse<S>(&self) -> GeomId<0>
    where
        V::PrimitiveData:
            Add<Output = V::PrimitiveData> + Mul<S, Output = V::PrimitiveData>,
        S: num::Float,
    {
        debug_assert!(!self.is_in_boundary_edge());
        let v2 = self.prev().source();
        let v3 = self.pair().prev().source();

        let v0_id = self.source().id();
        let v1_id = self.dest().id();
        let v2_id = v2.id();
        let v3_id = v3.id();

        let e0 = self.id();
        let e1 = self.next().id();
        let e2 = self.prev().id();

        let e3 = self.pair().id();
        let e4 = self.pair().next().id();
        let e5 = self.pair().prev().id();

        let e6 = self.pair().prev().pair().id();
        let e7 = self.pair().prev().pair().next().id();
        let e8 = self.pair().prev().pair().prev().id();

        let e9 = self.next().pair().prev().id();
        let e10 = self.next().pair().id();
        let e11 = self.next().pair().next().id();

        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e0).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e1).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e2).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e3).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e4).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e5).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e6).is_in_boundary_edge() });
        // debug_assert!(unsafe { !(*self.mesh).hedge_handle(e7).is_in_boundary_edge()
        // });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e8).is_in_boundary_edge() });
        // debug_assert!(unsafe { !(*self.mesh).hedge_handle(e9).is_in_boundary_edge()
        // });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e10).is_in_boundary_edge() });
        debug_assert!(unsafe { !(*self.mesh).hedge_handle(e11).is_in_boundary_edge() });

        // For the surviving vertices in the quad, find safe hedges that will
        // remain valid after the collapse.
        let v0_id_edge_safe = self
            .source()
            .iter_hedges()
            .find(|e| e.id() != self.id())
            .unwrap()
            .id();
        let v2_id_edge_safe = v2
            .iter_hedges()
            .find(|e| (e.dest().id() != v1_id) && (e.dest().id() != v0_id))
            .unwrap()
            .id();
        let v3_id_edge_safe = v3
            .iter_hedges()
            .find(|e| (e.dest().id() != v1_id) && (e.dest().id() != v0_id))
            .unwrap()
            .id();

        let f0_id = self.face().id();
        let f1_id = self.pair().face().id();
        let f2_id = self.next().pair().face().id();
        let f3_id = self.pair().prev().pair().face().id();

        let mut to_be_removed = HashSet::new();
        to_be_removed.insert(self.id());
        to_be_removed.insert(self.dest().shared_hedge(v2_id).unwrap().id());
        to_be_removed.insert(self.dest().shared_hedge(v3_id).unwrap().id());

        let vdata = (self.source().data() + self.dest().data()) * S::from(0.5).unwrap();

        unsafe {
            for e in self.dest().iter_hedges() {
                (*self.mesh).hedges_meta.borrow_mut()[e.id().0 as usize].vert_id = v0_id;
            }

            assert!(!to_be_removed.contains(&e4));
            assert!(!to_be_removed.contains(&e7));
            assert!(!to_be_removed.contains(&e8));
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f3_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e4,
                e7,
                e8,
            );

            assert!(!to_be_removed.contains(&e2));
            assert!(!to_be_removed.contains(&e11));
            assert!(!to_be_removed.contains(&e9));
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e2,
                e11,
                e9,
            );

            (*self.mesh).hedges_meta.borrow_mut()[e0.0 as usize].enabled = false;
            (*self.mesh).hedges_meta.borrow_mut()[e3.0 as usize].enabled = false;

            (*self.mesh).hedges_meta.borrow_mut()[e1.0 as usize].enabled = false;
            (*self.mesh).hedges_meta.borrow_mut()[e10.0 as usize].enabled = false;

            (*self.mesh).hedges_meta.borrow_mut()[e5.0 as usize].enabled = false;
            (*self.mesh).hedges_meta.borrow_mut()[e6.0 as usize].enabled = false;

            (*self.mesh).verts_meta.borrow_mut()[v1_id.0 as usize].enabled = false;
            (*self.mesh).verts_meta.borrow_mut()[v1_id.0 as usize].hedge_id = ABSENT();

            (*self.mesh).verts.borrow_mut().set(v0_id.0, vdata);
            (*self.mesh).verts_meta.borrow_mut()[v0_id.0 as usize].hedge_id =
                v0_id_edge_safe;

            (*self.mesh).hedges_meta.borrow_mut()[v2_id_edge_safe.0 as usize].vert_id =
                v2_id;
            (*self.mesh).hedges_meta.borrow_mut()[v3_id_edge_safe.0 as usize].vert_id =
                v3_id;

            (*self.mesh).verts_meta.borrow_mut()[v2_id.0 as usize].hedge_id =
                v2_id_edge_safe;
            (*self.mesh).verts_meta.borrow_mut()[v3_id.0 as usize].hedge_id =
                v3_id_edge_safe;

            (*self.mesh).faces_meta.borrow_mut()[f0_id.0 as usize].enabled = false;
            (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize].enabled = false;
        }

        v0_id
    }

    /// Get the 2 opposite verrtices to the edge in a butterfly. Only use on
    /// triangle meshes.
    pub fn opposite_ids(&self) -> [GeomId<0>; 2] {
        [self.prev().source().id(), self.pair().prev().source().id()]
    }

    pub fn opposite_verts(&self) -> [VertHandle<V, E, F>; 2] {
        [self.prev().source(), self.pair().prev().source()]
    }

    /// Tests if collapsing the current edge would violate topology. If it
    /// returns true collapsing the edge is safe.
    pub fn can_collapse(&self) -> bool {
        let v1_set: HashSet<GeomId<0>> =
            HashSet::from_iter(self.source().iter_verts().map(|v| v.id()));
        let v2_set: HashSet<GeomId<0>> =
            HashSet::from_iter(self.dest().iter_verts().map(|v| v.id()));

        let intersection_count = v1_set.intersection(&v2_set).count();

        let v2 = self.prev().source().id();
        let v3 = self.pair().prev().source().id();

        // These would be deleted by collapsing the edge, so we must make sure
        // that they don't belong to the set of edges that would make the new
        // connectivity.
        let mut to_be_removed = HashSet::new();
        to_be_removed.insert(self.id());
        to_be_removed.insert(self.dest().shared_hedge(v2).unwrap().id());
        to_be_removed.insert(self.dest().shared_hedge(v3).unwrap().id());

        let e2 = self.prev().id();
        let e4 = self.pair().next().id();
        let e7 = self.pair().prev().pair().next().id();
        let e8 = self.pair().prev().pair().prev().id();
        let e9 = self.next().pair().prev().id();
        let e11 = self.next().pair().next().id();

        let is_valid = !to_be_removed.contains(&e4)
            && !to_be_removed.contains(&e7)
            && !to_be_removed.contains(&e8)
            && !to_be_removed.contains(&e2)
            && !to_be_removed.contains(&e11)
            && !to_be_removed.contains(&e9);

        !self.is_in_boundary_edge() && intersection_count == 2 && is_valid
    }

    /// Tests if flipping the current edge would violate topology. If it returns
    /// true flipping the edge is safe.
    pub fn can_flip(&self) -> bool {
        let [v1_id, v2_id] = self.opposite_ids();
        let v1 = unsafe { (*self.mesh).vert_handle(v1_id) };
        let v2 = unsafe { (*self.mesh).vert_handle(v2_id) };

        let v1_set: HashSet<GeomId<0>> =
            HashSet::from_iter(v1.iter_verts().map(|v| v.id()));
        let v2_set: HashSet<GeomId<0>> =
            HashSet::from_iter(v2.iter_verts().map(|v| v.id()));

        !v1_set.contains(&v2.id()) && !v2_set.contains(&v1.id())
    }

    pub fn data(&self) -> E::PrimitiveData {
        unsafe { (*self.mesh).edges.borrow().get(self.id.0).clone() }
    }

    pub fn next(&self) -> HEdgeHandle<V, E, F> {
        unsafe {
            let next_id = (*self.mesh).hedges_meta.borrow()[self.id.0 as usize]
                .next_id
                .0 as usize;
            HEdgeHandle::<V, E, F> {
                id: (*self.mesh).hedges_meta.borrow()[next_id].id,
                mesh: self.mesh,
            }
        }
    }

    pub fn prev(&self) -> HEdgeHandle<V, E, F> {
        unsafe {
            let prev_id = (*self.mesh).hedges_meta.borrow()[self.id.0 as usize]
                .prev_id
                .0 as usize;
            HEdgeHandle::<V, E, F> {
                id: (*self.mesh).hedges_meta.borrow()[prev_id].id,
                mesh: self.mesh,
            }
        }
    }

    pub fn pair(&self) -> HEdgeHandle<V, E, F> {
        unsafe {
            let edge_pair_id =
                (*self.mesh).hedges_meta.borrow()[self.id.0 as usize].pair_id;
            HEdgeHandle::<V, E, F> {
                id: (*self.mesh).hedges_meta.borrow()[edge_pair_id.0 as usize].id,
                mesh: self.mesh,
            }
        }
    }

    pub fn source(&self) -> VertHandle<V, E, F> {
        unsafe {
            let edge_vert_id =
                (*self.mesh).hedges_meta.borrow()[self.id.0 as usize].vert_id;
            VertHandle::<V, E, F> {
                id: (*self.mesh).verts_meta.borrow()[edge_vert_id.0 as usize].id,
                mesh: self.mesh,
            }
        }
    }

    pub fn dest(&self) -> VertHandle<V, E, F> {
        unsafe {
            VertHandle::<V, E, F> {
                id: (*self.mesh).verts_meta.borrow()[self.next().source().id.0 as usize]
                    .id,
                mesh: self.mesh,
            }
        }
    }

    /// Returns the even indexed representative of the edge containing this
    /// hedge.
    pub fn even_hedge(&self) -> HEdgeHandle<V, E, F> {
        return if self.id().0 % 2 == 0 {
            self.clone()
        } else {
            self.pair()
        };
    }

    pub fn face(&self) -> FaceHandle<V, E, F> {
        unsafe {
            FaceHandle::<V, E, F> {
                id: (*self.mesh).hedges_meta.borrow()[self.id.0 as usize].face_id,
                mesh: self.mesh,
            }
        }
    }

    /// Test if this half edge *specifically* is in the boundary. If this half
    /// edge is in the boundary edge but its face is not abssent it returns
    /// false.
    ///
    /// Returns true if the hedge is on the boundary false otherwise.
    pub fn is_boundary_hedge(&self) -> bool {
        unsafe {
            (*self.mesh).hedges_meta.borrow()[self.id.0 as usize].face_id == ABSENT()
        }
    }

    /// Tests whether this edge is on the boundary. In this case it doesn't
    /// matter whether this half edge is the one facing the boundary or if
    /// it is its pair.
    ///
    /// Returns true if it is on the boundary false otherwise.
    pub fn is_in_boundary_edge(&self) -> bool {
        self.is_boundary_hedge() || self.pair().is_boundary_hedge()
    }

    /// If this hedge is on the boundary edge, returns the boundary hedge (i.e.
    /// either itself or its pair depending on which is the boundary hedge).
    /// Returns none otherwise.
    ///
    /// Returns the boundary hege or None if input is not on the boundary.
    pub fn get_boundary_hedge(&self) -> Option<Self> {
        if self.is_boundary_hedge() {
            return Some(self.clone());
        }
        if self.pair().is_boundary_hedge() {
            return Some(self.pair());
        }

        None
    }

    pub fn get_non_boundary_hedge(&self) -> Option<Self> {
        if self.is_boundary_hedge() {
            return Some(self.pair());
        }
        if self.pair().is_boundary_hedge() {
            return Some(self.clone());
        }

        None
    }

    /// Iterator around the closed loop of a sequence of edges, e.g. the edges
    /// of a face or the edges of a boundary.
    pub fn iter_loop(&self) -> HEdgeLoopIterExp<V, E, F> {
        HEdgeLoopIterExp::<V, E, F> {
            edge: self.clone(),
            start_id: ABSENT(),
        }
    }

    /// Split the current half edge, preserving topology. Cannot handle boundary
    /// edges.
    ///
    /// Returns a triplet of the ids of the new point, the ids of the two new
    /// half edges that cross the existing edge transversally. And the ids
    /// of the two half edges that overlap the old edge, in that order. The
    /// half edges are those whose destination vertex is the newly added
    /// one.
    ///
    /// This ads 6 new half edges such that the first 4 correspond to the
    /// transversal edges and the last 2 are one of the two longitudinal
    /// ones. The ids of al 6 are guaranteed to appear at the end of the
    /// hedge list.
    pub fn split<S>(&self) -> (GeomId<0>, [GeomId<1>; 2], [GeomId<1>; 2])
    where
        V::PrimitiveData:
            Add<Output = V::PrimitiveData> + Mul<S, Output = V::PrimitiveData>,
        S: num::Float,
    {
        debug_assert!(!self.is_in_boundary_edge());
        let edge = &self;

        let v1_id = edge.source().id;
        let v2_id = edge.pair().prev().source().id;
        let v3_id = edge.dest().id;
        let v4_id = edge.prev().source().id;

        let e0_id = edge.id;
        let e1_id = edge.pair().id;
        let e2_id = edge.prev().id;
        let e3_id = edge.pair().next().id;
        let e4_id = edge.pair().prev().id;
        let e5_id = edge.next().id;

        let vdata = (edge.source().data() + edge.dest().data()) * S::from(0.5).unwrap();

        let edata = edge.data();
        let fdata1 = edge.face().data();
        let fdata2 = edge.pair().face().data();

        let f1_id = edge.face().id;
        let f2_id = edge.pair().face().id;

        let vn_id = unsafe { (*self.mesh).add_vert(vdata) };

        // e7 starts at v2.
        let (e6_id, e7_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v2_id, edata.clone(), edata.clone()) };
        let (e8_id, e9_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v4_id, edata.clone(), edata.clone()) };
        let (e10_id, e11_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v3_id, edata.clone(), edata.clone()) };

        let f3_id = unsafe { (*self.mesh).add_face_through_edge(e11_id, fdata1.clone()) };
        let f4_id = unsafe { (*self.mesh).add_face_through_edge(e10_id, fdata2.clone()) };

        unsafe {
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e0_id,
                e8_id,
                e2_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e1_id,
                e3_id,
                e7_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f3_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e6_id,
                e4_id,
                e11_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f4_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e10_id,
                e5_id,
                e9_id,
            );

            (*self.mesh).hedges_meta.borrow_mut()[e0_id.0 as usize].vert_id = v1_id;
            (*self.mesh).hedges_meta.borrow_mut()[e1_id.0 as usize].vert_id = vn_id;

            (*self.mesh).hedges_meta.borrow_mut()[e6_id.0 as usize].vert_id = vn_id;
            (*self.mesh).hedges_meta.borrow_mut()[e7_id.0 as usize].vert_id = v2_id;

            (*self.mesh).hedges_meta.borrow_mut()[e10_id.0 as usize].vert_id = vn_id;
            (*self.mesh).hedges_meta.borrow_mut()[e11_id.0 as usize].vert_id = v3_id;

            (*self.mesh).hedges_meta.borrow_mut()[e8_id.0 as usize].vert_id = vn_id;
            (*self.mesh).hedges_meta.borrow_mut()[e9_id.0 as usize].vert_id = v4_id;

            return (vn_id, [e7_id, e9_id], [e11_id, e0_id]);
        }
    }

    /// Split a half edge at the boundary. Can only handle the half edge whose
    /// face is not absent.
    ///
    /// Returns a triplet of the id of the new point, the id of the new half
    /// edge that crosses transversally from the old edge (i.e. it's not
    /// contained in the old edge) and the two new half edges that overlap
    /// the old edge. Adds 4 new half edges to the list, their ids are
    /// guaranteed to appear at the end of the existing hedge ids.
    ///
    /// The first new id corresponds to the transversal half edge, the next 2
    /// to one of the two longitudinal ones.
    // The reason this returns a 1D array is for API uniformity with `split`.
    pub fn split_boundary<S>(&self) -> (GeomId<0>, [GeomId<1>; 1], [GeomId<1>; 2])
    where
        V::PrimitiveData:
            Add<Output = V::PrimitiveData> + Mul<S, Output = V::PrimitiveData>,
        S: num::Float,
    {
        let vdata = (self.source().data() + self.dest().data()) * S::from(0.5).unwrap();
        let vn_id = unsafe { (*self.mesh).add_vert(vdata) };

        self.split_and_fit_boundary(vn_id)
    }

    // Similar to `split_boundary` but assigns the new point to a known existing
    // vertex. (use with care, this can easily break topology).
    pub fn split_and_fit_boundary<S>(
        &self,
        vid: GeomId<0>,
    ) -> (GeomId<0>, [GeomId<1>; 1], [GeomId<1>; 2])
    where
        V::PrimitiveData:
            Add<Output = V::PrimitiveData> + Mul<S, Output = V::PrimitiveData>,
        S: num::Float,
    {
        debug_assert!(!self.is_boundary_hedge());
        debug_assert!(self.pair().is_boundary_hedge());

        let edge = self;

        let v1_id = edge.source().id;
        let v2_id = edge.dest().id;
        let v3_id = edge.prev().source().id;

        let e0_id = edge.id;
        let e1_id = edge.pair().id;
        let e2_id = edge.next().id;
        let e3_id = edge.prev().id;

        let eo_id = edge.pair().prev().id;

        let edata = edge.data();
        let fdata1 = edge.face().data();

        let f1_id = edge.face().id;

        let vn_id = vid;

        let (e4_id, e5_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v3_id, edata.clone(), edata.clone()) };
        // e7 starts at v2.
        let (e6_id, e7_id) =
            unsafe { (*self.mesh).add_edge(vn_id, v2_id, edata.clone(), edata.clone()) };

        let f2_id = unsafe { (*self.mesh).add_face_through_edge(e2_id, fdata1.clone()) };

        unsafe {
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e0_id,
                e4_id,
                e3_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e2_id,
                e5_id,
                e6_id,
            );

            (*self.mesh).hedges_meta.borrow_mut()[e0_id.0 as usize].vert_id = v1_id;
            (*self.mesh).hedges_meta.borrow_mut()[e1_id.0 as usize].vert_id = vn_id;

            (*self.mesh).hedges_meta.borrow_mut()[e4_id.0 as usize].vert_id = vn_id;
            (*self.mesh).hedges_meta.borrow_mut()[e5_id.0 as usize].vert_id = v3_id;

            (*self.mesh).hedges_meta.borrow_mut()[e6_id.0 as usize].vert_id = vn_id;
            (*self.mesh).hedges_meta.borrow_mut()[e7_id.0 as usize].vert_id = v2_id;

            (*self.mesh).hedges_meta.borrow_mut()[e7_id.0 as usize].next_id = e1_id;
            (*self.mesh).hedges_meta.borrow_mut()[e1_id.0 as usize].prev_id = e7_id;

            (*self.mesh).hedges_meta.borrow_mut()[eo_id.0 as usize].next_id = e7_id;
            (*self.mesh).hedges_meta.borrow_mut()[e7_id.0 as usize].prev_id = eo_id;

            (vn_id, [e5_id], [e7_id, e0_id])
        }
    }

    pub fn flip(&self) {
        let edge = &self;

        debug_assert!(
            !edge.is_in_boundary_edge(),
            "You cannot flip boundary edges!"
        );

        let v1_id = edge.source().id;
        let v2_id = edge.pair().prev().source().id;
        let v3_id = edge.dest().id;
        let v4_id = edge.prev().source().id;

        let e0_id = edge.id;
        let e1_id = edge.pair().id;
        let e2_id = edge.prev().id;
        let e3_id = edge.pair().next().id;
        let e4_id = edge.pair().prev().id;
        let e5_id = edge.next().id;

        let f1_id = edge.face().id;
        let f2_id = edge.pair().face().id;

        unsafe {
            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f1_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e0_id,
                e2_id,
                e3_id,
            );

            attach_face(
                &mut (*self.mesh).faces_meta.borrow_mut()[f2_id.0 as usize],
                &mut (*self.mesh).hedges_meta.borrow_mut(),
                e1_id,
                e4_id,
                e5_id,
            );

            (*self.mesh).hedges_meta.borrow_mut()[e0_id.0 as usize].vert_id = v2_id;
            (*self.mesh).hedges_meta.borrow_mut()[e1_id.0 as usize].vert_id = v4_id;

            (*self.mesh).verts_meta.borrow_mut()[v1_id.0 as usize].hedge_id = e3_id;
            (*self.mesh).verts_meta.borrow_mut()[v2_id.0 as usize].hedge_id = e4_id;
            (*self.mesh).verts_meta.borrow_mut()[v3_id.0 as usize].hedge_id = e5_id;
            (*self.mesh).verts_meta.borrow_mut()[v4_id.0 as usize].hedge_id = e2_id;
        }
    }

    pub fn dir<S>(&self) -> V::PrimitiveData
    where
        V::PrimitiveData: VectorSpace<Scalar = S>,
        S: RealField,
    {
        return self.dest().data() - self.source().data();
    }

    pub fn length<S>(&self) -> S
    where
        V::PrimitiveData: InnerSpace<S>,
        S: RealField,
    {
        return S::from((self.dest().data() - self.source().data()).norm()).unwrap();
    }

    pub fn endpoints(&self) -> [V::PrimitiveData; 2] {
        [self.source().data(), self.dest().data()]
    }
}

#[derive(Clone, Copy)]
pub struct FullEdgeIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) current_id: GeomId<1>,
    pub(crate) end_id: GeomId<1>,
}

impl<V, E, F> Iterator for FullEdgeIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = HEdgeHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item> {
        debug_assert!(self.end_id.0 % 2 == 0);
        let val = if self.current_id.0 < self.end_id.0 {
            Some(HEdgeHandle {
                mesh: self.mesh,
                id: self.current_id,
            })
        } else {
            None
        };

        self.current_id.0 += 2;

        val
    }
}

pub struct HEdgeLoopIterExp<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) edge: HEdgeHandle<V, E, F>,
    pub(crate) start_id: GeomId<1>,
}

impl<V, E, F> Iterator for HEdgeLoopIterExp<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = HEdgeHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item> {
        let val = Some(self.edge.clone());

        if self.edge.id == self.start_id {
            return None;
        }

        if self.start_id == ABSENT() {
            self.start_id = self.edge.id;
        }

        self.edge = self.edge.next();

        val
    }
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use chicken_wire::wavefront_loader::ObjData;

    use crate::mesh::{GeomId, HalfMesh};
    use crate::mesh_deleter::HalfMeshDeleter;

    #[test]
    fn test_edge_collapse() {
        let ObjData {
            vertices: verts,
            vertex_face_indices: verts_idx,
            ..
        } = ObjData::from_disk_file("../../Assets/convex_poly.obj");

        let mut mesh = HalfMesh::new(verts, (), (), verts_idx);
        ObjData::export(&mesh, "tmp/pre_collapsed_mesh.obj");

        let mesh_deleter = HalfMeshDeleter::new(&mut mesh);
        mesh_deleter.hedge_handle(GeomId(0)).collapse::<f32>();
        mesh_deleter.finish();

        ObjData::export(&mesh, "tmp/collapsed_mesh.obj")
    }
}

use std::{fs::File, io::Write};
pub fn export_points_to_obj<V, S>(
    points: &Vec<V>,
    indices: Option<&Vec<usize>>,
    path: &str,
) where
    V: VectorSpace<Scalar = S>,
    S: RealField + Mul<V, Output = V>,
{
    let mut file = File::create(path).unwrap();

    if indices.is_some() {
        let indices = indices.unwrap();
        for i in indices {
            let point = points[*i].clone();
            write!(file, "v {} {} {}\n", point[0], point[1], point[2]).unwrap();
        }
    } else {
        for point in points {
            write!(file, "v {} {} {}\n", point[0], point[1], point[2]).unwrap();
        }
    }
}
